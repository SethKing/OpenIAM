Obtaining OpenIAM
=================

.. toctree::

Downloading OpenIAM
-------------------

OpenIAM is currently in early development and its availability is restricted to developers at this time. This section will be updated in the future when the code is released. 

Installing OpenIAM
------------------

There is currently no installation process, but will be in the future.

Testing installation
--------------------

The test suite can be run by entering the OpenIAM *test* directory in a terminal and::

    python iam_test.py

Test results will be printed to the terminal.

