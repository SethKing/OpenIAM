.. openiam documentation master file, created by
   sphinx-quickstart on Tue Dec 12 18:01:41 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. |10^-12| replace:: 10\ :sup:`-12`\
.. |cdot| unicode:: U+000B7
.. |CO2| replace:: CO\ :sub:`2`\
.. |t_i| replace:: t\ :sub:`i`\
.. |m^2| replace:: m\ :sup:`2`\
.. |m^3| replace:: m\ :sup:`3`\
.. |m^4| replace:: m\ :sup:`4`\
.. |log10| replace:: log\ :sub:`10`\
.. |kg/m^3| replace:: kg/m\ :sup:`3`\
.. |m^2| replace:: m\ :sup:`2`\
.. |m^3/s| replace:: m\ :sup:`3`\/s
.. |m^4| replace:: m\ :sup:`4`\
.. |Pa^-1| replace:: Pa\ :sup:`-1`\
.. |Pa*s| replace:: Pa\ |cdot|\s
.. |times| unicode:: U+000D7

Welcome to OpenIAM's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
.. include:: installation.rst

.. include:: getting_started.rst

.. include:: use_cases.rst

Components Description
======================

.. toctree::

This section of the document will describe each of the component models
available for use in the OpenIAM. For each component model all parameters
that can be specified are described along with units and
acceptable ranges. Description of the output that can be requested is also
provided.

.. _stratigraphy_component:

Stratigraphy Component
-----------------------

.. automodule:: openiam.stratigraphy_component

Simple Reservoir Component
--------------------------

.. automodule:: openiam.simple_reservoir_component

Lookup Table Reservoir Component
---------------------------------

.. automodule:: openiam.lookup_table_reservoir_component

Multisegmented Wellbore Component
---------------------------------------

.. automodule:: openiam.multisegmented_wellbore_component

Cemented Wellbore Component
---------------------------------

.. automodule:: openiam.cemented_wellbore_component

Open Wellbore Component
-----------------------------

.. automodule:: openiam.open_wellbore_component

Carbonate Aquifer Component
---------------------------------

.. automodule:: openiam.carbonate_aquifer_component

Deep Alluvium Aquifer Component
---------------------------------

.. automodule:: openiam.deep_alluvium_aquifer_component

Atmospheric Model Component Class
---------------------------------

.. automodule:: openiam.atmRom_component

.. include:: bibliography.rst
