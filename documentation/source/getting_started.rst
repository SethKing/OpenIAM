Getting Started
===============

.. toctree::

The Integrated Assessment Model (IAM) has several way to a user can build and 
run simulations, including text based control files and python scripting.  The
simplest way to build and run simulations for the IAM is the Graphic User 
Interface (GUI).  This guide will primarily focus on using the GUI to interact
with the IAM.  To launch the GUI open a command prompt in the IAM/Source/GUI directory and type::

    python NRAP_OPENIAM.py

.. include:: gui_operation.rst

.. 
   # TODO Reference and cite use of Matk here
.. 
   # TODO Include control file text somewhere?
.. 
   # include:: control_file.rst

Output
-------

Output is written to the folder specified in the model definition with the
Output Directory.  
For each component model of the system Outputs can be specified. When an
output is specified for a forward model the values of that output are written
to a file (output_name.txt) in the output directory. For a stochastic model
(LHS or parstudy analysis) the outputs are included in the results file
(LHS_results.txt or parstudy_results.txt) as well as a file for a statistical summary
of the input parameters and output observations (LHS_statistics.txt or
parstudy_statistics.txt). A copy of the input control file is also written
to the OutputDirectory folder. Through the GUI, this input file can be loaded
back in to rerun the simulation.

After a simulation is ran, the post processing section of the GUI can be used
to generate plots of the results and run sensitivity analysis. When the post processing
view is first opened it asks for a folder specification.  This folder is the output folder 
of the results you want to analyze.  Navigate to that output folder using the browse button.

Plotting
~~~~~~~~
After a folder is selected a processing selection should appear that is already set for plotting.
There are several types of plots that can be created depending on what type of simulation was ran
and what components where specified. The simplest plot is a time series plot where the output 
will be plotted against time, multiple realizations will be plotted on separate lines. Specify
a title to give the plot and a file name along with making a selection of what output to plot.  
Pressing the plot button will generate the plot, it will be saved with the filename given to the
output directory for the results.  If a simulation was ran with multiple realizations a Time Series
Stats plot or a Time Series and Stats plot will be options in the plot type menu.  A time series stats
plot will shade the quadrants of the results along with plotting the mean and median results, but will
not plot the individual realizations.  A time series and stats plot will overlay the realizations on
the stats plots of the shaded quadrants.  If a Atmospheric component was included in the simulation, 
map-plots of the plume for a single realization or probabilistic ensemble plots can be generated.  

Sensitivity Analysis
~~~~~~~~~~~~~~~~~~~~
If the simulation results are from a LHS simulation the processing type menu will have options for 
several types of sensitivity analysis. Note that while a sensitivity analysis can be ran on simulations
with a small number of realizations, the results will most likely be meaningless. If the sensitivity coefficients
don't sum to one, or if they vary wildly through time, the number of realizations may need to be increased.
Generally 500 to 1000 realizations will be needed for a sensitivity analysis, however this will
change with the complexity of the simulation. Each type of sensitivity analysis will produce plots 
and/or text file output in the output directory.

Correlation coefficients produces a matrix of either Pearson or Spearman Correlation Coefficients.
Any particular output can be excluded from this matrix if desired, no exclusions need to be made.

Sensitivity coefficients will calculate the sensitivity coefficients for each input on the selected output.
Selecting multiple outputs will run the sensitivity coefficient calculation multiple times.  The capture point
is the index for point in time to calculate the sensitivity coefficient at. The analysis will produce a bar chart.

Multi-Sensitivity Coefficients calculates the impact of input parameters on multiple outputs.  Multiple outputs should
be selected here.  The capture point
is the index for point in time to calculate the sensitivity coefficient at.  The analysis will produce a bar chart.

Time Series Sensitivities will produce a line graph of how the impact from input parameters change over time
with respect to an output value.  Selecting multiple output values here will run the analysis multiple times.
The capture point will determine the time to select the most sensitive parameters and their ordering.  

Analysis
---------

The OpenIAM uses the Model Analysis ToolKit (MATK) :cite:`MATK` for the basis of its probabilistic
framework.  More information about MATK can be found here:
`http://dharp.github.io/matk/ <http://dharp.github.io/matk/>`_.  The MATK code repository can
be found here: `https://github.com/dharp/matk <https://github.com/dharp/matk>`_.

Parameter input and output interactions can be explored using the *Analysis* section
of the control file. Correlation coefficients can be calculated using the
*CorrelationCoeff* keyword. Parameter sensitivity coefficients for any output
simulation value can be calculated using a Random-Balanced-Design Fourier
Amplitude Sensitivity Test (RBD-Fast) technique. The control file keywords
*SensitivityCoeff*, *MultiSensitivities*, *TimeSeriesSensitivity* can be
used to access different sensitivity coefficient outputs.  See *ControlFile_ex8.yaml*
for details on using the analysis section.

The Sensitivity Analysis is done with the SALib package :cite:`Herman2017`. 
For more information on the RBD-Fast technique see :cite:`TISSOT2012205` and 
:cite:`PLISCHKE2010354`.  While not accessible through the control files, a
scripting interface is provided to a Sobol sensitivity analysis :cite:`SOBOL20093009`.

This section will be expanded in the future.

.. include:: units.rst