.. toctree::
    :maxdepth: 2

Use Cases
=========

.. toctree::

In the *examples* directory there are a number of example control files.
The first example file *ControlFile_ex1.yaml* was covered in the Control File
section of this document. It connects a simple reservoir model to a cemented
wellbore model and run a forward prediction.

The second example file *ControlFile_ex2.yaml* connects a simple reservoir model
to a multisegmented wellbore model and runs a Latin hypercube sampling (lhs)
stochastic analysis using 30 realizations. Note that a fixed seed could be specified
for the stochastic modeling but the # before the seed has commented it out.
This example shows an alternative way to specify wellbore locations inside
the wellbore component specification with the *Locations* keyword.

The third example file *ControlFile_ex3.yaml* connects a simple reservoir,
a multisegmented wellbore, and a carbonate aquifer together, and runs a
Latin hypercube sampling analysis with 30 realizations. This example illustrates
use of known and random wellbore locations and utilizes the carbonate aquifer
component.

The fourth example file *ControlFile_ex4.yaml* connects a simple reservoir,
an open wellbore, and a carbonate aquifer and runs a Latin hypercube sampling
analysis. This example shows the use of the open wellbore model. It also
demonstrates a number of plotting options. Some of these options are
the *TimeSeriesStats* and the *TimeSeriesAndStats* plot types. For the analysis of the
large numbers of realizations a user might find it more helpful to plot the time series statistics
rather than the data for each realization using *TimeSeriesStats*. *TimeSeriesAndStats*
plots the simulated values and the related statistics. The *subplot* keyword
can be used to present some of the results in subplots.

The fifth example file *ControlFile_ex5.yaml* connects a simple reservoir and
a cemented wellbore model and runs a parameter study (parstudy) analysis.
This example is used to demonstrate the parstudy analysis option.

The sixth example file *ControFile_ex6.yaml* shows the use of a lookup table based
reservoir. To run this example, the additional *Kimberlina* data set
will have to be downloaded and unzipped into the
`source/components/reservoir/lookuptables/Kimb_54_sims` folder. The data files
can be downloaded from the EDX directory where the OpenIAM was downloaded from or
from https://gitlab.com/NRAP/Kimberlina_data.  This data
set comes from simulation work done by *Wainwright et. al.* :cite:`Wainwright2012`.

The seventh example *ControlFile_ex7.yaml* shows the use of dynamic input to
drive a wellbore model without attaching a reservoir model. This functionality
can be used to quickly evaluate behavior of a single component model with a fixed input or
interactions between several component models without constructing a full systems model.

The eighth example *ControlFile_ex8.yaml* demonstrates the use of the *Analysis* section
to compute correlation and sensitivity coefficients and create visualizations of
their effects. Each sub-section of the *Analysis* section shows all available options
for the user to control (required inputs are marked as such).

The ninth example *ControlFile_ex9.yaml* demonstrates the use of the atmospheric model.
The Atmospheric model models |CO2| dispersion in the atmosphere as a dense gas leak.
Example illustrates two plotting options available only for the Atmospheric model.
First, in order to plot the critical distance from leakage points for a
single realization, *AtmPlumeSingle* keyword can be used. For multiple
Monte-Carlo simulations probability of being within a critical distance
can be estimated and plotted using *AtmPlumeEnsemble* keyword. Both types of
plots produce a map-view of the release area for each time step.
It is recommended that *{time_index}* modifier is provided to the *savefig*
keyword argument so that an image for each time-step plot produced can be
saved in a separate file.

The tenth example *ControlFile_ex10.yaml* demonstrates the use of the Alluvium Aquifer
component.  The nature of the Alluvium Aquifer gives it a higher rejection rate of
output, meaning it is common to see 20 to 30 percent of the results returned with
NaN (Not a Number) values.  To effectively use the Alluvium Aquifer higher numbers
of realizations will be needed to allow rejected results to be filtered out.

Beyond control files, the described scenarios can be implemented with a help of a Python script.
Example scripts can be found in the *scripts* sub-directory of the *examples* directory.