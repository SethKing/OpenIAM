Units
------

Data passed between models need to have consistent units.  Here
is a list of units used by the OpenIAM.

* Pressure is assumed to be in units of Pascals (Pa).
* Time is assumed to be in days (d).
* Distance is assumed to be in units of meters (m).
* Fluxes are assumed to be in units of kilograms per second (kg/s).
* Viscosities are assumed to be in units of Pascal seconds (Pa s).

.. # Do we need to specify Viscosity units?  Are it passed between component models?

