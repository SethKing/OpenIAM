GUI Operation
-------------

When the GUI is first opened a disclaimer screen will be shown followed
by the main interface.

.. figure:: images/GUI_ScreenShots/FrontPage.PNG
   :align: center
   :scale: 100%
   :alt: Front Page View
   
   Main IAM Interface
   
To begin building a model click on the "Enter Parameters" button.  
The process of building a model will consist of entering basic model parameters,
defining the geologic stratigraphy of the site, then defining a component model
for each component of the system to be modeled. Therefore the first view in the 
enter parameters window is the model parameters view.  

.. figure:: images/GUI_ScreenShots/Model_Params.PNG
   :align: center
   :scale: 100%
   :alt: Model Parameters View
   
   Model Parameters View

Start by defining a name for your model, this model name will be used as the 
IAM simulation name.  Time will be entered in years, the end time will be the
number of years you want to model.  A uniform time step will be taken during
the simulation specified by the time step entry (typically 1 year time steps
are used).  The IAM can do three types of simulations: forward, lhs, and parstudy.
A forward analysis run a single discrete scenario.  LHS (for Latin Hypercube 
Sampling) is the analysis used to run stochastic simulations.  Parstudy 
breaks each non-fixed variable up in equally spaced values, parstudy is useful
for studying the effects of several variables but the number of realizations 
grows exponentially with the number of variables.

The IAM creates a log file with each simulation run, the level of information
being logged can be set with the logging entry.  The default level of Info should
contain most useful messages. A debugging level of logging will contain more
information about component model connections and calls, but will produce very
large files and should be avoided with large simulations.  Warning and Error 
levels can be used if file sizes become an issue.

..
  #TODO add detail.

The IAM will save all the results of a simulation to the specified output folder.
Enter a folder to save the output to, if this folder doesn't exist it will be created.
A date_time stamp can be added so each run of the simulation is saved separately,
otherwise results from a previous run will be overwritten by subsequent runs until
the output folder is changed. 

Known wellbore coordinates, comma separated. Random wellbores generated in an
area when more wells are specified than number of known coordinates.  After 
completing the model parameters proceed to the Stratigraphy tab.

.. figure:: images/GUI_ScreenShots/Strat_Params.PNG
   :align: center
   :scale: 100%
   :alt: Stratigraphy View
   
   Stratigraphy View

In the Statigraphy tab model parameters related to the stratigraphy of the |CO2| storage site are
defined.  Model parameters for the stratigraphy and for component are defined here with either
a deterministic  fixed value or distribution to vary over.  When running a LHS analysis
parameters defined with a distribution will be sampled from that distribution. When running
a forward simulation all parameters should be specified with a fixed value.  
See the :ref:`stratigraphy_component`
section of this document for a list of all available parameters and their definitions.

Adding Component Models
~~~~~~~~~~~~~~~~~~~~~~~
The IAM is made so that only the components of interest in the system need to be modeled.
Generally the deepest components are added first as their outputs will connect to inputs
of shallower components. Generally, a simulation will be built from the deepest component upward
(reservoir, wellbore, aquifer, etc.).  To add a component, first give it a name (each
component must have a unique name).  

.. figure:: images/GUI_ScreenShots/Add_Component1.PNG
   :align: center
   :scale: 100%
   :alt: Add Component View
   
   Adding a component model
   
Next select the type of component model to be used.  
When adding subsequent components, a connection to existing components can be specified.
If a component is specified that need input from another component but the deeper component
is not to be part of the model (i.e. specifying a wellbore model without a reservoir model),
dynamic parameters can be used for the component model input.  For dynamic parameters a value 
must be specified for each time step in the simulation.  Values are separated by a comma. 

Some components will also need a specification of which formation in the stratigraphy they
represent (such as an aquifer model).  

.. figure:: images/GUI_ScreenShots/Add_Component3.PNG
   :align: center
   :scale: 100%
   :alt: Add Component View 3
   
   Adding a component model with dynamic parameters and a stratigraphy selection

Each component model will have component specific input parameters and outputs.  Parameters
can be specified to be sampled from different distributions, or take on set values.  When running
a forward model parameters should only be specified as fixed values.  When running a parameter study
the parameters to vary should be specified with a uniform distribution and a minimum and maximum value.
For stochastic simulations, any distributions can be specified.  Parameter and output definitions can be 
found in the specific component model parameter section.

After each component is specified, another component can be added to the system model.  When all components
desired have been added, save the model and return to the dashboard.  The system model can then be ran using 
the Run Simulation button on the main dashboard.
