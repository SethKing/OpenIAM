Obtaining OpenIAM
=================

.. toctree::

.. image:: images/nrap.png
   :align: center
   :scale: 100%
   :alt: Nrap Logo

Introduction
------------
OpenIAM is an open-source Integrated Assessment Model (IAM) for phase II of
the National Risk Assessment Partnership (NRAP). The goal of this software is
to go beyond risk assessment into risk management and containment assurance.
OpenIAM is currently in active development and its availability is for testing
and feedback only.

As this is a prototype of software being actively developed, we
are seeking any feedback or bug reports.  An online feedback form
can be found here: https://docs.google.com/forms/d/e/1FAIpQLSed5mcX0OBx1dLNmYGbmS4Vfc0mdOLapIzFqw-6vHoho9B19A/viewform?usp=sf_link

Or bug reports and feedback can be emailed to the OpenIAM project at
incoming+NRAP-IAM/UQ_example_setup@gitlab.com
or the lead developer
Seth.King@netl.doe.gov or any other
member of the development team.

If you have been given access to the code indirectly and would like to be
notified when updates are available for testing, please contact the development team
to be added to our email list.


Downloading OpenIAM
-------------------

The OpenIAM is distributed as a zip file that can be extracted in the location
specified by user.  If the OpenIAM was downloaded from our Gitlab repository,
the folder name will have the repository's current hash appended to it.  Feel
free to rename the folder something simple like "OpenIAM" to make navigation
easier.

This section will be updated in the future when the code is released fully.

Installing OpenIAM
------------------

The OpenIAM requires Python version 3.5 or greater to operate. 
If you need to install Python, the Anaconda Distribution is
recommended (64-bit Python 3.5). It can be found at
`Anaconda (www.anaconda.com/download) <https://www.anaconda.com/download/>`_.
We recommend Anaconda for all platforms (Windows, Mac, and Linux).
While Python can be downloaded from other sources (such as python.org) we
recommend the Anaconda Distribution for anyone needing to install Python
as it will come with the scientific libraries needed for the OpenIAM already
installed. If you are using an alternative distribution of Python you will
need the libraries NumPy, SciPy, PyYAML, Matplotlib, and six. All of these
libraries can be installed using a 'pip install {library}' command.

On Mac and Linux the gfortran compiler will be required to compile some
of the OpenIAM code (Mac users can find gfortran here:
`(https://gcc.gnu.org/wiki/GFortranBinariesMacOS)
<https://gcc.gnu.org/wiki/GFortranBinariesMacOS>`_).

With the proper version of Python, the OpenIAM can be set up and tested.
Extract the OpenIAM zip file where you would like it to be installed.
In the extracted folder there will be a number of sub-folders. First, open
the sub-folder setup. Next, open a command prompt in the setup folder (on Windows
this can be done by holding Shift and right clicking inside the folder when
no file is selected, then selecting "Open command window here" or
"Open PowerShell window here" from the menu).
Run the setup script by entering the command::

    python openiam_setup_tests.py

in the command prompt. This will test the version of Python your system has
(if your system defaults to Python 3, in this and all other OpenIAM
commands try replacing *python* with *python2*). Then the setup will test the Python
libraries versions that the OpenIAM depends on. All libraries the OpenIAM
requires can be installed or upgraded with pip (cmd: pip install {library}).
The setup script will then compile so necessary component model codes on Mac
and Linux (Windows will have component model codes included). Finally, the
setup script will run the test suite to see if the OpenIAM has been installed
correctly. If the results printed to the console indicate errors during the
testing the errors will need to be resolved before the OpenIAM can be used.
When contacting the developers to resolve problems please include all output
from the setup script or test suite runs.

Testing installation
--------------------

After setup the test suite can be run again by entering the OpenIAM *test*
directory in a terminal and typing::

    python iam_test.py

Test results will be printed to the terminal.