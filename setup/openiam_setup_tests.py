# -*- coding: utf-8 -*-
"""
OpenIAM setup script to test python
environment, compile components, and
test OpenIAM functionalility.
Created on Tue Dec 12 10:02:24 2017

@author: Seth King
AECOM supporting NETL
Seth.King@netl.doe.gov
"""
from distutils.spawn import find_executable
import importlib
import logging
from make_libs import make_from_file
import os
import pkgutil
import pkg_resources
import sys
import unittest


def setup_logging():
    """
    Set up logging for the console and for a file.
    """
    logging.basicConfig(level=logging.INFO,
                        format='from %(module)s %(funcName)s - %(levelname)s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename='OpenIAM_setup.log',
                        filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)


def is_tool(name):
    """Check whether `name` is on PATH."""
    return find_executable(name) is not None


def check_python():
    """
    Check whether the proper version of python is being used.

    Currently only python 3.5 is supported.
    """
    logging.info('Checking python version')
    py_major, py_minor = sys.version_info[0:2]
    if py_major != 3 or py_minor < 5:
        logging.error('Python 3.5 is required.\nPython '
                      '{maj}.{mnr} has been detected'.format(maj=py_major, mnr=py_minor))
    else:
        logging.debug('Python version: {info}'.format(info=sys.version_info))
    return


def get_package_name(package):
    """Get the package name from the distribution name."""
    return list(pkg_resources.get_distribution(package)._get_metadata('top_level.txt'))[0]


def test_dependencies():
    """
    Test whether all required python libraries are installed.

    Test whether all needed libraries are installed and satisfy the
    minimum version requirements. Uses requirements.txt file.
    """
    logging.info('Checking OpenIAM dependent libraries')
    pkgs = {}
    with open('requirements.txt', 'r') as req_file:
        for line in req_file:
            pkg, vers = line.strip().split('==')
            pkgs[pkg] = vers
    # Check PyYaml, because it doens't link to package name correctly
    try:
        import yaml
    except ImportError:
        logging.error('PyYaml not detected, install library using the command ' +
                      '"pip install pyyaml" and run setup again')
        sys.exit()

    for package, version in pkgs.items():
        found = pkgutil.find_loader(package)
        if not found:
            try:
                pkg_name = get_package_name(pkg)
                found = pkgutil.find_loader(pkg_name)
            except ImportError:
                logging.error('Package {pkg} not found.'.format(pkg=package) +
                              ' Try installing the package using "pip install {pkg}"'.format(pkg=package) +
                              ' then rerun the setup.')
            except IndexError:
                logging.warning('Index Error with package {pkg}.\n'.format(pkg=package) +
                                'Test that package is installed')
        else:
            ldpkg = importlib.import_module(package)
            if hasattr(ldpkg, '__version__'):
                if ldpkg.__version__ < version:
                    logging.warning('Version {version} '.format(version=version) +
                                    'of {package} needed, '.format(package=package) +
                                    'version {pv} installed'.format(pv=ldpkg.__version__))
    return


def check_compiler():
    """
    Check that gfortran and make are installed (required for Mac and Linux).
    """
    if is_tool('gfortran'):
        state = True
#        if is_tool('make'):
#            state = True
#        else:
#            state = False
#            raise AssertionError('The make utility program must be installed ' +
#                                 'for the OpenIAM to compile the needed libraries.  ' +
#                                 'Please install make and run the setup again.')
    else:
        state = False
        raise AssertionError('The gfortran compiler must be installed ' +
                             'for the OpenIAM to compile the needed libraries.  ' +
                             'Please install gfortran and run the setup again.')
    return state


def create_libraries():
    """
    Check the existence of internal OpenIAM libraries.

    For Windows systems test whether dlls exist, swap for 32-bit version if needed.
    For Mac and Linux systems compile component model code.
    """
    logging.info('Checking internal OpenIAM libraries')
    platform = sys.platform
    components_dir = os.path.join('..', 'source', 'components')
    carb_aqu_dir = os.path.join(components_dir, 'aquifer', 'carbonate')
    ca_make_filename = 'make_carbonate.yaml'
    atm_rom_dir = os.path.join(components_dir, 'atmosphere')
    atm_make_filename = 'make_atm_dis_rom.yaml'
    library_name = []
    if platform == "linux" or platform == "linux2":
        check_compiler()
        # linux
#        run_cmd = 'make -C ' + carb_aqu_dir + ' linux'
#        os.system(run_cmd)
        make_from_file(ca_make_filename, carb_aqu_dir, 'linux')
        library_name.append("carbonate.so")
        make_from_file(atm_make_filename, atm_rom_dir, 'linux')
        library_name.append('atmdisrom.so')
    elif platform == "darwin":
        # OS X
        check_compiler()
#        run_cmd = 'make -C ' + carb_aqu_dir + ' mac'
#        os.system(run_cmd)
        make_from_file(ca_make_filename, carb_aqu_dir, 'mac')
        library_name.append("carbonate.dylib")
        make_from_file(atm_make_filename, atm_rom_dir, 'mac')
        library_name.append('atmdisrom.dylib')
    elif platform == "win32":
        # Windows
        cdll = os.path.join(carb_aqu_dir, 'carbonate.dll')
        atmdll = os.path.join(atm_rom_dir, 'atmdisrom.dll')
        if not(sys.maxsize > 2**32):  # 32 bit
            logging.debug('32 bit python detected')
            os.rename(cdll, os.path.join(carb_aqu_dir, 'carbonate_x64.dll'))
            os.rename(os.path.join(carb_aqu_dir, 'carbonate_x32.dll'), cdll)
            os.rename(atmdll, os.path.join(atm_rom_dir, 'atmdisrom_x64.dll'))
            os.rename(os.path.join(atm_rom_dir, 'atmdisrom_x32.dll'), atmdll)
        else:
            cdll64 = os.path.join(carb_aqu_dir, 'carbonate_x64.dll')
            if os.path.exists(cdll64) and not(os.path.exists(cdll)):
                os.rename(cdll64, cdll)
                logging.debug('Renaming carbonate_x64.dll to carbonate.dll')
        library_name.append("carbonate.dll")
        library_name.append("atmdisrom.dll")
    else:
        library_name.append("carbonate.dll")
        library_name.append("atmdisrom.dll")
        logging.warning('OS platform not reconized')

    for lib, lib_dir in zip(library_name, (carb_aqu_dir, atm_rom_dir)):
        lib_name = os.path.join(lib_dir, lib)
        if not os.path.exists(lib_name):
            logging.error('Cannot find dynamic library:\n {libname}\n'.format(libname=library_name) +
                          'Platform: {platform}'.format(platform=platform))
        else:
            logging.debug('{libname} found'.format(libname=library_name))
    return


def run_tests():
    """
    Run the OpenIAM test suite.
    """
    logging.info('Testing OpenIAM Installation')
    main_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(os.path.join(main_dir, 'test'))
    import iam_test

    runner = unittest.TextTestRunner(verbosity=0)
    test_suite = iam_test.suite('all')
    test_results = runner.run(test_suite)
    if test_results.wasSuccessful():
        message = 'Test suite ran successfully, OpenIAM is ready to use.'
        print(message)
        logging.debug(message)
    else:
        message = 'There were errors testing the OpenIAM installations, please resolve these issues.'
        logging.warning(message)
        test_results.printErrors()
    return test_results

if __name__ == '__main__':
    __spec__ = None
    setup_logging()
    check_python()
    test_dependencies()
    create_libraries()
    run_tests()
