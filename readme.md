OpenIAM
=========
This is the prototype NRAP Phase II Integrated Assessment Model.  
To use the OpenIAM, download and extract the OpenIAM, then open
the User's Guide OpenIAM.pdf.  To download the OpenIAM click on 
the cloud icon in the lower right section of the header information
directly above the repository file.    
From the menu that appears select your favorite compressed folder
data type.  The cloud icon looks like:

![gitlab download image](https://gitlab.com/SethKing/OpenIAM/uploads/8b7bb5dfad20da60d68fb8939fc0b7d1/gitlab_download_cloud.png)

[Click here if you still can't find it](https://gitlab.com/NRAP/OpenIAM/repository/master/archive.zip)

The download will have the current repository hash as part of the 
folder name, feel free to rename the folder something simple
such as "OpenIAM".

Feedback
--------- 
As this is a prototype of software being actively developed, we
are seeking any feedback or bug reports.  An online feedback form 
can be found here: https://docs.google.com/forms/d/e/1FAIpQLSed5mcX0OBx1dLNmYGbmS4Vfc0mdOLapIzFqw-6vHoho9B19A/viewform?usp=sf_link

Or feedback can be emailed to the OpenIAM project at 
incoming+NRAP-IAM/UQ_example_setup@gitlab.com
or the lead developer
Seth.King@netl.doe.gov or any other
member of the development team.

Disclaimer 
-----------
This software was prepared as part of work sponsored by an agency 
of the United States Government. Neither the United States Government 
nor any agency thereof, nor any of their employees, makes any warranty, 
express or implied, or assumes any legal liability or responsibility 
for the accuracy, completeness, or usefulness of any information, 
apparatus, product, or process disclosed, or represents that its use 
would not infringe privately owned rights. Reference therein to any 
specific commercial product, process, or service by trade name, 
trademark, manufacturer, or otherwise does not necessarily constitute 
or imply its endorsement, recommendation, or favoring by the United 
States Government or any agency thereof. The views and opinions of 
authors expressed therein do not necessarily state or reflect those 
of the United States Government or any agency thereof.