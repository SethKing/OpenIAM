import ctypes
import os
import numpy as np
from sys import platform

def simple_model(p, input_array=None):
    """
    Return output based on the provided input parameters.

    :param p: dictionary of parameters passed to the function
    :type p: dict

    :param input_array: input array
    :type input_array: numpy.array

    :returns dictionary containing 3 output variables and array
    """
    # If parameters are floats and can vary stochastically, they should be
    # passed in p, dictionary of input parameters. If model parameters are
    # arrays they should be passed as separate arguments, similar to input_array.

    # Check which parameters are provided in the input dictionary
    if 'var1' in p: var1 = p['var1']
    else: var1 = -1.0  # default values if the desired parameter value was not passed

    if 'var2' in p: var2 = p['var2']
    else: var2 = 2.0

    if 'var3' in p: var3 = p['var3']
    else: var3 = -3.0

    # Check whether input_array was passed and define output array
    if input_array is None:
        out_array = np.ones(10)
    else:
        out_array = 4*input_array - 7*var1 + np.log10(var2) - var3**2

    # Define output variables of the component model
    out_var1 = var1**2 + var2*var3
    out_var2 = var2**3 + var1*var3
    out_var3 = var3**2 + var1*var2 + var1**3

    # Define output of the function (output is also a dictionary)
    output = dict()
    output['output1'] = out_var1  # float
    output['output2'] = out_var2  # float
    output['output3'] = out_var3  # float
    output['output4'] = out_array  # array

    # Component model should return a dictionary of outputs
    return output

def fortran_based_model(p, input_array=None):
    '''
    Return two outputs based on the provided input parameters.

    :param p: dictionary of parameters passed to the function
    :type p: dict

    :param input_array: input array
    :type input_array: numpy.array

    :returns dictionary containing output variable and array
    '''
    # Check what parameters are provided in the input dictionary
    # float input variable
    if 'var1' in p: var1 = p['var1']
    else: var1 = 1.0

    if 'var2' in p: var2 = p['var2']
    else: var2 = 3.0

    if input_array is None:
        input_array = np.array([3.4, 7.8, 6.5, 8.4, 7.6, 2.1, 6.4, 6.2, 6.5])

    # Determine the size of input array
    N = np.size(input_array)

    # Setup library and needed function names
    if platform == "linux" or platform == "linux2":
        # linux
         library = "example_rom.so"
    elif platform == "darwin":
        # OS X
        library = "example_rom.dylib"
    elif platform == "win32":
        # Windows...
        library = "example_rom.dll"
    functionName = "example_rom"

    # Define c classes
    INT = ctypes.c_int
    DOUBLE = ctypes.c_double
    NPointsArrayType = DOUBLE*N

    # Load DLL
    external_lib = ctypes.cdll.LoadLibrary(os.path.join(os.getcwd(),library))

    # Get needed function as attribute of the library
    function = getattr(external_lib, functionName)

    # Set argument types for value and pointers
    function.argtypes = [ctypes.POINTER(DOUBLE), ctypes.POINTER(DOUBLE),
                         ctypes.POINTER(INT),
                         ctypes.POINTER(DOUBLE),
                         ctypes.POINTER(DOUBLE)]
    function.restype = None

    # Define values of the input parameters that will be passed to fortran function
    fun_N = INT(N)
    fun_var1 = DOUBLE(var1+var2)
    fun_array1 = NPointsArrayType(*input_array)

    out_var1 = DOUBLE()                # initialize output variable
    out_array1 = NPointsArrayType()    # initialize output array

    function(fun_var1, fun_array1, fun_N, out_var1, out_array1)

    # Create output dictionary
    out = dict()
    out['out_var1'] = out_var1.value   # extract value from the float type of output
    out['out_array1'] = out_array1[0:N]   # extract values from array type of output

    # Return output dictionary
    return out

def test_ROM():
    """
    Test the correct work of fortran code compiled into library.
    """
    # Setup library and needed function names
    if platform == "linux" or platform == "linux2":
        # linux
         library = "example_rom.so"
    elif platform == "darwin":
        # OS X
        library = "example_rom.dylib"
    elif platform == "win32":
        # Windows...
        library = "example_rom.dll"
    functionName = "example_rom"

    # Define c classes
    INT = ctypes.c_int
    DOUBLE = ctypes.c_double

    N = 5
    FivePointsArrayType = DOUBLE*N

    # Load DLL
    external_lib = ctypes.cdll.LoadLibrary(os.path.join(os.getcwd(),library))

    # Get needed function as attribute of the library
    function = getattr(external_lib, functionName)

    # Set argument types for value and pointers
    function.argtypes = [ctypes.POINTER(DOUBLE), ctypes.POINTER(DOUBLE),
                         ctypes.POINTER(INT),
                         ctypes.POINTER(DOUBLE),
                         ctypes.POINTER(DOUBLE)]
    function.restype = None

    # Define values of the input parameters
    N = INT(5)
    var1 = DOUBLE(13.5)
    array1 = FivePointsArrayType(1.1, 4.5, 8.1, 2.7, 9.5)

    out_var1 = DOUBLE(3.6)
    out_array1 = FivePointsArrayType()
    function(var1, array1, N, out_var1, out_array1)

    # Return output array
    return out_array1[0:5]


if __name__=='__main__':
    # Test set up example
    res = test_ROM()
    print(res)

    # Test fortran_based_model function
    parameters = dict()
    parameters['var1'] = 3.5
    parameters['var2'] = 7.5
    input_array = np.array([3.4, 7.8, 6.5, 8.4])

    out = fortran_based_model(parameters, input_array)
    print('=========================================')
    print(out['out_array1'])
    print(out['out_var1'])

    # Test simple_model function
    parameters = dict()
    parameters['var1'] = 3.5
    parameters['var2'] = 0.5
    parameters['var3'] = 1.5
    input_array = np.array([3.4, 7.8, 6.5, 8.4, 0.4, 3.5, 8.7])
    out = simple_model(parameters, input_array)
    print('=========================================')
    print(out['output1'])
    print(out['output2'])
    print(out['output3'])
    print(out['output4'])