import sys,os
sys.path.insert(0,os.sep.join(['..','..','source']))
from matk import matk,pyDOE
import numpy as np

# number of samples
#numsamples = 480

# Create matk object
p = matk()

# aquifer parameters
p.add_par('thick',min=30.0,max=90.0)
p.add_par('depth',min=-550.0,max=-700.0)
p.add_par('por',min=0.02,max=0.2)
p.add_par('log_permh',min=-14.0,max=-11.0)
p.add_par('log_aniso',min=0.0,max=3.0)
p.add_par('rel_vol_frac_calcite',min=0.0,max=1.0)
# leakage parameters
p.add_par('log_co2_rate',min=-7.0,max=1.5)
p.add_par('log_brine_rate',min=-7.0,max=1.5)

# Create sampleset using pyDOE and parameter mins and maxs
#s = p.parmins + pyDOE.lhs(len(p.pars),samples=numsamples) * (p.parmaxs-p.parmins)
#s = p.parmins + pyDOE.bbdesign(len(p.pars)) * (p.parmaxs-p.parmins)
s = (p.parmins + p.parmaxs)/2 + pyDOE.bbdesign(8,center=1)*(p.parmaxs-p.parmins)/2
print(s)

