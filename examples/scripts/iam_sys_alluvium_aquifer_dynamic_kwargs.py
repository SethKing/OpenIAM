# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 09:23:01 2018

@author: Seth King
AECOM supporting NETL
Seth.King@NETL.DOE.GOV
"""
import sys,os
import numpy as np

sys.path.insert(0, os.sep.join(['..', '..', 'source']))
from openiam import SystemModel, AlluviumAquifer

if __name__ == '__main__':
    time_array = 365.25*np.arange(0.0, 3.0)
    sm_model_kwargs = {'time_array': time_array}  # time is given in days
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # Add alluvium aquifer model object and define parameters
    aa = sm.add_component_model_object(AlluviumAquifer(name='aa', parent=sm))

    # TODO fix parameters for boundaries
    aa.add_par('sandFraction', value=0.238806)
    aa.add_par('correlationLengthX', value=0.070152)
    aa.add_par('correlationLengthZ', value=0.689042)
    aa.add_par('permeabilityClay', value=0.553339)
    aa.add_par('NaMolality', value=0.780295)
    aa.add_par('PbMolality', value=0.739868)
    aa.add_par('benzeneMolality', value=0.109203)
    aa.add_par('tMitigation', value=0.436755)
    aa.add_par('simtime', value=0.005)
    aa.add_par('CEC', value=0.801316)
    aa.add_par('Asbrine', value=0.900818)
    aa.add_par('Babrine', value=0.608352)
    aa.add_par('Cdbrine', value=0.141883)
    aa.add_par('Pbbrine', value=0.223084)
    aa.add_par('Benzene_brine', value=0.0706014)
    aa.add_par('Benzene_kd', value=0.179084)
    aa.add_par('Benzene_decay', value=0.552091)
    aa.add_par('PAH_brine', value=0.48845)
    aa.add_par('PAH_kd', value=0.416288)
    aa.add_par('PAH_decay', value=0.47743)
    aa.add_par('phenol_brine', value=0.529151)
    aa.add_par('phenol_kd', value=0.757335)
    aa.add_par('phenol_decay', value=0.370127)
    aa.add_par('porositySand', value=0.873237)
    aa.add_par('densitySand', value=0.665953)
    aa.add_par('VG_mSand', value=0.394548)
    aa.add_par('VG_alphaSand', value=0.396608)
    aa.add_par('permeabilitySand', value=0.39261)
    aa.add_par('Clbrine', value=0.608252)
    aa.add_par('calcitevol', value=0.788496)
    aa.add_par('V_goethite', value=0.02)
    aa.add_par('V_illite', value=0.02)
    aa.add_par('V_kaolinite', value=0.02)
    aa.add_par('V_smectite', value=0.02)

    # TODO adjust kwargs to match reality
    aa.add_dynamic_kwarg('co2_rate', [0.0, 0.002048, 0.004096])
    aa.add_dynamic_kwarg('co2_mass', [0.0, 10**0.315539, 10**0.355])
    aa.add_dynamic_kwarg('brine_rate', [0.0, 0.293401, 0.586802])
    aa.add_dynamic_kwarg('brine_mass', [0.0, 10**0.394501, 10**0.450139])

    # Add observations (output) from the alluvium aquifer model
    aa.add_obs('TDS')
    aa.add_obs('pH')
    aa.add_obs('As')
    aa.add_obs('Pb')
    aa.add_obs('Cd')
    aa.add_obs('Ba')
    aa.add_obs('Benzene')
    aa.add_obs('Naphthalene')
    aa.add_obs('Phenol')

    # Run the system model
    sm.forward()

    # Print the observations
    print('TDS:', sm.collect_observations_as_time_series(aa, 'TDS'))
    print('pH:', sm.collect_observations_as_time_series(aa, 'pH'))
    print('As:', sm.collect_observations_as_time_series(aa, 'As'))
    print('Pb:', sm.collect_observations_as_time_series(aa, 'Pb'))
    print('Cd:', sm.collect_observations_as_time_series(aa, 'Cd'))
    print('Ba:', sm.collect_observations_as_time_series(aa, 'Ba'))
    print('Benzene:', sm.collect_observations_as_time_series(aa, 'Benzene'))
    print('Naphthalene:', sm.collect_observations_as_time_series(aa, 'Naphthalene'))
    print('Phenol:', sm.collect_observations_as_time_series(aa, 'Phenol'))

    # TODO fix expected output
    # Expected output