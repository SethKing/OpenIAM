# OpenIAM Control File Example 4
# To run this file, use the command (\ for Windows / for Mac or Linux): 
#  python ../source/openiam/openiam_cf.py --file ControlFile_ex4.yaml
# This example computes the aquifer impact from 2 leaky (open) wellbores
# connected to the simple reservoir model.
# This example produces eight plots. The plots produced by this example
# are meant to display the different ways the OpenIAM can create 
# visualizations of observations and the user controls available for
# plot customization.
# Last Modified: March 01, 2018
#-------------------------------------------------
ModelParams:
    EndTime: 50
    TimeStep: 1.0
    Analysis: 
        type: lhs
        siz: 30
    Components: [SimpleReservoir1,
                 OpenWellbore1,
                 CarbonateAquifer1]
    OutputDirectory: ../output/output_ex4_{datetime}
    Logging: Debug
Stratigraphy:
    numberOfShaleLayers:
        vary: False
        value: 3
    shale1Thickness:
        min: 500.0
        max: 550.0
        value: 525.0
    shale2Thickness:
        min: 450.0
        max: 500.0
        value: 475.0
    shale3Thickness:
        vary: False
        value: 111.2
    aquifer1Thickness:
        vary: False
        value: 122.4
    aquifer2Thickness:
        vary: False
        value: 119.2
    reservoirThickness:
        vary: False
        value: 51.2
WellboreLocations:
    coordx: [100, 540]
    coordy: [100, 630]
#-------------------------------------------------
# SimpleReservoir1 is a user defined name for component
# the type SimpleReservoir is the ROM model name
#-------------------------------------------------
SimpleReservoir1:
    Type: SimpleReservoir
    Parameters:
        injRate: 0.1
    Outputs: [pressure,
              CO2saturation]
#-------------------------------------------------
OpenWellbore1:
    Type: OpenWellbore
    Connection: SimpleReservoir1 
    Number: 2
    LeakTo: aquifer2
    Parameters:
        wellRadius:
            min: 0.001
            max: 0.002
            value: 0.0015
        logNormalizedTransmissivity:
            min: -1.0
            max: 1.0
            value: 0.0
        brineSalinity: 0.1
    Outputs: [CO2_aquifer2, 
              brine_aquifer2]
#-------------------------------------------------
CarbonateAquifer1:
    Type: CarbonateAquifer
    Connection: OpenWellbore1
    AquiferName: aquifer2
    Parameters: 
    Outputs: ['pH', 'TDS']    
#-------------------------------------------------
Plots:
    #------------------------------------------------------
    #The TimeSeriesStats option can be used to plot 
    # percentiles, min, mean, median, and max values.
    #This is the best way to view large numbers of 
    # realization results.
    #------------------------------------------------------
    pH_plume_volume_stats: 
        TimeSeriesStats: [pH]
    #------------------------------------------------------
    #Since there is only 30 realizations in this example
    # the data can be plotted on top of the statistics.
    #------------------------------------------------------
    TDS_plume_volumes: 
        TimeSeriesAndStats: [TDS]
        Title: TDS by Realizations and Statistics
    #------------------------------------------------------
    #Plot the brine leakage into the aquifer from both wells
    # on one plot
    #------------------------------------------------------
    Brine_Leakage:
        TimeSeriesStats: [brine_aquifer2]
    #------------------------------------------------------
    #Plot the brine leakage into the aquifer from each wells
    # on it's own plot
    #------------------------------------------------------
    Brine_Leakage_subplots:
        TimeSeriesStats: [brine_aquifer2]
        subplot:
            use: True
    #------------------------------------------------------
    #Plot the CO2 leakage into the aquifer from both wells
    # on one plot (explicitly turning off subplots not needed)
    #------------------------------------------------------
    CO2_Leakage:
        TimeSeriesStats: [CO2_aquifer2]
        Title: $CO_2$ leakage Stats
        subplot:
            use: False
    #------------------------------------------------------
    #Plot the CO2 leakage into the aquifer from each wells
    # on it's own plot
    #------------------------------------------------------
    CO2_Leakage_subplots:
        TimeSeriesStats: [CO2_aquifer2]
        Title: $CO_2$ leakage Stats
        subplot:
            use: True
            ncols: 1
    #------------------------------------------------------
    #Plot the CO2 leakage into the aquifer from each wells
    # on it's own plot by realization instead of statistics
    #------------------------------------------------------
    CO2_Leakage_real:
        TimeSeries: [CO2_aquifer2]
        Title: $CO_2$ leakage by realization
        subplot:
            use: True
    #------------------------------------------------------
    #Plot the reservoir pressure and saturation for each well
    # location with 2 columns of subplots and subplot titles
    # renames with cleaner values
    #------------------------------------------------------
    Reservoir:
        TimeSeriesStats: [pressure, CO2saturation]
        Title: Reservoir Response
        subplot: 
            ncols: 2
            SimpleReservoir1_000.pressure: 'Pressure at well #0'
            SimpleReservoir1_001.pressure: 'Pressure at well #1'
            SimpleReservoir1_000.CO2saturation: '$CO_2$ Saturation at well #0'
            SimpleReservoir1_001.CO2saturation: '$CO_2$ Saturation at well #1'
