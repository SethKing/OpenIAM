import os
import sys
import unittest
import numpy as np
import warnings
try:
    from openiam import (SystemModel, SimpleReservoir, MultisegmentedWellbore,
                         CementedWellbore, OpenWellbore, CarbonateAquifer,
                         RateToMassAdapter, ReservoirDataInterpolator,
                         LookupTableReservoir, AtmosphericROM, AlluviumAquifer, DeepAlluviumAquifer)
    import openiam.visualize as iam_vis
except:
    try:
        sys.path.append(os.sep.join(['..', 'source']))
        from openiam import (SystemModel, SimpleReservoir,
                             MultisegmentedWellbore, CementedWellbore,
                             OpenWellbore,
                             CarbonateAquifer, RateToMassAdapter,
                             ReservoirDataInterpolator, LookupTableReservoir,
                             AtmosphericROM, AlluviumAquifer, DeepAlluviumAquifer)
        import openiam.visualize as iam_vis
    except ImportError as err:
        print('Unable to load IAM class module: '+str(err))
currentWorkDir = os.getcwd()


class Tests(unittest.TestCase):

    def setUp(self):
        """Defines the actions performed before each test."""
        # return to original directory
        os.chdir(currentWorkDir)
        return

    def shortDescription(self):
        """Defines information that is printed about each of the tests.

        Method redefines method of TestCase class. In the original method
        only the first line of docstring is printed. In this method
        the full docstring of each test in Tests class is printed.
        """
        doc = self._testMethodDoc
        return doc

    def test_alluvium_aquifer(self):
        """Tests basic alluvum aquifer.

        Tests a simple alluvium aquifer component in a
        forward model against expected output for 2 years of data.
        """

        time_array = 365.25*np.arange(0.0,2.0)
        sm_model_kwargs = {'time_array': time_array} # time is given in days
        sm = SystemModel(model_kwargs=sm_model_kwargs)
    
    
        # Add carbonate aquifer model object and define parameters
        aa = sm.add_component_model_object(AlluviumAquifer(name='aa',parent=sm))
        #ca = sm.add_component_model_object(CarbonateAquifer(name='ca',parent=sm))
    
        #self.pars_bounds['perm_var'] = [0.017, 1.89]
        aa.add_par('sandFraction', value=0.422)
        aa.add_par('correlationLengthX', value=361.350)
        aa.add_par('correlationLengthZ', value=17.382)
        aa.add_par('permeabilityClay', value=-16.340)
        aa.add_par('NaMolality', value=0.121)
        aa.add_par('PbMolality', value=-5.910)
        aa.add_par('benzeneMolality', value=0.109)
        aa.add_par('tMitigation', value=87.914)
        aa.add_par('simtime', value=1.995)
        aa.add_par('CEC', value=32.073)
        aa.add_par('Asbrine', value=-5.397)
        aa.add_par('Babrine', value=-3.397)
        aa.add_par('Cdbrine', value=-8.574)
        aa.add_par('Pbbrine', value=-7.719)
        aa.add_par('Benzene_brine', value=-8.610)
        aa.add_par('Benzene_kd', value=-3.571)
        aa.add_par('Benzene_decay', value=-2.732)
        aa.add_par('PAH_brine', value=-7.118)
        aa.add_par('PAH_kd', value=-0.985)
        aa.add_par('PAH_decay', value=-3.371)
        aa.add_par('phenol_brine', value=-6.666)
        aa.add_par('phenol_kd', value=-1.342)
        aa.add_par('phenol_decay', value=-3.546)
        aa.add_par('porositySand', value=0.468)
        aa.add_par('densitySand', value=2165.953)
        aa.add_par('VG_mSand', value=0.627)
        aa.add_par('VG_alphaSand', value=-4.341)
        aa.add_par('permeabilitySand', value=-12.430)
        aa.add_par('Clbrine', value=-0.339)
        aa.add_par('calcitevol', value=0.165)
        aa.add_par('V_goethite', value=0.004)
        aa.add_par('V_illite', value=0.006)
        aa.add_par('V_kaolinite', value=0.004)
        aa.add_par('V_smectite', value=0.010)

        aa.model_kwargs['co2_rate'] = 1.90e-02 # kg/s
        aa.model_kwargs['co2_mass'] = 6.00e+05 # kg
        aa.model_kwargs['brine_rate'] = 4.62e-03 # kg/s
        aa.model_kwargs['brine_mass'] = 1.46e+05 # kg

        # Add observations (output) from the alluvium aquifer model
        aa.add_obs('TDS')
        aa.add_obs('pH')

        # Run the system model
        sm.forward()

        # Assign observations of the model to variables
        tds = sm.collect_observations_as_time_series(aa, 'TDS')
        ph = sm.collect_observations_as_time_series(aa, 'pH')

        # True values
        true_tds = [0.0, 6878026.69860782]
        true_ph = [ 0.0, 5742563782.01]
        
        # Test with helpful message!
        for ts, s, t in zip(true_tds, tds, time_array):
            self.assertTrue(abs((ts-s)) < 1.0,
                            'TDS at time {} is {} but should be {}'
                            .format(str(t), str(s), str(ts)))
        for ts, s, t in zip(true_ph, ph, time_array):
            self.assertTrue(abs((ts-s)) < 1.0,
                            'pH at time {} is {} but should be {}'
                            .format(str(t), str(s), str(ts)))
        return

    def test_atm(self):
        """Tests atmospheric ROM component.

        Tests the forward system model with only an atmospheric model component driven
        by known fixed flow rates against known data.
        """
        time_array = 365.25*np.arange(0, 7.0)
        sm_model_kwargs = {'time_array': time_array}
        sm = SystemModel(model_kwargs=sm_model_kwargs)

        atm = sm.add_component_model_object(AtmosphericROM(name='atm', parent=sm))
        atm.add_par('T_amb', value=25.0)
        atm.add_par('P_amb', value=0.9869233)
        atm.add_par('V_wind', value=5.0)
        atm.add_par('C0_critical', value=0.01)
        atm.add_par('T_source', value=25.0)

        atm.model_kwargs['x_receptor'] = [820.0]
        atm.model_kwargs['y_receptor'] = [1010.0]
        atm.model_kwargs['x_coor'] = [900.0, 1000.0]
        atm.model_kwargs['y_coor'] = [1000.0, 1050.0]

        atm.add_dynamic_kwarg('co2_leakrate', [[0.0, 0.0],
                                               [0.5, 1.0],
                                               [2.0, 3.0],
                                               [4.5, 5.0],
                                               [6.0, 7.0],
                                               [8.0, 9.0],
                                               [10.0, 10.0]])
        atm.add_obs('num_sources')
        atm.add_obs('x_new_s000')
        atm.add_obs('y_new_s000')
        atm.add_obs('critical_distance_s000')
        atm.add_obs('x_new_s001')
        atm.add_obs('y_new_s001')
        atm.add_obs('critical_distance_s001')
        atm.add_obs('outflag_r000')

        sm.forward()

        num_sources = sm.collect_observations_as_time_series(atm, 'num_sources')
        true_sources = [2, 2, 1, 1, 1, 1, 1]
        for tv, sv, time in zip(true_sources, num_sources, time_array):
            self.assertTrue(abs((tv-sv)) < 0.01,
                            'Number of sources at time {} is {} but should be {}'
                            .format(str(time), str(sv), str(tv)))
        x_new_0 = sm.collect_observations_as_time_series(atm, 'x_new_s000')
        y_new_0 = sm.collect_observations_as_time_series(atm, 'y_new_s000')
        crit_dist_0 = sm.collect_observations_as_time_series(atm, 'critical_distance_s000')
        x_true = [900., 900., 956.03217926, 951.57461885, 952.30292214,
                  951.76012878, 950.]
        y_true = [1000., 1000., 1028.01608963, 1025.78730943, 1026.15146107,
                  1025.88006439, 1025.]
        crit_dist_true = [0., 50.01595939, 198.20226211, 291.59765037, 341.10956199,
                          390.0737076, 423.09433987]
        test_now = [(x_true, x_new_0), (y_true, y_new_0), (crit_dist_true, crit_dist_0)]
        for true_vals, sim_vals in test_now:
            for tv, sv, time in zip(true_vals, sim_vals, time_array):
                self.assertTrue(abs((tv-sv)) < 0.01,
                                'Atmospheric ROM output at time {} is {} but should be {}'
                                .format(str(time), str(sv), str(tv)))
        out_flag = sm.collect_observations_as_time_series(atm, 'outflag_r000')
        true_flag = [0, 0, 1, 2, 2, 2, 2]
        for tv, sv, time in zip(true_flag, out_flag, time_array):
            self.assertTrue(abs((tv-sv)) < 0.01,
                            'Atmospheric ROM out_flag at time {} is {} but should be {}'
                            .format(str(time), str(sv), str(tv)))
        return

    def test_carb_aquifer(self):
        """Tests the carbonate aquifer component.

        Tests the forward system model with only an aquifer component driven
        by known fixed flow rates against known data.
        """
        # Create system model
        time_array = 365.25*np.arange(0.0, 2.0)
        sm_model_kwargs = {'time_array': time_array}  # time is given in days
        sm = SystemModel(model_kwargs=sm_model_kwargs)

        # Add carbonate aquifer model object and define parameters
        ca = sm.add_component_model_object(CarbonateAquifer(name='ca', parent=sm))
        ca.add_par('perm_var', value=1.24e+00)
        ca.add_par('corr_len', value=2.01E+00)
        ca.add_par('aniso', value=4.36e+01)
        ca.add_par('mean_perm', value=-1.14E+01)
        ca.add_par('aqu_thick', value=4.71E+02)
        ca.add_par('hyd_grad', value=1.63e-02)
        ca.add_par('calcite_ssa', value=4.13E-03)
        ca.add_par('cl', value=4.59E+00)
        ca.add_par('organic_carbon', value=5.15e-03)
        ca.add_par('benzene_kd', value=1.53e+00)
        ca.add_par('nap_kd', value=2.78e+00)
        ca.add_par('phenol_kd', value=1.29e+00)
        ca.add_par('benzene_decay', value=1.58e+00)
        ca.add_par('nap_decay', value=3.65e-01)
        ca.add_par('phenol_decay', value=2.61e-01)
        ca.model_kwargs['x'] = [0.]
        ca.model_kwargs['y'] = [0.]
        ca.model_kwargs['co2_rate'] = [1.90e-02]  # kg/s
        ca.model_kwargs['co2_mass'] = [6.00e+05]  # kg
        ca.model_kwargs['brine_rate'] = [4.62e-03]  # kg/s
        ca.model_kwargs['brine_mass'] = [1.46e+05]  # kg

        # Add observations (output) from the carbonate aquifer model
        ca.add_obs('TDS')

        # Run system model using current values of its parameters
        sm.forward()

        # Assign observations of the model to variables
        tds = sm.collect_observations_as_time_series(ca, 'TDS')

        # True values
        true_tds = [18940957.051663853, 18925088.43222359]  # added true value of observation at time 0.0

        # Test with helpful message!
        for ts, s, t in zip(true_tds, tds, time_array):
            self.assertTrue(abs((ts-s)/ts) < 0.01,
                            'TDS at time {} is {} but should be {}'
                            .format(str(t), str(s), str(ts)))

    def test_coupled_components_with_grid_obs(self):
        """ Tests coupling of two components utilizing gridded observations.

        Tests the system model with two coupled components one of which has
        gridded observation against known data. Tests the content of the file created
        for the storage of gridded observation simulated values at one
        of the time points.
        """
        def comp_model1(p, x=None, y=None, time_point=0.0):
            if (x is not None) and (y is not None):
                xx = x
                yy = y
            else:
                x = np.linspace(0., 10., 6)
                y = np.linspace(0., 10., 6)

            if 'var1' in p: var1 = p['var1']
            else: var1 = 2.

            out = {'plane': np.zeros((len(x)*len(y),))}
            ind = 0
            for xv in xx:
                for yv in yy:
                    out['plane'][ind] = np.log10(time_point+1)*(xv+yv)/(10*var1) + 4*np.log10(time_point+1)
                    ind = ind + 1
            out['time_squared'] = 4*np.sin(time_point)**2/1.0e+3 + var1*time_point/10.
            return out

        def comp_model2(p, time_point=0.0, kw1=1.0, kw2=[1.0], kw3=1.0, kw4=[1.3, 1.5]):
            # Check which parameters are provided in the input dictionary
            if 'var1' in p: var1 = p['var1']
            else: var1 = 1.0  # default values if the desired parameter value was not passed

            if 'var2' in p: var2 = p['var2']
            else: var2 = 1.0

            if 'var3' in p: var3 = p['var3']
            else: var3 = 1.0

            # Define output of the function (output is also a dictionary)
            output = dict()
            output['output1'] = np.abs((kw1*var1**2 + kw3*var2*var3)*np.sin(time_point))

            # Component model should return a dictionary of outputs
            return output

        # Define keyword arguments of the system model
        num_years = 4
        time_array = 365.25*np.arange(1.0, num_years+1)
        sm_model_kwargs = {'time_array': time_array}  # time is given in days

        # Create system model
        sm = SystemModel(model_kwargs=sm_model_kwargs)

        # Add component 1
        N = 100
        x = np.linspace(1., 10., N+1)

        cm1 = sm.add_component_model(name='cm1', model=comp_model1,
                                     model_kwargs={'x': x, 'y': x, 'time_point': 365.25})
        cm1.add_par('var1', min=2., max=5., value=3.)
        cm1.add_grid_obs('plane', constr_type='array', output_dir='test_output', index=[3])
        cm1.add_local_obs('plane_loc1', 'plane', constr_type='array', loc_ind=65)
        cm1.add_obs('time_squared')
        cm1.add_obs_to_be_linked('time_squared')
        cm1.add_obs_to_be_linked('plane', obs_type='grid')

        # Add component 2
        cm2 = sm.add_component_model(name='cm2', model=comp_model2,
                                     model_kwargs={'time_point': 365.25})
        cm2.add_par('var1', value=1., vary=False)
        cm2.add_kwarg_linked_to_obs('kw1', cm1.linkobs['time_squared'])
        cm2.add_kwarg_linked_to_obs('kw2', cm1.linkobs['plane'], obs_type='grid')
        cm2.add_kwarg_linked_to_obs('kw3', cm1.linkobs['plane'], obs_type='grid',
                                    constr_type='array', loc_ind=[45])
        cm2.add_kwarg_linked_to_obs('kw4', cm1.linkobs['plane'], obs_type='grid',
                                    constr_type='array', loc_ind=[23, 33, 43, 53])
        cm2.add_obs('output1')
        sm.forward()

        # Check saved files
        filename = '_'.join(['cm1', 'plane', 'sim_0', 'time_3'])+'.npz'
        d = np.load(os.sep.join(['test_output', filename]))
        file_data = d['data'][10:15]
        d.close()
        true_file_data = [12.9657344, 12.97522925, 12.98472409, 12.99421893, 13.00371377]

        plane_loc1 = sm.collect_observations_as_time_series(cm1, 'plane_loc1')
        time_squared = sm.collect_observations_as_time_series(cm1, 'time_squared')
        true_plane_loc1 = [10.92596568, 12.20632674, 12.95592541, 13.48795072]
        true_time_squared = [109.57715925, 219.15397464, 328.7265263, 438.3001008]

        output1 = sm.collect_observations_as_time_series(cm2, 'output1')
        true_output1 = [88.42290161, 230.4544242, 210.95033575, 71.68746713]

        for t, v, tv in zip(time_array, plane_loc1, true_plane_loc1):
            self.assertTrue(abs((v-tv)/tv) < 0.01,
                            'Observation plane_loc1 at time {} is {} but should be {}'
                            .format(str(t), str(v), str(tv)))

        for t, v, tv in zip(time_array, time_squared, true_time_squared):
            self.assertTrue(abs((v-tv)/tv) < 0.01,
                            'Observation time_squared at time {} is {} but should be {}'
                            .format(str(t), str(v), str(tv)))

        for t, v, tv in zip(time_array, output1, true_output1):
            self.assertTrue(abs((v-tv)/tv) < 0.01,
                            'Observation output1 at time {} is {} but should be {}'
                            .format(str(t), str(v), str(tv)))

        for v, tv in zip(file_data, true_file_data):
            self.assertTrue(abs((v-tv)/tv) < 0.01,
                            'Observation written to file is {} but should be {}'
                            .format(str(v), str(tv)))

    def test_coupled_reservoir_cemented_wellbore_forward(self):
        """Tests coupling of reservoir and cemented wellbore components.

        Tests the system model with a simple reservoir component coupled
        to a cemented wellbore component against 5 years of data.
        """
        # Create system model
        num_years = 5
        time_array = 365.25*np.arange(0.0, num_years+1)
        sm_model_kwargs = {'time_array': time_array}  # time is given in days
        sm = SystemModel(model_kwargs=sm_model_kwargs)

        # Add reservoir component
        sres = sm.add_component_model_object(SimpleReservoir(name='sres',
                                                             parent=sm,
                                                             locX=550.,
                                                             locY=100.))

        # Add parameters of reservoir component model
        sres.add_par('numberOfShaleLayers', value=3, vary=False)
        sres.add_par('shale1Thickness', min=500.0, max=550., value=525.0)
        sres.add_par('shale2Thickness', min=450.0, max=500., value=475.0)
        # Shale 3 has a fixed thickness of 11.2 m
        sres.add_par('shale3Thickness', value=11.2, vary=False)
        # Aquifer 1 (thief zone has a fixed thickness of 22.4)
        sres.add_par('aquifer1Thickness', value=22.4, vary=False)
        # Aquifer 2 (shallow aquifer) has a fixed thickness of 19.2
        sres.add_par('aquifer2Thickness', value=19.2, vary=False)
        # Reservoir has a fixed thickness of 51.2
        sres.add_par('reservoirThickness', value=51.2, vary=False)

        # Add observations of reservoir component model
        sres.add_obs('pressure')
        sres.add_obs('CO2saturation')
        sres.add_obs_to_be_linked('pressure')
        sres.add_obs_to_be_linked('CO2saturation')

        # Add cemented wellbore component
        cw = sm.add_component_model_object(CementedWellbore(name='cw', parent=sm))

        # Add parameters of cemented wellbore component
        cw.add_par('logWellPerm', min=-13.9, max=-12.0, value=-13.0)

        # Add keyword arguments of the cemented wellbore component model
        cw.add_kwarg_linked_to_obs('pressure', sres.linkobs['pressure'])
        cw.add_kwarg_linked_to_obs('CO2saturation', sres.linkobs['CO2saturation'])

        cw.add_composite_par('wellDepth', expr='sres.shale1Thickness' +
                             '+sres.shale2Thickness + sres.shale3Thickness' +
                             '+sres.aquifer1Thickness+ sres.aquifer2Thickness')
        cw.add_composite_par('depthRatio',
                             expr='(sres.shale2Thickness+sres.shale3Thickness' +
                             '+ sres.aquifer2Thickness + sres.aquifer1Thickness/2)/cw.wellDepth')
        cw.add_composite_par('initPressure',
                             expr='sres.datumPressure + cw.wellDepth*cw.g*sres.brineDensity')
        # 98/10 above replaces g constant of 9.8: constants can not have point in them

        # Add observations of the cemented wellbore component
        cw.add_obs('CO2_aquifer1')
        cw.add_obs('CO2_aquifer2')
        cw.add_obs('CO2_atm')
        cw.add_obs('brine_aquifer1')
        cw.add_obs('brine_aquifer2')
        sm.forward()

        # True values: simulation results for the last time point
        true_flowrates = [4.3860606501324e-05, -5.1476682991198892e-06,
                          7.076679337185885e-07, 1.1428047240968337e-05,
                          9.355562530558559e-07]

        # Test with helpful message!
        labels = ['CO2_aquifer1', 'CO2_aquifer2', 'CO2_atm', 'brine_aquifer1', 'brine_aquifer2']
        # Collect results as dictionary of lists for every observation in labels list
        results = dict()
        for label in labels:
            results[label] = sm.collect_observations_as_time_series(cw, label)

        for tf, l in zip(true_flowrates, labels):
            self.assertTrue(abs((tf-results[l][num_years-1])/tf) < 0.01,
                            'Flowrate at {} is {} but should be {}'
                            .format(l, str(results[l][num_years-1]), str(tf)))

    def test_coupled_reservoir_MSW_forward(self):
        """Tests coupling of reservoir and multisegmented wellbore components.

        Tests the system model with a simple reservoir component
        coupled with a multi-segmented wellbore component against 5 years of data.
        """
        num_years = 5
        time_array = 365.25*np.arange(0.0, num_years+1)
        sm_model_kwargs = {'time_array': time_array}  # time is given in days

        # Create system model
        sm = SystemModel(model_kwargs=sm_model_kwargs)

        # Add reservoir component
        sres = sm.add_component_model_object(SimpleReservoir(name='sres',
                                                             parent=sm,
                                                             locX=550.,
                                                             locY=100.))

        # Add parameters of reservoir component model
        sres.add_par('numberOfShaleLayers', value=3, vary=False)
        sres.add_par('shale1Thickness', min=30.0, max=150., value=45.0)

        # Add observations of reservoir component model
        sres.add_obs_to_be_linked('pressure')
        sres.add_obs_to_be_linked('CO2saturation')
        sres.add_obs('pressure')
        sres.add_obs('CO2saturation')

        # Add multisegmented wellbore component
        ms = sm.add_component_model_object(MultisegmentedWellbore(name='ms', parent=sm))
        ms.add_par('wellRadius', min=0.01, max=0.02, value=0.015)

        # Add linked parameters: common to both components
        ms.add_par_linked_to_par('numberOfShaleLayers',
                                 sres.deterministic_pars['numberOfShaleLayers'])
        ms.add_par_linked_to_par('shale1Thickness', sres.pars['shale1Thickness'])
        ms.add_par_linked_to_par('shale2Thickness',
                                 sres.default_pars['shaleThickness'])
        ms.add_par_linked_to_par('shale3Thickness',
                                 sres.default_pars['shaleThickness'])
        ms.add_par_linked_to_par('aquifer1Thickness',
                                 sres.default_pars['aquiferThickness'])
        ms.add_par_linked_to_par('aquifer2Thickness',
                                 sres.default_pars['aquiferThickness'])
        ms.add_par_linked_to_par('reservoirThickness',
                                 sres.default_pars['reservoirThickness'])
        ms.add_par_linked_to_par('datumPressure',
                                 sres.default_pars['datumPressure'])

        # Add keyword arguments linked to the output provided by reservoir model
        ms.add_kwarg_linked_to_obs('pressure', sres.linkobs['pressure'])
        ms.add_kwarg_linked_to_obs('CO2saturation', sres.linkobs['CO2saturation'])

        # Add observations of multisegmented wellbore component model
        ms.add_obs('CO2_aquifer1')
        ms.add_obs('CO2_aquifer2')
        ms.add_obs('CO2_atm')
        ms.add_obs('brine_aquifer1')
        ms.add_obs('brine_aquifer2')

        sm.forward()

        # True values
        true_flowrates = [8.2100655769942837e-06, 2.3657549565098642e-06,
                          8.6892074433330559e-07, 2.2016900868462108e-06,
                          1.2819111953169083e-08]

        # Test with helpful message!
        labels = ['CO2_aquifer1', 'CO2_aquifer2', 'CO2_atm', 'brine_aquifer1', 'brine_aquifer2']
        # Collect results as dictionary of lists for every observation in labels list
        results = dict()
        for label in labels:
            results[label] = sm.collect_observations_as_time_series(ms, label)

        for tf, l in zip(true_flowrates, labels):
            self.assertTrue((tf-results[l][num_years-1] == 0.0) or
                            abs((tf-results[l][num_years-1])/tf) < 0.01,
                            'Flowrate at {} is {} but should be {}'
                            .format(l, str(results[l][num_years-1]), str(tf)))

    def test_coupled_reservoir_open_carbonate_forward(self):
        """Tests coupling of reservoir, wellbore and aquifer components.

        Tests the system model with coupled simple reservoir, open wellbore,
        adapter and carbonate aquifer components against the expected input.
        """
        # Define keyword arguments of the system model
        num_years = 50
        time_array = 365*np.arange(0.0, num_years+1)
        sm_model_kwargs = {'time_array': time_array}  # time is given in days

        # Create system model
        sm = SystemModel(model_kwargs=sm_model_kwargs)

        # Add reservoir component
        sres = sm.add_component_model_object(SimpleReservoir(name='sres',
                                                             parent=sm,
                                                             locX=550.,
                                                             locY=100.))

        # Add parameters of reservoir component model
        sres.add_par('numberOfShaleLayers', value=3, vary=False)
        sres.add_par('shale1Thickness', min=900.0, max=1100., value=1000.0)
        sres.add_par('shale2Thickness', min=900.0, max=1100., value=1000.0)
        # Shale 3 has a fixed thickness of 11.2 m
        sres.add_par('shale3Thickness', value=11.2, vary=False)
        # Aquifer 1 (thief zone has a fixed thickness of 22.4)
        sres.add_par('aquifer1Thickness', value=22.4, vary=False)
        # Aquifer 2 (shallow aquifer) has a fixed thickness of 19.2
        sres.add_par('aquifer2Thickness', value=500, vary=False)
        # Reservoir has a fixed thickness of 51.2
        sres.add_par('reservoirThickness', value=51.2, vary=False)
        # sres.add_par('injRate', value=0.01, vary=False)
        # sres.add_par('logResPerm', value=-11.3, vary=False)

        # Add observations of reservoir component model
        sres.add_obs('pressure')
        sres.add_obs('CO2saturation')
        sres.add_obs_to_be_linked('pressure')
        sres.add_obs_to_be_linked('CO2saturation')

        # Add open wellbore component
        ow = sm.add_component_model_object(OpenWellbore(name='ow', parent=sm))

        # Add parameters of open wellbore component
        ow.add_par('wellRadius', min=0.001, max=0.002, value=0.0015)
        ow.add_par('logNormalizedTransmissivity', min=-1.0, max=1.0, value=0.0)
        ow.add_par('brineSalinity', value=0.2, vary=False)

        # Add keyword arguments of the open wellbore component model
        ow.add_kwarg_linked_to_obs('pressure', sres.linkobs['pressure'])
        ow.add_kwarg_linked_to_obs('CO2saturation', sres.linkobs['CO2saturation'])

        # Add composite parameter of open wellbore component
        ow.add_composite_par('reservoirDepth', expr='sres.shale1Thickness' +
                             '+sres.shale2Thickness + sres.shale3Thickness' +
                             '+sres.aquifer1Thickness+ sres.aquifer2Thickness')
        ow.add_composite_par('wellTop', expr='sres.shale3Thickness' +
                             '+sres.aquifer2Thickness')

        # Add observations of multisegmented wellbore component model
        ow.add_obs_to_be_linked('CO2_aquifer')
        ow.add_obs_to_be_linked('brine_aquifer')
        ow.add_obs_to_be_linked('brine_atm')
        ow.add_obs_to_be_linked('CO2_atm')
        ow.add_obs('brine_aquifer')
        ow.add_obs('CO2_atm')  # zero since well top is in aquifer
        ow.add_obs('CO2_aquifer')
        ow.add_obs('brine_atm')  # zero since well top is in aquifer

        # Add adapter that transforms leakage rates to accumulated mass
        adapt = sm.add_component_model_object(RateToMassAdapter(name='adapt', parent=sm))
        adapt.add_kwarg_linked_to_collection('CO2_aquifer',
                                             [ow.linkobs['CO2_aquifer'],
                                              ow.linkobs['CO2_atm']])
        adapt.add_kwarg_linked_to_collection('brine_aquifer',
                                             [ow.linkobs['brine_aquifer'],
                                              ow.linkobs['brine_atm']])
        adapt.add_obs_to_be_linked('mass_CO2_aquifer')
        adapt.add_obs_to_be_linked('mass_brine_aquifer')
        adapt.add_obs('mass_CO2_aquifer')
        adapt.add_obs('mass_brine_aquifer')

        # add carbonate aquifer model object and define parameters
        ca = sm.add_component_model_object(CarbonateAquifer(name='ca', parent=sm))
        ca.add_par('perm_var', min=0.017, max=1.89, value=0.9535)
        ca.add_par('corr_len', min=1.0, max=3.95, value=2.475)
        ca.add_par('aniso', min=1.1, max=49.1, value=25.1)
        ca.add_par('mean_perm', min=-13.8, max=-10.3, value=-12.05)
        ca.add_par('aqu_thick', min=100., max=500., value=300.)
        ca.add_par('hyd_grad', min=2.88e-4, max=1.89e-2, value=9.59e-3)
        ca.add_par('calcite_ssa', min=0, max=1.e-2, value=5.5e-03)
        ca.add_par('organic_carbon', min=0, max=1.e-2, value=5.5e-03)
        ca.add_par('benzene_kd', min=1.49, max=1.73, value=1.61)
        ca.add_par('benzene_decay', min=0.15, max=2.84, value=1.5)
        ca.add_par('nap_kd', min=2.78, max=3.18, value=2.98)
        ca.add_par('nap_decay', min=-0.85, max=2.04, value=0.595)
        ca.add_par('phenol_kd', min=1.21, max=1.48, value=1.35)
        ca.add_par('phenol_decay', min=-1.22, max=2.06, value=0.42)
        ca.add_par('cl', min=0.1, max=6.025, value=0.776)
        ca.model_kwargs['x'] = [100.]
        ca.model_kwargs['y'] = [100.]

        CO2_rate_obs_list = []
        brine_rate_obs_list = []
        CO2_mass_obs_list = []
        brine_mass_obs_list = []
        CO2_rate_obs_list.append(ow.linkobs['CO2_aquifer'])
        brine_rate_obs_list.append(ow.linkobs['brine_aquifer'])
        CO2_mass_obs_list.append(adapt.linkobs['mass_CO2_aquifer'])
        brine_mass_obs_list.append(adapt.linkobs['mass_brine_aquifer'])

        # Add aquifer component's keyword argument co2_rate linked to the collection created above
        ca.add_kwarg_linked_to_collection('co2_rate', CO2_rate_obs_list)
        ca.add_kwarg_linked_to_collection('brine_rate', brine_rate_obs_list)
        ca.add_kwarg_linked_to_collection('co2_mass', CO2_mass_obs_list)
        ca.add_kwarg_linked_to_collection('brine_mass', brine_mass_obs_list)

        # Add observations (output) from the carbonate aquifer model
        ca.add_obs('pH')
        ca.add_obs('TDS')

        # Run system model using current values of its parameters
        sm.forward()  # system model is run deterministically

        tds = sm.collect_observations_as_time_series(ca, 'TDS')
        ph = sm.collect_observations_as_time_series(ca, 'pH')

        # True values
        true_tds = [0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,
            0.00000000e+00,0.00000000e+00,2.02576714e+06,4.56335060e+06,
            7.19804293e+06,1.43097279e+07,2.18764456e+07,2.96410187e+07,
            3.75882133e+07,4.57048144e+07,5.39792155e+07,6.24011162e+07,
            7.09612938e+07,7.82245263e+07,8.47947095e+07,9.19424573e+07,
            9.90995092e+07,1.06265554e+08,1.13440304e+08,1.20623494e+08,
            1.27770740e+08,1.34856447e+08,1.41949831e+08,1.49050693e+08,
            1.56158847e+08,1.63274117e+08,1.70396340e+08,1.77525358e+08,
            1.84661026e+08,1.91803203e+08,1.98951759e+08,2.06106566e+08,
            2.13267505e+08,2.20434463e+08,2.27619410e+08,2.34822823e+08,
            2.42029839e+08,2.49240391e+08,2.56454412e+08,2.63671838e+08,
            2.72083105e+08,2.82650821e+08,2.93224475e+08,3.03803974e+08,
            3.14389226e+08,3.24980146e+08,3.35576650e+08]
        true_ph = [0.00000000e+00,0.00000000e+00,0.00000000e+00,0.00000000e+00,
            576396.31185043,1421398.01288473,2460037.02636372,3633305.69708463,
            4871388.72121241,6169323.60273244,7523117.40434046,8929468.93006913,
            10385590.49082986,11889087.74127265,13437875.3936294,15030116.15105348,
            16664175.2543894,18338585.86968181,20055351.35180624,21841949.5893804,
            23632633.19692566,25427263.1183387,27225712.52759339,29027865.19466632,
            30833614.13605278,32642860.48990111,34455512.57042897,36271485.06689478,
            38090698.36022557,39913077.9362158,41738553.87863263,43567060.42890903,
            45398535.6017084,47232920.84766296,49070160.75617152,50910202.79240677,
            52752997.06368133,54598496.11113483,56447329.73485261,58299108.59585369,
            60152561.22097182,62007657.45129208,63864368.49490508,65722666.83168101,
            67582526.12684648,69443921.15235455,71306827.71518588,73171222.59182495,
            75037083.46825324,76904388.88488983,78962088.48592487]

        # Suppress Numpy FutureWarning
        warnings.simplefilter(action='ignore', category=FutureWarning)

        # Test with helpful message!
        for ts, s, t in zip(true_tds, tds, time_array):
            self.assertTrue((ts-s == 0.0) or abs((ts-s)/ts) < 0.01,
                            'TDS at time {} is {} but should be {}'.format(str(t), str(s), str(ts)))
        for tp, p, t in zip(true_ph, ph, time_array):
            self.assertTrue((tp-p == 0.0) or abs((tp-p)/tp) < 0.01,
                            'pH at time {} is {} but should be {}'.format(str(t), str(p), str(tp)))

    def test_coupled_reservoir_open_wellbore_forward(self):
        """Tests coupling of reservoir and open wellbore components.

        Tests the system model with a simple reservoir component coupled
        to an open wellbore component against 5 years of data.
        """
        # Create system model
        num_years = 5
        time_array = 365.25*np.arange(0.0, num_years+1)
        sm_model_kwargs = {'time_array': time_array}  # time is given in days
        sm = SystemModel(model_kwargs=sm_model_kwargs)

        # Add reservoir component
        sres = sm.add_component_model_object(SimpleReservoir(name='sres',
                                                             parent=sm,
                                                             locX=550.,
                                                             locY=100.))

        # Add parameters of reservoir component model
        sres.add_par('numberOfShaleLayers', value=3, vary=False)
        sres.add_par('shale1Thickness', min=500.0, max=550., value=525.0)
        sres.add_par('shale2Thickness', min=510.0, max=550., value=525.0)
        # Shale 3 has a fixed thickness of 11.2 m
        sres.add_par('shale3Thickness', min=1.0, max=2.0, value=1.5)
        # Aquifer 1 (thief zone has a fixed thickness of 22.4)
        sres.add_par('aquifer1Thickness', value=1.0, vary=False)
        # Aquifer 2 (shallow aquifer) has a fixed thickness of 19.2
        sres.add_par('aquifer2Thickness', value=200.0, vary=False)
        # Reservoir has a fixed thickness of 51.2
        sres.add_par('reservoirThickness', value=51.2, vary=False)

        # Add observations of reservoir component model
        sres.add_obs('pressure')
        sres.add_obs('CO2saturation')
        sres.add_obs_to_be_linked('pressure')
        sres.add_obs_to_be_linked('CO2saturation')

        # Add open wellbore component
        ow = sm.add_component_model_object(OpenWellbore(name='ow', parent=sm))

        # Add parameters of open wellbore component
        ow.add_par('logNormalizedTransmissivity', value=-1.0, vary=False)
        ow.add_par('brineSalinity', value=0.0, vary=False)

        # Add keyword arguments of the open wellbore component model
        ow.add_kwarg_linked_to_obs('pressure', sres.linkobs['pressure'])
        ow.add_kwarg_linked_to_obs('CO2saturation', sres.linkobs['CO2saturation'])

        # Add composite parameter of open wellbore component
        ow.add_composite_par('reservoirDepth', expr='sres.shale1Thickness' +
                             '+sres.shale2Thickness + sres.shale3Thickness' +
                             '+sres.aquifer1Thickness+ sres.aquifer2Thickness')
        ow.add_composite_par('wellTop', expr='sres.shale1Thickness' +
                             '+ sres.aquifer2Thickness')

        # Add observations of the open wellbore component
        ow.add_obs('CO2_aquifer')
        ow.add_obs('brine_aquifer')

        sm.forward()

        # True values: simulation results for the last time point
        true_flowrates = [4.27764347, 11.96914902]

        # Test with helpful message!
        labels = ['CO2_aquifer', 'brine_aquifer']
        for tf, l in zip(true_flowrates, labels):
            # The observations at the last time point can be accessed directly,
            # e.g. as ow.obs['CO2_aquifer_5'].sim (ow.obs[l+'_'+str(num_years)].sim) or
            # as sm.collect_observations_as_time_series(ow,'CO2_aquifer')[num_years]
            self.assertTrue(abs((tf-ow.obs[l+'_'+str(num_years)].sim)/tf) < 0.01,
                            'Flowrate at {} is {} but should be {}'
                            .format(l, str(ow.obs[l+'_'+str(num_years)].sim), str(tf)))

    def test_deep_alluvium_aquifer(self):
        """Tests basic deep alluvum aquifer.

        Tests a simple deep alluvium aquifer component in a
        forward model against expected output for 2 years of data.
        """

        time_array = 365.25*np.arange(0.0,2.0)
        sm_model_kwargs = {'time_array': time_array} # time is given in days
        sm = SystemModel(model_kwargs=sm_model_kwargs)
    
    
        # Add carbonate aquifer model object and define parameters
        daa = sm.add_component_model_object(DeepAlluviumAquifer(name='daa',parent=sm))
    
        daa.add_par('logK_sand1', value=-11.92098495)
        daa.add_par('logK_sand2', value=-11.7198002)
        daa.add_par('logK_sand3', value=-11.70137252)
        daa.add_par('logK_caprock', value=-15.69758676)
        daa.add_par('correlationLengthX', value=1098.994284)
        daa.add_par('correlationLengthZ', value=79.8062668)
        daa.add_par('sandFraction', value=0.800121364)
        daa.add_par('groundwater_gradient', value=0.001333374)
        daa.add_par('leak_depth', value=885.5060281)

        daa.model_kwargs['brine_rate'] = 3.21903E-05 # kg/s
        daa.model_kwargs['brine_mass'] = 10**4.71081307 # kg
        daa.model_kwargs['co2_rate'] = 0.060985038 # kg/s
        daa.model_kwargs['co2_mass'] = 10**6.737803184 # kg

        # Add observations (output) from the alluvium aquifer model
        daa.add_obs('TDS_volume')
        daa.add_obs('Pressure_dx')
        daa.add_obs('pH_dy')

        # Run the system model
        sm.forward()

        # Assign observations of the model to variables
        tds = sm.collect_observations_as_time_series(daa, 'TDS_volume')
        pressdx = sm.collect_observations_as_time_series(daa, 'Pressure_dx')
        phdy = sm.collect_observations_as_time_series(daa, 'pH_dy')

        # True values
        true_tds = [0.0, 2436214.13519926]
        true_press = [0.0, 176.71076959]
        true_pH = [0.0, 88.08772333]
        
        # Test with helpful message!
        for ts, s, t in zip(true_tds, tds, time_array):
            self.assertTrue(abs((ts-s)) < 1.0,
                            'TDS volume at time {} is {} but should be {}'
                            .format(str(t), str(s), str(ts)))

        for ts, s, t in zip(true_press, pressdx, time_array):
            self.assertTrue(abs((ts-s)) < 0.01,
                            'TDS volume at time {} is {} but should be {}'
                            .format(str(t), str(s), str(ts)))
        for ts, s, t in zip(true_pH, phdy, time_array):
            self.assertTrue(abs((ts-s)) < 0.01,
                            'TDS volume at time {} is {} but should be {}'
                            .format(str(t), str(s), str(ts)))
        return

    def test_lhs(self):
        """Tests Latin Hypercube sampling method of system model class.

        Tests the system model latin hypercube sampling with a simple reservoir
        component coupled to a cemented wellbore component. Tests
        the simulated mean and standard deviation against expected values.
        """
        # Create system model
        sm = SystemModel()

        # Add reservoir component
        sres = sm.add_component_model_object(SimpleReservoir(name='sres',
                                                             parent=sm,
                                                             locX=550.,
                                                             locY=100.))

        # Add parameters of reservoir component model
        sres.add_par('numberOfShaleLayers', value=3, vary=False)
        sres.add_par('shale1Thickness', value=525.0, vary=False)
        sres.add_par('shale2Thickness', value=475.0, vary=False)
        # Shale 3 has a fixed thickness of 11.2 m
        sres.add_par('shale3Thickness', value=11.2, vary=False)
        # Aquifer 1 (thief zone has a fixed thickness of 22.4)
        sres.add_par('aquifer1Thickness', value=22.4, vary=False)
        # Aquifer 2 (shallow aquifer) has a fixed thickness of 19.2
        sres.add_par('aquifer2Thickness', value=19.2, vary=False)
        # Reservoir has a fixed thickness of 51.2
        sres.add_par('reservoirThickness', value=51.2, vary=False)

        # Add observations of reservoir component model
        sres.add_obs_to_be_linked('pressure')
        sres.add_obs_to_be_linked('CO2saturation')

        # Add cemented wellbore component
        cw = sm.add_component_model_object(CementedWellbore(name='cw', parent=sm))

        # Add parameters of cemented wellbore component
        cw.add_par('logWellPerm', min=-13.9, max=-11., value=-12.8)
        cw.add_par('logThiefPerm', min=-13.9, max=-12.1, value=-12.2)

        # Add keyword arguments of the cemented wellbore component model
        cw.add_kwarg_linked_to_obs('pressure', sres.linkobs['pressure'])
        cw.add_kwarg_linked_to_obs('CO2saturation', sres.linkobs['CO2saturation'])

        cw.add_composite_par('wellDepth', expr='sres.shale1Thickness' +
                             '+sres.shale2Thickness + sres.shale3Thickness' +
                             '+sres.aquifer1Thickness+ sres.aquifer2Thickness')
        cw.add_composite_par('depthRatio',
                             expr='(sres.shale2Thickness+sres.shale3Thickness' +
                             '+ sres.aquifer2Thickness + sres.aquifer1Thickness/2)/cw.wellDepth')
        cw.add_composite_par('initPressure',
                             expr='sres.datumPressure + cw.wellDepth*cw.g*sres.brineDensity')

        cw.add_obs('CO2_aquifer1')
        cw.add_obs('CO2_aquifer2')
        cw.add_obs('CO2_atm')
        cw.add_obs('brine_aquifer1')
        cw.add_obs('brine_aquifer2')

        # Decided to fix seed: just to make sampling more determined
        s = sm.lhs(siz=1000, seed=465)
        s.run(verbose=False)

        # True values of means and standard deviations obtained by averaging
        # over 100000 simulations
        true_means = [2.33367410e-04,   5.00779064e-06,   1.17394693e-07,
                      8.95726881e-05,   9.70133545e-07]
        true_stds = [2.69470787e-04,   6.13084605e-05,   1.09320690e-06,
                     2.06751467e-04,   1.97299843e-07]

        # Add tolerance appropriate for each observation
        # Tolerance calculations are based on probability theory:
        # true_st_dev/sampleSize^0.5/true_means
        tols = [0.03651502,  0.38714553,  0.29447871,  0.07299162,  0.00643125]

        # Test with helpful message!
        labels = s.recarray.dtype.names
        sim_means = np.mean(s.recarray[list(labels[2:])].tolist(), axis=0)
        sim_stds = np.std(s.recarray[list(labels[2:])].tolist(), axis=0)
        for l, tm, sim, tol in zip(labels[2:], true_means, sim_means, tols):
            self.assertTrue(abs((tm-sim)/tm) < tol,
                            'Mean for {} is {} but should be {}'.format(l, str(sim), str(tm)))
        for l, ts, ss in zip(labels[2:], true_stds, sim_stds):
            self.assertTrue(abs((ts-ss)/ts) < 0.05,
                            'Standard deviation for {} is {} but should be {}'.format(l, str(ss), str(ts)))

        if not os.path.exists('test_output'):
            os.mkdir('test_output')
        base_outfile = os.path.join('test_output', 'sens_test_{n}.{fmt}')
        # Run RBD_fast sensitivity analysis on CO2 leaked into aquifer
        CO2_aq1 = s.rbd_fast(obsname='cw.CO2_aquifer1', print_to_console=False)
        CO2_aq1_ssens = iam_vis.simple_sensitivities_barplot(CO2_aq1,
                                                             sm,
                                                             savefig=base_outfile.format(n=1, fmt='.png'),
                                                             outfile=base_outfile.format(n=1, fmt='.txt'))
        true_CO2aq1_logWell = 0.96
        self.assertTrue(os.path.exists(base_outfile.format(n=1, fmt='.png')),
                        'Output image not created')
        self.assertTrue(os.path.exists(base_outfile.format(n=1, fmt='.txt')),
                        'Output data file not created')
        self.assertTrue(abs(true_CO2aq1_logWell - CO2_aq1_ssens[0]) < 0.01,
                        'CO2_aq1-logWellPerm is {ov} but should be {tv}'
                        .format(ov=CO2_aq1_ssens[0], tv=true_CO2aq1_logWell))

        ms = iam_vis.multi_sensitivities_barplot(['cw.CO2_aquifer1',
                                                  'cw.brine_aquifer1'],
                                                 sm, s,
                                                 savefig=base_outfile.format(n=2, fmt='.png'),
                                                 outfile=base_outfile.format(n=2, fmt='.csv'))
        true_brine_aq1_logWell = 0.99

        self.assertTrue(os.path.exists(base_outfile.format(n=2, fmt='.png')),
                        'Output image not created')
        self.assertTrue(os.path.exists(base_outfile.format(n=2, fmt='.csv')),
                        'Output data file not created')
        ov = ms['cw.CO2_aquifer1'][0]
        self.assertTrue(abs(true_CO2aq1_logWell - ov) < 0.01,
                        'CO2_aq1-logWellPerm is {ov} but should be {tv}'
                        .format(ov=ov, tv=true_CO2aq1_logWell))
        ov = ms['cw.brine_aquifer1'][0]
        self.assertTrue(abs(true_brine_aq1_logWell - ov) < 0.01,
                        'brine_aq1-logWellPerm is {ov} but should be {tv}'
                        .format(ov=ov, tv=true_brine_aq1_logWell))

    def test_lookup_table_reservoir(self):
        """
        Tests lookup table reservoir component.

        Tests the system model with a lookup table reservoir component
        linked to two interpolators in a forward model against the expected
        output for 6 years of data.
        """

        # Define keyword arguments of the system model
        time_array = 365.25*np.linspace(0.0, 50.0, 6)
        sm_model_kwargs = {'time_array': time_array}  # time is given in days

        # Create system model
        sm = SystemModel(model_kwargs=sm_model_kwargs)

        # Read file with signatures of interpolators and names of files with the corresponding data
        signature_data = np.genfromtxt(os.path.join('..', 'source', 'components', 'reservoir',
                                                    'lookuptables', 'Test_2_sims',
                                                    'parameters_and_filenames.csv'),
                                       delimiter=",", dtype='str')

        # The first row (except the last element) of the file contains names of the parameters
        par_names = signature_data[0, 0:-1]
        num_pars = len(par_names)

        # Create and add two interpolators to the system model
        for ind in range(2):
            signature = {par_names[j]: float(signature_data[ind+1, j]) for j in range(num_pars)}

            sm.add_interpolator(ReservoirDataInterpolator(name='int'+str(ind+1), parent=sm,
                                header_file_dir=os.path.join('..', 'source', 'components', 'reservoir',
                                                             'lookuptables', 'Test_2_sims'),
                                time_file='time_points.csv',
                                data_file='Reservoir_data_sim0'+str(ind+1)+'.csv',
                                signature=signature),
                                intr_family='reservoir')

        # Add reservoir component
        ltres = sm.add_component_model_object(LookupTableReservoir(name='ltres',
                                                                   parent=sm,
                                                                   intr_family='reservoir',
                                                                   locX=37478.0,
                                                                   locY=48233.0))

        # Add parameters of reservoir component model
        for j in range(num_pars):
            # add first line with values from signature_file
            ltres.add_par(par_names[j], value=float(signature_data[1, j]), vary=False)

        # Add observations of reservoir component model
        ltres.add_obs('pressure')
        ltres.add_obs('CO2saturation')

        # Run system model using current values of its parameters
        sm.forward()

        # Collect pressure and saturation observations
        pressure = sm.collect_observations_as_time_series(ltres, 'pressure')[:] / 1.0e+6
        saturation = sm.collect_observations_as_time_series(ltres, 'CO2saturation')[:]

        true_pressure = [27.11992857, 33.16685714, 34.10442857, 34.16092857,
                         34.08864286, 33.98235714]
        true_saturation = [0., 0.34310857, 0.45430571, 0.47404286,
                           0.48760786, 0.50069071]

        # Test with helpful message!
        for tp, p, t in zip(true_pressure, pressure, time_array):
            self.assertTrue(abs((tp-p)/tp) < 0.01,
                            'Pressure at time {} is {} MPa but should be {} MPa'
                            .format(str(t), str(p), str(tp)))
        for ts, s, t in zip(true_saturation[1:], saturation[1:], time_array[1:]):
            self.assertTrue(abs((ts-s)/ts) < 0.01,
                            'Saturation at time {} is {} but should be {}'
                            .format(str(t), str(s), str(ts)))

    def test_openiam_cf(self):
        """Tests Control File functionality of the OpenIAM.

        Tests the OpenIAM run from a control file with a simple reservoir
        component in a forward model.
        """
        import pickle
        from openiam.openiam_cf import main as openiam_cf

        # Change directories to find input and output files
        current_work_dir = os.getcwd()
        test_dir = os.path.dirname(os.path.abspath(__file__))
        os.chdir(test_dir)

        outfiles = ['SimpleReservoir1_000.pressure.txt', 'SimpleReservoir1_000.CO2saturation.txt']

        for ofile in outfiles:
            odfile = os.path.join('test_output', ofile)
            if os.path.exists(odfile):
                os.remove(odfile)

        # Run Test Control File
        test_control_file = 'test_control_file.yaml'
        self.assertTrue(openiam_cf(test_control_file),
                        'OpenIAM failed to run properly')

        # Test output directory was created
        self.assertTrue(os.path.exists('test_output'),
                        'Output directory not created')
        # Read yaml_dump file
        with open(os.sep.join(['test_output', 'output_dump.pkl']), 'rb') as yaml_out:
            output = pickle.load(yaml_out)

        # Read output pressure and saturation files
        out_vals = {}
        out_vals['pressure'] = np.loadtxt(os.sep.join(['test_output', 'SimpleReservoir1_000.pressure.txt']))
        out_vals['saturation'] = np.loadtxt(os.sep.join(['test_output', 'SimpleReservoir1_000.CO2saturation.txt']))
        out_vals['co2aq1'] = np.loadtxt(os.sep.join(['test_output', 'CementedWellbore1_000.CO2_aquifer1.txt']))
        out_vals['co2aq2'] = np.loadtxt(os.sep.join(['test_output', 'CementedWellbore1_000.CO2_aquifer2.txt']))
        out_vals['co2atm'] = np.loadtxt(os.sep.join(['test_output', 'CementedWellbore1_000.CO2_atm.txt']))
        out_vals['brine_aq1'] = np.loadtxt(os.sep.join(['test_output', 'CementedWellbore1_000.brine_aquifer1.txt']))
        out_vals['brine_aq2'] = np.loadtxt(os.sep.join(['test_output', 'CementedWellbore1_000.brine_aquifer2.txt']))
        out_vals['TDS'] = np.loadtxt(os.sep.join(['test_output', 'CarbonateAquifer1.TDS.txt']))
        out_vals['msw_co2'] = np.loadtxt(os.sep.join(['test_output', 'msw1_000.CO2_aquifer1.txt']))
        out_vals['msw_brine'] = np.loadtxt(os.sep.join(['test_output', 'msw1_000.brine_aquifer1.txt']))
        out_vals['crit_dist'] = np.loadtxt(os.sep.join(['test_output', 'atmRom1.critical_distance_s000.txt']))

        # Change back to original working directory
        os.chdir(current_work_dir)

        # Test model parameters set correctly
        modelparams = output['ModelParams']
        self.assertTrue(modelparams['EndTime'] == 5,
                        'EndTime not loaded correctly')
        self.assertTrue(modelparams['Analysis_type'] == 'forward',
                        'Analysis_type not loaded correctly')
        self.assertTrue(modelparams['TimeStep'] == 1.0,
                        'TimeStep not loaded correctly')

        # Test Stratigraphy parameters set correctly
        true_sr_params = {'aquifer1Thickness': {'value': 142.4, 'vary': False},
                          'aquifer2Thickness': {'value': 19.2, 'vary': False},
                          'numberOfShaleLayers': {'value': 3, 'vary': False},
                          'reservoirThickness': {'value': 51.2, 'vary': False},
                          'shale1Thickness': {'max': 550.0, 'min': 400.0, 'value': 405.0},
                          'shale2Thickness': {'max': 500.0, 'min': 450.0, 'value': 475.0},
                          'shale3Thickness': {'value': 11.2, 'vary': False},
                          'datumPressure': {'value': 101325.0, 'vary': False}}
        stratparams = output['Stratigraphy']
        for key, value in true_sr_params.items():
            self.assertTrue(stratparams[key] == value,
                            '{key} not loaded into Stratigraphy properly'.format(key=key))

        # Test pressure and saturation values
        true_vals = {}
        true_vals['pressure'] = [10418765., 14919301.38843215, 15192401.37235173,
                                 15352154.92651543, 15465501.96157327, 15553420.7877774]
        true_vals['saturation'] = [0.,  0.21384356,  0.30898839,  0.38199558,  0.44354349,
                                   0.49776829]
        true_vals['co2aq1'] = [0.000000000000000000e+00, 4.009259450441665808e-05, 3.328369138093752655e-05,
                               2.693611103739417147e-05, 2.107207114594151191e-05, 1.560442836007955292e-05]
        true_vals['co2aq2'] = [0.000000000000000000e+00, 1.858034134482971605e-06, 7.995150506733141689e-06,
                               1.277841678447248961e-05, 1.684263101312697962e-05, 2.044104130297448487e-05]
        true_vals['co2atm'] = [0.000000000000000000e+00, -1.030710967084584508e-08, -2.143717012310900791e-07,
                               1.353328298553025458e-06, 2.674960534125898739e-06, 3.268333388857705667e-06]
        true_vals['brine_aq1'] = [0.000000000000000000e+00, 1.174587248555037712e-05, 1.181246277069288336e-05,
                                  1.185186206817051792e-05, 1.187983716205768980e-05, 1.190154380165251380e-05]
        true_vals['brine_aq2'] = [0.000000000000000000e+00, 1.029048012603127806e-06, 1.076541008069456788e-06,
                                  1.104337101947985265e-06, 1.124060054107538356e-06, 1.139358847736474137e-06]
        true_vals['TDS'] = [0.000000000000000000e+00, 4.953953824156254530e+06, 4.963694820895016193e+06,
                            4.969981809714376926e+06, 4.973163490473896265e+06, 4.973523676882386208e+06]
        true_vals['msw_co2'] = [0.000000000000000000e+00, 1.685779384170747642e-06, 2.332231253976781943e-06,
                                2.439835615310001397e-06, 2.501344676892450799e-06, 2.555921287545392011e-06]
        true_vals['msw_brine'] = [0.000000000000000000e+00, 7.625411632876481455e-06, 7.970275967325878469e-06,
                                  7.988670590201842547e-06, 7.958216246107291283e-06, 7.914684507949380389e-06]
        true_vals['crit_dist'] = [1.887653039503896366e+02, 1.887653039503896366e+02, 1.979787210106966882e+02,
                                  2.311893379099141441e+02, 2.311893379099141441e+02, 2.311893379099141441e+02]

        for key in true_vals:
            for tv, ov in zip(true_vals[key], out_vals[key]):
                self.assertTrue(abs(tv - ov) < 0.01,
                                '{key} is {ov} but should be {tv}'.format(key=key, ov=ov, tv=tv))

        return

    def test_parstudy(self):
        """Test parameter study method of system model class.

        Tests the system model parameter study method with a simple reservoir
        component coupled to a cemented wellbore component.
        """
        # Create system model
        sm = SystemModel()

        # Add reservoir component
        sres = sm.add_component_model_object(SimpleReservoir(name='sres',
                                                             parent=sm,
                                                             locX=550.,
                                                             locY=100.))

        # Add parameters of reservoir component model
        sres.add_par('numberOfShaleLayers', value=3, vary=False)
        sres.add_par('shale1Thickness', value=525.0, vary=False)
        sres.add_par('shale2Thickness', value=475.0, vary=False)
        # Shale 3 has a fixed thickness of 11.2 m
        sres.add_par('shale3Thickness', value=11.2, vary=False)
        # Aquifer 1 (thief zone has a fixed thickness of 22.4)
        sres.add_par('aquifer1Thickness', value=22.4, vary=False)
        # Aquifer 2 (shallow aquifer) has a fixed thickness of 19.2
        sres.add_par('aquifer2Thickness', value=19.2, vary=False)
        # Reservoir has a fixed thickness of 51.2
        sres.add_par('reservoirThickness', value=51.2, vary=False)

        # Add observations of reservoir component model
        sres.add_obs_to_be_linked('pressure')
        sres.add_obs_to_be_linked('CO2saturation')

        # Add cemented wellbore component
        cw = sm.add_component_model_object(CementedWellbore(name='cw', parent=sm))

        # Add parameters of cemented wellbore component
        cw.add_par('logWellPerm', min=-13.9, max=-11., value=-12.8)
        cw.add_par('logThiefPerm', min=-13.9, max=-12.1, value=-12.2)

        # Add keyword arguments of the cemented wellbore component model
        cw.add_kwarg_linked_to_obs('pressure', sres.linkobs['pressure'])
        cw.add_kwarg_linked_to_obs('CO2saturation', sres.linkobs['CO2saturation'])

        cw.add_composite_par('wellDepth', expr='sres.shale1Thickness' +
                             '+sres.shale2Thickness + sres.shale3Thickness' +
                             '+sres.aquifer1Thickness+ sres.aquifer2Thickness')
        cw.add_composite_par('depthRatio',
                             expr='(sres.shale2Thickness+sres.shale3Thickness' +
                             '+ sres.aquifer2Thickness + sres.aquifer1Thickness/2)/cw.wellDepth')
        cw.add_composite_par('initPressure',
                             expr='sres.datumPressure + cw.wellDepth*cw.g*sres.brineDensity')

        cw.add_obs('CO2_aquifer1')
        cw.add_obs('CO2_aquifer2')
        cw.add_obs('CO2_atm')
        cw.add_obs('brine_aquifer1')
        cw.add_obs('brine_aquifer2')
        s = sm.parstudy(nvals=3)
        s.run(verbose=False)

        # True values
        truth = np.array([[-1.39000000e+01, -1.39000000e+01, -1.33468571e-05,
                           4.29074294e-06, -7.43479684e-07, 6.70347962e-06, 6.28400179e-07],
                          [ -1.39000000e+01, -1.30000000e+01, -1.33468571e-05,
                           4.29074294e-06, -7.43479684e-07, 6.70347962e-06, 6.28400179e-07],
                          [ -1.39000000e+01, -1.21000000e+01, -1.33468571e-05,
                           4.29074294e-06, -7.43479684e-07, 6.70347962e-06, 6.28400179e-07],
                          [ -1.24500000e+01, -1.39000000e+01, 6.06820888e-05,
                           -5.20568475e-05, 2.18681429e-06, 1.24656269e-05, 9.70133534e-07],
                          [ -1.24500000e+01, -1.30000000e+01, 4.88898887e-06,
                           -5.20568475e-05, 2.18681429e-06, 1.24656269e-05, 9.70133534e-07],
                          [ -1.24500000e+01, -1.21000000e+01, 4.72184654e-05,
                           -5.20568475e-05, 2.18681429e-06, 1.24656269e-05, 9.70133534e-07],
                          [ -1.10000000e+01, -1.39000000e+01, 1.08186778e-03,
                           1.87104728e-04, 1.68948606e-07, 1.03203707e-03, 1.31186689e-06],
                          [ -1.10000000e+01, -1.30000000e+01, 8.12772437e-04,
                           1.87104728e-04, 1.68948606e-07, 1.03203707e-03, 1.31186689e-06],
                          [ -1.10000000e+01, -1.21000000e+01, 1.01693146e-03,
                           1.87104728e-04, 1.68948606e-07, 1.03203707e-03, 1.31186689e-06]])

        # Test with helpful message!
        labels = s.recarray.dtype.names
        # Test that parameter values are exactly correct
        for i, l in enumerate(labels[:2]):
            for j, t, sim in zip(list(range(truth.shape[0])), truth[:, i], s.recarray[l]):
                self.assertEqual(t, sim, 'Parameter {} for sample {} is {} but should be {}'.format(l, j+1, str(sim), str(t)))
        # Test simulated values are within tolerance
        for i, l in enumerate(labels[2:]):
            for j, t, sim in zip(list(range(truth.shape[0])), truth[:, i+2], s.recarray[l]):
                self.assertTrue(abs((t-sim)/t) < 0.01,
                                '{} for sample {} is {} but should be {}'
                                .format(l, j+1, str(sim), str(t)))

    def test_rate_to_mass_adapter(self):
        """Tests adapter component.

        Tests system model containing only an adapter component present
        with fixed in time input parameters against expected output.
        """
        # Create system model
        num_years = 5
        time_array = 365.25*np.arange(0.0, num_years+1)
        sm_model_kwargs = {'time_array': time_array}  # time is given in days
        sm = SystemModel(model_kwargs=sm_model_kwargs)

        # Add adapter component
        adapt = sm.add_component_model_object(RateToMassAdapter(name='adapt', parent=sm))
        adapt.model_kwargs['CO2_aquifer1'] = [2.0e-6, 4.0e-7]
        adapt.model_kwargs['CO2_aquifer2'] = [4.0e-7, 1.0e-8]
        adapt.add_obs('mass_CO2_aquifer1')
        adapt.add_obs('mass_CO2_aquifer2')
        sm.forward()
        true_masses = [302.95296, 73.844784]

        # Test with helpful message!
        labels = ['mass_CO2_aquifer1', 'mass_CO2_aquifer2']
        for tf, l in zip(true_masses, labels):
            # The observations at the last time point can be accessed directly,
            # e.g. as adapt.obs['mass_CO2_aquifer1_5'].sim;
            # general form: adapt.obs[l+'_'+str(num_years)].sim.
            self.assertTrue(abs((tf-adapt.obs[l+'_'+str(num_years)].sim)/tf) < 0.01,
                            'Flowrate at {} is {} but should be {}'
                            .format(l, str(adapt.obs[l+'_'+str(num_years)].sim), str(tf)))

    def test_reservoir_data_interpolator(self):
        """Tests reservoir data interpolator.

        Tests reservoir data interpolator for a sample data set
        against expected output for 6 years of data.
        """
        # Create system model
        sm = SystemModel()

        # Create interpolator
        int1 = sm.add_interpolator(ReservoirDataInterpolator(name='int1', parent=sm,
                                   header_file_dir=os.path.join('..', 'source', 'components', 'reservoir',
                                                                'lookuptables', 'Test_2_sims'),
                                   time_file='time_points.csv',
                                   data_file='Reservoir_data_sim01.csv',
                                   signature={'logResPerm': -13.2773, 'reservoirPorosity': 0.2145, 'logShalePerm': -18.7}),
                                   intr_family='reservoir')

        # Setup location of interest
        locX, locY = [37478.0, 48233.0]

        from openiam.grid import interp_weights
        # Calculate weights of the location of interest
        vertices, weights = interp_weights(int1.triangulation, np.array([[locX, locY]]))

        # Calculate pressure and saturation for several time points
        time_array = 365.25*np.linspace(0, 50, 6)
        pressure = []
        saturation = []

        for t in time_array:
            output = int1(t, vertices, weights)
            pressure.append(output['pressure'][0]/1.0e+6)
            saturation.append(output['CO2saturation'][0])

        # True values
        true_pressure = [27.119928571428566, 33.16685714285714,
                         34.104428571428571, 34.160928571428578,
                         34.088642857142858, 33.98235714285714]
        true_saturation = [0.0, 0.34310857142857143, 0.45430571428571431,
                           0.4740428571428571, 0.4876078571428572,
                           0.50069071428571432]

        # Test with helpful message!
        for tp, p, t in zip(true_pressure, pressure, time_array):
            self.assertTrue(abs((tp-p)/tp) < 0.01,
                            'Pressure at time {} is {} MPa but should be {} MPa'
                            .format(str(t/365.25), str(p), str(tp)))
        # To avoid comparing zero saturations we start with the second element
        for ts, s, t in zip(true_saturation[1:], saturation[1:], time_array[1:]):
            self.assertTrue(abs((ts-s)/ts) < 0.01,
                            'Saturation at time {} is {} but should be {}'
                            .format(str(t/365.25), str(s), str(ts)))

    def test_simple_reservoir_forward(self):
        """Tests simple reservoir component.

        Tests the system model with a simple reservoir component in a
        forward model against expected output for 4 years of data.
        """
        # Create system model
        time_array = 365.25*np.arange(0, 5)
        model_kwargs = {'time_array': time_array}  # time is given in days
        sm = SystemModel(model_kwargs=model_kwargs)

        # Add multisegmented wellbore component
        sres = sm.add_component_model_object(SimpleReservoir(name='sres', parent=sm))
        # Add observations of multisegmented wellbore component model
        sres.add_obs('pressure')
        sres.add_obs('CO2saturation')

        # Run system model using current values of its parameters
        sm.forward()

        # Assign observations of the model to pressure and CO2saturation variables
        # Obtain pressure and CO2 saturation as lists
        pressure = sm.collect_observations_as_time_series(sres, 'pressure')
        CO2saturation = sm.collect_observations_as_time_series(sres, 'CO2saturation')

        # True values
        true_pressure = [9411325., 14019874.26175452, 14299528.64528817,
                         14463116.2847518, 14579183.64865103]
        true_saturation = [0., 0.21658361, 0.31286341, 0.3867415, 0.4490236]

        # Test with helpful message!
        for tp, p, t in zip(true_pressure, pressure, time_array):
            self.assertTrue(abs((tp-p)/tp) < 0.01,
                            'Pressure at time {} is {} Pa but should be {} Pa'
                            .format(str(t), str(p), str(tp)))
        for ts, s, t in zip(true_saturation[1:], CO2saturation[1:], time_array[1:]):
            self.assertTrue(abs((ts-s)/ts) < 0.01,
                            'Saturation at time {} is {} but should be {}'
                            .format(str(t), str(s), str(ts)))


# Set up cases in the future when needed...
def suite(case):
    """Determines set of tests to be performed."""

    suite = unittest.TestSuite()
    if case == 'base' or case == 'all':
        # Tests are arranged in alphabetical order of their names
        suite.addTest(Tests('test_alluvium_aquifer'))
        suite.addTest(Tests('test_atm'))
        suite.addTest(Tests('test_carb_aquifer'))
        suite.addTest(Tests('test_coupled_components_with_grid_obs'))
        suite.addTest(Tests('test_coupled_reservoir_cemented_wellbore_forward'))
        suite.addTest(Tests('test_coupled_reservoir_MSW_forward'))
        suite.addTest(Tests('test_coupled_reservoir_open_carbonate_forward'))
        suite.addTest(Tests('test_coupled_reservoir_open_wellbore_forward'))
        suite.addTest(Tests('test_deep_alluvium_aquifer'))
        suite.addTest(Tests('test_lhs'))
        suite.addTest(Tests('test_lookup_table_reservoir'))
        suite.addTest(Tests('test_openiam_cf'))
        suite.addTest(Tests('test_parstudy'))
        suite.addTest(Tests('test_rate_to_mass_adapter'))
        suite.addTest(Tests('test_reservoir_data_interpolator'))
        suite.addTest(Tests('test_simple_reservoir_forward'))
        
    return suite

if __name__ == '__main__':
    # For multiprocessing in Spyder
    __spec__ = None
    # unittest.main()
    # Maybe do this down the road...
    if len(sys.argv) > 1: case = sys.argv[1]
    else:
        case = 'all'
        #print("    nAssuming all tests wanted")
        #print("USAGE: python iam_test.py <option>")
        #print("Option includes: all, parallel, base    n")

    # Second keyword argument makes testing messages printed when running test suite in IPython console
    runner = unittest.TextTestRunner(verbosity=2, stream=sys.stderr)
    test_suite = suite(case)
    runner.run(test_suite)
