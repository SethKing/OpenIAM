# Changelog
All notable changes to this project will be documented in this file.

## [a0.5.0 - 2018-07-30]
### Added
 - Deep Alluvium Aquifer ROM
 - Plume Stability Calculation
### Fixed/Updated
 - Modified Reservoir LUTs for optional input data

## [a0.4.0] - 2018-04-20
### Added
 - Atomospheric Dispersion ROM
 - Progress bar for ensemble simulations

### Fixed/Updated
 - Reservoir LUT gridded observations
 - Cemented Wellbore parameter limits
 - Multi-segmented wellbore pressure checking
 - Renamed example files for clarity/consistency
 - Updated logging messages
 - Updated MCMC model update example
 - Added temporal input checking to Carbonate Aquifer component

## [a0.3.0] - 2018-03-01 
### Added
 - Sensitivity Analysis UI and visualization
 - Dynamic kwarg 
 - Gridded Parameters (Needs UI)
 - Gridded Observations 
 - Plotting updates including 
   - Statistics plotting for large realization runs
   - User Specified Titles
   - Subplotting Options
 - Advanced MCMC example
 - Matk Updates
   - Added sample statistics
   - Added statistics file output
   - Added RBD-Fast sensitivity analysis

### Fixed/Updated
 - Mass of CO2 in aquifers reported from wellbore models
 - Statistics output from Matk functionality
 - Clarify points on install process and lookup table files in User's Guide
 - Removed dependency on make utility for mac and linux install
 - Modified examples based on User Feedback

## [a0.2.0] - 2018-01-25
### Added
 - Reservoir Lookup Table component added
 - Stratigraphy Component added
   - This moves stratigraphy specification to new section in control files.
 - Run time output from control file runs
 - Analysis log files created for restarting options
 - Parameters and Observations written to CSV file
 - Parameter and Observation statistics (min, max, mean, etc.) written to CSV file

### Updated
 - Updated MATK
   - Handles discrete parameters (LHS)
   - Sobol Sensitivity Analysis
   - Matplotlib display check fix
 - Component model method add_obs restructured

## [a0.1.0] - 2017-12-31
### Added
 - Initial OpenIAM testing version
 - Probabilistic Framework for integrated assessment modeling, including
   - forward modeling 
   - latin hypercube sampling
   - parameter studys
 - Component models included:
   - Simple Reservoir Model (semi-analytical model)
   - Multi-segmented Wellbore Model (semi-analytical model)
   - Cemented Wellbore Model
   - Open Wellbore Model
   - Carbonate Aquifer Model
 - Control-file based User Interface
 - User's Guide
 - Setup Script
 - Test Suite
 - Examples
