# csv is imported to read lookup table params file
# Pmw is imported to enable hover text
# pickle is used to generate and read binary files
import csv, Pmw, pickle
import os

from Disclaimer import Disclaimer_Page
from Dashboard import Dashboard_Page
from OpenIAM_Page import OpenIAM_Page
from PostPocessor_Page import PostPocessor_Page

from dictionarydata import d
from dictionarydata import componentVars
from dictionarydata import componentChoices
from dictionarydata import componentTypeDictionary
from dictionarydata import connectionsDictionary
from dictionarydata import distributionOptions
from dictionarydata import connections
from dictionarydata import LUTWeights
from dictionarydata import connectionTypes
from dictionarydata import componentTypes
from dictionarydata import shaleThickness
from dictionarydata import aquiferThickness
from dictionarydata import aquifers
from dictionarydata import lookupTableParams
from dictionarydata import savedDictionary
from dictionarydata import LABEL_FONT
from dictionarydata import DASHBOARD_FONT

try:
    # Python2
    import Tkinter as tk
    from Tkinter import ttk
    from Tkinter import StringVar
    from Tkinter import IntVar
    from Tkinter import DoubleVar
    from Tkinter import BooleanVar
    from Tkinter import tkMessageBox as messagebox
except ImportError:
    # Python3
    import tkinter as tk
    from tkinter import ttk
    from tkinter import StringVar
    from tkinter import IntVar
    from tkinter import DoubleVar
    from tkinter import BooleanVar
    from tkinter import messagebox

class NRAPOpenIAM(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        container = tk.Frame(self)
        container.grid(row=0, column=0, sticky='nsew')

        self.frames = {}

        for F in (Disclaimer_Page, Dashboard_Page, OpenIAM_Page, PostPocessor_Page):
            frame = F(container, self)
            self.frames[F]=frame
            frame.grid(row=0, column=0, rowspan=27, columnspan=30, sticky="nsew")

        self.show_frame(Disclaimer_Page)

    # this method saves the current simulation out to a binary file to allow
    # later editing with the load sim method
    def populate_dictionary(self, shaleThickness, aquiferThickness, lookupTableParams, LUTWeights):
        d['ModelParams']={}
        if(componentVars['outputDirectoryGenerate'].get()):
            d['ModelParams']['OutputDirectory']=componentVars['outputDirectory'].get()+'{datetime}'
        else:
            d['ModelParams']['OutputDirectory']=componentVars['outputDirectory'].get()

        d['WellboreLocations']={}
        d['WellboreLocations']['coordx']=[]
        for numx in componentVars['xCoordinates'].get().split(','):
            d['WellboreLocations']['coordx'].append(float(numx.strip()))

        d['WellboreLocations']['coordy']=[]
        for numy in componentVars['yCoordinates'].get().split(','):
            d['WellboreLocations']['coordy'].append(float(numy.strip()))

        d['RandomWellDomain']={}
        d['RandomWellDomain']['xmin']=componentVars['RandomWellDomain']['xmin'].get()
        d['RandomWellDomain']['ymin']=componentVars['RandomWellDomain']['ymin'].get()
        d['RandomWellDomain']['xmax']=componentVars['RandomWellDomain']['xmax'].get()
        d['RandomWellDomain']['ymax']=componentVars['RandomWellDomain']['ymax'].get()

        i=0

        for choice in componentChoices:
            d[choice]={}
            d[choice]['Parameters']={}
            d[choice]['connection']=connectionsDictionary[i]

            if(componentTypeDictionary[i]=='simplereservoir'):
                modelOutputs = []

                d[choice]['type']='SimpleReservoir'
                d[choice]['Parameters']['logResPerm']={}
                d[choice]['Parameters']['reservoirPorosity']={}
                d[choice]['Parameters']['brineDensity']={}
                d[choice]['Parameters']['CO2Density']={}
                d[choice]['Parameters']['brineViscosity']={}
                d[choice]['Parameters']['CO2Viscosity']={}
                d[choice]['Parameters']['brineResSaturation']={}
                d[choice]['Parameters']['compressibility']={}
                d[choice]['Parameters']['injRate']={}

                if(componentVars[choice]['CO2saturation'].get()):
                    modelOutputs.append('CO2saturation')
                if(componentVars[choice]['pressure'].get()):
                    modelOutputs.append('pressure')
                if(componentVars[choice]['mass_CO2_reservoir'].get()):
                    modelOutputs.append('mass_CO2_reservoir')

                d[choice]['Outputs']=modelOutputs

            if(componentTypeDictionary[i]=='cementedwellbore'):
                modelOutputs = []

                d[choice]['type']='CementedWellbore'
                d[choice]['number']=abs(componentVars[choice]['number'].get())
                d[choice]['Parameters']['logWellPerm']={}
                d[choice]['Parameters']['logThiefPerm']={}
                d[choice]['Parameters']['wellRadius']={}

                if("Dynamic" in connectionsDictionary[i]):
                    d[choice]['connection']='Dynamic Parameters'
                    d[choice]['DynamicParameters']={}
                    pressure = componentVars[choice]['pressure'].split(',')
                    saturation = componentVars[choice]['saturation'].split(',')
                    d[choice]['DynamicParameters']['pressure']=[]
                    for p in pressure:
                        d[choice]['DynamicParameters']['pressure'].append(float(p.strip()))
                    d[choice]['DynamicParameters']['saturation']=[]
                    for s in saturation:
                        d[choice]['DynamicParameters']['saturation'].append(float(s.strip()))

                if(componentVars[choice]['CO2_atm'].get()):
                    modelOutputs.append('CO2_atm')
                if(componentVars[choice]['brine_atm'].get()):
                    modelOutputs.append('brine_atm')
                if(componentVars[choice]['CO2_aquifer1'].get()):
                    modelOutputs.append('CO2_aquifer1')
                if(componentVars[choice]['CO2_aquifer2'].get()):
                    modelOutputs.append('CO2_aquifer2')
                if(componentVars[choice]['brine_aquifer1'].get()):
                    modelOutputs.append('brine_aquifer1')
                if(componentVars[choice]['brine_aquifer2'].get()):
                    modelOutputs.append('brine_aquifer2')
                if(componentVars[choice]['mass_CO2_aquifer1'].get()):
                    modelOutputs.append('mass_CO2_aquifer1')
                if(componentVars[choice]['mass_CO2_aquifer2'].get()):
                    modelOutputs.append('mass_CO2_aquifer2')

                d[choice]['Outputs']=modelOutputs

            if(componentTypeDictionary[i]=='multiSegemented'):
                modelOutputs = []

                d[choice]['type']='MultisegmentedWellbore'
                d[choice]['number']=abs(componentVars[choice]['number'].get())

                d[choice]['Parameters']['logWellPerm']={}
                d[choice]['Parameters']['logAquaPerm']={}
                d[choice]['Parameters']['brineDensity']={}
                d[choice]['Parameters']['CO2Density']={}
                d[choice]['Parameters']['brineViscosity']={}
                d[choice]['Parameters']['CO2Viscosity']={}
                d[choice]['Parameters']['brineResSaturation']={}
                d[choice]['Parameters']['compressibility']={}
                d[choice]['Parameters']['wellRadius']={}

                if("Dynamic" in connectionsDictionary[i]):
                    d[choice]['connection']='Dynamic Parameters'
                    d[choice]['DynamicParameters']={}
                    pressure = componentVars[choice]['pressure'].split(',')
                    saturation = componentVars[choice]['saturation'].split(',')
                    d[choice]['DynamicParameters']['pressure']=[]
                    for p in pressure:
                        d[choice]['DynamicParameters']['pressure'].append(float(p.strip()))
                    d[choice]['DynamicParameters']['saturation']=[]
                    for s in saturation:
                        d[choice]['DynamicParameters']['saturation'].append(float(s.strip()))

                keys = componentVars[choice]['outputs'].keys()

                for key in keys:
                    if (componentVars[choice]['outputs'][key].get()):
                        modelOutputs.append(key)

                d[choice]['Outputs']=modelOutputs

            if(componentTypeDictionary[i]=='openwellbore'):
                modelOutputs = []

                d[choice]['type']='OpenWellbore'
                d[choice]['Parameters']['logNormalizedTransmissivity']={}
                d[choice]['Parameters']['brineSalinity']={}
                d[choice]['Parameters']['wellRadius']={}

                d[choice]['number']=abs(componentVars[choice]['number'].get())
                d[choice]['LeakTo']=componentVars[choice]['LeakTo'].get()

                if("Dynamic" in connectionsDictionary[i]):
                    d[choice]['connection']='Dynamic Parameters'
                    d[choice]['DynamicParameters']={}
                    pressure = componentVars[choice]['pressure'].split(',')
                    saturation = componentVars[choice]['saturation'].split(',')
                    d[choice]['DynamicParameters']['pressure']=[]
                    for p in pressure:
                        d[choice]['DynamicParameters']['pressure'].append(float(p.strip()))
                    d[choice]['DynamicParameters']['saturation']=[]
                    for s in saturation:
                        d[choice]['DynamicParameters']['saturation'].append(float(s.strip()))

                if(componentVars[choice]['CO2_aquifer'].get()):
                    modelOutputs.append('CO2_aquifer')
                if(componentVars[choice]['CO2_atm'].get()):
                    modelOutputs.append('CO2_atm')
                if(componentVars[choice]['brine_aquifer'].get()):
                    modelOutputs.append('brine_aquifer')
                if(componentVars[choice]['brine_atm'].get()):
                    modelOutputs.append('brine_atm')

                d[choice]['Outputs']=modelOutputs

            if(componentTypeDictionary[i]=='carbonateAquifer'):
                modelOutputs = []

                d[choice]['type']='CarbonateAquifer'
                d[choice]['Parameters']['rmin']= {}
                d[choice]['Parameters']['perm_var']= {}
                d[choice]['Parameters']['corr_len']= {}
                d[choice]['Parameters']['aniso']= {}
                d[choice]['Parameters']['mean_perm']= {}
                d[choice]['Parameters']['hyd_grad']={}
                d[choice]['Parameters']['calcite_ssa']= {}
                d[choice]['Parameters']['organic_carbon']= {}
                d[choice]['Parameters']['benzene_kd']= {}
                d[choice]['Parameters']['benzene_decay']= {}
                d[choice]['Parameters']['nap_kd']= {}
                d[choice]['Parameters']['nap_decay']= {}
                d[choice]['Parameters']['phenol_kd']= {}
                d[choice]['Parameters']['phenol_decay']= {}
                d[choice]['Parameters']['cl']= {}

                if("Dynamic" in connectionsDictionary[i]):
                    d[choice]['connection']='Dynamic Parameters'
                    d[choice]['DynamicParameters']={}
                    pressure = componentVars[choice]['pressure'].split(',')
                    saturation = componentVars[choice]['saturation'].split(',')
                    d[choice]['DynamicParameters']['brine_flux']=[]
                    for p in pressure:
                        d[choice]['DynamicParameters']['brine_flux'].append(float(p.strip()))
                    d[choice]['DynamicParameters']['co2_flux']=[]
                    for s in saturation:
                        d[choice]['DynamicParameters']['co2_flux'].append(float(s.strip()))

                if(componentVars[choice]['ithresh'].get().find('MCL')):
                    d[choice]['Parameters']['ithresh']=2
                if(componentVars[choice]['ithresh'].get().find('No')):
                    d[choice]['Parameters']['ithresh']=1

                if(componentVars[choice]['logf'].get()=='Linear'):
                    d[choice]['Parameters']['logf']=0
                if(componentVars[choice]['logf'].get()=='Log'):
                    d[choice]['Parameters']['logf']=1

                d[choice]['AquiferName']=componentVars[choice]['aquiferName'].get()

                if(componentVars[choice]['pH'].get()):
                    modelOutputs.append('pH')
                if(componentVars[choice]['Flux'].get()):
                    modelOutputs.append('Flux')
                if(componentVars[choice]['dx'].get()):
                    modelOutputs.append('dx')
                if(componentVars[choice]['dy'].get()):
                    modelOutputs.append('dy')
                if(componentVars[choice]['TDS'].get()):
                    modelOutputs.append('TDS')
                if(componentVars[choice]['As'].get()):
                    modelOutputs.append('As')
                if(componentVars[choice]['Pb'].get()):
                    modelOutputs.append('Pb')
                if(componentVars[choice]['Cd'].get()):
                    modelOutputs.append('Cd')
                if(componentVars[choice]['Ba'].get()):
                    modelOutputs.append('Ba')
                if(componentVars[choice]['Benzene'].get()):
                    modelOutputs.append('Benzene')
                if(componentVars[choice]['Naphthalene'].get()):
                    modelOutputs.append('Naphthalene')
                if(componentVars[choice]['Phenol'].get()):
                    modelOutputs.append('Phenol')

                d[choice]['Outputs']=modelOutputs

            if(componentTypeDictionary[i]=='lookupTable'):
                modelOutputs=[]

                d[choice]['type']='LookupTableReservoir'
                d[choice]['FileDirectory']=lookupTableParams['lookupTable_input_dir'].get()
                d[choice]['TimeFile']=lookupTableParams['lookupTable_time_file'].get()
                d[choice]['ParameterFilename']=lookupTableParams['lookupTable_param_file'].get()

                if(lookupTableParams['pressure'].get()):
                    modelOutputs.append('pressure')
                if(lookupTableParams['CO2saturation'].get()):
                    modelOutputs.append('CO2saturation')

                d[choice]['Outputs']=modelOutputs

                for key in LUTWeights.keys():
                    d[choice]['Parameters'][key.strip()]={}
                    d[choice]['Parameters'][key.strip()]['values']=[]
                    d[choice]['Parameters'][key.strip()]['weights']=[]

                    for value in LUTWeights[key].keys():
                        d[choice]['Parameters'][key.strip()]['values'].append(float(value))
                        d[choice]['Parameters'][key.strip()]['weights'].append(LUTWeights[key][value].get())

                break

            if(componentTypeDictionary[i]=='alluviumAquifer'):
                modelOutputs=[]

                d[choice]['type']='DeepAlluviumAquifer'
                d[choice]['AquiferName']=componentVars[choice]['aquiferName'].get()

                d[choice]['Parameters']['brine_rate']={}
                d[choice]['Parameters']['brine_mass']={}
                d[choice]['Parameters']['co2_rate']={}
                d[choice]['Parameters']['co2_mass']={}
                d[choice]['Parameters']['logK_sand1']={}
                d[choice]['Parameters']['logK_sand2']={}
                d[choice]['Parameters']['logK_sand3']={}
                d[choice]['Parameters']['logK_caprock']={}
                d[choice]['Parameters']['correlationLengthX']={}
                d[choice]['Parameters']['correlationLengthZ']={}
                d[choice]['Parameters']['sandFraction']={}
                d[choice]['Parameters']['groundwater_gradient']={}
                d[choice]['Parameters']['leak_depth']={}

                if("Dynamic" in connectionsDictionary[i]):
                    d[choice]['connection']='Dynamic Parameters'
                    d[choice]['DynamicParameters']={}
                    pressure = componentVars[choice]['pressure'].split(',')
                    saturation = componentVars[choice]['saturation'].split(',')
                    d[choice]['DynamicParameters']['brine_flux']=[]
                    for p in pressure:
                        d[choice]['DynamicParameters']['brine_flux'].append(float(p.strip()))
                    d[choice]['DynamicParameters']['co2_flux']=[]
                    for s in saturation:
                        d[choice]['DynamicParameters']['co2_flux'].append(float(s.strip()))

                if(componentVars[choice]['TDS_volume'].get()):
                    modelOutputs.append('TDS_volume')
                if(componentVars[choice]['TDS_dx'].get()):
                    modelOutputs.append('TDS_dx')
                if(componentVars[choice]['TDS_dy'].get()):
                    modelOutputs.append('TDS_dy')
                if(componentVars[choice]['TDS_dz'].get()):
                    modelOutputs.append('TDS_dz')
                if(componentVars[choice]['Pressure_volume'].get()):
                    modelOutputs.append('Pressure_volume')
                if(componentVars[choice]['Pressure_dx'].get()):
                    modelOutputs.append('Pressure_dx')
                if(componentVars[choice]['Pressure_dy'].get()):
                    modelOutputs.append('Pressure_dy')
                if(componentVars[choice]['Pressure_dz'].get()):
                    modelOutputs.append('Pressure_dz')
                if(componentVars[choice]['pH_volume'].get()):
                    modelOutputs.append('pH_volume')
                if(componentVars[choice]['pH_dx'].get()):
                    modelOutputs.append('pH_dx')
                if(componentVars[choice]['pH_dy'].get()):
                    modelOutputs.append('pH_dy')
                if(componentVars[choice]['pH_dz'].get()):
                    modelOutputs.append('pH_dz')

                d[choice]['Outputs']=modelOutputs

            if(componentTypeDictionary[i]=='atmospheric'):
                modelOutputs=[]

                d[choice]['type']='AtmosphericROM'
                d[choice]['Parameters']['T_amb']={}
                d[choice]['Parameters']['P_amb']={}
                d[choice]['Parameters']['V_wind']={}
                d[choice]['Parameters']['C0_critical']={}
                d[choice]['Parameters']['T_source']={}
                d[choice]['Parameters']['x_receptor']={}
                d[choice]['Parameters']['y_receptor']={}

                if("Dynamic" in connectionsDictionary[i]):
                    d[choice]['connection']='Dynamic Parameters'
                    d[choice]['DynamicParameters']={}
                    pressure = componentVars[choice]['pressure'].split(',')
                    saturation = componentVars[choice]['saturation'].split(',')
                    d[choice]['DynamicParameters']['brine_flux']=[]
                    for p in pressure:
                        d[choice]['DynamicParameters']['brine_flux'].append(float(p.strip()))
                    d[choice]['DynamicParameters']['co2_flux']=[]
                    for s in saturation:
                        d[choice]['DynamicParameters']['co2_flux'].append(float(s.strip()))

                if(componentVars[choice]['out_flag'].get()):
                    modelOutputs.append('out_flag')
                if(componentVars[choice]['num_sources'].get()):
                    modelOutputs.append('num_sources')
                if(componentVars[choice]['x_new'].get()):
                    modelOutputs.append('x_new')
                if(componentVars[choice]['y_new'].get()):
                    modelOutputs.append('y_new')
                if(componentVars[choice]['critical_distance'].get()):
                    modelOutputs.append('critical_distance')

                d[choice]['Outputs']=modelOutputs

            keys = componentVars[choice]['Params'].keys()

            for key in keys:
                if(componentVars[choice]['Params'][key]['distribution'].get()=='Fixed Value'):
                    d[choice]['Parameters'][key]['value']=componentVars[choice]['Params'][key]['value'].get()
                    d[choice]['Parameters'][key]['vary']=False

                if(componentVars[choice]['Params'][key]['distribution'].get()=='Uniform'):
                    d[choice]['Parameters'][key]['dist']='uniform'
                    d[choice]['Parameters'][key]['min']=componentVars[choice]['Params'][key]['min'].get()
                    d[choice]['Parameters'][key]['max']=componentVars[choice]['Params'][key]['max'].get()

                if(componentVars[choice]['Params'][key]['distribution'].get()=='Normal'):
                    d[choice]['Parameters'][key]['dist']='norm'
                    d[choice]['Parameters'][key]['mean']=componentVars[choice]['Params'][key]['mean'].get()
                    d[choice]['Parameters'][key]['std']=componentVars[choice]['Params'][key]['std'].get()

                if(componentVars[choice]['Params'][key]['distribution'].get()=='Lognormal'):
                    d[choice]['Parameters'][key]['dist']='lognorm'
                    d[choice]['Parameters'][key]['dist_pars']=[]
                    d[choice]['Parameters'][key]['dist_pars'].append(componentVars[choice]['Params'][key]['mean'].get())
                    d[choice]['Parameters'][key]['dist_pars'].append(0)
                    d[choice]['Parameters'][key]['dist_pars'].append(componentVars[choice]['Params'][key]['std'].get())

                if(componentVars[choice]['Params'][key]['distribution'].get()=='Truncated'):
                    d[choice]['Parameters'][key]['dist']='norm'
                    d[choice]['Parameters'][key]['min']=componentVars[choice]['Params'][key]['min'].get()
                    d[choice]['Parameters'][key]['max']=componentVars[choice]['Params'][key]['max'].get()
                    d[choice]['Parameters'][key]['mean']=componentVars[choice]['Params'][key]['mean'].get()
                    d[choice]['Parameters'][key]['std']=componentVars[choice]['Params'][key]['std'].get()

                if(componentVars[choice]['Params'][key]['distribution'].get()=='Triangular'):
                    d[choice]['Parameters'][key]['dist']='triang'
                    d[choice]['Parameters'][key]['dist_pars']=[]
                    d[choice]['Parameters'][key]['dist_pars'].append(componentVars[choice]['Params'][key]['mean'].get())
                    d[choice]['Parameters'][key]['dist_pars'].append(componentVars[choice]['Params'][key]['std'].get())
                    d[choice]['Parameters'][key]['dist_pars'].append(componentVars[choice]['Params'][key]['value'].get())

                if(componentVars[choice]['Params'][key]['distribution'].get()=='Discrete'):
                    d[choice]['Parameters'][key]['discrete_vals']=[]
                    discrete_values=[]
                    discrete_weights=[]

                    for value in componentVars[choice]['Params'][key]['values'].get().split(','):
                        discrete_values.append(float(value.strip()))

                    for weight in componentVars[choice]['Params'][key]['weights'].get().split(','):
                        discrete_weights.append(float(weight.strip()))

                    d[choice]['Parameters'][key]['discrete_vals'].append(discrete_values)
                    d[choice]['Parameters'][key]['discrete_vals'].append(discrete_weights)

            i=i+1

        d['Stratigraphy']={}
        d['Stratigraphy']['numberOfShaleLayers']={}
        d['Stratigraphy']['reservoirThickness']={}
        d['Stratigraphy']['numberOfShaleLayers']['value']=componentVars['numberOfShaleLayers'].get()
        d['Stratigraphy']['pressure']=componentVars['datumPressure'].get()

        if(componentVars['reservoirThickness']['distriubtion'].get()=='Fixed Value'):
            d['Stratigraphy']['reservoirThickness']['value']=componentVars['reservoirThickness']['Value'].get()
            d['Stratigraphy']['reservoirThickness']['vary']=False

        if(componentVars['reservoirThickness']['distriubtion'].get()=='Uniform'):
            d['Stratigraphy']['reservoirThickness']['dist']='uniform'
            d['Stratigraphy']['reservoirThickness']['min']=componentVars['reservoirThickness']['min'].get()
            d['Stratigraphy']['reservoirThickness']['max']=componentVars['reservoirThickness']['max'].get()

        if(componentVars['reservoirThickness']['distriubtion'].get()=='Normal'):
            d['Stratigraphy']['reservoirThickness']['dist']='norm'
            d['Stratigraphy']['reservoirThickness']['mean']=componentVars['reservoirThickness']['mean'].get()
            d['Stratigraphy']['reservoirThickness']['std']=componentVars['reservoirThickness']['std'].get()

        if(componentVars['reservoirThickness']['distriubtion'].get()=='Truncated'):
            d['Stratigraphy']['reservoirThickness']['dist']='norm'
            d['Stratigraphy']['reservoirThickness']['mean']=componentVars['reservoirThickness']['mean'].get()
            d['Stratigraphy']['reservoirThickness']['std']=componentVars['reservoirThickness']['std'].get()
            d['Stratigraphy']['reservoirThickness']['min']=componentVars['reservoirThickness']['min'].get()
            d['Stratigraphy']['reservoirThickness']['max']=componentVars['reservoirThickness']['max'].get()

        if(componentVars['reservoirThickness']['distriubtion'].get()=='Lognormal'):
            d['Stratigraphy']['reservoirThickness']['dist']='lognorm'
            d['Stratigraphy']['reservoirThickness']['dist_pars']=[]
            d['Stratigraphy']['reservoirThickness']['dist_pars'].append(componentVars['reservoirThickness']['mean'].get())
            d['Stratigraphy']['reservoirThickness']['dist_pars'].append(0)
            d['Stratigraphy']['reservoirThickness']['dist_pars'].append(componentVars['reservoirThickness']['std'].get())

        if(componentVars['reservoirThickness']['distriubtion'].get()=='Triangular'):
            d['Stratigraphy']['reservoirThickness']['dist']='triang'
            d['Stratigraphy']['reservoirThickness']['dist_pars']=[]
            d['Stratigraphy']['reservoirThickness']['dist_pars'].append(componentVars['reservoirThickness']['mean'].get())
            d['Stratigraphy']['reservoirThickness']['dist_pars'].append(componentVars['reservoirThickness']['max'].get())
            d['Stratigraphy']['reservoirThickness']['dist_pars'].append(componentVars['reservoirThickness']['std'].get())

        if(componentVars['reservoirThickness']['distriubtion'].get()=='Discrete'):
            d['Stratigraphy']['reservoirThickness']['discrete_vals']=[]
            discrete_values=[]
            discrete_weights=[]

            for value in componentVars['reservoirThickness']['values'].get().split(','):
                discrete_values.append(float(value.strip()))

            for weight in componentVars['reservoirThickness']['weights'].get().split(','):
                discrete_weights.append(float(weight.strip()))

            d['Stratigraphy']['reservoirThickness']['discrete_vals'].append(discrete_values)
            d['Stratigraphy']['reservoirThickness']['discrete_vals'].append(discrete_weights)

        if(componentVars['analysis']['type'].get()=='Forward'):
            d['ModelParams']['Analysis']={}
            d['ModelParams']['Analysis']['type']=componentVars['analysis']['type'].get()

        if(componentVars['analysis']['type'].get()=='LHS'):
            d['ModelParams']['Analysis']={}
            d['ModelParams']['Analysis']['type']=componentVars['analysis']['type'].get()
            d['ModelParams']['Analysis']['siz']=componentVars['analysis']['size'].get()
            d['ModelParams']['Analysis']['seed']=componentVars['analysis']['seed'].get()

        if(componentVars['analysis']['type'].get()=='Parstudy'):
            d['ModelParams']['Analysis']={}
            d['ModelParams']['Analysis']['type']=componentVars['analysis']['type'].get()
            d['ModelParams']['Analysis']['nvals']=componentVars['analysis']['nvals'].get()

        aquiferKeys=aquiferThickness.keys()
        shaleKeys=shaleThickness.keys()

        for akey in aquiferKeys:
            d['Stratigraphy'][akey]={}

            if aquiferThickness[akey]['distribution'].get()=='Fixed Value':
                d['Stratigraphy'][akey]['value']=aquiferThickness[akey]['value'].get()
                d['Stratigraphy'][akey]['vary']=False

            if(aquiferThickness[akey]['distribution'].get()=='Uniform'):
                d['Stratigraphy'][akey]['dist']='uniform'
                d['Stratigraphy'][akey]['min']=aquiferThickness[akey]['min'].get()
                d['Stratigraphy'][akey]['max']=aquiferThickness[akey]['max'].get()

            if(aquiferThickness[akey]['distribution'].get()=='Normal'):
                d['Stratigraphy'][akey]['dist']='norm'
                d['Stratigraphy'][akey]['mean']=aquiferThickness[akey]['mean'].get()
                d['Stratigraphy'][akey]['std']=aquiferThickness[akey]['std'].get()

            if(aquiferThickness[akey]['distribution'].get()=='Truncated'):
                d['Stratigraphy'][akey]['dist']='norm'
                d['Stratigraphy'][akey]['mean']=aquiferThickness[akey]['mean'].get()
                d['Stratigraphy'][akey]['std']=aquiferThickness[akey]['std'].get()
                d['Stratigraphy'][akey]['min']=aquiferThickness[akey]['min'].get()
                d['Stratigraphy'][akey]['max']=aquiferThickness[akey]['max'].get()

            if(aquiferThickness[akey]['distribution'].get()=='Lognormal'):
                d['Stratigraphy'][akey]['dist']='lognorm'
                d['Stratigraphy'][akey]['dist_pars']=[]
                d['Stratigraphy'][akey]['dist_pars'].append(aquiferThickness[akey]['mean'].get())
                d['Stratigraphy'][akey]['dist_pars'].append(0)
                d['Stratigraphy'][akey]['dist_pars'].append(aquiferThickness[akey]['std'].get())

            if(aquiferThickness[akey]['distribution'].get()=='Triangular'):
                d['Stratigraphy'][akey]['dist']='triang'
                d['Stratigraphy'][akey]['dist_pars']=[]
                d['Stratigraphy'][akey]['dist_pars'].append(aquiferThickness[akey]['mean'].get())
                d['Stratigraphy'][akey]['dist_pars'].append(aquiferThickness[akey]['max'].get())
                d['Stratigraphy'][akey]['dist_pars'].append(aquiferThickness[akey]['std'].get())

            if(aquiferThickness[akey]['distribution'].get()=='Discrete'):
                d['Stratigraphy'][akey]['discrete_vals']=[]
                discrete_values=[]
                discrete_weights=[]

                for value in aquiferThickness[akey]['values'].get().split(','):
                    discrete_values.append(float(value.strip()))

                for weight in aquiferThickness[akey]['weights'].get().split(','):
                    discrete_weights.append(float(weight.strip()))

                d['Stratigraphy'][akey]['discrete_vals'].append(discrete_values)
                d['Stratigraphy'][akey]['discrete_vals'].append(discrete_weights)

        for skey in shaleKeys:
            d['Stratigraphy'][skey]={}

            if shaleThickness[skey]['distribution'].get()=='Fixed Value':
                d['Stratigraphy'][skey]['value']=shaleThickness[skey]['value'].get()
                d['Stratigraphy'][skey]['vary']=False

            if(shaleThickness[skey]['distribution'].get()=='Uniform'):
                d['Stratigraphy'][skey]['dist']='uniform'
                d['Stratigraphy'][skey]['min']=shaleThickness[skey]['min'].get()
                d['Stratigraphy'][skey]['max']=shaleThickness[skey]['max'].get()

            if(shaleThickness[skey]['distribution'].get()=='Normal'):
                d['Stratigraphy'][skey]['dist']='norm'
                d['Stratigraphy'][skey]['mean']=shaleThickness[skey]['mean'].get()
                d['Stratigraphy'][skey]['std']=shaleThickness[skey]['std'].get()

            if(shaleThickness[skey]['distribution'].get()=='Truncated'):
                d['Stratigraphy'][skey]['dist']='norm'
                d['Stratigraphy'][skey]['mean']=shaleThickness[skey]['mean'].get()
                d['Stratigraphy'][skey]['std']=shaleThickness[skey]['std'].get()
                d['Stratigraphy'][skey]['min']=shaleThickness[skey]['min'].get()
                d['Stratigraphy'][skey]['max']=shaleThickness[skey]['max'].get()

            if(shaleThickness[skey]['distribution'].get()=='Lognormal'):
                d['Stratigraphy'][skey]['dist']='lognorm'
                d['Stratigraphy'][skey]['dist_pars']=[]
                d['Stratigraphy'][skey]['dist_pars'].append(shaleThickness[skey]['mean'].get())
                d['Stratigraphy'][skey]['dist_pars'].append(0)
                d['Stratigraphy'][skey]['dist_pars'].append(shaleThickness[skey]['std'].get())

            if(shaleThickness[skey]['distribution'].get()=='Triangular'):
                d['Stratigraphy'][skey]['dist']='triang'
                d['Stratigraphy'][skey]['dist_pars']=[]
                d['Stratigraphy'][skey]['dist_pars'].append(shaleThickness[skey]['mean'].get())
                d['Stratigraphy'][skey]['dist_pars'].append(shaleThickness[skey]['max'].get())
                d['Stratigraphy'][skey]['dist_pars'].append(shaleThickness[skey]['std'].get())

            if(shaleThickness[skey]['distribution'].get()=='Discrete'):
                d['Stratigraphy'][skey]['discrete_vals']=[]
                discrete_values=[]
                discrete_weights=[]

                for value in shaleThickness[skey]['values'].get().split(','):
                    discrete_values.append(float(value.strip()))

                for weight in shaleThickness[skey]['weights'].get().split(','):
                    discrete_weights.append(float(weight.strip()))

                d['Stratigraphy'][skey]['discrete_vals'].append(discrete_values)
                d['Stratigraphy'][skey]['discrete_vals'].append(discrete_weights)

        d['ModelParams']['EndTime']=componentVars['endTime'].get()
        d['ModelParams']['TimeStep']=componentVars['timeStep'].get()
        d['ModelParams']['Logging']=componentVars['logging'].get()
        d['ModelParams']['Components']=componentChoices

        try:
            fileName = d['simName'].get()+'.OpenIAM'
            d['simName'] = d['simName'].get()
        except:
            fileName = d['simName']+'.OpenIAM'

        try:
            with open(fileName, 'wb') as outPutFile:
                pickle.dump(d, outPutFile, pickle.HIGHEST_PROTOCOL)
        except:
            return

    # This changes the distribution for any parameter that needs to changed
    # based on user inputs
    def DistributionChange(self, frame):
        toolTip = Pmw.Balloon(self)
        for widget in frame.winfo_children():
            widget.destroy()

        flabel = ttk.Label(frame, text=frame.labelText, width=30)
        fmenu = tk.OptionMenu(frame, componentVars[frame.component]['Params'][frame.text]['distribution'], *distributionOptions, command=lambda _:self.DistributionChange(frame))
        flabel.grid(row=0, column=0, sticky='e', padx=5)
        fmenu.config(width=15)
        toolTip.bind(fmenu, 'Select Distribution for '+frame.toolTipText)
        fmenu.grid(row=0, column=1, sticky='ew', padx=5)

        if(frame.distType.get()=='Fixed Value'):
            fvlabel=ttk.Label(frame, text='Value:')
            ftxtField = tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['value'])
            fvlabel.grid(row=0, column=2, sticky='ew', padx=5)
            ftxtField.grid(row=0, column=3, padx=5)
            toolTip.bind(ftxtField, 'Set value for '+frame.toolTipText)

        if(frame.distType.get()=='Uniform'):
            Min_label=ttk.Label(frame, text="Minimum:")
            Max_label=ttk.Label(frame, text="Maximum:")
            Min_textField=tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['min'])
            Max_textField=tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['max'])

            Min_label.grid(row=0, column=2, sticky='ew', padx=5)
            Min_textField.grid(row=0, column=3, sticky='e')
            toolTip.bind(Min_textField, 'Set Minimum '+frame.toolTipText)
            Max_label.grid(row=0, column=4, sticky='e', padx=5)
            Max_textField.grid(row=0, column=5, sticky='e')
            toolTip.bind(Max_textField, 'Set Maximum '+frame.toolTipText)

        if(frame.distType.get()=='Normal'):
            Mean_label=ttk.Label(frame, text="Mean:")
            Std_label=ttk.Label(frame, text="Standard Deviation:")
            Mean_txtField = tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['mean'])
            Std_textField = tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['std'])

            Mean_label.grid(row=0, column=2, sticky='e')
            Mean_txtField.grid(row=0, column=3, sticky='w', padx=5)
            toolTip.bind(Mean_txtField, 'Set Mean '+frame.toolTipText)
            Std_label.grid(row=0, column=4, sticky='e')
            Std_textField.grid(row=0, column=5, sticky='w', padx=5)
            toolTip.bind(Std_textField, 'Set Standard Deviation for '+frame.toolTipText)

        if(frame.distType.get()=='Lognormal'):
            Mean_label=ttk.Label(frame, text="Mean:")
            Std_label=ttk.Label(frame, text="Standard Deviation:")
            Mean_txtField = tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['mean'])
            Std_textField = tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['std'])

            Mean_label.grid(row=0, column=2, sticky='e')
            Mean_txtField.grid(row=0, column=3, sticky='w', padx=5)
            toolTip.bind(Mean_txtField, 'Set Mean '+frame.toolTipText)
            Std_label.grid(row=0, column=4, sticky='e')
            Std_textField.grid(row=0, column=5, sticky='w', padx=5)
            toolTip.bind(Std_textField, 'Set Standard Deviation for '+frame.toolTipText)

        if(frame.distType.get()=='Truncated'):
            Mean_label=ttk.Label(frame, text="Mean:")
            Std_label=ttk.Label(frame, text="Standard Deviation:")
            Mean_txtField = tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['mean'])
            Std_textField = tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['std'])
            Min_label=ttk.Label(frame, text="Minimum:")
            Max_label=ttk.Label(frame, text="Maximum:")
            Min_textField=tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['min'])
            Max_textField=tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['max'])

            Min_label.grid(row=0, column=2, sticky='ew', padx=5)
            Min_textField.grid(row=0, column=3, sticky='e')
            toolTip.bind(Min_textField, 'Set Minimum '+frame.toolTipText)
            Max_label.grid(row=0, column=4, sticky='e', padx=5)
            Max_textField.grid(row=0, column=5, sticky='e')
            toolTip.bind(Max_textField, 'Set Maximum '+frame.toolTipText)
            Mean_label.grid(row=0, column=6, sticky='e')
            Mean_txtField.grid(row=0, column=7, sticky='w', padx=5)
            toolTip.bind(Mean_txtField, 'Set Mean '+frame.toolTipText)
            Std_label.grid(row=0, column=8, sticky='e')
            Std_textField.grid(row=0, column=9, sticky='w', padx=5)
            toolTip.bind(Std_textField, 'Set Standard Deviation for '+frame.toolTipText)

        if(frame.distType.get()=='Triangular'):
            Min_label=ttk.Label(frame, text="Minimum:")
            Max_label=ttk.Label(frame, text="Maximum:")
            Mean_label=ttk.Label(frame, text="Mean:")
            Min_textField=tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['min'])
            Max_textField=tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['max'])
            Mean_txtField = tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['mean'])

            Min_label.grid(row=0, column=2, sticky='ew', padx=5)
            Min_textField.grid(row=0, column=3, sticky='e')
            toolTip.bind(Min_textField, 'Set Minimum '+frame.toolTipText)
            Max_label.grid(row=0, column=4, sticky='e', padx=5)
            Max_textField.grid(row=0, column=5, sticky='e')
            toolTip.bind(Max_textField, 'Set Maximum '+frame.toolTipText)
            Mean_label.grid(row=0, column=6, sticky='e')
            Mean_txtField.grid(row=0, column=7, sticky='w', padx=5)
            toolTip.bind(Mean_txtField, 'Set Mean '+frame.toolTipText)

        if(frame.distType.get()=='Discrete'):
            Values_label=ttk.Label(frame, text="Values:")
            Weights_label=ttk.Label(frame, text="Weights:")
            Values_textField=tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['values'])
            Weights_textField=tk.Entry(frame, textvariable=componentVars[frame.component]['Params'][frame.text]['weights'])

            Values_label.grid(row=0, column=2, sticky='ew', padx=5)
            Values_textField.grid(row=0, column=3, sticky='e')
            toolTip.bind(Values_textField, 'Set values for '+frame.toolTipText)
            Weights_label.grid(row=0, column=4, sticky='e', padx=5)
            Weights_textField.grid(row=0, column=5, sticky='e')
            toolTip.bind(Weights_textField, 'Set weights for '+frame.toolTipText)

    # loads the selected simulation into a global variable
    # then calls the add_component method to add any existing component models
    def load_sim(self, data):
        d['simName']=data['simName']

        for component in componentChoices:
            del componentVars[component]

            self.tabControl.connection_menu.children['menu'].delete(0, 'end')

            for c in connectionsDictionary:
                connectionTypes.append(c)
                self.tabControl.connection_menu.children['menu'].add_command(label=c, command=lambda con=c: connection.set(con))

            for tab in self.tabControl.tabs():
                if(self.tabControl.tab(tab, option="text") == component):
                    self.tabControl.forget(tab)

        i = len(componentChoices)-1
        while i>=0:
            componentChoices.pop(i)
            connectionTypes.pop(i)
            componentTypeDictionary.pop(i)
            connectionsDictionary.pop(i)
            connections.pop(i)
            i = i - 1

        componentVars['timeStep'].set(data['ModelParams']['TimeStep'])
        componentVars['endTime'].set(data['ModelParams']['EndTime'])
        componentVars['outputDirectory'].set(data['ModelParams']['OutputDirectory'])
        componentVars['analysis']['type'].set(data['ModelParams']['Analysis']['type'])
        componentVars['logging'].set(data['ModelParams']['Logging'])
        componentVars['RandomWellDomain']['xmin'].set(data['RandomWellDomain']['xmin'])
        componentVars['RandomWellDomain']['ymin'].set(data['RandomWellDomain']['ymin'])
        componentVars['RandomWellDomain']['xmax'].set(data['RandomWellDomain']['xmax'])
        componentVars['RandomWellDomain']['ymax'].set(data['RandomWellDomain']['ymax'])
        componentVars['datumPressure'].set(data['Stratigraphy']['pressure'])

        coordx = ''
        coordy = ''
        i=0

        for coordinate in data['WellboreLocations']['coordx']:
            if(i==0):
                coordx = str(data['WellboreLocations']['coordx'][i])
                coordy = str(data['WellboreLocations']['coordy'][i])
            if(i!=0):
                coordx = coordx + ', '+ str(data['WellboreLocations']['coordx'][i])
                coordy = coordy + ', '+ str(data['WellboreLocations']['coordy'][i])

            i+=1

        if(data['ModelParams']['Analysis']['type']=='LHS'):
            componentVars['analysis']['size'].set(data['ModelParams']['Analysis']['siz'])
            componentVars['analysis']['seed'].set(data['ModelParams']['Analysis']['seed'])

        if(data['ModelParams']['Analysis']['type']=='Prastudy'):
            componentVars['analysis']['size'].set(data['ModelParams']['Analysis']['siz'])

        try:
            componentVars['reservoirThickness']['Value'].set(data['Stratigraphy']['reservoirThickness']['value'])
        except:
            if(data['Stratigraphy']['reservoirThickness']['dist']=='Uniform'):
                componentVars['reservoirThickness']['min'].set(data['Stratigraphy']['reservoirThickness']['min'])
                componentVars['reservoirThickness']['max'].set(data['Stratigraphy']['reservoirThickness']['max'])

            if(data['Stratigraphy']['reservoirThickness']['dist']=='Normal'):
                componentVars['reservoirThickness']['mean'].set(data['Stratigraphy']['reservoirThickness']['mean'])
                componentVars['reservoirThickness']['std'].set(data['Stratigraphy']['reservoirThickness']['std'])

            if(data['Stratigraphy']['reservoirThickness']['dist']=='Lognormal'):
                componentVars['reservoirThickness']['mean'].set(data['Stratigraphy']['reservoirThickness']['mean'])
                componentVars['reservoirThickness']['std'].set(data['Stratigraphy']['reservoirThickness']['std'])

            if(data['Stratigraphy']['reservoirThickness']['dist']=='Truncated'):
                componentVars['reservoirThickness']['mean'].set(data['Stratigraphy']['reservoirThickness']['mean'])
                componentVars['reservoirThickness']['std'].set(data['Stratigraphy']['reservoirThickness']['std'])
                componentVars['reservoirThickness']['min'].set(data['Stratigraphy']['reservoirThickness']['min'])
                componentVars['reservoirThickness']['max'].set(data['Stratigraphy']['reservoirThickness']['max'])

            if(data['Stratigraphy']['reservoirThickness']['dist']=='Triangular'):
                componentVars['reservoirThickness']['std'].set(data['Stratigraphy']['reservoirThickness']['std'])
                componentVars['reservoirThickness']['min'].set(data['Stratigraphy']['reservoirThickness']['min'])
                componentVars['reservoirThickness']['max'].set(data['Stratigraphy']['reservoirThickness']['max'])

            if(data['Stratigraphy']['reservoirThickness']['dist']=='Discrete'):
                componentVars['reservoirThickness']['mean'].set(data['Stratigraphy']['reservoirThickness']['values'])
                componentVars['reservoirThickness']['std'].set(data['Stratigraphy']['reservoirThickness']['weights'])

        keys = data['Stratigraphy'].keys()
        akeys = []
        skeys = []

        for key in keys:
            if(key.find('aquifer')!=-1 and key != 'aquiferThickness'):
                akeys.append(key)
            if(key.find('shale')!=-1 and key !='shaleThickness'):
                skeys.append(key)

        for key in akeys:
            try:
                aquiferThickness[key]['value'].set(data['Stratigraphy'][key]['value'])
            except:
                if(data['Stratigraphy'][key]['dist']=='Uniform'):
                    aquiferThickness[key]['min'].set(data['Stratigraphy'][key]['min'])
                    aquiferThickness[key]['max'].set(data['Stratigraphy'][key]['max'])

                if(data['Stratigraphy'][key]['dist']=='Normal'):
                    aquiferThickness[key]['mean'].set(data['Stratigraphy'][key]['mean'])
                    aquiferThickness[key]['std'].set(data['Stratigraphy'][key]['std'])

                if(data['Stratigraphy'][key]['dist']=='Lognormal'):
                    aquiferThickness[key]['mean'].set(data['Stratigraphy'][key]['mean'])
                    aquiferThickness[key]['std'].set(data['Stratigraphy'][key]['std'])

                if(data['Stratigraphy'][key]['dist']=='Truncated'):
                    aquiferThickness[key]['mean'].set(data['Stratigraphy'][key]['mean'])
                    aquiferThickness[key]['std'].set(data['Stratigraphy'][key]['std'])
                    aquiferThickness[key]['min'].set(data['Stratigraphy'][key]['min'])
                    aquiferThickness[key]['max'].set(data['Stratigraphy'][key]['max'])

                if(data['Stratigraphy'][key]['dist']=='Triangular'):
                    aquiferThickness[key]['std'].set(data['Stratigraphy'][key]['std'])
                    aquiferThickness[key]['min'].set(data['Stratigraphy'][key]['min'])
                    aquiferThickness[key]['max'].set(data['Stratigraphy'][key]['max'])

                if(data['Stratigraphy'][key]['dist']=='Discrete'):
                    aquiferThickness[key]['mean'].set(data['Stratigraphy'][key]['values'])
                    aquiferThickness[key]['std'].set(data['Stratigraphy'][key]['weights'])

        for key in skeys:
            try:
                shaleThickness[key]['value'].set(data['Stratigraphy'][key]['value'])
            except:
                if(data['Stratigraphy'][key]['dist']=='Uniform'):
                    shaleThickness[key]['min'].set(data['Stratigraphy'][key]['min'])
                    shaleThickness[key]['max'].set(data['Stratigraphy'][key]['max'])

                if(data['Stratigraphy'][key]['dist']=='Normal'):
                    shaleThickness[key]['mean'].set(data['Stratigraphy'][key]['mean'])
                    shaleThickness[key]['std'].set(data['Stratigraphy'][key]['std'])

                if(data['Stratigraphy'][key]['dist']=='Lognormal'):
                    shaleThickness[key]['mean'].set(data['Stratigraphy'][key]['mean'])
                    shaleThickness[key]['std'].set(data['Stratigraphy'][key]['std'])

                if(data['Stratigraphy'][key]['dist']=='Truncated'):
                    shaleThickness[key]['mean'].set(data['Stratigraphy'][key]['mean'])
                    shaleThickness[key]['std'].set(data['Stratigraphy'][key]['std'])
                    shaleThickness[key]['min'].set(data['Stratigraphy'][key]['min'])
                    shaleThickness[key]['max'].set(data['Stratigraphy'][key]['max'])

                if(data['Stratigraphy'][key]['dist']=='Triangular'):
                    shaleThickness[key]['std'].set(data['Stratigraphy'][key]['std'])
                    shaleThickness[key]['min'].set(data['Stratigraphy'][key]['min'])
                    shaleThickness[key]['max'].set(data['Stratigraphy'][key]['max'])

                if(data['Stratigraphy'][key]['dist']=='Discrete'):
                    shaleThickness[key]['mean'].set(data['Stratigraphy'][key]['values'])
                    shaleThickness[key]['std'].set(data['Stratigraphy'][key]['weights'])

        ckeys = data['ModelParams']['Components']

        for key in ckeys:
            pkeys = data[key]['Parameters'].keys()

            try:
                aquiferName = data[key]['AquiferName']
                connection = data[key]['connection']
            except:
                aquiferName = 'none'
                connection = 'none'

            self.add_component(connection, aquiferName, self.tabControl, key, data[key]['type'], self.tabControl.connection_menu, self.tabControl.componentsSetupFrame, self, 1, 1)

            if(data[key]['type'].find('Wellbore')!=-1):
                componentVars[key]['number'].set(data[key]['number'])
                if(data[key]['type']=='openwellbore'):
                    componentVars[key]['LeakTo'].set(data[key]['LeakTo'])

            if(data[key]['type']=='LookupTableReservoir'):
                lookupTableParams['lookupTable_time_file'].set(data[key]['TimeFile'])
                lookupTableParams['lookupTable_param_file'].set(data[key]['ParameterFilename'])
                lookupTableParams['lookupTable_input_dir'].set(data[key]['FileDirectory'])

                for widget in self.LUTValuesFrame.winfo_children():
                    widget.destroy()

                dictionary = csv.DictReader(open(data[key]['ParameterFilename']))

                params = dictionary.fieldnames
                values = {}

                for param in params:
                    values[param]=[]
                    LUTWeights[param] = {}

                for row in dictionary:
                    for param in params:
                        try:
                            values[param].index(row[param])
                        except ValueError:
                            values[param].append(row[param])
                            LUTWeights[param][row[param]]=DoubleVar()
                            LUTWeights[param][row[param]].set(1.0)
                        else:
                            "Do nothing"

                del values[params[-1]]
                del LUTWeights[params[-1]]
                del params[-1]

                i = 0
                for param in params:
                    label = ttk.Label(self.LUTValuesFrame, text=param.strip()+":", font=LABEL_FONT)
                    label.grid(row=i, column=0, sticky='w', pady=5)

                    j = 0
                    for value in values[param]:
                        if(j>5):
                            i = i + 1
                            j=0

                        valuelabel = ttk.Label(self.LUTValuesFrame, text=value+":", font=DASHBOARD_FONT)
                        valuelabel.grid(row = i+1, column=j, sticky='w', padx=5, pady=2)

                        weightField = tk.Entry(self.LUTValuesFrame, textvariable=LUTWeights[param][value])
                        weightField.grid(row = i+1 , column = j+1, sticky='w')

                        j=j+2

                    i=i+2

                if('pressure' in data[key]['Outputs']):
                    lookupTableParams['pressure'].set(1)
                if('CO2saturation' in data[key]['Outputs']):
                    lookupTableParams['CO2saturation'].set(1)

                frame = self.frames[OpenIAM_Page]
                frame.tkraise()

                return

            for output in data[key]['Outputs']:
                if(data[key]['type']=='MultisegmentedWellbore'):
                    componentVars[key]['outputs'][output].set(1)
                else:
                    componentVars[key][output].set(1)

            if(data[key]['connection'] == 'Dynamic Parameters'):
                for pkey in data[key]['DynamicParameters'].keys():
                    componentVars[key]['DynamicParameters'] = {}
                    componentVars[key]['DynamicParameters'][pkey] = DoubleVar()
                    componentVars[key]['DynamicParameters'][pkey].set(data[key]['DynamicParameters'][pkey])

            for pkey in pkeys:
                if(pkey != 'pressure' and pkey != 'saturation' and pkey != 'brine_flux' and pkey != 'co2_flux' and pkey != 'ithresh' and pkey != 'logf'):
                    try:
                        componentVars[key]['Params'][pkey]['value'].set(data[key]['Parameters'][pkey]['value'])
                    except:
                        try:
                            if(data[key]['Parameters'][pkey]['dist']=='uniform'):
                                componentVars[key]['Params'][pkey]['distribution'].set('Uniform')
                                componentVars[key]['Params'][pkey]['min'].set(data[key]['Parameters'][pkey]['min'])
                                componentVars[key]['Params'][pkey]['max'].set(data[key]['Parameters'][pkey]['max'])

                            if(data[key]['Parameters'][pkey]['dist']=='lognorm'):
                                componentVars[key]['Params'][pkey]['distribution'].set('Lognormal')
                                componentVars[key]['Params'][pkey]['mean'].set(data[key]['Parameters'][pkey]['dist_pars'][0])
                                componentVars[key]['Params'][pkey]['std'].set(data[key]['Parameters'][pkey]['dist_pars'][2])

                            if(data[key]['Parameters'][pkey]['dist']=='triang'):
                                componentVars[key]['Params'][pkey]['distribution'].set('Triangular')
                                componentVars[key]['Params'][pkey]['std'].set(data[key]['Parameters'][pkey]['dist_pars'][0])
                                componentVars[key]['Params'][pkey]['min'].set(data[key]['Parameters'][pkey]['dist_pars'][1])
                                componentVars[key]['Params'][pkey]['max'].set(data[key]['Parameters'][pkey]['dist_pars'][2])

                            if(data[key]['Parameters'][pkey]['dist']=='norm'):
                                try:
                                    componentVars[key]['Params'][pkey]['distribution'].set('Normal')
                                    componentVars[key]['Params'][pkey]['mean'].set(data[key]['Parameters'][pkey]['mean'])
                                    componentVars[key]['Params'][pkey]['std'].set(data[key]['Parameters'][pkey]['std'])
                                except:
                                    componentVars[key]['Params'][pkey]['distribution'].set('Truncated')
                                    componentVars[key]['Params'][pkey]['mean'].set(data[key]['Parameters'][pkey]['mean'])
                                    componentVars[key]['Params'][pkey]['std'].set(data[key]['Parameters'][pkey]['std'])
                                    componentVars[key]['Params'][pkey]['min'].set(data[key]['Parameters'][pkey]['min'])
                                    componentVars[key]['Params'][pkey]['max'].set(data[key]['Parameters'][pkey]['max'])

                        except:
                            componentVars[key]['Params'][pkey]['distribution'].set('Discrete')
                            componentVars[key]['Params'][pkey]['values'].set(data[key]['Parameters'][pkey]['discrete_vals'][0])
                            componentVars[key]['Params'][pkey]['weights'].set(data[key]['Parameters'][pkey]['discrete_vals'][1])

                if(pkey == 'ithresh'):
                    if(data[key]['Parameters'][pkey] == 2):
                        componentVars[key][pkey].set('No Impact')
                    if(data[key]['Parameters'][pkey] == 1):
                        componentVars[key][pkey].set('MCL')
                    break

                if(pkey == 'logf'):
                    if(data[key]['Parameters'][pkey] == 0):
                        componentVars[key][pkey].set('Linear')
                    if(data[key]['Parameters'][pkey] == 1):
                        componentVars[key][pkey].set('Log')
                    break

                self.DistributionChange(self.nametowidget(self.getvar(pkey)))

        frame = self.frames[OpenIAM_Page]
        frame.tkraise()

    # method to navigate through all frames that can be imported into the
    # class being navigated from
    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

    # method to allow proper navigation to and from dashboard
    def showDashboard(self):
        frame = self.frames[Dashboard_Page]
        frame.tkraise()

    # sets the outputDir value in the output dictionary
    def chooseOutputDir(self, outputDir):
        from tkinter.filedialog import askdirectory
        fileDialog = tk.Tk()
        fileDialog.withdraw()

        try:
            dirname = askdirectory(initialdir=outputDir.get(), title="Choose Directory To Save Outputs")
        except:
            fileDialog.destroy()
        else:
            if(dirname!=''):
                outputDir.set(dirname)

            fileDialog.destroy()

    # reads the parameters file selected and then creates the inputs for the
    # weights for each value that exists for each possible variable name
    def chooseParametersFilenameFile(self, lookupTableParams, LUTValuesFrame, LUTWeights):
        from tkinter.filedialog import askopenfilename
        valuesarray={}
        fileDialog=tk.Tk()

        fileDialog.withdraw()

        try:
            timefilename = askopenfilename(initialdir=lookupTableParams['lookupTable_input_dir'].get(), title="Choose Time Points File")
            dictionary = csv.DictReader(open(timefilename))
        except:
            fileDialog.destroy()
        else:
            for widget in LUTValuesFrame.winfo_children():
                widget.destroy()

            keys = dictionary.fieldnames
            values = {}

            for key in keys:
                values[key]=[]
                LUTWeights[key] = {}

            for row in dictionary:
                for key in keys:
                    try:
                        values[key].index(row[key])
                    except ValueError:
                        values[key].append(row[key])
                        LUTWeights[key][row[key]]=DoubleVar()
                        LUTWeights[key][row[key]].set(1.0)
                    else:
                        "Do nothing"

            del values[keys[-1]]
            del LUTWeights[keys[-1]]
            del keys[-1]

            i = 0
            for key in keys:
                label = ttk.Label(LUTValuesFrame, text=key.strip()+":", font=LABEL_FONT)
                label.grid(row=i, column=0, sticky='w', pady=5)

                j = 0
                for value in values[key]:
                    if(j>5):
                        i = i + 1
                        j=0

                    valuelabel = ttk.Label(LUTValuesFrame, text=value+":", font=DASHBOARD_FONT)
                    valuelabel.grid(row = i+1, column=j, sticky='w', padx=5, pady=2)

                    weightField = tk.Entry(LUTValuesFrame, textvariable=LUTWeights[key][value])
                    weightField.grid(row = i+1 , column = j+1, sticky='w')

                    j=j+2

                i=i+2

            lookupTableParams['lookupTable_param_file'].set(timefilename)
            fileDialog.destroy()

    # sets the input directory for the lookuptable component model
    def chooseInputDir(self, lookupTableParams):
        from tkinter.filedialog import askdirectory
        fileDialog = tk.Tk()

        fileDialog.withdraw()
        try:
            directory = askdirectory(initialdir=lookupTableParams['lookupTable_input_dir'].get(), title="Choose Input Directory")
        except:
            fileDialog.destroy()
        else:
            if(directory != ''):
                lookupTableParams['lookupTable_input_dir'].set(directory)

            fileDialog.destroy()

    # sets the time points file for the lookuptable component model
    def chooseTimePointsFile(self, lookupTableParams):
        from tkinter.filedialog import askopenfilename
        fileDialog = tk.Tk()

        fileDialog.withdraw()
        try:
            parametersfilename = askopenfilename(initialdir=lookupTableParams['lookupTable_input_dir'].get(), title="Choose Params File")
        except:
            fileDialog.destroy()
        else:
            if(parametersfilename != ''):
                lookupTableParams['lookupTable_time_file'].set(parametersfilename)

            fileDialog.destroy()

    # defines Disclaimer page to allow change frame to work properly
    def Disclaimer_Page(self):
        Disclaimer_Page(tk.Frame)

    # defines dashboard page to allow change frame to work properly and also
    # show dashboard
    def Dashboard_Page(self):
        Dashboard_Page(tk.Frame)

    # defines postprocessing page to allow change frame to work properly
    def PostPocessor_Page(self):
        PostPocessor_Page(tk.Frame)

    # defines open iam page to allow change frame to work properly
    def OpenIAM_Page(self):
        OpenIAM_Page(tk.Frame)

    # removes the component model that is currently displayed on screen
    def removeComponent(self, tab, tabControl, connection_menu):
        MsgBox = messagebox.askquestion("Confirm Removal", "Click yes to Confirm Removal and No to Keep Component")

        if MsgBox == 'yes':
            del componentVars[tabControl.tab(tab)['text']]
            index = componentChoices.index(tabControl.tab(tab)['text'])

            connectionTypes = []

            componentChoices.pop(index)
            componentTypeDictionary.pop(index)
            connectionsDictionary.pop(index)
            connections.pop(index+1)

            connection_menu.children['menu'].delete(0, 'end')

            for c in connectionsDictionary:
                connectionTypes.append(c)
                connection_menu.children['menu'].add_command(label=c, command=lambda con=c: connection.set(con))

            tabControl.select('.!frame.!openiam_page.!notebook.!frame3')
            self.connection.set(connections[0])
            self.componentType.set(componentTypes[0])

            for widget in tabControl.nametowidget('.!frame.!openiam_page.!notebook.!frame3.!frame').winfo_children():
                widget.destroy()

            tabControl.forget(tab)
        else:
            return

    # adds component model to simulation based on type determines what params
    # are added and what values are set
    def add_component(self, conn, aqName, tabControl, compName, compType, connection_menu, componentsSetupFrame, controller, press, sat):
        toolTip = Pmw.Balloon(self)
        newTab = ttk.Frame(tabControl, padding=10)
        tabType = ttk.Frame(newTab, padding=10)
        componentName = StringVar()
        componentType = StringVar()
        connection = StringVar()
        pressure = StringVar()
        saturation = StringVar()
        aquiferName = StringVar()

        try:
            tabControl.add(newTab, text=compName.get())
            componentName.set(compName.get())
            componentType.set(compType.get())
            connection.set(conn.get())
            pressure.set(press.get())
            saturation.set(sat.get())
            aquiferName.set(aqName.get())
        except:
            tabControl.add(newTab, text=compName)
            componentName.set(compName)
            componentType.set(compType)
            connection.set(conn)
            pressure.set(press)
            saturation.set(sat)
            aquiferName.set(aqName)

        tabControl.pack(expand=1, fill="both")
        tabType.grid(row=0, column=0, columnspan=10)

        addNextComponentButton = tk.Button(newTab, text='Add another Component', command=lambda :tabControl.select('.!frame.!openiam_page.!notebook.!frame3'))
        addNextComponentButton.grid(row=2, column=11)
        toolTip.bind(addNextComponentButton, 'Return to add Componet Panel.')

        removeComponentButton = tk.Button(newTab, text='Remove this Component', command=lambda: controller.removeComponent(newTab, tabControl, connection_menu))
        removeComponentButton.grid(row=2, column=5)
        toolTip.bind(removeComponentButton, 'Remove Current Componet Model and Return to Add Component Tab.')

        if(componentName.get()==''):
            tabControl.forget(newTab)
            messagebox.showerror("Error", "You must enter a Unique name for each Component Model.")
            return

        try:
            componentChoices.index(componentName.get())
        except ValueError:
            if(componentType.get()=='Simple Reservoir' or componentType.get()=='SimpleReservoir'):
                componentChoices.append(componentName.get())
                componentTypeDictionary.append('simplereservoir')
                connectionsDictionary.append('none')

                componentVars[componentName.get()]={}
                componentVars[componentName.get()]['componentName']=StringVar()
                componentVars[componentName.get()]['componentType']=StringVar()

                componentVars[componentName.get()]['componentName']=componentName.get()
                componentVars[componentName.get()]['componentType']=componentType.get()

                componentVars[componentName.get()]['Params']={}
                componentVars[componentName.get()]['Params']['logResPerm']={}
                componentVars[componentName.get()]['Params']['reservoirPorosity']={}
                componentVars[componentName.get()]['Params']['brineDensity']={}
                componentVars[componentName.get()]['Params']['brineViscosity']={}
                componentVars[componentName.get()]['Params']['CO2Density']={}
                componentVars[componentName.get()]['Params']['CO2Viscosity']={}
                componentVars[componentName.get()]['Params']['brineResSaturation']={}
                componentVars[componentName.get()]['Params']['compressibility']={}
                componentVars[componentName.get()]['Params']['injRate']={}

                componentVars[componentName.get()]['Params']['logResPerm']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['injRate']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['reservoirPorosity']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['brineDensity']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Density']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['compressibility']['distribution']=StringVar()

                componentVars[componentName.get()]['Params']['logResPerm']['values']=StringVar()
                componentVars[componentName.get()]['Params']['reservoirPorosity']['values']=StringVar()
                componentVars[componentName.get()]['Params']['brineDensity']['values']=StringVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['values']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Density']['values'] =StringVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['values']=StringVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['values']=StringVar()
                componentVars[componentName.get()]['Params']['compressibility']['values']=StringVar()
                componentVars[componentName.get()]['Params']['injRate']['values']=StringVar()

                componentVars[componentName.get()]['Params']['logResPerm']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['reservoirPorosity']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['brineDensity']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Density']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['compressibility']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['injRate']['weights']=StringVar()

                componentVars[componentName.get()]['Params']['logResPerm']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['reservoirPorosity']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['brineDensity']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['brineViscosity']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['CO2Density']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['CO2Viscosity']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['brineResSaturation']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['compressibility']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['injRate']['distribution'].set(distributionOptions[0])

                componentVars[componentName.get()]['Params']['logResPerm']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['reservoirPorosity']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineDensity']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Density']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['compressibility']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['injRate']['value'] = DoubleVar()

                componentVars[componentName.get()]['Params']['logResPerm']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['reservoirPorosity']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineDensity']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Density']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['compressibility']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['injRate']['max'] = DoubleVar()

                componentVars[componentName.get()]['Params']['logResPerm']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['reservoirPorosity']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineDensity']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Density']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['compressibility']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['injRate']['min'] = DoubleVar()

                componentVars[componentName.get()]['Params']['logResPerm']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['reservoirPorosity']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineDensity']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Density']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['compressibility']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['injRate']['mean'] = DoubleVar()

                componentVars[componentName.get()]['Params']['logResPerm']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['reservoirPorosity']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineDensity']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Density']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['compressibility']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['injRate']['std'] = DoubleVar()

                componentVars[componentName.get()]['pressure'] = BooleanVar()
                componentVars[componentName.get()]['CO2saturation'] = BooleanVar()
                componentVars[componentName.get()]['mass_CO2_reservoir'] = BooleanVar()

                componentVars[componentName.get()]['Params']['logResPerm']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['reservoirPorosity']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineDensity']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineViscosity']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['CO2Density']['values'] .set('15, 21')
                componentVars[componentName.get()]['Params']['CO2Viscosity']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineResSaturation']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['compressibility']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['injRate']['values'].set('15, 21')

                componentVars[componentName.get()]['Params']['logResPerm']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['reservoirPorosity']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineDensity']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineViscosity']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['CO2Density']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['CO2Viscosity']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineResSaturation']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['compressibility']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['injRate']['weights'].set('15, 21')

                componentVars[componentName.get()]['Params']['logResPerm']['value'].set(-12)
                componentVars[componentName.get()]['Params']['reservoirPorosity']['value'].set(0.3)
                componentVars[componentName.get()]['Params']['brineDensity']['value'].set(1000)
                componentVars[componentName.get()]['Params']['brineViscosity']['value'].set(2.535e-3)
                componentVars[componentName.get()]['Params']['CO2Density']['value'].set(479)
                componentVars[componentName.get()]['Params']['CO2Viscosity']['value'].set(3.95e-5)
                componentVars[componentName.get()]['Params']['brineResSaturation']['value'].set(0.1)
                componentVars[componentName.get()]['Params']['compressibility']['value'].set(5.1e-11)
                componentVars[componentName.get()]['Params']['injRate']['value'].set(0.1)

                componentVars[componentName.get()]['Params']['logResPerm']['min'].set(-14)
                componentVars[componentName.get()]['Params']['reservoirPorosity']['min'].set(0.01)
                componentVars[componentName.get()]['Params']['brineDensity']['min'].set(900)
                componentVars[componentName.get()]['Params']['brineViscosity']['min'].set(1.0e-4)
                componentVars[componentName.get()]['Params']['CO2Density']['min'].set(100)
                componentVars[componentName.get()]['Params']['CO2Viscosity']['min'].set(1.0e-4)
                componentVars[componentName.get()]['Params']['brineResSaturation']['min'].set(0)
                componentVars[componentName.get()]['Params']['compressibility']['min'].set(5.0e-11)
                componentVars[componentName.get()]['Params']['injRate']['min'].set(1.0e-3)

                componentVars[componentName.get()]['Params']['logResPerm']['max'].set(-9)
                componentVars[componentName.get()]['Params']['reservoirPorosity']['max'].set(1)
                componentVars[componentName.get()]['Params']['brineDensity']['max'].set(1500)
                componentVars[componentName.get()]['Params']['brineViscosity']['max'].set(5.0e-3)
                componentVars[componentName.get()]['Params']['CO2Density']['max'].set(1500)
                componentVars[componentName.get()]['Params']['CO2Viscosity']['max'].set(1.0e-4)
                componentVars[componentName.get()]['Params']['brineResSaturation']['max'].set(0.7)
                componentVars[componentName.get()]['Params']['compressibility']['max'].set(1.0e-9)
                componentVars[componentName.get()]['Params']['injRate']['max'].set(10)

                componentVars[componentName.get()]['Params']['logResPerm']['mean'].set(0)
                componentVars[componentName.get()]['Params']['reservoirPorosity']['mean'].set(0)
                componentVars[componentName.get()]['Params']['brineDensity']['mean'].set(0)
                componentVars[componentName.get()]['Params']['brineViscosity']['mean'].set(0)
                componentVars[componentName.get()]['Params']['CO2Density']['mean'].set(0)
                componentVars[componentName.get()]['Params']['CO2Viscosity']['mean'].set(0)
                componentVars[componentName.get()]['Params']['brineResSaturation']['mean'].set(0)
                componentVars[componentName.get()]['Params']['compressibility']['mean'].set(0)
                componentVars[componentName.get()]['Params']['injRate']['mean'].set(0)

                componentVars[componentName.get()]['Params']['logResPerm']['std'].set(1)
                componentVars[componentName.get()]['Params']['reservoirPorosity']['std'].set(1)
                componentVars[componentName.get()]['Params']['brineDensity']['std'].set(1)
                componentVars[componentName.get()]['Params']['brineViscosity']['std'].set(1)
                componentVars[componentName.get()]['Params']['CO2Density']['std'].set(1)
                componentVars[componentName.get()]['Params']['CO2Viscosity']['std'].set(1)
                componentVars[componentName.get()]['Params']['brineResSaturation']['std'].set(1)
                componentVars[componentName.get()]['Params']['compressibility']['std'].set(1)
                componentVars[componentName.get()]['Params']['injRate']['std'].set(1)

                componentVars[componentName.get()]['pressure'].set(0)
                componentVars[componentName.get()]['CO2saturation'].set(0)
                componentVars[componentName.get()]['mass_CO2_reservoir'].set(0)

                simpleReservoir_label = ttk.Label(tabType, text="Simple Reservoir:", font=LABEL_FONT)
                simpleReservoir_label.grid(row=0, column=0, sticky='w')

                logResPerm = tk.Frame(tabType)
                logResPerm.grid(row=1, column=0, sticky='w', columnspan=100, padx=15)
                logResPerm.text="logResPerm"
                logResPerm.labelText="Reservoir Permeability: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]"
                logResPerm.component=componentVars[componentName.get()]['componentName']
                logResPerm.distType=componentVars[componentName.get()]['Params']['logResPerm']['distribution']
                logResPerm_label = ttk.Label(logResPerm, text="Reservoir Permeability: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]", width=30)
                logResPermvalue_label=ttk.Label(logResPerm, text='Value:')
                logResPerm_menu = tk.OptionMenu(logResPerm, componentVars[componentName.get()]['Params']['logResPerm']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(logResPerm))
                logResPerm_txtField = tk.Entry(logResPerm, textvariable=componentVars[componentName.get()]['Params']['logResPerm']['value'])
                logResPerm.toolTipText = 'Reservoir Permeablity'

                logResPerm_menu.config(width=15)
                toolTip.bind(logResPerm_menu, 'Select Distriubtion for Reservoir Permeabilty.')
                logResPerm_label.grid(row=0, column=0, sticky='e', padx=5)
                logResPerm_menu.grid(row=0, column=1, sticky='ew', padx=5)
                logResPermvalue_label.grid(row=0, column=2, sticky='ew', padx=5)
                logResPerm_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(logResPerm_txtField, 'Set value for Reservoir Permeablity.')
                controller.setvar(name='logResPerm',value=logResPerm)

                reservoirPorosity = tk.Frame(tabType)
                reservoirPorosity.grid(row=2, column=0, sticky='w', columnspan=100, padx=15)

                reservoirPorosity_label = ttk.Label(reservoirPorosity, text="Porosity of reservoir: [-]", width=30)
                reservoirPorosityvalue_label=ttk.Label(reservoirPorosity, text='Value:')
                reservoirPorosity_menu = tk.OptionMenu(reservoirPorosity, componentVars[componentName.get()]['Params']['reservoirPorosity']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(reservoirPorosity))
                reservoirPorosity_txtField = tk.Entry(reservoirPorosity, textvariable=componentVars[componentName.get()]['Params']['reservoirPorosity']['value'])
                reservoirPorosity.text="reservoirPorosity"
                reservoirPorosity.labelText='Porosity of reservoir: [-]'
                reservoirPorosity.component=componentVars[componentName.get()]['componentName']
                reservoirPorosity.distType=componentVars[componentName.get()]['Params']['reservoirPorosity']['distribution']
                reservoirPorosity.toolTipText = 'Reservoir Porosity'

                reservoirPorosity_menu.config(width=15)
                toolTip.bind(reservoirPorosity_menu, 'Select Distriubtion '+reservoirPorosity.toolTipText+'.')
                reservoirPorosity_label.grid(row=0, column=0, sticky='e', padx=5)
                reservoirPorosity_menu.grid(row=0, column=1, sticky='ew', padx=5)
                reservoirPorosityvalue_label.grid(row=0, column=2, sticky='ew', padx=5)
                reservoirPorosity_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(reservoirPorosity_txtField, 'Set value '+reservoirPorosity.toolTipText+'.')
                controller.setvar(name='reservoirPorosity', value=reservoirPorosity)

                brineDensity = tk.Frame(tabType)
                brineDensity.grid(row=3, column=0, sticky='w', columnspan=100, padx=15)

                brineDensity_label = ttk.Label(brineDensity, text="Brine Density: [kg/m"+str(u'\u00B3')+"]", width=30)
                brineDensity_menu = tk.OptionMenu(brineDensity, componentVars[componentName.get()]['Params']['brineDensity']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(brineDensity))
                brineDensity_value_label=ttk.Label(brineDensity, text='Value:')
                brineDensity_txtField = tk.Entry(brineDensity, textvariable=componentVars[componentName.get()]['Params']['brineDensity']['value'])
                brineDensity.text="brineDensity"
                brineDensity.labelText="Brine Density: [kg/m"+str(u'\u00B3')+"]"
                brineDensity.component=componentVars[componentName.get()]['componentName']
                brineDensity.distType=componentVars[componentName.get()]['Params']['brineDensity']['distribution']
                brineDensity.toolTipText = 'Brine Density'

                brineDensity_menu.config(width=15)
                toolTip.bind(brineDensity_menu, 'Select Distriubtion '+brineDensity.toolTipText+'.')
                brineDensity_label.grid(row=0, column=0, sticky='e', padx=5)
                brineDensity_menu.grid(row=0, column=1, sticky='ew', padx=5)
                brineDensity_value_label.grid(row=0, column=2, sticky='ew', padx=5)
                brineDensity_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(brineDensity_txtField, 'Set value '+brineDensity.toolTipText+'.')
                controller.setvar(name='brineDensity', value=brineDensity)

                CO2Density = tk.Frame(tabType)
                CO2Density.grid(row=4, column=0, sticky='w', columnspan=100, padx=15)

                CO2Density_label = ttk.Label(CO2Density, text="CO"+str(u'\u2082')+" Density: [kg/m"+str(u'\u00B3')+"]", width=30)
                CO2Density_menu = tk.OptionMenu(CO2Density, componentVars[componentName.get()]['Params']['CO2Density']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(CO2Density))
                CO2Density_value_label=ttk.Label(CO2Density, text='Value:')
                CO2Density_txtField = tk.Entry(CO2Density, textvariable=componentVars[componentName.get()]['Params']['CO2Density']['value'])
                CO2Density.text="CO2Density"
                CO2Density.labelText="CO"+str(u'\u2082')+"  Density: [kg/m"+str(u'\u00B3')+"]"
                CO2Density.component=componentVars[componentName.get()]['componentName']
                CO2Density.distType=componentVars[componentName.get()]['Params']['CO2Density']['distribution']
                CO2Density.toolTipText = "CO"+str(u'\u2082')+" Density"

                CO2Density_menu.config(width=15)
                toolTip.bind(CO2Density_menu, 'Select Distriubtion '+CO2Density.toolTipText+'.')
                CO2Density_label.grid(row=0, column=0, sticky='e', padx=5)
                CO2Density_menu.grid(row=0, column=1, sticky='ew', padx=5)
                CO2Density_value_label.grid(row=0, column=2, sticky='ew', padx=5)
                CO2Density_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(CO2Density_txtField, 'Set value '+CO2Density.toolTipText+'.')
                controller.setvar(name='CO2Density', value=CO2Density)

                brineViscosity = tk.Frame(tabType)
                brineViscosity.grid(row=5, column=0, sticky='w', columnspan=100, padx=15)

                brineViscosity_label = ttk.Label(brineViscosity, text="Brine Viscosity: [Pa"+str(u'\u22C5')+"s]", width=30)
                brineViscosity_menu = tk.OptionMenu(brineViscosity, componentVars[componentName.get()]['Params']['brineViscosity']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(brineViscosity))
                brineViscosity_value_label=ttk.Label(brineViscosity, text='Value:')
                brineViscosity_txtField = tk.Entry(brineViscosity, textvariable=componentVars[componentName.get()]['Params']['brineViscosity']['value'])
                brineViscosity.text="brineViscosity"
                brineViscosity.labelText='Brine Viscosity: [Pa'+str(u'\u22C5')+'s]'
                brineViscosity.component=componentVars[componentName.get()]['componentName']
                brineViscosity.distType=componentVars[componentName.get()]['Params']['brineViscosity']['distribution']
                brineViscosity.toolTipText = 'Brine Viscosity'

                brineViscosity_menu.config(width=15)
                toolTip.bind(brineViscosity_menu, 'Select Distriubtion '+brineViscosity.toolTipText+'.')
                brineViscosity_label.grid(row=0, column=0, sticky='e', padx=5)
                brineViscosity_menu.grid(row=0, column=1, sticky='ew', padx=5)
                brineViscosity_value_label.grid(row=0, column=2, sticky='ew', padx=5)
                brineViscosity_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(brineViscosity_txtField, 'Set value '+brineViscosity.toolTipText+'.')
                controller.setvar(name='brineViscosity', value=brineViscosity)

                CO2Viscosity = tk.Frame(tabType)
                CO2Viscosity.grid(row=6, column=0, sticky='w', columnspan=100, padx=15)

                CO2Viscosity_label = ttk.Label(CO2Viscosity, text="CO"+str(u'\u2082')+" Viscosity: [Pa"+str(u'\u22C5')+"s]", width=30)
                CO2Viscosity_menu = tk.OptionMenu(CO2Viscosity, componentVars[componentName.get()]['Params']['CO2Viscosity']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(CO2Viscosity))
                CO2Viscosity_value_label=ttk.Label(CO2Viscosity, text='Value:')
                CO2Viscosity_txtField = tk.Entry(CO2Viscosity, textvariable=componentVars[componentName.get()]['Params']['CO2Viscosity']['value'])
                CO2Viscosity.text="CO2Viscosity"
                CO2Viscosity.labelText="CO"+str(u'\u2082')+" Viscosity: [Pa"+str(u'\u22C5')+"s]"
                CO2Viscosity.component=componentVars[componentName.get()]['componentName']
                CO2Viscosity.distType=componentVars[componentName.get()]['Params']['CO2Viscosity']['distribution']
                CO2Viscosity.toolTipText = "CO"+str(u'\u2082')+" Viscosity"

                CO2Viscosity_menu.config(width=15)
                toolTip.bind(CO2Viscosity_menu, 'Select Distriubtion '+CO2Viscosity.toolTipText+'.')
                CO2Viscosity_label.grid(row=0, column=0, sticky='e', padx=5)
                CO2Viscosity_menu.grid(row=0, column=1, sticky='ew', padx=5)
                CO2Viscosity_value_label.grid(row=0, column=2, sticky='ew', padx=5)
                CO2Viscosity_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(CO2Viscosity_txtField, 'Set value '+CO2Viscosity.toolTipText +'.')
                controller.setvar(name='CO2Viscosity', value=CO2Viscosity)

                brineResSaturation = tk.Frame(tabType)
                brineResSaturation.grid(row=7, column=0, sticky='w', columnspan=100, padx=15)

                brineResSaturation_label = ttk.Label(brineResSaturation, text="Brine Saturation: [-]", width=30)
                brineResSaturation_menu = tk.OptionMenu(brineResSaturation, componentVars[componentName.get()]['Params']['brineResSaturation']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(brineResSaturation))
                brineResSaturation_value_label=ttk.Label(brineResSaturation, text='Value:')
                brineResSaturation_txtField = tk.Entry(brineResSaturation, textvariable=componentVars[componentName.get()]['Params']['brineResSaturation']['value'])
                brineResSaturation.text="brineResSaturation"
                brineResSaturation.labelText='Brine Saturation: [-]'
                brineResSaturation.component=componentVars[componentName.get()]['componentName']
                brineResSaturation.distType=componentVars[componentName.get()]['Params']['brineResSaturation']['distribution']
                brineResSaturation.toolTipText = 'Brine Saturation'

                brineResSaturation_menu.config(width=15)
                toolTip.bind(brineResSaturation_menu, 'Select Distriubtion '+brineResSaturation.toolTipText+'.')
                brineResSaturation_label.grid(row=0, column=0, sticky='e', padx=5)
                brineResSaturation_menu.grid(row=0, column=1, sticky='ew', padx=5)
                brineResSaturation_value_label.grid(row=0, column=2, sticky='ew', padx=5)
                brineResSaturation_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(brineResSaturation_txtField, 'Set value '+brineResSaturation.toolTipText+'.')
                controller.setvar(name='brineResSaturation', value=brineResSaturation)

                compressibility = tk.Frame(tabType)
                compressibility.grid(row=8, column=0, sticky='w', columnspan=100, padx=15)

                compressibility_label = ttk.Label(compressibility, text="Compressibility: [Pa"+str(u'\u207B'u'\u2071')+"]", width=30)
                compressibility_menu = tk.OptionMenu(compressibility, componentVars[componentName.get()]['Params']['compressibility']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(compressibility))
                compressibility_value_label=ttk.Label(compressibility, text='Value:')
                compressibility_txtField = tk.Entry(compressibility, textvariable=componentVars[componentName.get()]['Params']['compressibility']['value'])
                compressibility.text="compressibility"
                compressibility.labelText='Compressibility: [Pa'+str(u'\u207B'u'\u2071')+']'
                compressibility.component=componentVars[componentName.get()]['componentName']
                compressibility.distType=componentVars[componentName.get()]['Params']['compressibility']['distribution']
                compressibility.toolTipText = 'Compressibility'

                compressibility_menu.config(width=15)
                toolTip.bind(compressibility_menu, 'Select Distriubtion '+compressibility.toolTipText+'.')
                compressibility_label.grid(row=0, column=0, sticky='e', padx=5)
                compressibility_menu.grid(row=0, column=1, sticky='ew', padx=5)
                compressibility_value_label.grid(row=0, column=2, sticky='ew', padx=5)
                compressibility_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(compressibility_txtField, 'Set value '+compressibility.toolTipText+'.')
                controller.setvar(name='compressibility', value=compressibility)

                injRate = tk.Frame(tabType)
                injRate.grid(row=9, column=0, sticky='w', columnspan=100, padx=15)

                injRate_label = ttk.Label(injRate, text="CO"+str(u'\u2082')+" injection rate: [m"+str(u'\u00B3')+"/s]", width=30)
                injRate_menu = tk.OptionMenu(injRate, componentVars[componentName.get()]['Params']['injRate']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(injRate))
                injRate_value_label=ttk.Label(injRate, text='Value:')
                injRate_txtField = tk.Entry(injRate, textvariable=componentVars[componentName.get()]['Params']['injRate']['value'])
                injRate.text="injRate"
                injRate.labelText="CO"+str(u'\u2082')+" injection rate:"
                injRate.component=componentVars[componentName.get()]['componentName']
                injRate.distType=componentVars[componentName.get()]['Params']['injRate']['distribution']
                injRate.toolTipText = "CO"+str(u'\u2082')+" injection rate"

                injRate_menu.config(width=15)
                toolTip.bind(injRate_menu, 'Select Distriubtion '+injRate.toolTipText+'.')
                injRate_label.grid(row=0, column=0, sticky='e', padx=5)
                injRate_menu.grid(row=0, column=1, sticky='ew', padx=5)
                injRate_value_label.grid(row=0, column=2, sticky='ew', padx=5)
                injRate_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(injRate_txtField, 'Set value '+injRate.toolTipText+'.')
                controller.setvar(name='injRate', value=injRate)

                simpleReservoir_label = ttk.Label(tabType, text="Outputs:", font=LABEL_FONT)
                simpleReservoir_label.grid(row=10, column=0, sticky='w')

                outputFrame = ttk.Frame(tabType, padding=10)
                outputFrame.grid(row=11, column=0, sticky='w', padx=15)

                simple_reservior_output_pressure_label = ttk.Label(outputFrame, text="Pressure: [Pa]")
                simple_reservior_output_pressure_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['pressure'])
                simple_reservior_output_pressure_label.grid(row=1, column=0, pady=5, sticky='e')
                simple_reservior_output_pressure_checkbox.grid(row=1, column=1, pady=5, sticky='w')
                toolTip.bind(simple_reservior_output_pressure_checkbox, 'Enable pressure output.')

                simple_reservior_output_co2sat_label = ttk.Label(outputFrame, text="CO"+str(u'\u2082')+" Saturation:")
                simple_reservior_output_co2sat_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['CO2saturation'])
                simple_reservior_output_co2sat_label.grid(row=1, column=2, pady=5, sticky='e')
                simple_reservior_output_co2sat_checkbox.grid(row=1, column=3, pady=5, sticky='w')
                toolTip.bind(simple_reservior_output_co2sat_checkbox, 'Enable CO'+str(u'\u2082')+' Saturation as Output')

                simple_reservior_output_mass_co2_reservoir_label = ttk.Label(outputFrame, text="Mass CO"+str(u'\u2082')+" [kg]:")
                simple_reservior_output_mass_co2_reservoir_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['mass_CO2_reservoir'])
                simple_reservior_output_mass_co2_reservoir_label.grid(row=1, column=4, pady=5, sticky='e')
                simple_reservior_output_mass_co2_reservoir_checkbox.grid(row=1, column=5, pady=5, sticky='w')
                toolTip.bind(simple_reservior_output_mass_co2_reservoir_checkbox, 'Enable CO'+str(u'\u2082')+' mass as Output.')

            if(componentType.get()=='Lookup Table Reservoir' or componentType.get()=='LookupTableReservoir'):
                componentChoices.append(componentName.get())
                componentTypeDictionary.append('lookupTable')
                connectionsDictionary.append(connection.get())

                componentVars[componentName.get()] = {}
                lookupTableParams['pressure'] = BooleanVar()
                lookupTableParams['CO2saturation'] = BooleanVar()
                lookupTableParams['componentName']=StringVar()
                lookupTableParams['componentType']=StringVar()

                lookupTableParams['componentName']=componentName.get()
                lookupTableParams['componentType']=componentType.get()
                lookupTableParams['pressure'].set(0)
                lookupTableParams['CO2saturation'].set(0)

                valuesFrame = ttk.Frame(tabType)
                valuesFrame.grid(row=4, column=0, columnspan=20, sticky='w')

                valuesCanvas = tk.Canvas(valuesFrame, relief=tk.SUNKEN)
                valuesCanvas.config(width=1024, height=150)
                valuesCanvas.config(scrollregion=(0, 0, 1500, 3000))
                valuesCanvas.config(highlightthickness=0)

                valuesybar = tk.Scrollbar(valuesFrame)
                valuesybar.config(command=valuesCanvas.yview)

                valuesCanvas.config(yscrollcommand=valuesybar.set)
                valuesybar.pack(side=tk.RIGHT, fill=tk.Y)
                valuesCanvas.pack(side=tk.LEFT)

                self.LUTValuesFrame = tk.Frame(valuesCanvas)
                valuesCanvas.create_window((10,0), window=self.LUTValuesFrame, anchor='nw')

                lookupTableTab_label = ttk.Label(tabType, text="Lookup Table Component:", font=LABEL_FONT)
                lookupTableTab_label.grid(row=0, column=0, sticky='w')

                lookupTable_file_dir_label = ttk.Label(tabType, text="Directory of Input Files:", width=25)
                lookupTable_file_dir_textfile = tk.Entry(tabType, textvariable=lookupTableParams['lookupTable_input_dir'])
                lookupTable_file_dir_button = ttk.Button(tabType, text="Browse", command=lambda:controller.chooseInputDir(lookupTableParams))
                lookupTable_file_dir_label.grid(row=1, column=0, sticky='w', pady=5, padx=5)
                lookupTable_file_dir_textfile.grid(row=1, column=1, pady=5, padx=5)
                toolTip.bind(lookupTable_file_dir_textfile, 'Enter Directory that conatins all lookup table files.')
                lookupTable_file_dir_button.grid(row=1, column=2, padx=5)
                toolTip.bind(lookupTable_file_dir_button, 'Browse for Directory that contains all lookup table files.')

                lookupTable_time_file_label = ttk.Label(tabType, text="Input Time Files:", width=25)
                lookupTable_time_file_textfile = tk.Entry(tabType, textvariable=lookupTableParams['lookupTable_time_file'])
                lookupTable_time_file_button = ttk.Button(tabType, text="Browse", command=lambda:controller.chooseTimePointsFile(lookupTableParams))
                lookupTable_time_file_label.grid(row=2, column=0, sticky='w', pady=5, padx=5)
                lookupTable_time_file_textfile.grid(row=2, column=1, pady=5, padx=5)
                toolTip.bind(lookupTable_time_file_textfile, 'Enter time file.')
                lookupTable_time_file_button.grid(row=2, column=2, padx=5)
                toolTip.bind(lookupTable_time_file_button, 'Browse for Time File.')

                lookupTable_param_file_label = ttk.Label(tabType, text="Params Input Files:", width=25)
                lookupTable_param_file_textfile = tk.Entry(tabType, textvariable=lookupTableParams['lookupTable_param_file'])
                lookupTable_param_file_button = ttk.Button(tabType, text="Browse", command=lambda:controller.chooseParametersFilenameFile(lookupTableParams, self.LUTValuesFrame, LUTWeights))
                lookupTable_param_file_label.grid(row=3, column=0, sticky='w', pady=5, padx=5)
                lookupTable_param_file_textfile.grid(row=3, column=1, pady=5, padx=5)
                toolTip.bind(lookupTable_param_file_textfile, 'Enter Parameters Filename.')
                lookupTable_param_file_button.grid(row=3, column=2, padx=5)
                toolTip.bind(lookupTable_param_file_button, 'Browse for Parameters Filename.')

                output_label = ttk.Label(tabType, text="Outputs:", font=LABEL_FONT)
                output_label.grid(row=5, column=0, sticky='w')

                outputFrame = ttk.Frame(tabType, padding=10)
                outputFrame.grid(row=6, column=0, sticky='w')

                lookupTable_pressure_label = ttk.Label(outputFrame, text="Pressure: [Pa]")
                lookupTable_pressure_checkbox = tk.Checkbutton(outputFrame, variable=lookupTableParams['pressure'])
                lookupTable_pressure_label.grid(row=1, column=0, pady=5, sticky='e')
                lookupTable_pressure_checkbox.grid(row=1, column=1, pady=5, sticky='w')
                toolTip.bind(lookupTable_pressure_checkbox, 'Enable Presure output.')

                lookupTable_co2sat_label = ttk.Label(outputFrame, text="CO"+str(u'\u2082')+" Saturation:")
                lookupTable_co2sat_checkbox = tk.Checkbutton(outputFrame, variable=lookupTableParams['CO2saturation'])
                lookupTable_co2sat_label.grid(row=1, column=2, pady=5, sticky='e')
                lookupTable_co2sat_checkbox.grid(row=1, column=3, pady=5, sticky='w')
                toolTip.bind(lookupTable_co2sat_checkbox, 'Enable CO'+str(u'\u2082')+' output.')

            if(componentType.get()=='Multisegemented Wellbore' or componentType.get()=='MultisegmentedWellbore'):
                componentChoices.append(componentName.get())
                componentTypeDictionary.append('multiSegemented')
                connectionsDictionary.append(connection.get())

                componentVars[componentName.get()]={}
                componentVars[componentName.get()]['Params']={}
                componentVars[componentName.get()]['outputs']={}

                componentVars[componentName.get()]['number'] = IntVar()
                componentVars[componentName.get()]['number'].set(1)

                if(connection.get().find('Param')):
                    componentVars[componentName.get()]['pressure']=pressure.get()
                    componentVars[componentName.get()]['saturation']=saturation.get()

                componentVars[componentName.get()]['Params']['logWellPerm']={}
                componentVars[componentName.get()]['Params']['logAquaPerm']={}
                componentVars[componentName.get()]['Params']['wellRadius'] = {}
                componentVars[componentName.get()]['Params']['brineDensity'] = {}
                componentVars[componentName.get()]['Params']['CO2Density'] = {}
                componentVars[componentName.get()]['Params']['brineViscosity'] = {}
                componentVars[componentName.get()]['Params']['CO2Viscosity'] = {}
                componentVars[componentName.get()]['Params']['brineResSaturation'] = {}
                componentVars[componentName.get()]['Params']['compressibility'] = {}

                componentVars[componentName.get()]['componentName']=StringVar()
                componentVars[componentName.get()]['componentType']=StringVar()
                componentVars[componentName.get()]['componentName']=componentName.get()
                componentVars[componentName.get()]['componentType']=componentType.get()

                componentVars[componentName.get()]['Params']['logWellPerm']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['logAquaPerm']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['wellRadius']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['brineDensity']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Density']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['compressibility']['distribution']=StringVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['values']=StringVar()
                componentVars[componentName.get()]['Params']['logAquaPerm']['values']=StringVar()
                componentVars[componentName.get()]['Params']['wellRadius']['values']=StringVar()
                componentVars[componentName.get()]['Params']['brineDensity']['values']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Density']['values']=StringVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['values']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['values']=StringVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['values']=StringVar()
                componentVars[componentName.get()]['Params']['compressibility']['values']=StringVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['logAquaPerm']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['wellRadius']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['brineDensity']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Density']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['compressibility']['weights']=StringVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['logAquaPerm']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['wellRadius']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['brineDensity']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['CO2Density']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['brineViscosity']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['CO2Viscosity']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['brineResSaturation']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['compressibility']['distribution'].set(distributionOptions[0])

                componentVars[componentName.get()]['Params']['logWellPerm']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['logAquaPerm']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['wellRadius']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineDensity']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['CO2Density']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineViscosity']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['CO2Viscosity']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineResSaturation']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['compressibility']['values'].set('15, 21')

                componentVars[componentName.get()]['Params']['logWellPerm']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['logAquaPerm']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['wellRadius']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineDensity']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['CO2Density']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineViscosity']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['CO2Viscosity']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['brineResSaturation']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['compressibility']['weights'].set('15, 21')

                componentVars[componentName.get()]['Params']['logWellPerm']['value']=DoubleVar()
                componentVars[componentName.get()]['Params']['logAquaPerm']['value']=DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['value']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineDensity']['value']=DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Density']['value']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['value']=DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['value']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['value']=DoubleVar()
                componentVars[componentName.get()]['Params']['compressibility']['value']=DoubleVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['min']=DoubleVar()
                componentVars[componentName.get()]['Params']['logAquaPerm']['min']=DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['min']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineDensity']['min']=DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Density']['min']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['min']=DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['min']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['min']=DoubleVar()
                componentVars[componentName.get()]['Params']['compressibility']['min']=DoubleVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['max']=DoubleVar()
                componentVars[componentName.get()]['Params']['logAquaPerm']['max']=DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['max']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineDensity']['max']=DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Density']['max']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['max']=DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['max']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['max']=DoubleVar()
                componentVars[componentName.get()]['Params']['compressibility']['max']=DoubleVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['mean']=DoubleVar()
                componentVars[componentName.get()]['Params']['logAquaPerm']['mean']=DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['mean']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineDensity']['mean']=DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Density']['mean']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['mean']=DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['mean']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['mean']=DoubleVar()
                componentVars[componentName.get()]['Params']['compressibility']['mean']=DoubleVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['std']=DoubleVar()
                componentVars[componentName.get()]['Params']['logAquaPerm']['std']=DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['std']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineDensity']['std']=DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Density']['std']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineViscosity']['std']=DoubleVar()
                componentVars[componentName.get()]['Params']['CO2Viscosity']['std']=DoubleVar()
                componentVars[componentName.get()]['Params']['brineResSaturation']['std']=DoubleVar()
                componentVars[componentName.get()]['Params']['compressibility']['std']=DoubleVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['value'].set(-13)
                componentVars[componentName.get()]['Params']['logAquaPerm']['value'].set(-11)
                componentVars[componentName.get()]['Params']['wellRadius']['value'].set(0.01)
                componentVars[componentName.get()]['Params']['brineDensity']['value'].set(1000)
                componentVars[componentName.get()]['Params']['CO2Density']['value'].set(479)
                componentVars[componentName.get()]['Params']['brineViscosity']['value'].set(2.535e-3)
                componentVars[componentName.get()]['Params']['CO2Viscosity']['value'].set(3.95e-5)
                componentVars[componentName.get()]['Params']['brineResSaturation']['value'].set(0.1)
                componentVars[componentName.get()]['Params']['compressibility']['value'].set(5.1e-11)

                componentVars[componentName.get()]['Params']['logWellPerm']['max'].set(-9)
                componentVars[componentName.get()]['Params']['logAquaPerm']['max'].set(-9)
                componentVars[componentName.get()]['Params']['wellRadius']['max'].set(0.5)
                componentVars[componentName.get()]['Params']['brineDensity']['max'].set(1500)
                componentVars[componentName.get()]['Params']['CO2Density']['max'].set(1500)
                componentVars[componentName.get()]['Params']['brineViscosity']['max'].set(5.0e-3)
                componentVars[componentName.get()]['Params']['CO2Viscosity']['max'].set(1.0e-4)
                componentVars[componentName.get()]['Params']['brineResSaturation']['max'].set(0.7)
                componentVars[componentName.get()]['Params']['compressibility']['max'].set(1.0e-9)

                componentVars[componentName.get()]['Params']['logWellPerm']['min'].set(-17)
                componentVars[componentName.get()]['Params']['logAquaPerm']['min'].set(-14)
                componentVars[componentName.get()]['Params']['wellRadius']['min'].set(0.01)
                componentVars[componentName.get()]['Params']['brineDensity']['min'].set(900)
                componentVars[componentName.get()]['Params']['CO2Density']['min'].set(100)
                componentVars[componentName.get()]['Params']['brineViscosity']['min'].set(1.0e-6)
                componentVars[componentName.get()]['Params']['CO2Viscosity']['min'].set(1.0e-6)
                componentVars[componentName.get()]['Params']['brineResSaturation']['min'].set(0)
                componentVars[componentName.get()]['Params']['compressibility']['min'].set(5.0e-11)

                componentVars[componentName.get()]['Params']['logWellPerm']['mean'].set(0)
                componentVars[componentName.get()]['Params']['logAquaPerm']['mean'].set(0)
                componentVars[componentName.get()]['Params']['wellRadius']['mean'].set(0)
                componentVars[componentName.get()]['Params']['brineDensity']['mean'].set(0)
                componentVars[componentName.get()]['Params']['CO2Density']['mean'].set(0)
                componentVars[componentName.get()]['Params']['brineViscosity']['mean'].set(0)
                componentVars[componentName.get()]['Params']['CO2Viscosity']['mean'].set(0)
                componentVars[componentName.get()]['Params']['brineResSaturation']['mean'].set(0)
                componentVars[componentName.get()]['Params']['compressibility']['mean'].set(0)

                componentVars[componentName.get()]['Params']['logWellPerm']['std'].set(1)
                componentVars[componentName.get()]['Params']['logAquaPerm']['std'].set(1)
                componentVars[componentName.get()]['Params']['wellRadius']['std'].set(1)
                componentVars[componentName.get()]['Params']['brineDensity']['std'].set(1)
                componentVars[componentName.get()]['Params']['CO2Density']['std'].set(1)
                componentVars[componentName.get()]['Params']['brineViscosity']['std'].set(1)
                componentVars[componentName.get()]['Params']['CO2Viscosity']['std'].set(1)
                componentVars[componentName.get()]['Params']['brineResSaturation']['std'].set(1)
                componentVars[componentName.get()]['Params']['compressibility']['std'].set(1)

                multisegmentedWellbore_label = ttk.Label(tabType, text="Multisegmented Wellbore Component:", font=LABEL_FONT)
                multisegmentedWellbore_label.grid(row=0, column=0, sticky='w')

                logWellPerm=tk.Frame(tabType)
                logWellPerm.grid(row=1, column=0, sticky='w', padx=15)

                multiSegement_logWellPerm_label = ttk.Label(logWellPerm, text="Permeabilty Logarithm: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]", width=30)
                multiSegement_logWellPerm_menu = tk.OptionMenu(logWellPerm, componentVars[componentName.get()]['Params']['logWellPerm']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(logWellPerm))
                multiSegement_logWellPerm_value_label=ttk.Label(logWellPerm, text='Value:')
                multiSegement_logWellPerm_txtField = tk.Entry(logWellPerm, textvariable=componentVars[componentName.get()]['Params']['logWellPerm']['value'])
                logWellPerm.labelText='Permeabilty Logarithm: [log'+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]"
                logWellPerm.component=componentVars[componentName.get()]['componentName']
                logWellPerm.distType=componentVars[componentName.get()]['Params']['logWellPerm']['distribution']
                logWellPerm.toolTipText = 'Permeablity Logarithm'
                logWellPerm.text = 'logWellPerm'

                multiSegement_logWellPerm_menu.config(width=15)
                toolTip.bind(multiSegement_logWellPerm_menu, 'Select Distriubtion for Permeablity Logarithm.')
                multiSegement_logWellPerm_label.grid(row=0, column=0, sticky='e', padx=5)
                multiSegement_logWellPerm_menu.grid(row=0, column=1, padx=5)
                multiSegement_logWellPerm_value_label.grid(row=0, column=2, padx=5)
                multiSegement_logWellPerm_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(multiSegement_logWellPerm_txtField, 'Set value for Permeablity Logarithm.')
                controller.setvar(name='logWellPerm', value=logWellPerm)

                logAquaPerm=tk.Frame(tabType)
                logAquaPerm.grid(row=2, column=0, sticky='w', padx=15)

                multiSegement_logAquaPerm_label = ttk.Label(logAquaPerm, text="Aquifer Permeabilty: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]", width=30)
                multiSegement_logAquaPerm_menu = tk.OptionMenu(logAquaPerm, componentVars[componentName.get()]['Params']['logAquaPerm']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(logAquaPerm))
                multiSegement_logAquaPerm_value_label=ttk.Label(logAquaPerm, text='Value:')
                multiSegement_logAquaPerm_txtField = tk.Entry(logAquaPerm, textvariable=componentVars[componentName.get()]['Params']['logAquaPerm']['value'])
                logAquaPerm.labelText='Aquifer Permeabilty: [log'+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+']'
                logAquaPerm.component=componentVars[componentName.get()]['componentName']
                logAquaPerm.distType=componentVars[componentName.get()]['Params']['logAquaPerm']['distribution']
                logAquaPerm.toolTipText = 'Aquifer Permeablity'
                logAquaPerm.text = 'logAquaPerm'

                multiSegement_logAquaPerm_menu.config(width=15)
                toolTip.bind(multiSegement_logAquaPerm_menu, 'Select Distribution for Aquifer Permeablity')
                multiSegement_logAquaPerm_label.grid(row=0, column=0, sticky='e', padx=5)
                multiSegement_logAquaPerm_menu.grid(row=0, column=1, padx=5)
                multiSegement_logAquaPerm_value_label.grid(row=0, column=2, padx=5)
                multiSegement_logAquaPerm_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(multiSegement_logAquaPerm_txtField, 'Set value for Aquifer Permeablity')
                controller.setvar(name='logAquaPerm', value=logAquaPerm)

                brineDensity=tk.Frame(tabType)
                brineDensity.grid(row=3, column=0, sticky='w', padx=15)

                multiSegement_brineDensity_label = ttk.Label(brineDensity, text="Brine Density: [kg/m"+str(u'\u00B3')+"]", width=30)
                multiSegement_brineDensity_menu = tk.OptionMenu(brineDensity, componentVars[componentName.get()]['Params']['brineDensity']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(brineDensity))
                multiSegement_brineDensity_value_label=ttk.Label(brineDensity, text='Value:')
                multiSegement_brineDensity_txtField = tk.Entry(brineDensity, textvariable=componentVars[componentName.get()]['Params']['brineDensity']['value'])
                brineDensity.labelText='Brine Density: [kg/m'+str(u'\u00B3')+']'
                brineDensity.component=componentVars[componentName.get()]['componentName']
                brineDensity.distType=componentVars[componentName.get()]['Params']['brineDensity']['distribution']
                brineDensity.toolTipText = 'Brine Density'
                brineDensity.text = 'brineDensity'

                multiSegement_brineDensity_menu.config(width=15)
                toolTip.bind(multiSegement_brineDensity_menu, 'Select Distriubtion for Brine Density.')
                multiSegement_brineDensity_label.grid(row=0, column=0, sticky='e', padx=5)
                multiSegement_brineDensity_menu.grid(row=0, column=1, padx=5)
                multiSegement_brineDensity_value_label.grid(row=0, column=2, padx=5)
                multiSegement_brineDensity_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(multiSegement_brineDensity_txtField, 'Set value for Brine Density.')
                controller.setvar(name='brineDensity', value=brineDensity)

                CO2Density=tk.Frame(tabType)
                CO2Density.grid(row=4, column=0, sticky='w', padx=15)

                multiSegement_CO2Density_label = ttk.Label(CO2Density, text="CO"+str(u'\u2082')+" Density: [kg/m"+str(u'\u00B3')+"]", width=30)
                multiSegement_CO2Density_menu = tk.OptionMenu(CO2Density, componentVars[componentName.get()]['Params']['CO2Density']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(CO2Density))
                multiSegement_CO2Density_value_label=ttk.Label(CO2Density, text='Value:')
                multiSegement_CO2Density_txtField = tk.Entry(CO2Density, textvariable=componentVars[componentName.get()]['Params']['CO2Density']['value'])
                CO2Density.labelText="CO"+str(u'\u2082')+" Density: [kg/m"+str(u'\u00B3')+"]"
                CO2Density.component=componentVars[componentName.get()]['componentName']
                CO2Density.distType=componentVars[componentName.get()]['Params']['CO2Density']['distribution']
                CO2Density.toolTipText = "CO"+str(u'\u2082')+" Density"
                CO2Density.text = 'CO2Density'

                multiSegement_CO2Density_menu.config(width=15)
                toolTip.bind(multiSegement_CO2Density_menu, "Select Distriubtion for CO"+str(u'\u2082')+" Density")
                multiSegement_CO2Density_label.grid(row=0, column=0, sticky='e', padx=5)
                multiSegement_CO2Density_menu.grid(row=0, column=1, padx=5)
                multiSegement_CO2Density_value_label.grid(row=0, column=2, padx=5)
                multiSegement_CO2Density_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(multiSegement_CO2Density_txtField, "Set value for CO"+str(u'\u2082')+" Density")
                controller.setvar(name='CO2Density', value=CO2Density)

                brineViscosity=tk.Frame(tabType)
                brineViscosity.grid(row=5, column=0, sticky='w', padx=15)

                multiSegement_brineViscosity_label = ttk.Label(brineViscosity, text="Brine Viscosity: [Pa"+str(u'\u22C5')+"s]", width=30)
                multiSegement_brineViscosity_menu = tk.OptionMenu(brineViscosity, componentVars[componentName.get()]['Params']['brineViscosity']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(brineViscosity))
                multiSegement_brineViscosity_value_label=ttk.Label(brineViscosity, text='Value:')
                multiSegement_brineViscosity_txtField = tk.Entry(brineViscosity, textvariable=componentVars[componentName.get()]['Params']['brineViscosity']['value'])
                brineViscosity.labelText='Brine Viscosity: [Pa'+str(u'\u22C5')+"s]"
                brineViscosity.component=componentVars[componentName.get()]['componentName']
                brineViscosity.distType=componentVars[componentName.get()]['Params']['brineViscosity']['distribution']
                brineViscosity.toolTipText = 'Brine Viscosity'
                brineViscosity.text = 'brineViscosity'

                multiSegement_brineViscosity_menu.config(width=15)
                toolTip.bind(multiSegement_brineViscosity_menu, 'Select Distriubtion for Brine Viscosity.')
                multiSegement_brineViscosity_label.grid(row=0, column=0, sticky='e', padx=5)
                multiSegement_brineViscosity_menu.grid(row=0, column=1, padx=5)
                multiSegement_brineViscosity_value_label.grid(row=0, column=2, padx=5)
                multiSegement_brineViscosity_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(multiSegement_brineViscosity_txtField, 'Set value for Brine Viscosity.')
                controller.setvar(name='brineViscosity', value=brineViscosity)

                CO2Viscosity=tk.Frame(tabType)
                CO2Viscosity.grid(row=6, column=0, sticky='w', padx=15)

                multiSegement_CO2Viscosity_label = ttk.Label(CO2Viscosity, text="CO"+str(u'\u2082')+" Viscosity: [Pa"+str(u'\u22C5')+"s]", width=30)
                multiSegement_CO2Viscosity_menu = tk.OptionMenu(CO2Viscosity, componentVars[componentName.get()]['Params']['CO2Viscosity']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(CO2Viscosity))
                multiSegement_CO2Viscosity_value_label=ttk.Label(CO2Viscosity, text='Value:')
                multiSegement_CO2Viscosity_txtField = tk.Entry(CO2Viscosity, textvariable=componentVars[componentName.get()]['Params']['CO2Viscosity']['value'])
                CO2Viscosity.labelText="CO"+str(u'\u2082')+" Viscosity: [Pa"+str(u'\u22C5')+"s]"
                CO2Viscosity.component=componentVars[componentName.get()]['componentName']
                CO2Viscosity.distType=componentVars[componentName.get()]['Params']['CO2Viscosity']['distribution']
                CO2Viscosity.toolTipText = "CO"+str(u'\u2082')+" Viscosity"
                CO2Viscosity.text = 'CO2Viscosity'

                multiSegement_CO2Viscosity_menu.config(width=15)
                toolTip.bind(multiSegement_CO2Viscosity_menu, 'Select Distriubtion for CO'+str(u'\u2082')+' Viscosity.')
                multiSegement_CO2Viscosity_label.grid(row=0, column=0, sticky='e', padx=5)
                multiSegement_CO2Viscosity_menu.grid(row=0, column=1, padx=5)
                multiSegement_CO2Viscosity_value_label.grid(row=0, column=2, padx=5)
                multiSegement_CO2Viscosity_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(multiSegement_CO2Viscosity_txtField, 'Set value for CO'+str(u'\u2082')+' Viscosity.')
                controller.setvar(name='CO2Viscosity', value=CO2Viscosity)

                brineResSaturation=tk.Frame(tabType)
                brineResSaturation.grid(row=7, column=0, sticky='w', padx=15)

                multiSegement_brineResSaturation_label = ttk.Label(brineResSaturation, text="Brine Saturation: [-]", width=30)
                multiSegement_brineResSaturation_menu = tk.OptionMenu(brineResSaturation, componentVars[componentName.get()]['Params']['brineResSaturation']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(brineResSaturation))
                multiSegement_brineResSaturation_value_label=ttk.Label(brineResSaturation, text='Value:')
                multiSegement_brineResSaturation_txtField = tk.Entry(brineResSaturation, textvariable=componentVars[componentName.get()]['Params']['brineResSaturation']['value'])
                brineResSaturation.labelText='Brine Saturation: [-]'
                brineResSaturation.component=componentVars[componentName.get()]['componentName']
                brineResSaturation.distType=componentVars[componentName.get()]['Params']['brineResSaturation']['distribution']
                brineResSaturation.toolTipText = 'Brine Saturation'
                brineResSaturation.text = 'brineResSaturation'

                multiSegement_brineResSaturation_menu.config(width=15)
                toolTip.bind(multiSegement_brineResSaturation_menu, 'Select Distriubtion for Brine Saturation.')
                multiSegement_brineResSaturation_label.grid(row=0, column=0, sticky='e', padx=5)
                multiSegement_brineResSaturation_menu.grid(row=0, column=1, padx=5)
                multiSegement_brineResSaturation_value_label.grid(row=0, column=2, padx=5)
                multiSegement_brineResSaturation_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(multiSegement_brineResSaturation_txtField, 'Set value for Brine Saturation.')
                controller.setvar(name='brineResSaturation', value=brineResSaturation)

                compressibility=tk.Frame(tabType)
                compressibility.grid(row=8, column=0, sticky='w', padx=15)

                multiSegement_compressibility_label = ttk.Label(compressibility, text="Compressibility: [Pa"+str(u'\u207B'u'\u2071')+"]", width=30)
                multiSegement_compressibility_menu = tk.OptionMenu(compressibility, componentVars[componentName.get()]['Params']['compressibility']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(compressibility))
                multiSegement_compressibility_value_label=ttk.Label(compressibility, text='Value:')
                multiSegement_compressibility_txtField = tk.Entry(compressibility, textvariable=componentVars[componentName.get()]['Params']['compressibility']['value'])
                compressibility.labelText='Compressibility: [Pa'+str(u'\u207B'u'\u2071')+"]"
                compressibility.component=componentVars[componentName.get()]['componentName']
                compressibility.distType=componentVars[componentName.get()]['Params']['compressibility']['distribution']
                compressibility.toolTipText = 'Compressibility'
                compressibility.text = 'compressibility'

                multiSegement_compressibility_menu.config(width=15)
                toolTip.bind(multiSegement_compressibility_menu, 'Select Distriubtion for Compressibility.')
                multiSegement_compressibility_label.grid(row=0, column=0, sticky='e', padx=5)
                multiSegement_compressibility_menu.grid(row=0, column=1, padx=5)
                multiSegement_compressibility_value_label.grid(row=0, column=2, padx=5)
                multiSegement_compressibility_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(multiSegement_compressibility_txtField, 'Set value for Compressibility.')
                controller.setvar(name='compressibility', value=compressibility)

                wellRadius=tk.Frame(tabType)
                wellRadius.grid(row=9, column=0, sticky='w', padx=15)

                multiSegement_wellRadius_label = ttk.Label(wellRadius, text="Radius of Well: [m]", width=30)
                multiSegement_wellRadius_menu = tk.OptionMenu(wellRadius, componentVars[componentName.get()]['Params']['wellRadius']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(wellRadius))
                multiSegement_wellRadius_value_label=ttk.Label(wellRadius, text='Value:')
                multiSegement_wellRadius_txtField = tk.Entry(wellRadius, textvariable=componentVars[componentName.get()]['Params']['wellRadius']['value'])
                wellRadius.labelText='Radius of Well: [m]'
                wellRadius.component=componentVars[componentName.get()]['componentName']
                wellRadius.distType=componentVars[componentName.get()]['Params']['wellRadius']['distribution']
                wellRadius.toolTipText = 'Radius of Well'
                wellRadius.text = 'wellRadius'

                multiSegement_wellRadius_menu.config(width=15)
                toolTip.bind(multiSegement_wellRadius_menu, 'Select Distriubtion for Radius of Well.')
                multiSegement_wellRadius_label.grid(row=0, column=0, sticky='e', padx=5)
                multiSegement_wellRadius_menu.grid(row=0, column=1, padx=5)
                multiSegement_wellRadius_value_label.grid(row=0, column=2, padx=5)
                multiSegement_wellRadius_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(multiSegement_wellRadius_txtField, 'Set value for Radius of Well.')
                controller.setvar(name='wellRadius', value=wellRadius)

                number=tk.Frame(tabType)
                number.grid(row=10, column=0, sticky='w', padx=15)

                multiSegement_number_label = ttk.Label(number, text='Number of Wellbores:', width=30)
                multiSegement_number_spinbox = tk.Spinbox(number, from_=1, to=1000, textvariable=componentVars[componentName.get()]['number'])
                multiSegement_number_label.grid(row=0, column=0, sticky='e', padx=5)
                multiSegement_number_spinbox.grid(row=0, column=1, padx=5)
                toolTip.bind(multiSegement_number_spinbox, 'Set the total number of wellbores for this Component Model.')

                multiSegement_outputs = ttk.Label(tabType, text="Outputs:", font=LABEL_FONT)
                multiSegement_outputs.grid(row=12, column=0, sticky='w')

                outputFrame = ttk.Frame(tabType, padding=10)
                outputFrame.grid(row=13, column=0, sticky='w', padx=15)

                keys = aquiferThickness.keys()

                row = 0
                col = 0
                for key in keys:
                    key = key[:-9]

                    aquifers.append(key)

                    componentVars[componentName.get()]['outputs']['CO2_'+key] = BooleanVar()
                    componentVars[componentName.get()]['outputs']['brine_'+key] = BooleanVar()
                    componentVars[componentName.get()]['outputs']['mass_CO2_'+key] = BooleanVar()
                    componentVars[componentName.get()]['outputs']['CO2_'+key].set(0)
                    componentVars[componentName.get()]['outputs']['brine_'+key].set(0)
                    componentVars[componentName.get()]['outputs']['mass_CO2_'+key].set(0)

                    label = ttk.Label(outputFrame, text='CO2_'+key+": [kg/s]")
                    checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['outputs']['CO2_'+key])
                    brine_label = ttk.Label(outputFrame, text='brine_'+key+": [kg/s]")
                    brine_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['outputs']['brine_'+key])
                    mass_CO2_label = ttk.Label(outputFrame, text='mass_CO2_'+key+": [kg]")
                    mass_CO2_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['outputs']['mass_CO2_'+key])

                    label.grid(row=row, column=col, pady=5, sticky='e')
                    checkbox.grid(row=row, column=col+1, pady=5, sticky='w')
                    toolTip.bind(checkbox, 'Select to Enable CO2 Output for '+key+'.')

                    brine_label.grid(row=row, column=col+2, pady=5, sticky='e')
                    brine_checkbox.grid(row=row, column=col+3, pady=5, sticky='w')
                    toolTip.bind(brine_checkbox, 'Select to Enable Brine Output for '+key+'.')

                    mass_CO2_label.grid(row=row, column=col+4, pady=5, sticky='e')
                    mass_CO2_checkbox.grid(row=row, column=col+5, pady=5, sticky='w')
                    toolTip.bind(mass_CO2_checkbox, 'Select to Enable CO2 mass output for '+key+'.')

                    col = col + 6

                    if (col == 12):
                        row = row + 1
                        col = 0

                componentVars[componentName.get()]['outputs']['CO2_atm'] = BooleanVar()
                componentVars[componentName.get()]['outputs']['brine_atm'] = BooleanVar()
                componentVars[componentName.get()]['outputs']['CO2_atm'].set(0)
                componentVars[componentName.get()]['outputs']['brine_atm'].set(0)

                CO2_atm_label = ttk.Label(outputFrame, text='CO2_atm:')
                CO2_atm_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['outputs']['CO2_atm'])
                brine_atm_label = ttk.Label(outputFrame, text='brine_atm:')
                brine_atm_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['outputs']['brine_atm'])

                CO2_atm_label.grid(row=row+1, column=0, pady=5, sticky='e')
                CO2_atm_checkbox.grid(row=row+1, column=1, pady=5, sticky='w')
                toolTip.bind(CO2_atm_checkbox, 'Select to enable Atmospheric CO2 output.')
                brine_atm_label.grid(row=row+1, column=2, pady=5, sticky='e')
                brine_atm_checkbox.grid(row=row+1, column=3, pady=5, sticky='w')
                toolTip.bind(brine_atm_checkbox, 'Select to enable Atmospheric brine output.')

            if(componentType.get()=='Cemented Wellbore' or componentType.get()=='CementedWellbore'):
                componentChoices.append(componentName.get())
                componentTypeDictionary.append('cementedwellbore')
                connectionsDictionary.append(connection.get())

                componentVars[componentName.get()]={}
                componentVars[componentName.get()]['Params']={}
                componentVars[componentName.get()]['Params']['logWellPerm']={}
                componentVars[componentName.get()]['Params']['logThiefPerm']={}
                componentVars[componentName.get()]['Params']['wellRadius'] = {}

                if(connection.get().find('Param')):
                    componentVars[componentName.get()]['pressure']=pressure.get()
                    componentVars[componentName.get()]['saturation']=saturation.get()

                componentVars[componentName.get()]['Params']['logWellPerm']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['logThiefPerm']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['wellRadius']['distribution']=StringVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['values']=StringVar()
                componentVars[componentName.get()]['Params']['logThiefPerm']['values']=StringVar()
                componentVars[componentName.get()]['Params']['wellRadius']['values']=StringVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['logThiefPerm']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['wellRadius']['weights']=StringVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['logThiefPerm']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['wellRadius']['distribution'].set(distributionOptions[0])

                componentVars[componentName.get()]['Params']['logWellPerm']['values'].set('15, 32')
                componentVars[componentName.get()]['Params']['logThiefPerm']['values'].set('15, 32')
                componentVars[componentName.get()]['Params']['wellRadius']['values'].set('15, 32')

                componentVars[componentName.get()]['Params']['logWellPerm']['weights'].set('15, 32')
                componentVars[componentName.get()]['Params']['logThiefPerm']['weights'].set('15, 32')
                componentVars[componentName.get()]['Params']['wellRadius']['weights'].set('15, 32')

                componentVars[componentName.get()]['CO2_aquifer1'] = BooleanVar()
                componentVars[componentName.get()]['CO2_aquifer2'] = BooleanVar()
                componentVars[componentName.get()]['CO2_atm'] = BooleanVar()
                componentVars[componentName.get()]['brine_aquifer1'] = BooleanVar()
                componentVars[componentName.get()]['brine_aquifer2'] = BooleanVar()
                componentVars[componentName.get()]['brine_atm'] = BooleanVar()
                componentVars[componentName.get()]['mass_CO2_aquifer1'] = BooleanVar()
                componentVars[componentName.get()]['mass_CO2_aquifer2'] = BooleanVar()

                componentVars[componentName.get()]['number'] = IntVar()
                componentVars[componentName.get()]['number'].set(1)

                componentVars[componentName.get()]['componentName']=StringVar()
                componentVars[componentName.get()]['componentType']=StringVar()
                componentVars[componentName.get()]['componentName']=componentName.get()
                componentVars[componentName.get()]['componentType']=componentType.get()

                componentVars[componentName.get()]['Params']['logWellPerm']['value']=DoubleVar()
                componentVars[componentName.get()]['Params']['logThiefPerm']['value']=DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['value']=DoubleVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['mean']=DoubleVar()
                componentVars[componentName.get()]['Params']['logThiefPerm']['mean']=DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['mean']=DoubleVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['std']=DoubleVar()
                componentVars[componentName.get()]['Params']['logThiefPerm']['std']=DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['std']=DoubleVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['max']=DoubleVar()
                componentVars[componentName.get()]['Params']['logThiefPerm']['max']=DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['max']=DoubleVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['min']=DoubleVar()
                componentVars[componentName.get()]['Params']['logThiefPerm']['min']=DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['min']=DoubleVar()

                componentVars[componentName.get()]['Params']['logWellPerm']['max'].set(-10.1)
                componentVars[componentName.get()]['Params']['logThiefPerm']['max'].set(-12)
                componentVars[componentName.get()]['Params']['wellRadius']['max'].set(0.25)

                componentVars[componentName.get()]['Params']['logWellPerm']['min'].set(-13.95)
                componentVars[componentName.get()]['Params']['logThiefPerm']['min'].set(-13.995)
                componentVars[componentName.get()]['Params']['wellRadius']['min'].set(0.025)

                componentVars[componentName.get()]['Params']['logWellPerm']['mean'].set(0)
                componentVars[componentName.get()]['Params']['logThiefPerm']['mean'].set(0)
                componentVars[componentName.get()]['Params']['wellRadius']['mean'].set(0)

                componentVars[componentName.get()]['Params']['logWellPerm']['std'].set(1)
                componentVars[componentName.get()]['Params']['logThiefPerm']['std'].set(1)
                componentVars[componentName.get()]['Params']['wellRadius']['std'].set(1)

                componentVars[componentName.get()]['Params']['logWellPerm']['value'].set(-13)
                componentVars[componentName.get()]['Params']['logThiefPerm']['value'].set(-12)
                componentVars[componentName.get()]['Params']['wellRadius']['value'].set(0.05)

                cementedWellboreTab_label = ttk.Label(tabType, text="Cemented Wellbore Component:", font=LABEL_FONT)
                cementedWellboreTab_label.grid(row=0, column=0, sticky='w')

                logWellPerm=tk.Frame(tabType)
                logWellPerm.grid(row=1, column=0, sticky='w', padx=15,)

                cementedWellbore_logWellPerm_label = ttk.Label(logWellPerm, text="Wellbore Permeability: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]", width=30)
                cementedWellbore_logWellPerm_menu = tk.OptionMenu(logWellPerm, componentVars[componentName.get()]['Params']['logWellPerm']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(logWellPerm))
                cementedWellbore_logWellPerm_value_label=ttk.Label(logWellPerm, text='Value:')
                cementedWellbore_logWellPerm_txtField = tk.Entry(logWellPerm, textvariable=componentVars[componentName.get()]['Params']['logWellPerm']['value'])
                logWellPerm.labelText="Wellbore Permeability: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]"
                logWellPerm.component=componentVars[componentName.get()]['componentName']
                logWellPerm.distType=componentVars[componentName.get()]['Params']['logWellPerm']['distribution']
                logWellPerm.toolTipText = 'Wellbore Permeablity'
                logWellPerm.text = 'logWellPerm'

                cementedWellbore_logWellPerm_menu.config(width=15)
                cementedWellbore_logWellPerm_label.grid(row=0, column=0, sticky='e', padx=5)
                toolTip.bind(cementedWellbore_logWellPerm_label, 'Select Distriubtion for Wellbore Permeabilty.')
                cementedWellbore_logWellPerm_menu.grid(row=0, column=1, padx=5)
                cementedWellbore_logWellPerm_value_label.grid(row=0, column=2, padx=5)
                cementedWellbore_logWellPerm_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(cementedWellbore_logWellPerm_txtField, 'Set value for Wellbore Permeablity.')
                controller.setvar(name='logWellPerm', value=logWellPerm)

                logThiefPerm=tk.Frame(tabType)
                logThiefPerm.grid(row=2, column=0, sticky='w', padx=15,)

                cementedWellbore_logThiefPerm_label = ttk.Label(logThiefPerm, text="Thief Permeabilty: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]", width=30)
                cementedWellbore_logThiefPerm_menu = tk.OptionMenu(logThiefPerm, componentVars[componentName.get()]['Params']['logThiefPerm']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(logThiefPerm))
                cementedWellbore_logThiefPerm_value_label=ttk.Label(logThiefPerm, text='Value:')
                cementedWellbore_logThiefPerm_txtField = tk.Entry(logThiefPerm, textvariable=componentVars[componentName.get()]['Params']['logThiefPerm']['value'])
                logThiefPerm.labelText="Thief Permeabilty: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]"
                logThiefPerm.component=componentVars[componentName.get()]['componentName']
                logThiefPerm.distType=componentVars[componentName.get()]['Params']['logThiefPerm']['distribution']
                logThiefPerm.toolTipText = 'Thief Permeabilty'
                logThiefPerm.text = 'logThiefPerm'

                cementedWellbore_logThiefPerm_menu.config(width=15)
                toolTip.bind(cementedWellbore_logThiefPerm_menu, 'Select Distriubtion for Thief Permeabilty.')
                cementedWellbore_logThiefPerm_label.grid(row=0, column=0, sticky='e', padx=5)
                cementedWellbore_logThiefPerm_menu.grid(row=0, column=1, padx=5)
                cementedWellbore_logThiefPerm_value_label.grid(row=0, column=2, padx=5)
                cementedWellbore_logThiefPerm_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(cementedWellbore_logThiefPerm_menu, 'Set value for Thief Permeabilty.')
                controller.setvar(name='logThiefPerm', value=logThiefPerm)

                wellRadius=tk.Frame(tabType)
                wellRadius.grid(row=3, column=0, sticky='w', padx=15,)

                cementedWellbore_wellRadius_label = ttk.Label(wellRadius, text="Radius of the wellbore: [m]", width=30)
                cementedWellbore_wellRadius_menu = tk.OptionMenu(wellRadius, componentVars[componentName.get()]['Params']['wellRadius']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(wellRadius))
                cementedWellbore_wellRadius_value_label=ttk.Label(wellRadius, text='Value:')
                cementedWellbore_wellRadius_txtField = tk.Entry(wellRadius, textvariable=componentVars[componentName.get()]['Params']['wellRadius']['value'])
                wellRadius.labelText='Radius of the wellbore: [m]'
                wellRadius.component=componentVars[componentName.get()]['componentName']
                wellRadius.distType=componentVars[componentName.get()]['Params']['wellRadius']['distribution']
                wellRadius.toolTipText = 'Radius of the wellbore'
                wellRadius.text = 'wellRadius'

                cementedWellbore_wellRadius_menu.config(width=15)
                toolTip.bind(cementedWellbore_wellRadius_menu, 'Select Distriubtion for Radius of the wellbore.')
                cementedWellbore_wellRadius_label.grid(row=0, column=0, sticky='e', padx=5)
                cementedWellbore_wellRadius_menu.grid(row=0, column=1, padx=5)
                cementedWellbore_wellRadius_value_label.grid(row=0, column=2, padx=5)
                cementedWellbore_wellRadius_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(cementedWellbore_wellRadius_txtField, 'Set value for Radius of the wellbore.')
                controller.setvar(name='wellRadius', value=wellRadius)

                number=tk.Frame(tabType)
                number.grid(row=7, column=0, sticky='w', padx=15,)

                cementedWellbore_number_label = ttk.Label(number, text='Number of Wellbores:', width=30)
                cementedWellbore_number_spinbox = tk.Spinbox(number, from_=1, to=1000, textvariable=componentVars[componentName.get()]['number'])
                cementedWellbore_number_label.grid(row=0, column=0, sticky='e', padx=5)
                cementedWellbore_number_spinbox.grid(row=0, column=1, padx=5)
                toolTip.bind(cementedWellbore_number_spinbox, 'Set the total number of wellbores for this component model.')

                cementedWellbore_outputs = ttk.Label(tabType, text="Outputs:", font=LABEL_FONT)
                cementedWellbore_outputs.grid(row=8, column=0, sticky='w')

                outputFrame = ttk.Frame(tabType)
                outputFrame.grid(row=9, column=0, sticky='w', padx=15, columnspan=10)

                cementedWellbore_co2_aquifer1_label = ttk.Label(outputFrame, text="CO"+str(u'\u2082')+" Aquifer 1: [kg/s]")
                cementedWellbore_co2_aquifer1_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['CO2_aquifer1'])
                cementedWellbore_co2_aquifer1_label.grid(row=1, column=0)
                cementedWellbore_co2_aquifer1_checkbox.grid(row=1, column=1)
                toolTip.bind(cementedWellbore_co2_aquifer1_checkbox, 'Enable output of CO2 for Aquifer 1.')

                cementedWellbore_co2_aquifer2_label = ttk.Label(outputFrame, text="CO"+str(u'\u2082')+" Aquifer 2: [kg/s]")
                cementedWellbore_co2_aquifer2_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['CO2_aquifer2'])
                cementedWellbore_co2_aquifer2_label.grid(row=1, column=2)
                cementedWellbore_co2_aquifer2_checkbox.grid(row=1, column=3)
                toolTip.bind(cementedWellbore_co2_aquifer2_checkbox, 'Enable output of CO2 for Aquifer 2.')

                cementedWellbore_co2_atm_label = ttk.Label(outputFrame, text="CO"+str(u'\u2082')+" atm: [kg/s]")
                cementedWellbore_co2_atm_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['CO2_atm'])
                cementedWellbore_co2_atm_label.grid(row=1, column=4)
                cementedWellbore_co2_atm_checkbox.grid(row=1, column=5)
                toolTip.bind(cementedWellbore_co2_atm_checkbox, 'Enable output of CO2 for Atmosphere.')

                cementedWellbore_brine_aquifer1_label = ttk.Label(outputFrame, text="Brine Aquifer 1: [kg/s]")
                cementedWellbore_brine_aquifer1_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['brine_aquifer1'])
                cementedWellbore_brine_aquifer1_label.grid(row=1, column=6)
                cementedWellbore_brine_aquifer1_checkbox.grid(row=1, column=7)
                toolTip.bind(cementedWellbore_brine_aquifer1_checkbox, 'Enable output of Brine for Aquifer 1.')

                cementedWellbore_brine_aquifer2_label = ttk.Label(outputFrame, text="Brine Aquifer 2: [kg/s]")
                cementedWellbore_brine_aquifer2_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['brine_aquifer2'])
                cementedWellbore_brine_aquifer2_label.grid(row=2, column=0)
                cementedWellbore_brine_aquifer2_checkbox.grid(row=2, column=1)
                toolTip.bind(cementedWellbore_brine_aquifer2_checkbox, 'Enable output of Brine for Aquifer 2.')

                cementedWellbore_brine_atm_label = ttk.Label(outputFrame, text="Brine Atm: [kg/s]")
                cementedWellbore_brine_atm_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['brine_atm'])
                cementedWellbore_brine_atm_label.grid(row=2, column=2)
                cementedWellbore_brine_atm_checkbox.grid(row=2, column=3)
                toolTip.bind(cementedWellbore_brine_atm_checkbox, 'Enable Brine output for Atmosphere.')

                cementedWellbore_mass_co2_aquifer1_label = ttk.Label(outputFrame, text="Mass CO"+str(u'\u2082')+" Aquifer 1: [kg]")
                cementedWellbore_mass_co2_aquifer1_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['mass_CO2_aquifer1'])
                cementedWellbore_mass_co2_aquifer1_label.grid(row=2, column=4)
                cementedWellbore_mass_co2_aquifer1_checkbox.grid(row=2, column=5)
                toolTip.bind(cementedWellbore_mass_co2_aquifer1_checkbox, 'Enable CO2 Mass for Aquifer 2.')

                cementedWellbore_mass_co2_aquifer2_label = ttk.Label(outputFrame, text="Mass CO"+str(u'\u2082')+" Aquifer 2: [kg]")
                cementedWellbore_mass_co2_aquifer2_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['mass_CO2_aquifer2'])
                cementedWellbore_mass_co2_aquifer2_label.grid(row=2, column=6)
                cementedWellbore_mass_co2_aquifer2_checkbox.grid(row=2, column=7)
                toolTip.bind(cementedWellbore_mass_co2_aquifer2_checkbox, 'Enable CO2 Mass for Aquifer 2.')

            if(componentType.get()=='Open Wellbore' or componentType.get()=='OpenWellbore'):
                componentChoices.append(componentName.get())
                componentTypeDictionary.append('openwellbore')
                connectionsDictionary.append(connection.get())

                componentVars[componentName.get()]={}
                componentVars[componentName.get()]['LeakTo']=StringVar()
                componentVars[componentName.get()]['LeakTo'].set(aquifers[0])

                componentVars[componentName.get()]['Params']={}
                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']={}
                componentVars[componentName.get()]['Params']['brineSalinity']={}
                componentVars[componentName.get()]['Params']['wellRadius']={}

                if(connection.get().find('Param')):
                    componentVars[componentName.get()]['pressure']=pressure.get()
                    componentVars[componentName.get()]['saturation']=saturation.get()

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['brineSalinity']['distribution']=StringVar()
                componentVars[componentName.get()]['Params']['wellRadius']['distribution']=StringVar()

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['values']=StringVar()
                componentVars[componentName.get()]['Params']['brineSalinity']['values']=StringVar()
                componentVars[componentName.get()]['Params']['wellRadius']['values']=StringVar()

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['brineSalinity']['weights']=StringVar()
                componentVars[componentName.get()]['Params']['wellRadius']['weights']=StringVar()

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['brineSalinity']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['wellRadius']['distribution'].set(distributionOptions[0])

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['values'].set('34, 23')
                componentVars[componentName.get()]['Params']['brineSalinity']['values'].set('34, 23')
                componentVars[componentName.get()]['Params']['wellRadius']['values'].set('34, 23')

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['weights'].set('34, 23')
                componentVars[componentName.get()]['Params']['brineSalinity']['weights'].set('34, 23')
                componentVars[componentName.get()]['Params']['wellRadius']['weights'].set('34, 23')

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineSalinity']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['value'] = DoubleVar()

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineSalinity']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['min'] = DoubleVar()

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineSalinity']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['max'] = DoubleVar()

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineSalinity']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['mean'] = DoubleVar()

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brineSalinity']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['wellRadius']['std'] = DoubleVar()

                componentVars[componentName.get()]['CO2_aquifer'] = BooleanVar()
                componentVars[componentName.get()]['CO2_atm'] = BooleanVar()
                componentVars[componentName.get()]['brine_aquifer'] = BooleanVar()
                componentVars[componentName.get()]['brine_atm'] = BooleanVar()

                componentVars[componentName.get()]['componentName']=StringVar()
                componentVars[componentName.get()]['componentType']=StringVar()
                componentVars[componentName.get()]['componentName']=componentName.get()
                componentVars[componentName.get()]['componentType']=componentType.get()
                componentVars[componentName.get()]['number']=IntVar()
                componentVars[componentName.get()]['number'].set(1)

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['value'].set(0.0)
                componentVars[componentName.get()]['Params']['brineSalinity']['value'].set(0.1)
                componentVars[componentName.get()]['Params']['wellRadius']['value'].set(0.05)

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['min'].set(-1)
                componentVars[componentName.get()]['Params']['brineSalinity']['min'].set(0)
                componentVars[componentName.get()]['Params']['wellRadius']['min'].set(0.025)

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['max'].set(1)
                componentVars[componentName.get()]['Params']['brineSalinity']['max'].set(0.2)
                componentVars[componentName.get()]['Params']['wellRadius']['max'].set(0.25)

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['mean'].set(1)
                componentVars[componentName.get()]['Params']['brineSalinity']['mean'].set(1)
                componentVars[componentName.get()]['Params']['wellRadius']['mean'].set(1)

                componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['std'].set(1)
                componentVars[componentName.get()]['Params']['brineSalinity']['std'].set(1)
                componentVars[componentName.get()]['Params']['wellRadius']['std'].set(1)

                componentVars[componentName.get()]['CO2_aquifer'].set(0)
                componentVars[componentName.get()]['CO2_atm'].set(0)
                componentVars[componentName.get()]['brine_aquifer'].set(0)
                componentVars[componentName.get()]['brine_atm'].set(0)

                openWellboreTab_label = ttk.Label(tabType, text="Open Wellbore Component:", font=LABEL_FONT)
                openWellboreTab_label.grid(row=0, column=0, sticky='w')

                logNormalizedTransmissivity=tk.Frame(tabType)
                logNormalizedTransmissivity.grid(row=1, column=0, sticky='w', padx=15,)

                openWellboreTab_logNormalizedTransmissivity_label = ttk.Label(logNormalizedTransmissivity, text="Transmissivity: [log"+str(u'\u2081'u'\u2080')+"]", width=30)
                openWellboreTab_logNormalizedTransmissivity_menu = tk.OptionMenu(logNormalizedTransmissivity, componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(logNormalizedTransmissivity))
                openWellboreTab_logNormalizedTransmissivity_value_label=ttk.Label(logNormalizedTransmissivity, text='Value:')
                openWellboreTab_logNormalizedTransmissivity_txtField = tk.Entry(logNormalizedTransmissivity, textvariable=componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['value'])
                logNormalizedTransmissivity.component=componentVars[componentName.get()]['componentName']
                logNormalizedTransmissivity.distType=componentVars[componentName.get()]['Params']['logNormalizedTransmissivity']['distribution']
                logNormalizedTransmissivity.labelText='Transmissivity: [log'+str(u'\u2081'u'\u2080')+']'
                logNormalizedTransmissivity.toolTipText = 'Transmissivity'
                logNormalizedTransmissivity.text = 'logNormalizedTransmissivity'

                openWellboreTab_logNormalizedTransmissivity_menu.config(width=15)
                toolTip.bind(openWellboreTab_logNormalizedTransmissivity_menu, 'Select Distriubtion for Transmissivity.')
                openWellboreTab_logNormalizedTransmissivity_label.grid(row=0, column=0, sticky='e', padx=5)
                openWellboreTab_logNormalizedTransmissivity_menu.grid(row=0, column=1, padx=5)
                openWellboreTab_logNormalizedTransmissivity_value_label.grid(row=0, column=2, padx=5)
                openWellboreTab_logNormalizedTransmissivity_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(openWellboreTab_logNormalizedTransmissivity_txtField, 'Set value for Transmissivity.')
                controller.setvar(name='logNormalizedTransmissivity', value=logNormalizedTransmissivity)

                brineSalinity=tk.Frame(tabType)
                brineSalinity.grid(row=2, column=0, sticky='w', padx=15,)

                openWellboreTab_brineSalinity_label = ttk.Label(brineSalinity, text="Brine Salinity: [-]", width=30)
                openWellboreTab_brineSalinity_menu = tk.OptionMenu(brineSalinity, componentVars[componentName.get()]['Params']['brineSalinity']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(brineSalinity))
                openWellboreTab_brineSalinity_value_label=ttk.Label(brineSalinity, text='Value:')
                openWellboreTab_brineSalinity_txtField = tk.Entry(brineSalinity, textvariable=componentVars[componentName.get()]['Params']['brineSalinity']['value'])
                brineSalinity.component=componentVars[componentName.get()]['componentName']
                brineSalinity.distType=componentVars[componentName.get()]['Params']['brineSalinity']['distribution']
                brineSalinity.labelText='Brine Salinity: [-]'
                brineSalinity.toolTipText = 'Reservoir Permeablity'
                brineSalinity.text = 'brineSalinity'

                openWellboreTab_brineSalinity_menu.config(width=15)
                toolTip.bind(openWellboreTab_brineSalinity_menu, 'Select Distriubtion for Brine Salinity.')
                openWellboreTab_brineSalinity_label.grid(row=0, column=0, sticky='e', padx=5)
                openWellboreTab_brineSalinity_menu.grid(row=0, column=1, padx=5)
                openWellboreTab_brineSalinity_value_label.grid(row=0, column=2, padx=5)
                openWellboreTab_brineSalinity_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(openWellboreTab_brineSalinity_txtField, 'Set value for Brine Salinity.')
                controller.setvar(name='brineSalinity', value=brineSalinity)

                wellRadius=tk.Frame(tabType)
                wellRadius.grid(row=3, column=0, sticky='w', padx=15,)

                openWellboreTab_wellRadius_label = ttk.Label(wellRadius, text="Radius of the wellbore: [m]", width=30)
                openWellboreTab_wellRadius_menu = tk.OptionMenu(wellRadius, componentVars[componentName.get()]['Params']['wellRadius']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(wellRadius))
                openWellboreTab_wellRadius_value_label=ttk.Label(wellRadius, text='Value:')
                openWellboreTab_wellRadius_txtField = tk.Entry(wellRadius, textvariable=componentVars[componentName.get()]['Params']['wellRadius']['value'])
                wellRadius.component=componentVars[componentName.get()]['componentName']
                wellRadius.distType=componentVars[componentName.get()]['Params']['wellRadius']['distribution']
                wellRadius.labelText='Radius of the wellbore: [m]'
                wellRadius.toolTipText = 'Radius of the wellbore'
                wellRadius.text = 'wellRadius'

                openWellboreTab_wellRadius_menu.config(width=15)
                toolTip.bind(openWellboreTab_wellRadius_menu, 'Select Distriubtion for Radius of the wellbore.')
                openWellboreTab_wellRadius_label.grid(row=0, column=0, sticky='e', padx=5)
                openWellboreTab_wellRadius_menu.grid(row=0, column=1, padx=5)
                openWellboreTab_wellRadius_value_label.grid(row=0, column=2, padx=5)
                openWellboreTab_wellRadius_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(openWellboreTab_wellRadius_txtField, 'Set value for Radius of the wellbore.')
                controller.setvar(name='wellRadius', value=wellRadius)

                number=tk.Frame(tabType)
                number.grid(row=4, column=0, sticky='w', padx=15,)

                openWellboreTab_number_label = ttk.Label(number, text='Number of Wellbores:', width=30)
                openWellboreTab_number_spinbox = tk.Spinbox(number, from_=1, to=1000, textvariable=componentVars[componentName.get()]['number'])
                openWellboreTab_number_label.grid(row=0, column=0, sticky='e', padx=5)
                openWellboreTab_number_spinbox.grid(row=0, column=1, padx=5)
                toolTip.bind(openWellboreTab_number_spinbox, 'Set the total number of wellbores within this component Modle.')
                controller.setvar(name='brineSalinity', value=brineSalinity)

                leakToFrame = tk.Frame(tabType)
                leakToFrame.grid(row=5, column=0, sticky='w', padx=15)

                leakTo_label = ttk.Label(leakToFrame, text='Leak To:', width=25)
                leakTo_menu = tk.OptionMenu(leakToFrame, componentVars[componentName.get()]['LeakTo'], *['Atmosphere', *aquifers])
                leakTo_menu.config(width=15)
                toolTip.bind(leakTo_menu, 'Select the Aquifer that this wellbore will leak to.')
                leakTo_label.grid(row=0, column=0, sticky='w', padx=5)
                leakTo_menu.grid(row=0, column=1, sticky='e', padx=5)

                openWellboreTab_label = ttk.Label(tabType, text="Outputs:", font=LABEL_FONT)
                openWellboreTab_label.grid(row=10, column=0, sticky='w')

                outputFrame = ttk.Frame(tabType, padding=10)
                outputFrame.grid(row=11, column=0, sticky='w', padx=15)

                openWellbore_co2_aquifer_label = ttk.Label(outputFrame, text="CO"+str(u'\u2082')+" Aquifer: [kg/s]")
                openWellbore_co2_aquifer_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['CO2_aquifer'])
                openWellbore_co2_aquifer_label.grid(row=1, column=0, pady=5, sticky='e')
                openWellbore_co2_aquifer_checkbox.grid(row=1, column=1, pady=5, sticky='w')
                toolTip.bind(openWellbore_co2_aquifer_checkbox, 'Enabled CO2 output of Aquifer.')

                openWellbore_co2_atm_label = ttk.Label(outputFrame, text="CO"+str(u'\u2082')+" Atm: [kg/s]")
                openWellbore_co2_atm_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['CO2_atm'])
                openWellbore_co2_atm_label.grid(row=1, column=2, pady=5, sticky='e')
                openWellbore_co2_atm_checkbox.grid(row=1, column=3, pady=5, sticky='w')
                toolTip.bind(openWellbore_co2_atm_checkbox, 'Enable CO'+str(u'\u2082')+' Atmosphere output')

                openWellbore_brine_aquifer_label = ttk.Label(outputFrame, text="Brine Aquifer: [kg/s]")
                openWellbore_brine_aquifer_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['brine_aquifer'])
                openWellbore_brine_aquifer_label.grid(row=1, column=4, pady=5, sticky='e')
                openWellbore_brine_aquifer_checkbox.grid(row=1, column=5, pady=5, sticky='w')
                toolTip.bind(openWellbore_brine_aquifer_checkbox, 'Enable Brine output of Aquifer.')

                openWellbore_brine_atm_label = ttk.Label(outputFrame, text="Brine Atm: [kg/s]")
                openWellbore_brine_atm_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['brine_atm'])
                openWellbore_brine_atm_label.grid(row=1, column=6, pady=5, sticky='e')
                openWellbore_brine_atm_checkbox.grid(row=1, column=7, pady=5, sticky='w')
                toolTip.bind(openWellbore_brine_atm_checkbox, 'Enable Brine output to Atmosphere.')

            if(componentType.get()=='Carbonate Aquifer' or componentType.get()=='CarbonateAquifer'):
                componentChoices.append(componentName.get())
                componentTypeDictionary.append('carbonateAquifer')
                connectionsDictionary.append(connection.get())

                componentVars[componentName.get()]={}
                componentVars[componentName.get()]['Params']={}
                componentVars[componentName.get()]['ithresh'] = StringVar()
                componentVars[componentName.get()]['ithresh'].set('No-Impact')
                componentVars[componentName.get()]['logf'] = StringVar()
                componentVars[componentName.get()]['logf'].set('Linear')
                componentVars[componentName.get()]['aquiferName']=StringVar()
                componentVars[componentName.get()]['aquiferName'].set(aquiferName.get())

                if(connection.get().find('Param')):
                    componentVars[componentName.get()]['pressure']=pressure.get()
                    componentVars[componentName.get()]['saturation']=saturation.get()

                componentVars[componentName.get()]['Params']['rmin'] = {}
                componentVars[componentName.get()]['Params']['perm_var'] = {}
                componentVars[componentName.get()]['Params']['corr_len'] = {}
                componentVars[componentName.get()]['Params']['aniso'] = {}
                componentVars[componentName.get()]['Params']['mean_perm'] = {}
                componentVars[componentName.get()]['Params']['hyd_grad'] = {}
                componentVars[componentName.get()]['Params']['calcite_ssa'] = {}
                componentVars[componentName.get()]['Params']['organic_carbon'] = {}
                componentVars[componentName.get()]['Params']['benzene_kd'] = {}
                componentVars[componentName.get()]['Params']['benzene_decay'] = {}
                componentVars[componentName.get()]['Params']['nap_kd'] = {}
                componentVars[componentName.get()]['Params']['nap_decay'] = {}
                componentVars[componentName.get()]['Params']['phenol_kd'] = {}
                componentVars[componentName.get()]['Params']['phenol_decay'] = {}
                componentVars[componentName.get()]['Params']['cl'] = {}

                componentVars[componentName.get()]['Params']['rmin']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['perm_var']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['corr_len']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['aniso']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['mean_perm']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['hyd_grad']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['calcite_ssa']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['organic_carbon']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['benzene_kd']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['benzene_decay']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['nap_kd']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['nap_decay']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['phenol_kd']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['phenol_decay']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['cl']['distribution'] = StringVar()

                componentVars[componentName.get()]['Params']['rmin']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['perm_var']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['corr_len']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['aniso']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['mean_perm']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['hyd_grad']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['calcite_ssa']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['organic_carbon']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['benzene_kd']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['benzene_decay']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['nap_kd']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['nap_decay']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['phenol_kd']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['phenol_decay']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['cl']['values'] = StringVar()

                componentVars[componentName.get()]['Params']['rmin']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['perm_var']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['corr_len']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['aniso']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['mean_perm']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['hyd_grad']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['calcite_ssa']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['organic_carbon']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['benzene_kd']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['benzene_decay']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['nap_kd']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['nap_decay']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['phenol_kd']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['phenol_decay']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['cl']['weights'] = StringVar()

                componentVars[componentName.get()]['Params']['rmin']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['perm_var']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['corr_len']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['aniso']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['mean_perm']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['hyd_grad']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['calcite_ssa']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['organic_carbon']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['benzene_kd']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['benzene_decay']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['nap_kd']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['nap_decay']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['phenol_kd']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['phenol_decay']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['cl']['distribution'].set(distributionOptions[0])

                componentVars[componentName.get()]['Params']['rmin']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['perm_var']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['corr_len']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['aniso']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['mean_perm']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['hyd_grad']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['calcite_ssa']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['organic_carbon']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['benzene_kd']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['benzene_decay']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['nap_kd']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['nap_decay']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['phenol_kd']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['phenol_decay']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['cl']['values'].set('15, 21')

                componentVars[componentName.get()]['Params']['rmin']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['perm_var']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['corr_len']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['aniso']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['mean_perm']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['hyd_grad']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['calcite_ssa']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['organic_carbon']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['benzene_kd']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['benzene_decay']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['nap_kd']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['nap_decay']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['phenol_kd']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['phenol_decay']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['cl']['weights'].set('15, 21')

                componentVars[componentName.get()]['Params']['rmin']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['perm_var']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['corr_len']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['aniso']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['mean_perm']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['hyd_grad']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['calcite_ssa']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['organic_carbon']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['benzene_kd']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['benzene_decay']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['nap_kd']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['nap_decay']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['phenol_kd']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['phenol_decay']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['cl']['value'] = DoubleVar()

                componentVars[componentName.get()]['Params']['rmin']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['perm_var']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['corr_len']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['aniso']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['mean_perm']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['hyd_grad']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['calcite_ssa']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['organic_carbon']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['benzene_kd']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['benzene_decay']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['nap_kd']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['nap_decay']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['phenol_kd']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['phenol_decay']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['cl']['min'] = DoubleVar()

                componentVars[componentName.get()]['Params']['rmin']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['perm_var']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['corr_len']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['aniso']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['mean_perm']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['hyd_grad']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['calcite_ssa']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['organic_carbon']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['benzene_kd']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['benzene_decay']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['nap_kd']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['nap_decay']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['phenol_kd']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['phenol_decay']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['cl']['max'] = DoubleVar()

                componentVars[componentName.get()]['Params']['rmin']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['perm_var']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['corr_len']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['aniso']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['mean_perm']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['hyd_grad']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['calcite_ssa']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['organic_carbon']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['benzene_kd']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['benzene_decay']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['nap_kd']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['nap_decay']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['phenol_kd']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['phenol_decay']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['cl']['mean'] = DoubleVar()

                componentVars[componentName.get()]['Params']['rmin']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['perm_var']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['corr_len']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['aniso']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['mean_perm']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['hyd_grad']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['calcite_ssa']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['organic_carbon']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['benzene_kd']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['benzene_decay']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['nap_kd']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['nap_decay']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['phenol_kd']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['phenol_decay']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['cl']['std'] = DoubleVar()

                componentVars[componentName.get()]['pH'] = BooleanVar()
                componentVars[componentName.get()]['Flux'] = BooleanVar()
                componentVars[componentName.get()]['dx'] = BooleanVar()
                componentVars[componentName.get()]['dy'] = BooleanVar()
                componentVars[componentName.get()]['TDS'] = BooleanVar()
                componentVars[componentName.get()]['As'] = BooleanVar()
                componentVars[componentName.get()]['Pb'] = BooleanVar()
                componentVars[componentName.get()]['Cd'] = BooleanVar()
                componentVars[componentName.get()]['Ba'] = BooleanVar()
                componentVars[componentName.get()]['Benzene'] = BooleanVar()
                componentVars[componentName.get()]['Naphthalene'] = BooleanVar()
                componentVars[componentName.get()]['Phenol'] = BooleanVar()

                componentVars[componentName.get()]['componentName']=StringVar()
                componentVars[componentName.get()]['componentType']=StringVar()

                componentVars[componentName.get()]['componentName']=componentName.get()
                componentVars[componentName.get()]['componentType']=componentType.get()

                componentVars[componentName.get()]['Params']['rmin']['value'].set(15)
                componentVars[componentName.get()]['Params']['perm_var']['value'].set(0.9535)
                componentVars[componentName.get()]['Params']['corr_len']['value'].set(2.475)
                componentVars[componentName.get()]['Params']['aniso']['value'].set(25.1)
                componentVars[componentName.get()]['Params']['mean_perm']['value'].set(-12.5)
                componentVars[componentName.get()]['Params']['hyd_grad']['value'].set(9.59e-03)
                componentVars[componentName.get()]['Params']['calcite_ssa']['value'].set(5.5e-03)
                componentVars[componentName.get()]['Params']['organic_carbon']['value'].set(5.5e-03)
                componentVars[componentName.get()]['Params']['benzene_kd']['value'].set(1.61)
                componentVars[componentName.get()]['Params']['benzene_decay']['value'].set(0.595)
                componentVars[componentName.get()]['Params']['nap_kd']['value'].set(2.98)
                componentVars[componentName.get()]['Params']['nap_decay']['value'].set(0.595)
                componentVars[componentName.get()]['Params']['phenol_kd']['value'] .set(1.35)
                componentVars[componentName.get()]['Params']['phenol_decay']['value'].set(0.42)
                componentVars[componentName.get()]['Params']['cl']['value'].set(0.776)

                componentVars[componentName.get()]['Params']['rmin']['min'].set(0)
                componentVars[componentName.get()]['Params']['perm_var']['min'].set(0.17)
                componentVars[componentName.get()]['Params']['corr_len']['min'].set(1)
                componentVars[componentName.get()]['Params']['aniso']['min'].set(1.1)
                componentVars[componentName.get()]['Params']['mean_perm']['min'].set(-13.8)
                componentVars[componentName.get()]['Params']['hyd_grad']['min'].set(2.88e-4)
                componentVars[componentName.get()]['Params']['calcite_ssa']['min'].set(0)
                componentVars[componentName.get()]['Params']['organic_carbon']['min'].set(0)
                componentVars[componentName.get()]['Params']['benzene_kd']['min'].set(1.49)
                componentVars[componentName.get()]['Params']['benzene_decay']['min'].set(0.15)
                componentVars[componentName.get()]['Params']['nap_kd']['min'].set(2.78)
                componentVars[componentName.get()]['Params']['nap_decay']['min'].set(-0.85)
                componentVars[componentName.get()]['Params']['phenol_kd']['min'] .set(1.21)
                componentVars[componentName.get()]['Params']['phenol_decay']['min'].set(-1.22)
                componentVars[componentName.get()]['Params']['cl']['min'].set(0.1)

                componentVars[componentName.get()]['Params']['rmin']['max'].set(100)
                componentVars[componentName.get()]['Params']['perm_var']['max'].set(1.89)
                componentVars[componentName.get()]['Params']['corr_len']['max'].set(3.95)
                componentVars[componentName.get()]['Params']['aniso']['max'].set(49.1)
                componentVars[componentName.get()]['Params']['mean_perm']['max'].set(-10.3)
                componentVars[componentName.get()]['Params']['hyd_grad']['max'].set(1.89e-2)
                componentVars[componentName.get()]['Params']['calcite_ssa']['max'].set(1e-2)
                componentVars[componentName.get()]['Params']['organic_carbon']['max'].set(1e-2)
                componentVars[componentName.get()]['Params']['benzene_kd']['max'].set(1.73)
                componentVars[componentName.get()]['Params']['benzene_decay']['max'].set(2.84)
                componentVars[componentName.get()]['Params']['nap_kd']['max'].set(3.18)
                componentVars[componentName.get()]['Params']['nap_decay']['max'].set(2.04)
                componentVars[componentName.get()]['Params']['phenol_kd']['max'].set(1.48)
                componentVars[componentName.get()]['Params']['phenol_decay']['max'].set(2.06)
                componentVars[componentName.get()]['Params']['cl']['max'].set(6.025)

                componentVars[componentName.get()]['Params']['rmin']['mean'].set(1)
                componentVars[componentName.get()]['Params']['perm_var']['mean'].set(1)
                componentVars[componentName.get()]['Params']['corr_len']['mean'].set(1)
                componentVars[componentName.get()]['Params']['aniso']['mean'].set(1)
                componentVars[componentName.get()]['Params']['mean_perm']['mean'].set(1)
                componentVars[componentName.get()]['Params']['hyd_grad']['mean'].set(1)
                componentVars[componentName.get()]['Params']['calcite_ssa']['mean'].set(1)
                componentVars[componentName.get()]['Params']['organic_carbon']['mean'].set(1)
                componentVars[componentName.get()]['Params']['benzene_kd']['mean'].set(1)
                componentVars[componentName.get()]['Params']['benzene_decay']['mean'].set(1)
                componentVars[componentName.get()]['Params']['nap_kd']['mean'].set(1)
                componentVars[componentName.get()]['Params']['nap_decay']['mean'].set(1)
                componentVars[componentName.get()]['Params']['phenol_kd']['mean'] .set(1)
                componentVars[componentName.get()]['Params']['phenol_decay']['mean'].set(1)
                componentVars[componentName.get()]['Params']['cl']['mean'].set(1)

                componentVars[componentName.get()]['Params']['rmin']['std'].set(1)
                componentVars[componentName.get()]['Params']['perm_var']['std'].set(1)
                componentVars[componentName.get()]['Params']['corr_len']['std'].set(1)
                componentVars[componentName.get()]['Params']['aniso']['std'].set(1)
                componentVars[componentName.get()]['Params']['mean_perm']['std'].set(1)
                componentVars[componentName.get()]['Params']['hyd_grad']['std'].set(1)
                componentVars[componentName.get()]['Params']['calcite_ssa']['std'].set(1)
                componentVars[componentName.get()]['Params']['organic_carbon']['std'].set(1)
                componentVars[componentName.get()]['Params']['benzene_kd']['std'].set(1)
                componentVars[componentName.get()]['Params']['benzene_decay']['std'].set(1)
                componentVars[componentName.get()]['Params']['nap_kd']['std'].set(1)
                componentVars[componentName.get()]['Params']['nap_decay']['std'].set(1)
                componentVars[componentName.get()]['Params']['phenol_kd']['std'] .set(1)
                componentVars[componentName.get()]['Params']['phenol_decay']['std'].set(1)
                componentVars[componentName.get()]['Params']['cl']['std'].set(1)

                componentVars[componentName.get()]['pH'].set(0)
                componentVars[componentName.get()]['Flux'].set(0)
                componentVars[componentName.get()]['dx'].set(0)
                componentVars[componentName.get()]['dy'].set(0)
                componentVars[componentName.get()]['TDS'].set(0)
                componentVars[componentName.get()]['As'].set(0)
                componentVars[componentName.get()]['Pb'].set(0)
                componentVars[componentName.get()]['Cd'].set(0)
                componentVars[componentName.get()]['Ba'].set(0)
                componentVars[componentName.get()]['Benzene'].set(0)
                componentVars[componentName.get()]['Naphthalene'].set(0)
                componentVars[componentName.get()]['Phenol'].set(0)

                carbonateAquiferTab_label = ttk.Label(tabType, text="Carbonate Aquifer Component:", font=LABEL_FONT)
                carbonateAquiferTab_label.grid(row=0, column=0, sticky='w')

                ithresh=tk.Frame(tabType)
                ithresh.grid(row=1, column=0, sticky='w', padx=15)

                carbonateAquifer_ithresh_label = ttk.Label(ithresh, text="Threshold: [-]", width=30)
                carbonateAquifer_ithresh_menu = tk.OptionMenu(ithresh, componentVars[componentName.get()]['ithresh'], *['MCL', 'No-Impact'])
                carbonateAquifer_ithresh_menu.config(width=15)
                carbonateAquifer_ithresh_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_ithresh_menu.grid(row=0, column=1, padx=5)
                toolTip.bind(carbonateAquifer_ithresh_menu, 'Select which Threshold you wish to utilize.')

                rmin=tk.Frame(tabType)
                rmin.grid(row=2, column=0, sticky='w', padx=15)

                carbonateAquifer_rmin_label = ttk.Label(rmin, text="Distance between Leaks: [m]", width=30)
                carbonateAquifer_rmin_menu = tk.OptionMenu(rmin, componentVars[componentName.get()]['Params']['rmin']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(rmin))
                carbonateAquifer_rmin_value_label=ttk.Label(rmin, text='Value:')
                carbonateAquifer_rmin_txtField = tk.Entry(rmin, textvariable=componentVars[componentName.get()]['Params']['rmin']['value'])
                rmin.component=componentVars[componentName.get()]['componentName']
                rmin.distType=componentVars[componentName.get()]['Params']['rmin']['distribution']
                rmin.labelText='Distance between Leaks: [m]'
                rmin.toolTipText = 'Distance between Leaks'
                rmin.text = 'rmin'

                carbonateAquifer_rmin_menu.config(width=15)
                toolTip.bind(carbonateAquifer_rmin_menu, 'Select Distriubtion for Distance between Leaks.')
                carbonateAquifer_rmin_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_rmin_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_rmin_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_rmin_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_rmin_txtField, 'Set value for Reservoir Permeablity.')
                controller.setvar(name='rmin', value=rmin)

                perm_var=tk.Frame(tabType)
                perm_var.grid(row=3, column=0, sticky='w', padx=15)

                carbonateAquifer_perm_var_label = ttk.Label(perm_var, text="Permeability Variance: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u2074')+"]", width=30)
                carbonateAquifer_perm_var_menu = tk.OptionMenu(perm_var, componentVars[componentName.get()]['Params']['perm_var']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(perm_var))
                carbonateAquifer_perm_var_value_label=ttk.Label(perm_var, text='Value:')
                carbonateAquifer_perm_var_txtField = tk.Entry(perm_var, textvariable=componentVars[componentName.get()]['Params']['perm_var']['value'])
                perm_var.labelText="Permeability Variance: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u2074')+"]"
                perm_var.component=componentVars[componentName.get()]['componentName']
                perm_var.distType=componentVars[componentName.get()]['Params']['perm_var']['distribution']
                perm_var.toolTipText = 'Permeability Variance'
                perm_var.text = 'perm_var'

                carbonateAquifer_perm_var_menu.config(width=15)
                toolTip.bind(carbonateAquifer_perm_var_menu, 'Select Distriubtion for Permeability Variance.')
                carbonateAquifer_perm_var_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_perm_var_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_perm_var_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_perm_var_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_perm_var_txtField, 'Set value for Permeability Variance.')
                controller.setvar(name='perm_var', value=perm_var)

                corr_len=tk.Frame(tabType)
                corr_len.grid(row=4, column=0, sticky='w', padx=15)

                carbonateAquifer_corr_len_label = ttk.Label(corr_len, text="Correlation length: [m]", width=30)
                carbonateAquifer_corr_len_menu = tk.OptionMenu(corr_len, componentVars[componentName.get()]['Params']['corr_len']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(corr_len))
                carbonateAquifer_corr_len_value_label=ttk.Label(corr_len, text='Value:')
                carbonateAquifer_corr_len_txtField = tk.Entry(corr_len, textvariable=componentVars[componentName.get()]['Params']['corr_len']['value'])
                corr_len.labelText='Correlation length: [m]'
                corr_len.component=componentVars[componentName.get()]['componentName']
                corr_len.distType=componentVars[componentName.get()]['Params']['corr_len']['distribution']
                corr_len.toolTipText = 'Correlation length'
                corr_len.text = 'corr_len'

                carbonateAquifer_corr_len_menu.config(width=15)
                toolTip.bind(carbonateAquifer_corr_len_menu, 'Select Distriubtion for Correlation length.')
                carbonateAquifer_corr_len_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_corr_len_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_corr_len_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_corr_len_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_corr_len_txtField, 'Set value for Correlation length.')
                controller.setvar(name='corr_len', value=corr_len)

                aniso=tk.Frame(tabType)
                aniso.grid(row=5, column=0, sticky='w', padx=15)

                carbonateAquifer_aniso_label = ttk.Label(aniso, text="Anisotropy Factor: [-]", width=30)
                carbonateAquifer_aniso_menu = tk.OptionMenu(aniso, componentVars[componentName.get()]['Params']['aniso']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(aniso))
                carbonateAquifer_aniso_value_label=ttk.Label(aniso, text='Value:')
                carbonateAquifer_aniso_txtField = tk.Entry(aniso, textvariable=componentVars[componentName.get()]['Params']['aniso']['value'])
                aniso.labelText='Anisotropy Factor: [-]'
                aniso.component=componentVars[componentName.get()]['componentName']
                aniso.distType=componentVars[componentName.get()]['Params']['aniso']['distribution']
                aniso.toolTipText = 'Anisotropy Factor'
                aniso.text = 'aniso'

                carbonateAquifer_aniso_menu.config(width=15)
                toolTip.bind(carbonateAquifer_aniso_menu, 'Select Distriubtion for Anisotropy Factor.')
                carbonateAquifer_aniso_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_aniso_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_aniso_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_aniso_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_aniso_txtField, 'Set value for Anisotropy Factor.')
                controller.setvar(name='aniso', value=aniso)

                mean_perm=tk.Frame(tabType)
                mean_perm.grid(row=6, column=0, sticky='w', padx=15)

                carbonateAquifer_mean_perm_label = ttk.Label(mean_perm, text="Mean Permeability: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]", width=30)
                carbonateAquifer_mean_perm_menu = tk.OptionMenu(mean_perm, componentVars[componentName.get()]['Params']['mean_perm']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(mean_perm))
                carbonateAquifer_mean_perm_value_label=ttk.Label(mean_perm, text='Value:')
                carbonateAquifer_mean_perm_txtField = tk.Entry(mean_perm, textvariable=componentVars[componentName.get()]['Params']['mean_perm']['value'])
                mean_perm.labelText="Mean Permeability: [log"+str(u'\u2081'u'\u2080')+" m"+str(u'\u00B2')+"]"
                mean_perm.component=componentVars[componentName.get()]['componentName']
                mean_perm.distType=componentVars[componentName.get()]['Params']['mean_perm']['distribution']
                mean_perm.toolTipText = 'Mean Permeablity'
                mean_perm.text = 'mean_perm'

                carbonateAquifer_mean_perm_menu.config(width=15)
                toolTip.bind(carbonateAquifer_mean_perm_menu, 'Select Distriubtion for Mean Permeabilty.')
                carbonateAquifer_mean_perm_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_mean_perm_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_mean_perm_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_mean_perm_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_mean_perm_txtField, 'Set value for Mean Permeablity.')
                controller.setvar(name='mean_perm', value=mean_perm)

                hyd_grad=tk.Frame(tabType)
                hyd_grad.grid(row=7, column=0, sticky='w', padx=15)

                carbonateAquifer_hyd_grad_label = ttk.Label(hyd_grad, text="Hydraulic gradient: [-]", width=30)
                carbonateAquifer_hyd_grad_menu = tk.OptionMenu(hyd_grad, componentVars[componentName.get()]['Params']['hyd_grad']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(hyd_grad))
                carbonateAquifer_hyd_grad_value_label=ttk.Label(hyd_grad, text='Value:')
                carbonateAquifer_hyd_grad_txtField = tk.Entry(hyd_grad, textvariable=componentVars[componentName.get()]['Params']['hyd_grad']['value'])
                hyd_grad.labelText='Hydraulic gradient: [-]'
                hyd_grad.component=componentVars[componentName.get()]['componentName']
                hyd_grad.distType=componentVars[componentName.get()]['Params']['hyd_grad']['distribution']
                hyd_grad.toolTipText = 'Hydraulic gradient'
                hyd_grad.text = 'hyd_grad'

                carbonateAquifer_hyd_grad_menu.config(width=15)
                toolTip.bind(carbonateAquifer_hyd_grad_menu, 'Select Distriubtion for Hydraulic gradient.')
                carbonateAquifer_hyd_grad_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_hyd_grad_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_hyd_grad_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_hyd_grad_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_hyd_grad_txtField, 'Set value for Hydraulic gradient.')
                controller.setvar(name='hyd_grad', value=hyd_grad)

                calcite_ssa=tk.Frame(tabType)
                calcite_ssa.grid(row=8, column=0, sticky='w', padx=15)

                carbonateAquifer_calcite_ssa_label = ttk.Label(calcite_ssa, text="Calcite surface: [m"+str(u'\u00B2')+"/g]", width=30)
                carbonateAquifer_calcite_ssa_menu = tk.OptionMenu(calcite_ssa, componentVars[componentName.get()]['Params']['calcite_ssa']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(calcite_ssa))
                carbonateAquifer_calcite_ssa_value_label=ttk.Label(calcite_ssa, text='Value:')
                carbonateAquifer_calcite_ssa_txtField = tk.Entry(calcite_ssa, textvariable=componentVars[componentName.get()]['Params']['calcite_ssa']['value'])
                calcite_ssa.labelText='Calcite surface: [m'+str(u'\u00B2')+'/g]'
                calcite_ssa.component=componentVars[componentName.get()]['componentName']
                calcite_ssa.distType=componentVars[componentName.get()]['Params']['calcite_ssa']['distribution']
                calcite_ssa.toolTipText = 'Calcite surface'
                calcite_ssa.text = 'calcite_ssa'

                carbonateAquifer_calcite_ssa_menu.config(width=15)
                toolTip.bind(carbonateAquifer_calcite_ssa_menu, 'Select Distriubtion for Calcite surface.')
                carbonateAquifer_calcite_ssa_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_calcite_ssa_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_calcite_ssa_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_calcite_ssa_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_calcite_ssa_txtField, 'Set value for Calcite surface.')
                controller.setvar(name='calcite_ssa', value=calcite_ssa)

                organic_carbon=tk.Frame(tabType)
                organic_carbon.grid(row=9, column=0, sticky='w', padx=15)

                carbonateAquifer_organic_carbon_label = ttk.Label(organic_carbon, text="Organic carbon: [-]", width=30)
                carbonateAquifer_organic_carbon_menu = tk.OptionMenu(organic_carbon, componentVars[componentName.get()]['Params']['organic_carbon']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(organic_carbon))
                carbonateAquifer_organic_carbon_value_label=ttk.Label(organic_carbon, text='Value:')
                carbonateAquifer_organic_carbon_txtField = tk.Entry(organic_carbon, textvariable=componentVars[componentName.get()]['Params']['organic_carbon']['value'])
                organic_carbon.labelText='Organic carbon: [-]'
                organic_carbon.component=componentVars[componentName.get()]['componentName']
                organic_carbon.distType=componentVars[componentName.get()]['Params']['organic_carbon']['distribution']
                organic_carbon.toolTipText = 'Organic carbon'
                organic_carbon.text = 'organic_carbon'

                carbonateAquifer_organic_carbon_menu.config(width=15)
                toolTip.bind(carbonateAquifer_organic_carbon_menu, 'Select Distriubtion for Reservoir Permeabilty.')
                carbonateAquifer_organic_carbon_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_organic_carbon_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_organic_carbon_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_organic_carbon_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_organic_carbon_txtField, 'Set value for Reservoir Permeablity.')
                controller.setvar(name='organic_carbon', value=organic_carbon)

                benzene_kd=tk.Frame(tabType)
                benzene_kd.grid(row=10, column=0, sticky='w', padx=15)

                carbonateAquifer_benzene_kd_label = ttk.Label(benzene_kd, text="Benzene distribution: [log"+str(u'\u2081'u'\u2080')+" K_oc]", width=30)
                carbonateAquifer_benzene_kd_menu = tk.OptionMenu(benzene_kd, componentVars[componentName.get()]['Params']['benzene_kd']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(benzene_kd))
                carbonateAquifer_benzene_kd_value_label=ttk.Label(benzene_kd, text='Value:')
                carbonateAquifer_benzene_kd_txtField = tk.Entry(benzene_kd, textvariable=componentVars[componentName.get()]['Params']['benzene_kd']['value'])
                benzene_kd.labelText="Benzene distribution:  [log"+str(u'\u2081'u'\u2080')+" K_oc]"
                benzene_kd.component=componentVars[componentName.get()]['componentName']
                benzene_kd.distType=componentVars[componentName.get()]['Params']['benzene_kd']['distribution']
                benzene_kd.toolTipText = 'Benzene distribution'
                benzene_kd.text = 'benzene_kd'

                carbonateAquifer_benzene_kd_menu.config(width=15)
                toolTip.bind(carbonateAquifer_benzene_kd_menu, 'Select Distriubtion for Reservoir Permeabilty.')
                carbonateAquifer_benzene_kd_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_benzene_kd_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_benzene_kd_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_benzene_kd_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_benzene_kd_txtField, 'Set value for Reservoir Permeablity.')
                controller.setvar(name='benzene_kd', value=benzene_kd)

                benzene_decay=tk.Frame(tabType)
                benzene_decay.grid(row=11, column=0, sticky='w', padx=15)

                carbonateAquifer_benzene_decay_label = ttk.Label(benzene_decay, text="Benzene decay: [log"+str(u'\u2081'u'\u2080')+" day]", width=30)
                carbonateAquifer_benzene_decay_menu = tk.OptionMenu(benzene_decay, componentVars[componentName.get()]['Params']['benzene_decay']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(benzene_decay))
                carbonateAquifer_benzene_decay_value_label=ttk.Label(benzene_decay, text='Value:')
                carbonateAquifer_benzene_decay_txtField = tk.Entry(benzene_decay, textvariable=componentVars[componentName.get()]['Params']['benzene_decay']['value'])
                benzene_decay.labelText="Benzene decay:  [log"+str(u'\u2081'u'\u2080')+" day]"
                benzene_decay.component=componentVars[componentName.get()]['componentName']
                benzene_decay.distType=componentVars[componentName.get()]['Params']['benzene_decay']['distribution']
                benzene_decay.toolTipText = 'Benzene decay'
                benzene_decay.text = 'benzene_decay'

                carbonateAquifer_benzene_decay_menu.config(width=15)
                toolTip.bind(carbonateAquifer_benzene_decay_menu, 'Select Distriubtion for Benzene decay.')
                carbonateAquifer_benzene_decay_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_benzene_decay_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_benzene_decay_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_benzene_decay_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_benzene_decay_txtField, 'Set value for Benzene decay.')
                controller.setvar(name='benzene_decay', value=benzene_decay)

                nap_kd=tk.Frame(tabType)
                nap_kd.grid(row=12, column=0, sticky='w', padx=15)

                carbonateAquifer_nap_kd_label = ttk.Label(nap_kd, text="Naphthalene distribution: [log"+str(u'\u2081'u'\u2080')+" K_oc]", width=30)
                carbonateAquifer_nap_kd_menu = tk.OptionMenu(nap_kd, componentVars[componentName.get()]['Params']['nap_kd']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(nap_kd))
                carbonateAquifer_nap_kd_value_label=ttk.Label(nap_kd, text='Value:')
                carbonateAquifer_nap_kd_txtField = tk.Entry(nap_kd, textvariable=componentVars[componentName.get()]['Params']['nap_kd']['value'])
                nap_kd.labelText="Naphthalene distribution:  [log"+str(u'\u2081'u'\u2080')+" K_oc]"
                nap_kd.component=componentVars[componentName.get()]['componentName']
                nap_kd.distType=componentVars[componentName.get()]['Params']['nap_kd']['distribution']
                nap_kd.toolTipText = 'Naphthalene distribution'
                nap_kd.text = 'nap_kd'

                carbonateAquifer_nap_kd_menu.config(width=15)
                toolTip.bind(carbonateAquifer_nap_kd_menu, 'Select Distriubtion for Naphthalene distribution.')
                carbonateAquifer_nap_kd_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_nap_kd_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_nap_kd_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_nap_kd_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_nap_kd_txtField, 'Set value for Naphthalene distribution.')
                controller.setvar(name='nap_kd', value=nap_kd)

                nap_decay=tk.Frame(tabType)
                nap_decay.grid(row=13, column=0, sticky='w', padx=15)

                carbonateAquifer_nap_decay_label = ttk.Label(nap_decay, text="Naphthalene decay: [log"+str(u'\u2081'u'\u2080')+" day]", width=30)
                carbonateAquifer_nap_decay_menu = tk.OptionMenu(nap_decay, componentVars[componentName.get()]['Params']['nap_decay']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(nap_decay))
                carbonateAquifer_nap_decay_value_label=ttk.Label(nap_decay, text='Value:')
                carbonateAquifer_nap_decay_txtField = tk.Entry(nap_decay, textvariable=componentVars[componentName.get()]['Params']['nap_decay']['value'])
                nap_decay.labelText="Naphthalene decay:  [log"+str(u'\u2081'u'\u2080')+" day]"
                nap_decay.component=componentVars[componentName.get()]['componentName']
                nap_decay.distType=componentVars[componentName.get()]['Params']['nap_decay']['distribution']
                nap_decay.toolTipText = 'Naphthalene decay'
                nap_decay.text = 'nap_decay'

                carbonateAquifer_nap_decay_menu.config(width=15)
                toolTip.bind(carbonateAquifer_nap_decay_menu, 'Select Distriubtion for Naphthalene decay.')
                carbonateAquifer_nap_decay_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_nap_decay_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_nap_decay_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_nap_decay_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_nap_decay_txtField, 'Set value for Naphthalene decay.')
                controller.setvar(name='nap_decay', value=nap_decay)

                phenol_kd=tk.Frame(tabType)
                phenol_kd.grid(row=14, column=0, sticky='w', padx=15)

                carbonateAquifer_phenol_kd_label = ttk.Label(phenol_kd, text="Phenol distribution: [log"+str(u'\u2081'u'\u2080')+" K_oc]", width=30)
                carbonateAquifer_phenol_kd_menu = tk.OptionMenu(phenol_kd, componentVars[componentName.get()]['Params']['phenol_kd']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(phenol_kd))
                carbonateAquifer_phenol_kd_value_label=ttk.Label(phenol_kd, text='Value:')
                carbonateAquifer_phenol_kd_txtField = tk.Entry(phenol_kd, textvariable=componentVars[componentName.get()]['Params']['phenol_kd']['value'])
                phenol_kd.labelText="Phenol distribution:  [log"+str(u'\u2081'u'\u2080')+" K_oc]"
                phenol_kd.component=componentVars[componentName.get()]['componentName']
                phenol_kd.distType=componentVars[componentName.get()]['Params']['phenol_kd']['distribution']
                phenol_kd.toolTipText = 'Phenol distribution'
                phenol_kd.text = 'phenol_kd'

                carbonateAquifer_phenol_kd_menu.config(width=15)
                toolTip.bind(carbonateAquifer_phenol_kd_menu, 'Select Distriubtion for Phenol distribution.')
                carbonateAquifer_phenol_kd_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_phenol_kd_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_phenol_kd_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_phenol_kd_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_phenol_kd_txtField, 'Set value for Phenol distribution.')
                controller.setvar(name='phenol_kd', value=phenol_kd)

                phenol_decay=tk.Frame(tabType)
                phenol_decay.grid(row=15, column=0, sticky='w', padx=15)

                carbonateAquifer_phenol_decay_label = ttk.Label(phenol_decay, text="Phenol decay: [log"+str(u'\u2081'u'\u2080')+" day]", width=30)
                carbonateAquifer_phenol_decay_menu = tk.OptionMenu(phenol_decay, componentVars[componentName.get()]['Params']['phenol_decay']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(phenol_decay))
                carbonateAquifer_phenol_decay_value_label=ttk.Label(phenol_decay, text='Value:')
                carbonateAquifer_phenol_decay_txtField = tk.Entry(phenol_decay, textvariable=componentVars[componentName.get()]['Params']['phenol_decay']['value'])
                phenol_decay.labelText="Phenol decay constant: [log"+str(u'\u2081'u'\u2080')+" day]"
                phenol_decay.component=componentVars[componentName.get()]['componentName']
                phenol_decay.distType=componentVars[componentName.get()]['Params']['phenol_decay']['distribution']
                phenol_decay.toolTipText = 'Phenol decay constant'
                phenol_decay.text = 'phenol_decay'

                carbonateAquifer_phenol_decay_menu.config(width=15)
                toolTip.bind(carbonateAquifer_phenol_decay_menu, 'Select Distriubtion for Phenol decay constant.')
                carbonateAquifer_phenol_decay_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_phenol_decay_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_phenol_decay_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_phenol_decay_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_phenol_decay_txtField, 'Set value for Phenol decay constant.')
                controller.setvar(name='phenol_decay', value=phenol_decay)

                cl=tk.Frame(tabType)
                cl.grid(row=16, column=0, sticky='w', padx=15)

                carbonateAquifer_cl_label = ttk.Label(cl, text="Brine salinity: [log"+str(u'\u2081'u'\u2080')+" Molality]", width=30)
                carbonateAquifer_cl_menu = tk.OptionMenu(cl, componentVars[componentName.get()]['Params']['cl']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(cl))
                carbonateAquifer_cl_value_label=ttk.Label(cl, text='Value:')
                carbonateAquifer_cl_txtField = tk.Entry(cl, textvariable=componentVars[componentName.get()]['Params']['cl']['value'])
                cl.labelText="Brine salinity: [log"+str(u'\u2081'u'\u2080')+" Molality]"
                cl.component=componentVars[componentName.get()]['componentName']
                cl.distType=componentVars[componentName.get()]['Params']['cl']['distribution']
                cl.toolTipText = 'Brine salinity'
                cl.text = 'cl'

                carbonateAquifer_cl_menu.config(width=15)
                toolTip.bind(carbonateAquifer_cl_menu, 'Select Distriubtion for Brine salinity.')
                carbonateAquifer_cl_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_cl_menu.grid(row=0, column=1, padx=5)
                carbonateAquifer_cl_value_label.grid(row=0, column=2, padx=5)
                carbonateAquifer_cl_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(carbonateAquifer_cl_txtField, 'Set value for Brine salinity.')
                controller.setvar(name='cl', value=cl)

                logf=tk.Frame(tabType)
                logf.grid(row=17, column=0, sticky='w', padx=15)

                carbonateAquifer_logf_label = ttk.Label(logf, text="Log transform:", width=30)
                carbonateAquifer_logf_menu = tk.OptionMenu(logf, componentVars[componentName.get()]['logf'], *['Linear', 'Log'])

                carbonateAquifer_logf_menu.config(width=15)
                carbonateAquifer_logf_label.grid(row=0, column=0, sticky='e', padx=5)
                carbonateAquifer_logf_menu.grid(row=0, column=1, padx=5)
                toolTip.bind(carbonateAquifer_logf_menu, 'Select the type of Transform you wish to use for this Componet Model.')

                carbonateAquifer_outputs_label = ttk.Label(tabType, text="Outputs:", font=LABEL_FONT)
                carbonateAquifer_outputs_label.grid(row=18, column=0, sticky='w')

                outputFrame = ttk.Frame(tabType, padding=10)
                outputFrame.grid(row=19, column=0, sticky='w', padx=15, columnspan=20)

                carbonateAquifer_ph_label = ttk.Label(outputFrame, text="pH Threshold: [m"+str(u'\u00B3')+"]")
                carbonateAquifer_ph_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['pH'])
                carbonateAquifer_ph_label.grid(row=1, column=0, pady=5, sticky='e')
                carbonateAquifer_ph_checkbox.grid(row=1, column=1, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_ph_checkbox, 'Enable pH Threshold output.')

                carbonateAquifer_flux_label = ttk.Label(outputFrame, text="Flux Atmosphere: [kg/s]")
                carbonateAquifer_flux_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Flux'])
                carbonateAquifer_flux_label.grid(row=1, column=2, pady=5, sticky='e')
                carbonateAquifer_flux_checkbox.grid(row=1, column=3, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_flux_checkbox, 'Enable Flux Atmosphere output.')

                carbonateAquifer_dx_label = ttk.Label(outputFrame, text="Aquifer vol in X-Direction: [m]")
                carbonateAquifer_dx_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['dx'])
                carbonateAquifer_dx_label.grid(row=1, column=4, pady=5, sticky='e')
                carbonateAquifer_dx_checkbox.grid(row=1, column=5, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_dx_checkbox, 'Enable Aquifer vol in X-Direction output.')

                carbonateAquifer_dy_label = ttk.Label(outputFrame, text="Aquifer vol in Y-Direction: [m]")
                carbonateAquifer_dy_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['dy'])
                carbonateAquifer_dy_label.grid(row=1, column=6, pady=5, sticky='e')
                carbonateAquifer_dy_checkbox.grid(row=1, column=7, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_dy_checkbox, 'Enable Aquifer vol in Y-Direction output.')

                carbonateAquifer_tds_label = ttk.Label(outputFrame, text="Aquifer TDS Threshold: [m"+str(u'\u00B3')+"]")
                carbonateAquifer_tds_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['TDS'])
                carbonateAquifer_tds_label.grid(row=2, column=0, pady=5, sticky='e')
                carbonateAquifer_tds_checkbox.grid(row=2, column=1, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_tds_checkbox, 'Enable Aquifer TDS Threshold output.')

                carbonateAquifer_as_label = ttk.Label(outputFrame, text="Aquifer Arsenic Threshold: [m"+str(u'\u00B3')+"]")
                carbonateAquifer_as_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['As'])
                carbonateAquifer_as_label.grid(row=2, column=2, pady=5, sticky='e')
                carbonateAquifer_as_checkbox.grid(row=2, column=3, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_as_checkbox, 'Enable Aquifer Arsenic Threshold output.')

                carbonateAquifer_pb_label = ttk.Label(outputFrame, text="Aquifer Lead Threshold: [m"+str(u'\u00B3')+"]")
                carbonateAquifer_pb_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Pb'])
                carbonateAquifer_pb_label.grid(row=2, column=4, pady=5, sticky='e')
                carbonateAquifer_pb_checkbox.grid(row=2, column=5, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_pb_checkbox, 'Enable Aquifer Lead Threshold output.')

                carbonateAquifer_cd_label = ttk.Label(outputFrame, text="Aquifer Cadmium Threshold: [m"+str(u'\u00B3')+"]")
                carbonateAquifer_cd_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Cd'])
                carbonateAquifer_cd_label.grid(row=2, column=6, pady=5, sticky='e')
                carbonateAquifer_cd_checkbox.grid(row=2, column=7, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_cd_checkbox, 'Enable Aquifer Cadmium Threshold output')

                carbonateAquifer_ba_label = ttk.Label(outputFrame, text="Aquifer Barium Threshold: [m"+str(u'\u00B3')+"]")
                carbonateAquifer_ba_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Ba'])
                carbonateAquifer_ba_label.grid(row=3, column=0, pady=5, sticky='e')
                carbonateAquifer_ba_checkbox.grid(row=3, column=1, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_ba_checkbox, 'Enable Aquifer Barium Threshold output.')

                carbonateAquifer_benzene_label = ttk.Label(outputFrame, text="Aquifer Benzene Threshold: [m"+str(u'\u00B3')+"]")
                carbonateAquifer_benzene_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Benzene'])
                carbonateAquifer_benzene_label.grid(row=3, column=2, pady=5, sticky='e')
                carbonateAquifer_benzene_checkbox.grid(row=3, column=3, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_benzene_checkbox, 'Enable Aquifer Benzene Threshold output.')

                carbonateAquifer_naphthalene_label = ttk.Label(outputFrame, text="Aquifer Naphthalene Threshold: [m"+str(u'\u00B3')+"]")
                carbonateAquifer_naphthalene_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Naphthalene'])
                carbonateAquifer_naphthalene_label.grid(row=3, column=4, pady=5, sticky='e')
                carbonateAquifer_naphthalene_checkbox.grid(row=3, column=5, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_naphthalene_checkbox, 'Enable Aquifer Naphthalene Threshold output.')

                carbonateAquifer_phenol_label = ttk.Label(outputFrame, text="Aquifer Phenol Threshold: [m"+str(u'\u00B3')+"]")
                carbonateAquifer_phenol_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Phenol'])
                carbonateAquifer_phenol_label.grid(row=3, column=6, pady=5, sticky='e')
                carbonateAquifer_phenol_checkbox.grid(row=3, column=7, pady=5, sticky='w')
                toolTip.bind(carbonateAquifer_phenol_checkbox, 'Enable Aquifer Phenol Threshold output.')

            if(componentType.get()=='Deep Alluvium Aquifer' or componentType.get()=='DeepAlluviumAquifer'):
                componentChoices.append(componentName.get())
                componentTypeDictionary.append('alluviumAquifer')
                connectionsDictionary.append(connection.get())

                componentVars[componentName.get()] = {}
                componentVars[componentName.get()]['aquiferName']=StringVar()
                componentVars[componentName.get()]['aquiferName'].set(aquiferName.get())

                componentVars[componentName.get()]['Params'] = {}
                componentVars[componentName.get()]['Params']['brine_rate'] = {}
                componentVars[componentName.get()]['Params']['brine_mass'] = {}
                componentVars[componentName.get()]['Params']['co2_rate'] = {}
                componentVars[componentName.get()]['Params']['co2_mass'] = {}
                componentVars[componentName.get()]['Params']['logK_sand1'] = {}
                componentVars[componentName.get()]['Params']['logK_sand2'] = {}
                componentVars[componentName.get()]['Params']['logK_sand3'] = {}
                componentVars[componentName.get()]['Params']['logK_caprock'] = {}
                componentVars[componentName.get()]['Params']['correlationLengthX'] = {}
                componentVars[componentName.get()]['Params']['correlationLengthZ'] = {}
                componentVars[componentName.get()]['Params']['sandFraction'] = {}
                componentVars[componentName.get()]['Params']['groundwater_gradient'] = {}
                componentVars[componentName.get()]['Params']['leak_depth'] = {}

                if(connection.get().find('Param')):
                    componentVars[componentName.get()]['pressure']=pressure.get()
                    componentVars[componentName.get()]['saturation']=saturation.get()

                componentVars[componentName.get()]['Params']['brine_rate']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['brine_mass']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['co2_rate']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['co2_mass']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_sand1']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_sand2']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_sand3']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_caprock']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['correlationLengthX']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['correlationLengthZ']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['sandFraction']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['groundwater_gradient']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['leak_depth']['distribution'] = StringVar()

                componentVars[componentName.get()]['Params']['brine_rate']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['brine_mass']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['co2_rate']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['co2_mass']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_sand1']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_sand2']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_sand3']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_caprock']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['correlationLengthX']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['correlationLengthZ']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['sandFraction']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['groundwater_gradient']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['leak_depth']['values'] = StringVar()

                componentVars[componentName.get()]['Params']['brine_rate']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['brine_mass']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['co2_rate']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['co2_mass']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_sand1']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_sand2']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_sand3']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['logK_caprock']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['correlationLengthX']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['correlationLengthZ']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['sandFraction']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['groundwater_gradient']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['leak_depth']['weights'] = StringVar()

                componentVars[componentName.get()]['Params']['brine_rate']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['brine_mass']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['co2_rate']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['co2_mass']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['logK_sand1']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['logK_sand2']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['logK_sand3']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['logK_caprock']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['correlationLengthX']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['correlationLengthZ']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['sandFraction']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['groundwater_gradient']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['leak_depth']['distribution'].set(distributionOptions[0])

                componentVars[componentName.get()]['Params']['brine_rate']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['brine_mass']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['co2_rate']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['co2_mass']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['logK_sand1']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['logK_sand2']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['logK_sand3']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['logK_caprock']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['correlationLengthX']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['correlationLengthZ']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['sandFraction']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['groundwater_gradient']['values'].set('15, 223')
                componentVars[componentName.get()]['Params']['leak_depth']['values'].set('15, 223')

                componentVars[componentName.get()]['Params']['brine_rate']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['brine_mass']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['co2_rate']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['co2_mass']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['logK_sand1']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['logK_sand2']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['logK_sand3']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['logK_caprock']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['correlationLengthX']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['correlationLengthZ']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['sandFraction']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['groundwater_gradient']['weights'].set('15, 223')
                componentVars[componentName.get()]['Params']['leak_depth']['weights'].set('15, 223')

                componentVars[componentName.get()]['Params']['brine_rate']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brine_mass']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['co2_rate']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['co2_mass']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand1']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand2']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand3']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_caprock']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['correlationLengthX']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['correlationLengthZ']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['sandFraction']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['groundwater_gradient']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['leak_depth']['value'] = DoubleVar()

                componentVars[componentName.get()]['Params']['brine_rate']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brine_mass']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['co2_rate']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['co2_mass']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand1']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand2']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand3']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_caprock']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['correlationLengthX']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['correlationLengthZ']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['sandFraction']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['groundwater_gradient']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['leak_depth']['min'] = DoubleVar()

                componentVars[componentName.get()]['Params']['brine_rate']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brine_mass']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['co2_rate']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['co2_mass']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand1']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand2']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand3']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_caprock']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['correlationLengthX']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['correlationLengthZ']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['sandFraction']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['groundwater_gradient']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['leak_depth']['max'] = DoubleVar()

                componentVars[componentName.get()]['Params']['brine_rate']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brine_mass']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['co2_rate']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['co2_mass']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand1']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand2']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand3']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_caprock']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['correlationLengthX']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['correlationLengthZ']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['sandFraction']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['groundwater_gradient']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['leak_depth']['mean'] = DoubleVar()

                componentVars[componentName.get()]['Params']['brine_rate']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['brine_mass']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['co2_rate']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['co2_mass']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand1']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand2']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_sand3']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['logK_caprock']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['correlationLengthX']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['correlationLengthZ']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['sandFraction']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['groundwater_gradient']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['leak_depth']['std'] = DoubleVar()

                componentVars[componentName.get()]['TDS_volume'] = BooleanVar()
                componentVars[componentName.get()]['TDS_dx'] = BooleanVar()
                componentVars[componentName.get()]['TDS_dy'] = BooleanVar()
                componentVars[componentName.get()]['TDS_dz'] = BooleanVar()
                componentVars[componentName.get()]['Pressure_volume'] = BooleanVar()
                componentVars[componentName.get()]['Pressure_dx'] = BooleanVar()
                componentVars[componentName.get()]['Pressure_dy'] = BooleanVar()
                componentVars[componentName.get()]['Pressure_dz'] = BooleanVar()
                componentVars[componentName.get()]['pH_volume'] = BooleanVar()
                componentVars[componentName.get()]['pH_dx'] = BooleanVar()
                componentVars[componentName.get()]['pH_dy'] = BooleanVar()
                componentVars[componentName.get()]['pH_dz'] = BooleanVar()

                componentVars[componentName.get()]['componentName']=StringVar()
                componentVars[componentName.get()]['componentType']=StringVar()
                componentVars[componentName.get()]['componentName']=componentName.get()
                componentVars[componentName.get()]['componentType']=componentType.get()

                componentVars[componentName.get()]['Params']['brine_rate']['value'].set(0.00030)
                componentVars[componentName.get()]['Params']['brine_mass']['value'].set(4.928)
                componentVars[componentName.get()]['Params']['co2_rate']['value'].set(0.045)
                componentVars[componentName.get()]['Params']['co2_mass']['value'].set(7.214)
                componentVars[componentName.get()]['Params']['logK_sand1']['value'].set(-11.91)
                componentVars[componentName.get()]['Params']['logK_sand2']['value'].set(-11.71)
                componentVars[componentName.get()]['Params']['logK_sand3']['value'].set(-11.69)
                componentVars[componentName.get()]['Params']['logK_caprock']['value'].set(-15.70)
                componentVars[componentName.get()]['Params']['correlationLengthX']['value'].set(1098.235)
                componentVars[componentName.get()]['Params']['correlationLengthZ']['value'].set(79.827)
                componentVars[componentName.get()]['Params']['sandFraction']['value'].set(0.800)
                componentVars[componentName.get()]['Params']['groundwater_gradient']['value'].set(0.0013)
                componentVars[componentName.get()]['Params']['leak_depth']['value'].set(883.3)

                componentVars[componentName.get()]['Params']['brine_rate']['min'].set(0)
                componentVars[componentName.get()]['Params']['brine_mass']['min'].set(2.337)
                componentVars[componentName.get()]['Params']['co2_rate']['min'].set(0)
                componentVars[componentName.get()]['Params']['co2_mass']['min'].set(0.001)
                componentVars[componentName.get()]['Params']['logK_sand1']['min'].set(-12.92)
                componentVars[componentName.get()]['Params']['logK_sand2']['min'].set(-12.72)
                componentVars[componentName.get()]['Params']['logK_sand3']['min'].set(-12.70)
                componentVars[componentName.get()]['Params']['logK_caprock']['min'].set(-16.70)
                componentVars[componentName.get()]['Params']['correlationLengthX']['min'].set(200)
                componentVars[componentName.get()]['Params']['correlationLengthZ']['min'].set(10)
                componentVars[componentName.get()]['Params']['sandFraction']['min'].set(0.70)
                componentVars[componentName.get()]['Params']['groundwater_gradient']['min'].set(0.0010)
                componentVars[componentName.get()]['Params']['leak_depth']['min'].set(424.4)

                componentVars[componentName.get()]['Params']['brine_rate']['max'].set(0.017)
                componentVars[componentName.get()]['Params']['brine_mass']['max'].set(6.939)
                componentVars[componentName.get()]['Params']['co2_rate']['max'].set(0.385)
                componentVars[componentName.get()]['Params']['co2_mass']['max'].set(9.210)
                componentVars[componentName.get()]['Params']['logK_sand1']['max'].set(-10.92)
                componentVars[componentName.get()]['Params']['logK_sand2']['max'].set(-10.72)
                componentVars[componentName.get()]['Params']['logK_sand3']['max'].set(-10.70)
                componentVars[componentName.get()]['Params']['logK_caprock']['max'].set(-14.70)
                componentVars[componentName.get()]['Params']['correlationLengthX']['max'].set(2000)
                componentVars[componentName.get()]['Params']['correlationLengthZ']['max'].set(150)
                componentVars[componentName.get()]['Params']['sandFraction']['max'].set(0.90)
                componentVars[componentName.get()]['Params']['groundwater_gradient']['max'].set(0.0017)
                componentVars[componentName.get()]['Params']['leak_depth']['max'].set(1341.5)

                componentVars[componentName.get()]['Params']['brine_rate']['mean'].set(1)
                componentVars[componentName.get()]['Params']['brine_mass']['mean'].set(1)
                componentVars[componentName.get()]['Params']['co2_rate']['mean'].set(1)
                componentVars[componentName.get()]['Params']['co2_mass']['mean'].set(1)
                componentVars[componentName.get()]['Params']['logK_sand1']['mean'].set(1)
                componentVars[componentName.get()]['Params']['logK_sand2']['mean'].set(1)
                componentVars[componentName.get()]['Params']['logK_sand3']['mean'].set(1)
                componentVars[componentName.get()]['Params']['logK_caprock']['mean'].set(1)
                componentVars[componentName.get()]['Params']['correlationLengthX']['mean'].set(1)
                componentVars[componentName.get()]['Params']['correlationLengthZ']['mean'].set(1)
                componentVars[componentName.get()]['Params']['sandFraction']['mean'].set(1)
                componentVars[componentName.get()]['Params']['groundwater_gradient']['mean'].set(1)
                componentVars[componentName.get()]['Params']['leak_depth']['mean'].set(1)

                componentVars[componentName.get()]['Params']['brine_rate']['std'].set(1)
                componentVars[componentName.get()]['Params']['brine_mass']['std'].set(1)
                componentVars[componentName.get()]['Params']['co2_rate']['std'].set(1)
                componentVars[componentName.get()]['Params']['co2_mass']['std'].set(1)
                componentVars[componentName.get()]['Params']['logK_sand1']['std'].set(1)
                componentVars[componentName.get()]['Params']['logK_sand2']['std'].set(1)
                componentVars[componentName.get()]['Params']['logK_sand3']['std'].set(1)
                componentVars[componentName.get()]['Params']['logK_caprock']['std'].set(1)
                componentVars[componentName.get()]['Params']['correlationLengthX']['std'].set(1)
                componentVars[componentName.get()]['Params']['correlationLengthZ']['std'].set(1)
                componentVars[componentName.get()]['Params']['sandFraction']['std'].set(1)
                componentVars[componentName.get()]['Params']['groundwater_gradient']['std'].set(1)
                componentVars[componentName.get()]['Params']['leak_depth']['std'].set(1)

                componentVars[componentName.get()]['TDS_volume'].set(0)
                componentVars[componentName.get()]['TDS_dx'].set(0)
                componentVars[componentName.get()]['TDS_dy'].set(0)
                componentVars[componentName.get()]['TDS_dz'].set(0)
                componentVars[componentName.get()]['Pressure_volume'].set(0)
                componentVars[componentName.get()]['Pressure_dx'].set(0)
                componentVars[componentName.get()]['Pressure_dy'].set(0)
                componentVars[componentName.get()]['Pressure_dz'].set(0)
                componentVars[componentName.get()]['pH_volume'].set(0)
                componentVars[componentName.get()]['pH_dx'].set(0)
                componentVars[componentName.get()]['pH_dy'].set(0)
                componentVars[componentName.get()]['pH_dz'].set(0)

                alluviumAquiferTab_label = ttk.Label(tabType, text="Deep Alluvium Aquifer Component:", font=LABEL_FONT)
                alluviumAquiferTab_label.grid(row=0, column=0, sticky='w')

                brine_rate=tk.Frame(tabType)
                brine_rate.grid(row=1, column=0, sticky='w', padx=15)

                alluviumAquifer_brine_rate_label = ttk.Label(brine_rate, text="Brine Flux: [kg/s]", width=30)
                alluviumAquifer_brine_rate_menu = tk.OptionMenu(brine_rate, componentVars[componentName.get()]['Params']['brine_rate']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(brine_rate))
                alluviumAquifer_brine_rate_value_label=ttk.Label(brine_rate, text='Value:')
                alluviumAquifer_brine_rate_txtField = tk.Entry(brine_rate, textvariable=componentVars[componentName.get()]['Params']['brine_rate']['value'])
                brine_rate.labelText='Brine Flux: [kg/s]'
                brine_rate.component=componentVars[componentName.get()]['componentName']
                brine_rate.distType=componentVars[componentName.get()]['Params']['brine_rate']['distribution']
                brine_rate.toolTipText = 'Brine Flux'
                brine_rate.text = 'brine_rate'

                alluviumAquifer_brine_rate_menu.config(width=15)
                toolTip.bind(alluviumAquifer_brine_rate_menu, 'Select Distriubtion for Brine Flux.')
                alluviumAquifer_brine_rate_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_brine_rate_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_brine_rate_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_brine_rate_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_brine_rate_txtField, 'Set value for Brine Flux.')
                controller.setvar(name='brine_rate', value=brine_rate)

                brine_mass=tk.Frame(tabType)
                brine_mass.grid(row=2, column=0, sticky='w', padx=15)

                alluviumAquifer_brine_mass_label = ttk.Label(brine_mass, text="Cumulative Brine Mass: [log"+str(u'\u2081'u'\u2080')+" (kg)]", width=30)
                alluviumAquifer_brine_mass_menu = tk.OptionMenu(brine_mass, componentVars[componentName.get()]['Params']['brine_mass']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(brine_mass))
                alluviumAquifer_brine_mass_value_label=ttk.Label(brine_mass, text='Value:')
                alluviumAquifer_brine_mass_txtField = tk.Entry(brine_mass, textvariable=componentVars[componentName.get()]['Params']['brine_mass']['value'])
                brine_mass.labelText="Cumulative Brine Mass: [log"+str(u'\u2081'u'\u2080')+" (kg)]"
                brine_mass.component=componentVars[componentName.get()]['componentName']
                brine_mass.distType=componentVars[componentName.get()]['Params']['brine_mass']['distribution']
                brine_mass.toolTipText = 'Cumulative Brine Mass'
                brine_mass.text = 'brine_mass'

                alluviumAquifer_brine_mass_menu.config(width=15)
                toolTip.bind(alluviumAquifer_brine_mass_menu, 'Select Distriubtion for Cumulative Brine Mass.')
                alluviumAquifer_brine_mass_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_brine_mass_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_brine_mass_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_brine_mass_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_brine_mass_txtField, 'Set value for Cumulative Brine Mass.')
                controller.setvar(name='brine_mass', value=brine_mass)

                co2_rate=tk.Frame(tabType)
                co2_rate.grid(row=3, column=0, sticky='w', padx=15)

                alluviumAquifer_co2_rate_label = ttk.Label(co2_rate, text="CO"+str(u'\u2082')+" Flux: [kg/s]", width=30)
                alluviumAquifer_co2_rate_menu = tk.OptionMenu(co2_rate, componentVars[componentName.get()]['Params']['co2_rate']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(co2_rate))
                alluviumAquifer_co2_rate_value_label=ttk.Label(co2_rate, text='Value:')
                alluviumAquifer_co2_rate_txtField = tk.Entry(co2_rate, textvariable=componentVars[componentName.get()]['Params']['co2_rate']['value'])
                co2_rate.labelText="CO"+str(u'\u2082')+" Flux: [kg/s]"
                co2_rate.component=componentVars[componentName.get()]['componentName']
                co2_rate.distType=componentVars[componentName.get()]['Params']['co2_rate']['distribution']
                co2_rate.toolTipText = "CO"+str(u'\u2082')+" Flux"
                co2_rate.text = 'co2_rate'

                alluviumAquifer_co2_rate_menu.config(width=15)
                toolTip.bind(alluviumAquifer_co2_rate_menu, "Select Distriubtion for CO"+str(u'\u2082')+" Flux")
                alluviumAquifer_co2_rate_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_co2_rate_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_co2_rate_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_co2_rate_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_co2_rate_txtField, "Set value for CO"+str(u'\u2082')+" Flux")
                controller.setvar(name='co2_rate', value=co2_rate)

                co2_mass=tk.Frame(tabType)
                co2_mass.grid(row=4, column=0, sticky='w', padx=15)

                alluviumAquifer_co2_mass_label = ttk.Label(co2_mass, text="Cumulative CO"+str(u'\u2082')+" Mass:[log"+str(u'\u2081'u'\u2080')+" (kg)]", width=30)
                alluviumAquifer_co2_mass_menu = tk.OptionMenu(co2_mass, componentVars[componentName.get()]['Params']['co2_mass']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(co2_mass))
                alluviumAquifer_co2_mass_value_label=ttk.Label(co2_mass, text='Value:')
                alluviumAquifer_co2_mass_txtField = tk.Entry(co2_mass, textvariable=componentVars[componentName.get()]['Params']['co2_mass']['value'])
                co2_mass.labelText='Cumulative CO'+str(u'\u2082')+"Mass: [log"+str(u'\u2081'u'\u2080')+" (kg)]"
                co2_mass.component=componentVars[componentName.get()]['componentName']
                co2_mass.distType=componentVars[componentName.get()]['Params']['co2_mass']['distribution']
                co2_mass.toolTipText = 'Cumulative CO'+str(u'\u2082')+"Mass"
                co2_mass.text = 'co2_mass'

                alluviumAquifer_co2_mass_menu.config(width=15)
                toolTip.bind(alluviumAquifer_co2_mass_menu, 'Select Distriubtion for Cumulative CO'+str(u'\u2082')+"Mass.")
                alluviumAquifer_co2_mass_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_co2_mass_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_co2_mass_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_co2_mass_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_co2_mass_txtField, 'Set value for Cumulative CO'+str(u'\u2082')+"Mass.")
                controller.setvar(name='co2_mass', value=co2_mass)

                logK_sand1=tk.Frame(tabType)
                logK_sand1.grid(row=5, column=0, sticky='w', padx=15)

                alluviumAquifer_logK_sand1_label = ttk.Label(logK_sand1, text="Permeabilty in layer 1: [log"+str(u'\u2081'u'\u2080')+" (m"+str(u'\u00B2')+")]", width=30)
                alluviumAquifer_logK_sand1_menu = tk.OptionMenu(logK_sand1, componentVars[componentName.get()]['Params']['logK_sand1']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(logK_sand1))
                alluviumAquifer_logK_sand1_value_label=ttk.Label(logK_sand1, text='Value:')
                alluviumAquifer_logK_sand1_txtField = tk.Entry(logK_sand1, textvariable=componentVars[componentName.get()]['Params']['logK_sand1']['value'])
                logK_sand1.labelText="Permeabilty in layer 1: [log"+str(u'\u2081'u'\u2080')+" (m"+str(u'\u00B2')+")]"
                logK_sand1.component=componentVars[componentName.get()]['componentName']
                logK_sand1.distType=componentVars[componentName.get()]['Params']['logK_sand1']['distribution']
                logK_sand1.toolTipText = 'Permeabilty in layer 1'
                logK_sand1.text = 'logK_sand1'

                alluviumAquifer_logK_sand1_menu.config(width=15)
                toolTip.bind(alluviumAquifer_logK_sand1_menu, 'Select Distriubtion for Permeabilty in layer 1.')
                alluviumAquifer_logK_sand1_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_logK_sand1_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_logK_sand1_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_logK_sand1_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_logK_sand1_txtField, 'Set value for Permeabilty in layer 1.')
                controller.setvar(name='logK_sand1', value=logK_sand1)

                logK_sand2=tk.Frame(tabType)
                logK_sand2.grid(row=6, column=0, sticky='w', padx=15)

                alluviumAquifer_logK_sand2_label = ttk.Label(logK_sand2, text="Permeabilty in layer 2: [log"+str(u'\u2081'u'\u2080')+" (m"+str(u'\u00B2')+")]", width=30)
                alluviumAquifer_logK_sand2_menu = tk.OptionMenu(logK_sand2, componentVars[componentName.get()]['Params']['logK_sand2']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(logK_sand2))
                alluviumAquifer_logK_sand2_value_label=ttk.Label(logK_sand2, text='Value:')
                alluviumAquifer_logK_sand2_txtField = tk.Entry(logK_sand2, textvariable=componentVars[componentName.get()]['Params']['logK_sand2']['value'])
                logK_sand2.labelText="Permeabilty in layer 2: [log"+str(u'\u2081'u'\u2080')+" (m"+str(u'\u00B2')+")]"
                logK_sand2.component=componentVars[componentName.get()]['componentName']
                logK_sand2.distType=componentVars[componentName.get()]['Params']['logK_sand2']['distribution']
                logK_sand2.toolTipText = 'Permeabilty in layer 2'
                logK_sand2.text = 'logK_sand2'

                alluviumAquifer_logK_sand2_menu.config(width=15)
                toolTip.bind(alluviumAquifer_logK_sand2_menu, 'Select Distriubtion for Permeabilty in layer 2.')
                alluviumAquifer_logK_sand2_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_logK_sand2_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_logK_sand2_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_logK_sand2_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_logK_sand2_txtField, 'Set value for Permeabilty in layer 2.')
                controller.setvar(name='logK_sand2', value=logK_sand2)

                logK_sand3=tk.Frame(tabType)
                logK_sand3.grid(row=7, column=0, sticky='w', padx=15)

                alluviumAquifer_logK_sand3_label = ttk.Label(logK_sand3, text="Permeabilty in layer 3: [log"+str(u'\u2081'u'\u2080')+" (m"+str(u'\u00B2')+")]", width=30)
                alluviumAquifer_logK_sand3_menu = tk.OptionMenu(logK_sand3, componentVars[componentName.get()]['Params']['logK_sand3']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(logK_sand3))
                alluviumAquifer_logK_sand3_value_label=ttk.Label(logK_sand3, text='Value:')
                alluviumAquifer_logK_sand3_txtField = tk.Entry(logK_sand3, textvariable=componentVars[componentName.get()]['Params']['logK_sand3']['value'])
                logK_sand3.labelText="Permeabilty in layer 3: [log"+str(u'\u2081'u'\u2080')+" (m"+str(u'\u00B2')+")]"
                logK_sand3.component=componentVars[componentName.get()]['componentName']
                logK_sand3.distType=componentVars[componentName.get()]['Params']['logK_sand3']['distribution']
                logK_sand3.toolTipText = 'Permeabilty in layer 3'
                logK_sand3.text = 'logK_sand3'

                alluviumAquifer_logK_sand3_menu.config(width=15)
                toolTip.bind(alluviumAquifer_logK_sand3_menu, 'Select Distriubtion for Permeabilty in layer 3.')
                alluviumAquifer_logK_sand3_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_logK_sand3_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_logK_sand3_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_logK_sand3_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_logK_sand3_txtField, 'Set value for Permeabilty in layer 3.')
                controller.setvar(name='logK_sand3', value=logK_sand3)

                logK_caprock=tk.Frame(tabType)
                logK_caprock.grid(row=8, column=0, sticky='w', padx=15)

                alluviumAquifer_logK_caprock_label = ttk.Label(logK_caprock, text="Caprock Permeablity: [log"+str(u'\u2081'u'\u2080')+" (m"+str(u'\u00B2')+")]", width=30)
                alluviumAquifer_logK_caprock_menu = tk.OptionMenu(logK_caprock, componentVars[componentName.get()]['Params']['logK_caprock']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(logK_caprock))
                alluviumAquifer_logK_caprock_value_label=ttk.Label(logK_caprock, text='Value:')
                alluviumAquifer_logK_caprock_txtField = tk.Entry(logK_caprock, textvariable=componentVars[componentName.get()]['Params']['logK_caprock']['value'])
                logK_caprock.labelText="Caprock Permeablity: [log"+str(u'\u2081'u'\u2080')+" (m"+str(u'\u00B2')+")]"
                logK_caprock.component=componentVars[componentName.get()]['componentName']
                logK_caprock.distType=componentVars[componentName.get()]['Params']['logK_caprock']['distribution']
                logK_caprock.toolTipText = 'Caprock Permeablity'
                logK_caprock.text = 'logK_caprock'

                alluviumAquifer_logK_caprock_menu.config(width=15)
                toolTip.bind(alluviumAquifer_logK_caprock_menu, 'Select Distriubtion for Caprock Permeabilty.')
                alluviumAquifer_logK_caprock_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_logK_caprock_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_logK_caprock_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_logK_caprock_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_logK_caprock_txtField, 'Set value for Caprock Permeablity.')
                controller.setvar(name='logK_caprock', value=logK_caprock)

                correlationLengthX=tk.Frame(tabType)
                correlationLengthX.grid(row=9, column=0, sticky='w', padx=15)

                alluviumAquifer_correlationLengthX_label = ttk.Label(correlationLengthX, text="Correlation length X: [m]", width=30)
                alluviumAquifer_correlationLengthX_menu = tk.OptionMenu(correlationLengthX, componentVars[componentName.get()]['Params']['correlationLengthX']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(correlationLengthX))
                alluviumAquifer_correlationLengthX_value_label=ttk.Label(correlationLengthX, text='Value:')
                alluviumAquifer_correlationLengthX_txtField = tk.Entry(correlationLengthX, textvariable=componentVars[componentName.get()]['Params']['correlationLengthX']['value'])
                correlationLengthX.labelText='Correlation length in X-direction: [m]'
                correlationLengthX.component=componentVars[componentName.get()]['componentName']
                correlationLengthX.distType=componentVars[componentName.get()]['Params']['correlationLengthX']['distribution']
                correlationLengthX.toolTipText = 'Correlation length in X-direction'
                correlationLengthX.text = 'correlationLengthX'

                alluviumAquifer_correlationLengthX_menu.config(width=15)
                toolTip.bind(alluviumAquifer_correlationLengthX_menu, 'Select Distriubtion for Correlation length in X-direction.')
                alluviumAquifer_correlationLengthX_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_correlationLengthX_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_correlationLengthX_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_correlationLengthX_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_correlationLengthX_txtField, 'Set value for Correlation length in X-direction.')
                controller.setvar(name='correlationLengthX', value=correlationLengthX)

                correlationLengthZ=tk.Frame(tabType)
                correlationLengthZ.grid(row=10, column=0, sticky='w', padx=15)

                alluviumAquifer_correlationLengthZ_label = ttk.Label(correlationLengthZ, text="Correlation length Z: [m]", width=30)
                alluviumAquifer_correlationLengthZ_menu = tk.OptionMenu(correlationLengthZ, componentVars[componentName.get()]['Params']['correlationLengthZ']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(correlationLengthZ))
                alluviumAquifer_correlationLengthZ_value_label=ttk.Label(correlationLengthZ, text='Value:')
                alluviumAquifer_correlationLengthZ_txtField = tk.Entry(correlationLengthZ, textvariable=componentVars[componentName.get()]['Params']['correlationLengthZ']['value'])
                correlationLengthZ.labelText='Correlation length in Z-direction: [m]'
                correlationLengthZ.component=componentVars[componentName.get()]['componentName']
                correlationLengthZ.distType=componentVars[componentName.get()]['Params']['correlationLengthZ']['distribution']
                correlationLengthZ.toolTipText = 'Correlation length in Z-direction'
                correlationLengthZ.text = 'correlationLengthZ'

                alluviumAquifer_correlationLengthZ_menu.config(width=15)
                toolTip.bind(alluviumAquifer_correlationLengthZ_menu, 'Select Distriubtion for Correlation length in Z-direction.')
                alluviumAquifer_correlationLengthZ_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_correlationLengthZ_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_correlationLengthZ_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_correlationLengthZ_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_correlationLengthZ_txtField, 'Set value for Correlation length in Z-direction.')
                controller.setvar(name='correlationLengthZ', value=correlationLengthZ)

                sandFraction=tk.Frame(tabType)
                sandFraction.grid(row=11, column=0, sticky='w', padx=15)

                alluviumAquifer_sandFraction_label = ttk.Label(sandFraction, text="Sand Volume Fraction: [-]", width=30)
                alluviumAquifer_sandFraction_menu = tk.OptionMenu(sandFraction, componentVars[componentName.get()]['Params']['sandFraction']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(sandFraction))
                alluviumAquifer_sandFraction_value_label=ttk.Label(sandFraction, text='Value:')
                alluviumAquifer_sandFraction_txtField = tk.Entry(sandFraction, textvariable=componentVars[componentName.get()]['Params']['sandFraction']['value'])
                sandFraction.labelText='Sand Volume Fraction: [-]'
                sandFraction.component=componentVars[componentName.get()]['componentName']
                sandFraction.distType=componentVars[componentName.get()]['Params']['sandFraction']['distribution']
                sandFraction.toolTipText = 'Sand Volume Fraction'
                sandFraction.text = 'sandFraction'

                alluviumAquifer_sandFraction_menu.config(width=15)
                toolTip.bind(alluviumAquifer_sandFraction_menu, 'Select Distriubtion for Sand Volume Fraction.')
                alluviumAquifer_sandFraction_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_sandFraction_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_sandFraction_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_sandFraction_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_sandFraction_txtField, 'Set value for Sand Volume Fraction.')
                controller.setvar(name='sandFraction', value=sandFraction)

                groundwater_gradient=tk.Frame(tabType)
                groundwater_gradient.grid(row=12, column=0, sticky='w', padx=15)

                alluviumAquifer_groundwater_gradient_label = ttk.Label(groundwater_gradient, text="GroundWater Gradient: [-]", width=30)
                alluviumAquifer_groundwater_gradient_menu = tk.OptionMenu(groundwater_gradient, componentVars[componentName.get()]['Params']['groundwater_gradient']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(groundwater_gradient))
                alluviumAquifer_groundwater_gradient_value_label=ttk.Label(groundwater_gradient, text='Value:')
                alluviumAquifer_groundwater_gradient_txtField = tk.Entry(groundwater_gradient, textvariable=componentVars[componentName.get()]['Params']['groundwater_gradient']['value'])
                groundwater_gradient.labelText='GroundWater Gradient: [-]'
                groundwater_gradient.component=componentVars[componentName.get()]['componentName']
                groundwater_gradient.distType=componentVars[componentName.get()]['Params']['groundwater_gradient']['distribution']
                groundwater_gradient.toolTipText = 'GroundWater Gradient'
                groundwater_gradient.text = 'groundwater_gradient'

                alluviumAquifer_groundwater_gradient_menu.config(width=15)
                toolTip.bind(alluviumAquifer_groundwater_gradient_menu, 'Select Distriubtion for GroundWater Gradient.')
                alluviumAquifer_groundwater_gradient_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_groundwater_gradient_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_groundwater_gradient_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_groundwater_gradient_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_groundwater_gradient_txtField, 'Set value for GroundWater Gradient.')
                controller.setvar(name='groundwater_gradient', value=groundwater_gradient)

                leak_depth=tk.Frame(tabType)
                leak_depth.grid(row=13, column=0, sticky='w', padx=15)

                alluviumAquifer_leak_depth_label = ttk.Label(leak_depth, text="Depth of leakage: [-]", width=30)
                alluviumAquifer_leak_depth_menu = tk.OptionMenu(leak_depth, componentVars[componentName.get()]['Params']['leak_depth']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(leak_depth))
                alluviumAquifer_leak_depth_value_label = ttk.Label(leak_depth, text='Value:')
                alluviumAquifer_leak_depth_txtField = tk.Entry(leak_depth, textvariable=componentVars[componentName.get()]['Params']['leak_depth']['value'])
                leak_depth.labelText='Depth of leakage: [-]'
                leak_depth.component=componentVars[componentName.get()]['componentName']
                leak_depth.distType=componentVars[componentName.get()]['Params']['leak_depth']['distribution']
                leak_depth.toolTipText = 'Depth of leakage'
                leak_depth.text = 'leak_depth'

                alluviumAquifer_leak_depth_menu.config(width=15)
                toolTip.bind(alluviumAquifer_leak_depth_menu, 'Select Distriubtion for Depth of leakage.')
                alluviumAquifer_leak_depth_label.grid(row=0, column=0, sticky='e', padx=5)
                alluviumAquifer_leak_depth_menu.grid(row=0, column=1, padx=5)
                alluviumAquifer_leak_depth_value_label.grid(row=0, column=2, padx=5)
                alluviumAquifer_leak_depth_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(alluviumAquifer_leak_depth_txtField, 'Set value for Depth of leakage.')
                controller.setvar(name='leak_depth', value=leak_depth)

                alluviumAquifer_outputs_label = ttk.Label(tabType, text="Outputs:", font=LABEL_FONT)
                alluviumAquifer_outputs_label.grid(row=18, column=0, sticky='w')

                outputFrame = ttk.Frame(tabType, padding=10)
                outputFrame.grid(row=19, column=0, sticky='w', padx=15, columnspan=20)

                alluviumAquifer_tds_volume_label = ttk.Label(outputFrame, text="Volume Baseline TDS: [m"+str(u'\u00B3')+"]")
                alluviumAquifer_tds_volume_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['TDS_volume'])
                alluviumAquifer_tds_volume_label.grid(row=1, column=0, pady=5, sticky='e')
                alluviumAquifer_tds_volume_checkbox.grid(row=1, column=1, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_tds_volume_checkbox, 'Enable Volume Baseline TDS output')

                alluviumAquifer_tds_dx_label = ttk.Label(outputFrame, text="Length X Baseline TDS: [m]")
                alluviumAquifer_tds_dx_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['TDS_dx'])
                alluviumAquifer_tds_dx_label.grid(row=1, column=2, pady=5, sticky='e')
                alluviumAquifer_tds_dx_checkbox.grid(row=1, column=3, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_tds_dx_checkbox, 'Enable Length X Baseline TDS')

                alluviumAquifer_tds_dy_label = ttk.Label(outputFrame, text="Length Y Baseline TDS: [m]")
                alluviumAquifer_tds_dy_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['TDS_dy'])
                alluviumAquifer_tds_dy_label.grid(row=1, column=4, pady=5, sticky='e')
                alluviumAquifer_tds_dy_checkbox.grid(row=1, column=5, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_tds_dy_checkbox, 'Enable Length Y Baseline TDS output')

                alluviumAquifer_tds_dz_label = ttk.Label(outputFrame, text="Length Z Baseline TDS: [m]")
                alluviumAquifer_tds_dz_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['TDS_dz'])
                alluviumAquifer_tds_dz_label.grid(row=1, column=6, pady=5, sticky='e')
                alluviumAquifer_tds_dz_checkbox.grid(row=1, column=7, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_tds_dz_checkbox, 'Enable Length Z Baseline TDS output.')

                alluviumAquifer_pressure_volume_label = ttk.Label(outputFrame, text="Pressure Change Volume: [m"+str(u'\u00B3')+"]")
                alluviumAquifer_pressure_volume_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Pressure_volume'])
                alluviumAquifer_pressure_volume_label.grid(row=2, column=0, pady=5, sticky='e')
                alluviumAquifer_pressure_volume_checkbox.grid(row=2, column=1, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_pressure_volume_checkbox, 'Enable Pressure Change Volume output')

                alluviumAquifer_pressure_dx_label = ttk.Label(outputFrame, text="Pressure Change X Length: [m]")
                alluviumAquifer_pressure_dx_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Pressure_dx'])
                alluviumAquifer_pressure_dx_label.grid(row=2, column=2, pady=5, sticky='e')
                alluviumAquifer_pressure_dx_checkbox.grid(row=2, column=3, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_pressure_dx_checkbox, 'Enable Pressure Change X Length output')

                alluviumAquifer_pressure_dy_label = ttk.Label(outputFrame, text="Pressure Change Y Length: [m]")
                alluviumAquifer_pressure_dy_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Pressure_dy'])
                alluviumAquifer_pressure_dy_label.grid(row=2, column=4, pady=5, sticky='e')
                alluviumAquifer_pressure_dy_checkbox.grid(row=2, column=5, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_pressure_dy_checkbox, 'Enable Pressure Change Y Length output')

                alluviumAquifer_pressure_dz_label = ttk.Label(outputFrame, text="Pressure Change Z Length: [m]")
                alluviumAquifer_pressure_dz_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['Pressure_dz'])
                alluviumAquifer_pressure_dz_label.grid(row=2, column=6, pady=5, sticky='e')
                alluviumAquifer_pressure_dz_checkbox.grid(row=2, column=7, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_pressure_dz_checkbox, 'Enable Pressure Change Z Length output')

                alluviumAquifer_ph_volume_label = ttk.Label(outputFrame, text="pH Volume Threshold: [m"+str(u'\u00B3')+"]")
                alluviumAquifer_ph_volume_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['pH_volume'])
                alluviumAquifer_ph_volume_label.grid(row=3, column=0, pady=5, sticky='e')
                alluviumAquifer_ph_volume_checkbox.grid(row=3, column=1, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_ph_volume_checkbox, 'Enable pH Volume Threshold output')

                alluviumAquifer_ph_dx_label = ttk.Label(outputFrame, text="pH Length X Threshold: [m]")
                alluviumAquifer_ph_dx_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['pH_dx'])
                alluviumAquifer_ph_dx_label.grid(row=3, column=2, pady=5, sticky='e')
                alluviumAquifer_ph_dx_checkbox.grid(row=3, column=3, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_ph_dx_checkbox, 'Enable pH Length X Threshold output')

                alluviumAquifer_ph_dy_label = ttk.Label(outputFrame, text="pH Length Y Threshold: [m]")
                alluviumAquifer_ph_dy_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['pH_dy'])
                alluviumAquifer_ph_dy_label.grid(row=3, column=4, pady=5, sticky='e')
                alluviumAquifer_ph_dy_checkbox.grid(row=3, column=5, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_ph_dy_checkbox, 'Enable pH Length Y Threshold output')

                alluviumAquifer_ph_dz_label = ttk.Label(outputFrame, text="pH Length Z Threshold: [m]")
                alluviumAquifer_ph_dz_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['pH_dz'])
                alluviumAquifer_ph_dz_label.grid(row=3, column=6, pady=5, sticky='e')
                alluviumAquifer_ph_dz_checkbox.grid(row=3, column=7, pady=5, sticky='w')
                toolTip.bind(alluviumAquifer_ph_dz_checkbox, 'Enable pH Length Z Threshold output')

            if(componentType.get()=='Atmospheric' or componentType.get()=='AtmosphericROM'):
                componentChoices.append(componentName.get())
                componentTypeDictionary.append('atmospheric')
                connectionsDictionary.append(connection.get())

                componentVars[componentName.get()]={}
                componentVars[componentName.get()]['Params']={}
                componentVars[componentName.get()]['Params']['T_amb'] = {}
                componentVars[componentName.get()]['Params']['P_amb'] = {}
                componentVars[componentName.get()]['Params']['V_wind'] = {}
                componentVars[componentName.get()]['Params']['C0_critical'] = {}
                componentVars[componentName.get()]['Params']['T_source'] = {}
                componentVars[componentName.get()]['Params']['x_receptor'] = {}
                componentVars[componentName.get()]['Params']['y_receptor'] = {}

                if(connection.get().find('Param')):
                    componentVars[componentName.get()]['pressure']=pressure.get()
                    componentVars[componentName.get()]['saturation']=saturation.get()

                componentVars[componentName.get()]['Params']['T_amb']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['P_amb']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['V_wind']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['C0_critical']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['T_source']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['x_receptor']['distribution'] = StringVar()
                componentVars[componentName.get()]['Params']['y_receptor']['distribution'] = StringVar()

                componentVars[componentName.get()]['Params']['T_amb']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['P_amb']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['V_wind']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['C0_critical']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['T_source']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['x_receptor']['distribution'].set(distributionOptions[0])
                componentVars[componentName.get()]['Params']['y_receptor']['distribution'].set(distributionOptions[0])

                componentVars[componentName.get()]['Params']['T_amb']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['P_amb']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['V_wind']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['C0_critical']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['T_source']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['x_receptor']['values'] = StringVar()
                componentVars[componentName.get()]['Params']['y_receptor']['values'] = StringVar()

                componentVars[componentName.get()]['Params']['T_amb']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['P_amb']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['V_wind']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['C0_critical']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['T_source']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['x_receptor']['weights'] = StringVar()
                componentVars[componentName.get()]['Params']['y_receptor']['weights'] = StringVar()

                componentVars[componentName.get()]['Params']['T_amb']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['P_amb']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['V_wind']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['C0_critical']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['T_source']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['x_receptor']['value'] = DoubleVar()
                componentVars[componentName.get()]['Params']['y_receptor']['value'] = DoubleVar()

                componentVars[componentName.get()]['Params']['T_amb']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['P_amb']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['V_wind']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['C0_critical']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['T_source']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['x_receptor']['min'] = DoubleVar()
                componentVars[componentName.get()]['Params']['y_receptor']['min'] = DoubleVar()

                componentVars[componentName.get()]['Params']['T_amb']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['P_amb']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['V_wind']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['C0_critical']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['T_source']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['x_receptor']['max'] = DoubleVar()
                componentVars[componentName.get()]['Params']['y_receptor']['max'] = DoubleVar()

                componentVars[componentName.get()]['Params']['T_amb']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['P_amb']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['V_wind']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['C0_critical']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['T_source']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['x_receptor']['mean'] = DoubleVar()
                componentVars[componentName.get()]['Params']['y_receptor']['mean'] = DoubleVar()

                componentVars[componentName.get()]['Params']['T_amb']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['P_amb']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['V_wind']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['C0_critical']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['T_source']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['x_receptor']['std'] = DoubleVar()
                componentVars[componentName.get()]['Params']['y_receptor']['std'] = DoubleVar()

                componentVars[componentName.get()]['componentName']=StringVar()
                componentVars[componentName.get()]['componentType']=StringVar()
                componentVars[componentName.get()]['componentName']=componentName.get()
                componentVars[componentName.get()]['componentType']=componentType.get()

                componentVars[componentName.get()]['Params']['T_amb']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['P_amb']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['V_wind']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['C0_critical']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['T_source']['values'] .set('15, 21')
                componentVars[componentName.get()]['Params']['x_receptor']['values'].set('15, 21')
                componentVars[componentName.get()]['Params']['y_receptor']['values'].set('15, 21')

                componentVars[componentName.get()]['Params']['T_amb']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['P_amb']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['V_wind']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['C0_critical']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['T_source']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['x_receptor']['weights'].set('15, 21')
                componentVars[componentName.get()]['Params']['y_receptor']['weights'].set('15, 21')

                componentVars[componentName.get()]['Params']['T_amb']['value'].set(15)
                componentVars[componentName.get()]['Params']['P_amb']['value'].set(1.0)
                componentVars[componentName.get()]['Params']['V_wind']['value'].set(5)
                componentVars[componentName.get()]['Params']['C0_critical']['value'].set(0.01)
                componentVars[componentName.get()]['Params']['T_source']['value'].set(15)
                componentVars[componentName.get()]['Params']['x_receptor']['value'].set(5)
                componentVars[componentName.get()]['Params']['y_receptor']['value'].set(5)

                componentVars[componentName.get()]['Params']['T_amb']['min'].set(5)
                componentVars[componentName.get()]['Params']['P_amb']['min'].set(0.7)
                componentVars[componentName.get()]['Params']['V_wind']['min'].set(1e-10)
                componentVars[componentName.get()]['Params']['C0_critical']['min'].set(0.002)
                componentVars[componentName.get()]['Params']['T_source']['min'].set(5)
                componentVars[componentName.get()]['Params']['x_receptor']['min'].set(5)
                componentVars[componentName.get()]['Params']['y_receptor']['min'].set(5)

                componentVars[componentName.get()]['Params']['T_amb']['max'].set(40)
                componentVars[componentName.get()]['Params']['P_amb']['max'].set(1.08)
                componentVars[componentName.get()]['Params']['V_wind']['max'].set(20)
                componentVars[componentName.get()]['Params']['C0_critical']['max'].set(0.1)
                componentVars[componentName.get()]['Params']['T_source']['max'].set(50)
                componentVars[componentName.get()]['Params']['x_receptor']['max'].set(5)
                componentVars[componentName.get()]['Params']['y_receptor']['max'].set(5)

                componentVars[componentName.get()]['Params']['T_amb']['mean'].set(1)
                componentVars[componentName.get()]['Params']['P_amb']['mean'].set(1)
                componentVars[componentName.get()]['Params']['V_wind']['mean'].set(1)
                componentVars[componentName.get()]['Params']['C0_critical']['mean'].set(1)
                componentVars[componentName.get()]['Params']['T_source']['mean'].set(1)
                componentVars[componentName.get()]['Params']['x_receptor']['mean'].set(1)
                componentVars[componentName.get()]['Params']['y_receptor']['mean'].set(1)

                componentVars[componentName.get()]['Params']['T_amb']['std'].set(1)
                componentVars[componentName.get()]['Params']['P_amb']['std'].set(1)
                componentVars[componentName.get()]['Params']['V_wind']['std'].set(1)
                componentVars[componentName.get()]['Params']['C0_critical']['std'].set(1)
                componentVars[componentName.get()]['Params']['T_source']['std'].set(1)
                componentVars[componentName.get()]['Params']['x_receptor']['std'].set(1)
                componentVars[componentName.get()]['Params']['y_receptor']['std'].set(1)

                componentVars[componentName.get()]['out_flag'] = BooleanVar()
                componentVars[componentName.get()]['num_sources'] = BooleanVar()
                componentVars[componentName.get()]['x_new'] = BooleanVar()
                componentVars[componentName.get()]['y_new'] = BooleanVar()
                componentVars[componentName.get()]['critical_distance'] = BooleanVar()

                componentVars[componentName.get()]['out_flag'].set(0)
                componentVars[componentName.get()]['num_sources'].set(0)
                componentVars[componentName.get()]['x_new'].set(0)
                componentVars[componentName.get()]['y_new'].set(0)
                componentVars[componentName.get()]['critical_distance'].set(0)

                atmosphericTab_label = ttk.Label(tabType, text="Atmospheric ROM Component:", font=LABEL_FONT)
                atmosphericTab_label.grid(row=0, column=0, sticky='w')

                T_amb=tk.Frame(tabType)
                T_amb.grid(row=1, column=0, sticky='w', padx=15)

                atmospheric_T_amb_label = ttk.Label(T_amb, text="Ambient Temperature: [C]", width=30)
                atmospheric_T_amb_menu = tk.OptionMenu(T_amb, componentVars[componentName.get()]['Params']['T_amb']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(T_amb))
                atmospheric_T_amb_value_label=ttk.Label(T_amb, text='Value:')
                atmospheric_T_amb_txtField = tk.Entry(T_amb, textvariable=componentVars[componentName.get()]['Params']['T_amb']['value'])
                T_amb.labelText='Ambient Temperature: [C]'
                T_amb.component=componentVars[componentName.get()]['componentName']
                T_amb.distType=componentVars[componentName.get()]['Params']['T_amb']['distribution']
                T_amb.toolTipText = 'Ambient Temperature'
                T_amb.text = 'T_amb'

                atmospheric_T_amb_menu.config(width=15)
                toolTip.bind(atmospheric_T_amb_menu, 'Select Distriubtion for Ambient Temperature.')
                atmospheric_T_amb_label.grid(row=0, column=0, sticky='e', padx=5)
                atmospheric_T_amb_menu.grid(row=0, column=1, padx=5)
                atmospheric_T_amb_value_label.grid(row=0, column=2, padx=5)
                atmospheric_T_amb_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(atmospheric_T_amb_txtField, 'Set value for Ambient Temperature.')
                controller.setvar(name = 'T_amb', value = T_amb)

                P_amb=tk.Frame(tabType)
                P_amb.grid(row=2, column=0, sticky='w', padx=15)

                atmospheric_P_amb_label = ttk.Label(P_amb, text="Ambient Presure: [atmosphere]", width=30)
                atmospheric_P_amb_menu = tk.OptionMenu(P_amb, componentVars[componentName.get()]['Params']['P_amb']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(P_amb))
                atmospheric_P_amb_value_label=ttk.Label(P_amb, text='Value:')
                atmospheric_P_amb_txtField = tk.Entry(P_amb, textvariable=componentVars[componentName.get()]['Params']['P_amb']['value'])
                P_amb.labelText='Ambient Presure: [atmosphere]'
                P_amb.component=componentVars[componentName.get()]['componentName']
                P_amb.distType=componentVars[componentName.get()]['Params']['P_amb']['distribution']
                P_amb.toolTipText = 'Ambient Presure'
                P_amb.text = 'P_amb'

                atmospheric_P_amb_menu.config(width=15)
                toolTip.bind(atmospheric_P_amb_menu, 'Select Distriubtion for Ambient Presure.')
                atmospheric_P_amb_label.grid(row=0, column=0, sticky='e', padx=5)
                atmospheric_P_amb_menu.grid(row=0, column=1, padx=5)
                atmospheric_P_amb_value_label.grid(row=0, column=2, padx=5)
                atmospheric_P_amb_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(atmospheric_P_amb_txtField, 'Set value for Ambient Presure.')
                controller.setvar(name = 'P_amb', value = P_amb)

                V_wind=tk.Frame(tabType)
                V_wind.grid(row=3, column=0, sticky='w', padx=15)

                atmospheric_V_wind_label = ttk.Label(V_wind, text="Wind Velocity: [m/s]", width=30)
                atmospheric_V_wind_menu = tk.OptionMenu(V_wind, componentVars[componentName.get()]['Params']['V_wind']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(V_wind))
                atmospheric_V_wind_value_label=ttk.Label(V_wind, text='Value:')
                atmospheric_V_wind_txtField = tk.Entry(V_wind, textvariable=componentVars[componentName.get()]['Params']['V_wind']['value'])
                V_wind.labelText='Wind Velocity: [m/s]'
                V_wind.component=componentVars[componentName.get()]['componentName']
                V_wind.distType=componentVars[componentName.get()]['Params']['V_wind']['distribution']
                V_wind.toolTipText = 'Wind Velocity'
                V_wind.text = 'V_wind'

                atmospheric_V_wind_menu.config(width=15)
                toolTip.bind(atmospheric_V_wind_menu, 'Select Distriubtion for Wind Velocity.')
                atmospheric_V_wind_label.grid(row=0, column=0, sticky='e', padx=5)
                atmospheric_V_wind_menu.grid(row=0, column=1, padx=5)
                atmospheric_V_wind_value_label.grid(row=0, column=2, padx=5)
                atmospheric_V_wind_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(atmospheric_V_wind_txtField, 'Set value for Wind Velocity.')
                controller.setvar(name = 'V_wind', value = V_wind)

                C0_critical=tk.Frame(tabType)
                C0_critical.grid(row=4, column=0, sticky='w', padx=15)

                atmospheric_C0_critical_label = ttk.Label(C0_critical, text="Critical Concentration: [-]", width=30)
                atmospheric_C0_critical_menu = tk.OptionMenu(C0_critical, componentVars[componentName.get()]['Params']['C0_critical']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(C0_critical))
                atmospheric_C0_critical_value_label=ttk.Label(C0_critical, text='Value:')
                atmospheric_C0_critical_txtField = tk.Entry(C0_critical, textvariable=componentVars[componentName.get()]['Params']['C0_critical']['value'])
                C0_critical.labelText='Critical Concentration: [-]'
                C0_critical.component=componentVars[componentName.get()]['componentName']
                C0_critical.distType=componentVars[componentName.get()]['Params']['C0_critical']['distribution']
                C0_critical.toolTipText = 'Critical Concentration'
                C0_critical.text = 'C0_critical'

                atmospheric_C0_critical_menu.config(width=15)
                toolTip.bind(atmospheric_C0_critical_menu, 'Select Distriubtion for Critical Concentration.')
                atmospheric_C0_critical_label.grid(row=0, column=0, sticky='e', padx=5)
                atmospheric_C0_critical_menu.grid(row=0, column=1, padx=5)
                atmospheric_C0_critical_value_label.grid(row=0, column=2, padx=5)
                atmospheric_C0_critical_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(atmospheric_C0_critical_txtField, 'Set value for Critical Concentration.')
                controller.setvar(name = 'C0_critical', value = C0_critical)

                T_source=tk.Frame(tabType)
                T_source.grid(row=5, column=0, sticky='w', padx=15)

                atmospheric_T_source_label = ttk.Label(T_source, text="Released CO"+str(u'\u2082')+" Temp: [C]", width=30)
                atmospheric_T_source_menu = tk.OptionMenu(T_source, componentVars[componentName.get()]['Params']['T_source']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(T_source))
                atmospheric_T_source_value_label=ttk.Label(T_source, text='Value:')
                atmospheric_T_source_txtField = tk.Entry(T_source, textvariable=componentVars[componentName.get()]['Params']['T_source']['value'])
                T_source.labelText='Released CO'+str(u'\u2082')+' Temp: [C]'
                T_source.component=componentVars[componentName.get()]['componentName']
                T_source.distType=componentVars[componentName.get()]['Params']['T_source']['distribution']
                T_source.toolTipText = 'Released CO'+str(u'\u2082')+' Temp'
                T_source.text = 'T_source'

                atmospheric_T_source_menu.config(width=15)
                toolTip.bind(atmospheric_T_source_menu, 'Select Distriubtion for Released CO'+str(u'\u2082')+' Temp.')
                atmospheric_T_source_label.grid(row=0, column=0, sticky='e', padx=5)
                atmospheric_T_source_menu.grid(row=0, column=1, padx=5)
                atmospheric_T_source_value_label.grid(row=0, column=2, padx=5)
                atmospheric_T_source_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(atmospheric_T_source_txtField, 'Set value for Released CO'+str(u'\u2082')+' Temp.')
                controller.setvar(name = 'T_source', value = T_source)

                x_receptor=tk.Frame(tabType)
                x_receptor.grid(row=6, column=0, sticky='w', padx=15)

                atmospheric_x_receptor_label = ttk.Label(x_receptor, text="X-Coordinate Receptor: [m]", width=30)
                atmospheric_x_receptor_menu = tk.OptionMenu(x_receptor, componentVars[componentName.get()]['Params']['x_receptor']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(x_receptor))
                atmospheric_x_receptor_value_label=ttk.Label(x_receptor, text='Value:')
                atmospheric_x_receptor_txtField = tk.Entry(x_receptor, textvariable=componentVars[componentName.get()]['Params']['x_receptor']['value'])
                x_receptor.labelText='X-Coordinate Receptor: [m]'
                x_receptor.component=componentVars[componentName.get()]['componentName']
                x_receptor.distType=componentVars[componentName.get()]['Params']['x_receptor']['distribution']
                x_receptor.toolTipText = 'X-Coordinate Receptor'
                x_receptor.text = 'x_receptor'

                atmospheric_x_receptor_menu.config(width=15)
                toolTip.bind(atmospheric_x_receptor_menu, 'Select Distriubtion for X-Coordinate Receptor.')
                atmospheric_x_receptor_label.grid(row=0, column=0, sticky='e', padx=5)
                atmospheric_x_receptor_menu.grid(row=0, column=1, padx=5)
                atmospheric_x_receptor_value_label.grid(row=0, column=2, padx=5)
                atmospheric_x_receptor_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(atmospheric_x_receptor_txtField, 'Set value for X-Coordinate Receptor.')
                controller.setvar(name = 'x_receptor', value = x_receptor)

                y_receptor=tk.Frame(tabType)
                y_receptor.grid(row=7, column=0, sticky='w', padx=15)

                atmospheric_y_receptor_label = ttk.Label(y_receptor, text="Y-Coordinate Receptor: [m]", width=30)
                atmospheric_y_receptor_menu = tk.OptionMenu(y_receptor, componentVars[componentName.get()]['Params']['y_receptor']['distribution'], *distributionOptions, command=lambda _:controller.DistributionChange(y_receptor))
                atmospheric_y_receptor_value_label=ttk.Label(y_receptor, text='Value:')
                atmospheric_y_receptor_txtField = tk.Entry(y_receptor, textvariable=componentVars[componentName.get()]['Params']['y_receptor']['value'])
                y_receptor.labelText='Y-Coordinate Receptor: [m]'
                y_receptor.component=componentVars[componentName.get()]['componentName']
                y_receptor.distType=componentVars[componentName.get()]['Params']['y_receptor']['distribution']
                y_receptor.toolTipText = 'Y-Coordinate Receptor'
                y_receptor.text = 'y_receptor'

                atmospheric_y_receptor_menu.config(width=15)
                toolTip.bind(atmospheric_y_receptor_menu, 'Select Distriubtion for Y-Coordinate Receptor.')
                atmospheric_y_receptor_label.grid(row=0, column=0, sticky='e', padx=5)
                atmospheric_y_receptor_menu.grid(row=0, column=1, padx=5)
                atmospheric_y_receptor_value_label.grid(row=0, column=2, padx=5)
                atmospheric_y_receptor_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(atmospheric_y_receptor_txtField, 'Set value for Y-Coordinate Receptor.')
                controller.setvar(name = 'y_receptor', value = y_receptor)

                atomospheric_outputs_label = ttk.Label(tabType, text="Outputs:", font=LABEL_FONT)
                atomospheric_outputs_label.grid(row=8, column=0, sticky='w')

                outputFrame = ttk.Frame(tabType, padding=10)
                outputFrame.grid(row=9, column=0, sticky='w', padx=15)

                atmospheric_out_flag_label = ttk.Label(outputFrame, text="Out Flag: [-]")
                atmospheric_out_flag_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['out_flag'])
                atmospheric_out_flag_label.grid(row=0, column=0, pady=5, sticky='e')
                atmospheric_out_flag_checkbox.grid(row=0, column=1, pady=5, sticky='w')
                toolTip.bind(atmospheric_out_flag_checkbox, 'Enable Out Flag output')

                atmospheric_num_sources_label = ttk.Label(outputFrame, text="Num Sources: [-]")
                atmospheric_num_sources_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['num_sources'])
                atmospheric_num_sources_label.grid(row=0, column=2, pady=5, sticky='e')
                atmospheric_num_sources_checkbox.grid(row=0, column=3, pady=5, sticky='w')
                toolTip.bind(atmospheric_num_sources_checkbox, 'Enalbe Num Sources output')

                atmospheric_x_new_label = ttk.Label(outputFrame, text="X New: [m]")
                atmospheric_x_new_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['x_new'])
                atmospheric_x_new_label.grid(row=0, column=4, pady=5, sticky='e')
                atmospheric_x_new_checkbox.grid(row=0, column=5, pady=5, sticky='w')
                toolTip.bind(atmospheric_x_new_checkbox, 'Enable X New output')

                atmospheric_y_new_label = ttk.Label(outputFrame, text="Y New: [m]")
                atmospheric_y_new_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['y_new'])
                atmospheric_y_new_label.grid(row=0, column=6, pady=5, sticky='e')
                atmospheric_y_new_checkbox.grid(row=0, column=7, pady=5, sticky='w')
                toolTip.bind(atmospheric_y_new_checkbox, 'Enable Y New output')

                atmospheric_critical_distance_label = ttk.Label(outputFrame, text="Critical Distance: [m]")
                atmospheric_critical_distance_checkbox = tk.Checkbutton(outputFrame, variable=componentVars[componentName.get()]['critical_distance'])
                atmospheric_critical_distance_label.grid(row=0, column=8, pady=5, sticky='e')
                atmospheric_critical_distance_checkbox.grid(row=0, column=9, pady=5, sticky='w')
                toolTip.bind(atmospheric_critical_distance_checkbox, 'Enable Critical Distance output')

            connection_menu.children['menu'].delete(0, 'end')
            connections.append(componentName.get())

            for c in connections:
                connectionTypes.append(c)
                connection_menu.children['menu'].add_command(label=c, command=lambda con=c: connection.set(con))

            if(tabControl.index(tk.END)==11):
                tabControl.hide('.!frame.!openiam_page.!notebook.!frame3')

            for widget in componentsSetupFrame.winfo_children():
                widget.destroy()

            self.connection.set(connections[0])
            self.componentType.set(componentTypes[0])
            self.componentName_textField.delete(0, tk.END)
            tabControl.select(newTab)
        else:
            tabControl.forget(newTab)
            componentName.set('')
            messagebox.showerror("Error", "You must enter a Unique name for each Component Model.")
            return

    # This method saves and runs the simulation
    def run_simulation(self):
        self.populate_dictionary(shaleThickness, aquiferThickness, lookupTableParams, LUTWeights);

        filename = d['simName']+'.OpenIAM'
        source_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        run_file = os.path.join(source_dir, 'openiam', 'openiam_cf.py')
        run_command = 'python {0} --file {1} --binary True'.format(run_file, filename)
        os.system(run_command)

if __name__ == "__main__":
    app = NRAPOpenIAM()
    app.wm_title('NRAP-IAM-CS v2')
    app.mainloop()
