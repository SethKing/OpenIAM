# This file consists of all the variables that are globally accessible
d = {}
componentVars = {}
componentChoices = []
componentTypeDictionary = []
connectionsDictionary = []

shaleThickness = {}
aquiferThickness = {}
lookupTableParams = {}
LUTWeights = {}
savedDictionary = {}

aquifers = ['aquifer1', 'aquifer2']
connectionTypes = []
connections = ['Dynamic Parameters']
analysisTypes = ["Forward","LHS","Parstudy"]
loggingtypes = ["Debug","Info","Warn","Error"]
distributionOptions = ['Fixed Value','Uniform','Normal','Lognormal','Truncated','Triangular','Discrete']
componentTypes = ['Simple Reservoir','Lookup Table Reservoir','Multisegemented Wellbore','Cemented Wellbore','Open Wellbore','Carbonate Aquifer','Deep Alluvium Aquifer','Atmospheric']

processingTypes = ['Plotting']
plotTypes = ['Time Series']

LABEL_FONT = ('Helvetica', 14, 'bold')
DASHBOARD_FONT = ('Helvetica', 12)
