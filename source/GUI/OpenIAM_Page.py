# random is imported to generate a random seed value for LHS analysis type
import random, os, Pmw

from dictionarydata import d
from dictionarydata import componentVars
from dictionarydata import componentChoices
from dictionarydata import componentTypeDictionary
from dictionarydata import shaleThickness
from dictionarydata import aquiferThickness
from dictionarydata import lookupTableParams
from dictionarydata import LUTWeights
from dictionarydata import aquifers
from dictionarydata import connectionTypes
from dictionarydata import connections
from dictionarydata import analysisTypes
from dictionarydata import loggingtypes
from dictionarydata import distributionOptions
from dictionarydata import componentTypes
from dictionarydata import savedDictionary
from dictionarydata import LABEL_FONT
from dictionarydata import DASHBOARD_FONT

try:
    # Python2
    import Tkinter as tk
    from Tkinter import ttk
    from Tkinter import StringVar
    from Tkinter import IntVar
    from Tkinter import DoubleVar
    from Tkinter import BooleanVar
    from Tkinter import tkMessageBox as messagebox
except ImportError:
    # Python3
    import tkinter as tk
    from tkinter import ttk
    from tkinter import StringVar
    from tkinter import IntVar
    from tkinter import DoubleVar
    from tkinter import BooleanVar
    from tkinter import messagebox

class OpenIAM_Page(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        toolTip = Pmw.Balloon(parent)

        # Setup all variables needed for this frame
        controller.connection = StringVar()
        pressure = StringVar()
        saturation = StringVar()

        pressure.set(1)
        saturation.set(1)
        controller.connection.set(connections[0])

        oldNumberOfShaleLayers=IntVar()
        oldNumberOfShaleLayers.set(3)

        componentVars['analysis']={}
        componentVars['reservoirThickness']={}

        componentVars['reservoirThickness']['distriubtion']=StringVar()
        componentVars['reservoirThickness']['min']=DoubleVar()
        componentVars['reservoirThickness']['max']=DoubleVar()
        componentVars['reservoirThickness']['mean']=DoubleVar()
        componentVars['reservoirThickness']['std']=DoubleVar()
        componentVars['reservoirThickness']['values']=StringVar()
        componentVars['reservoirThickness']['weights']=StringVar()

        componentVars['reservoirThickness']['min'].set(0)
        componentVars['reservoirThickness']['max'].set(100)
        componentVars['reservoirThickness']['mean'].set(1)
        componentVars['reservoirThickness']['std'].set(1)
        componentVars['reservoirThickness']['values'].set('12, 23')
        componentVars['reservoirThickness']['weights'].set('12, 23')

        lookupTableParams['lookupTable_time_file']=StringVar()
        lookupTableParams['lookupTable_param_file']=StringVar()
        lookupTableParams['lookupTable_input_dir']=StringVar()

        lookupTableParams['lookupTable_time_file'].set('timestepfile.csv')
        lookupTableParams['lookupTable_param_file'].set('parameters.csv')
        lookupTableParams['lookupTable_input_dir'].set('../Inputs')

        components = []

        def showDashboard():
            currentDictionary = {}

            for key in componentVars:
                try:
                    currentDictionary[key]=componentVars[key].get()
                except:
                    currentDictionary[key] = {}

                    for object in componentVars[key]:
                        try:
                            currentDictionary[key][object]=componentVars[key][object].get()
                        except:
                            currentDictionary[key][object]=componentVars[key][object]

            if(savedDictionary != currentDictionary):
                MsgBox = messagebox.askquestion('Save', 'Do you want to save before Closing.', icon='warning')

                if MsgBox == 'yes':
                    controller.populate_dictionary(shaleThickness, aquiferThickness, lookupTableParams, LUTWeights)

            controller.showDashboard()

        # Method sets the component model that the new component model will
        # have a connection to
        def connectionChange(conn):
            for widget in componentsSetupFrame.winfo_children():
                widget.destroy()

            connection_label = ttk.Label(componentsSetupFrame, width=25, text="Connection:")
            connection_menu = tk.OptionMenu(componentsSetupFrame, controller.connection, *connections, command=connectionChange)

            connection_menu.config(width=25)
            connection_label.grid(row=0, column=0, pady=5, padx=5)
            connection_menu.grid(row=0, column=1, pady=5, padx=10, sticky='e')

            if(controller.componentType.get().find('Aquifer')!=-1):
                aquiferName.set(aquifers[0])
                label = ttk.Label(componentsSetupFrame, width=25, text="Aquifer Name:")
                menu = tk.OptionMenu(componentsSetupFrame, aquiferName, *aquifers)

                menu.config(width=25)
                label.grid(row=1, column=0, pady=5, padx=5)
                menu.grid(row=1, column=1, pady=5)

            if(conn!='Dynamic Parameters'):
                controller.connection.set(conn)

            if(conn=='Dynamic Parameters'):
                if(controller.componentType.get().find('Wellbore')!=-1):
                    pressure_label = ttk.Label(componentsSetupFrame, width=25, text="Pressure:")
                    pressure_textField = tk.Entry(componentsSetupFrame, width=25)

                    pressure_label.grid(row=1, column=0, pady=5, padx=5)
                    pressure_textField.grid(row=1, column=1, pady=5, padx=5)

                    saturation_label = ttk.Label(componentsSetupFrame, width=25, text="Saturation:")
                    saturation_textField = tk.Entry(componentsSetupFrame, width=25)

                    saturation_label.grid(row=2, column=0, pady=5, padx=5)
                    saturation_textField.grid(row=2, column=1, pady=5, padx=5)

                if(controller.componentType.get().find('Aquifer')!=-1):
                    brine_flux_label = ttk.Label(componentsSetupFrame, width=25, text="Brine Flux:")
                    brine_flux_textField = tk.Entry(componentsSetupFrame, width=25)

                    brine_flux_label.grid(row=2, column=0, pady=5, padx=5)
                    brine_flux_textField.grid(row=2, column=1, pady=5, padx=5)

                    co2_flux_label = ttk.Label(componentsSetupFrame, width=25, text="CO"+str(u'\u2082')+" Flux:")
                    co2_flux_textField = tk.Entry(componentsSetupFrame, width=25)

                    co2_flux_label.grid(row=3, column=0, pady=5, padx=5)
                    co2_flux_textField.grid(row=3, column=1, pady=5, padx=5)

                if(controller.componentType.get().find('Atmo')!=-1):
                    brine_flux_label = ttk.Label(componentsSetupFrame, width=25, text="Brine Flux:")
                    brine_flux_textField = tk.Entry(componentsSetupFrame, width=25)

                    brine_flux_label.grid(row=2, column=0, pady=5, padx=5)
                    brine_flux_textField.grid(row=2, column=1, pady=5, padx=5)

                    co2_flux_label = ttk.Label(componentsSetupFrame, width=25, text="CO"+str(u'\u2082')+" Flux:")
                    co2_flux_textField = tk.Entry(componentsSetupFrame, width=25)

                    co2_flux_label.grid(row=3, column=0, pady=5, padx=5)
                    co2_flux_textField.grid(row=3, column=1, pady=5, padx=5)

        # Changes the add component model frame to match what is needed for each
        # different type of component model
        def componentTypeSetup(componentType):
            for widget in componentsSetupFrame.winfo_children():
                widget.destroy()

            if(componentType.find('Reservoir')!=-1):
                return

            connection_label = ttk.Label(componentsSetupFrame, width=25, text="Connection:")
            connection_menu = tk.OptionMenu(componentsSetupFrame, controller.connection, *connections, command=connectionChange)

            connection_menu.config(width=25)
            toolTip.bind(connection_menu, 'Select the connection for your component model.')
            connection_label.grid(row=0, column=0, pady=5, padx=5, sticky='ew')
            connection_menu.grid(row=0, column=1, pady=5, padx=5, sticky='ew')

            if(componentType.find('Wellbore')!=-1):
                pressure_label = ttk.Label(componentsSetupFrame, width=25, text="Pressure:")
                pressure_textField = tk.Entry(componentsSetupFrame, textvariable=pressure, width=25)

                pressure_label.grid(row=1, column=0, pady=5, padx=5)
                pressure_textField.grid(row=1, column=1, pady=5, padx=5)
                toolTip.bind(pressure_textField, 'Enter the pressure parameter.')

                saturation_label = ttk.Label(componentsSetupFrame, width=25, text="Saturation:")
                saturation_textField = tk.Entry(componentsSetupFrame, textvariable=saturation, width=25)

                saturation_label.grid(row=2, column=0, pady=5, padx=5)
                saturation_textField.grid(row=2, column=1, pady=5, padx=5)
                toolTip.bind(saturation_textField, 'Enter the saturation parameter.')

            if(componentType.find('Aquifer')!=-1):
                aquiferName.set(aquifers[0])
                label = ttk.Label(componentsSetupFrame, width=25, text="Aquifer Name:")
                menu = tk.OptionMenu(componentsSetupFrame, aquiferName, *aquifers)

                menu.config(width=25)
                toolTip.bind(menu, 'Select which of your aquifer layer this component model will represent.')
                label.grid(row=1, column=0, pady=5, padx=5)
                menu.grid(row=1, column=1, pady=5)

                brine_flux_label = ttk.Label(componentsSetupFrame, width=25, text="Brine Flux:")
                brine_flux_textField = tk.Entry(componentsSetupFrame, textvariable=pressure, width=25)

                brine_flux_label.grid(row=2, column=0, pady=5, padx=5)
                brine_flux_textField.grid(row=2, column=1, pady=5, padx=5)
                toolTip.bind(brine_flux_textField, 'Enter the brine flux for your Aquifer.')

                co2_flux_label = ttk.Label(componentsSetupFrame, width=25, text="CO"+str(u'\u2082')+" Flux:")
                co2_flux_textField = tk.Entry(componentsSetupFrame, textvariable=saturation, width=25)

                co2_flux_label.grid(row=3, column=0, pady=5, padx=5)
                co2_flux_textField.grid(row=3, column=1, pady=5, padx=5)
                toolTip.bind(co2_flux_textField, 'Enter the CO'+str(u'\u2082')+' flux for your Aquifer.')

            if(componentType.find('Atmo')!=-1):
                brine_flux_label = ttk.Label(componentsSetupFrame, width=25, text="Brine Flux:")
                brine_flux_textField = tk.Entry(componentsSetupFrame, textvariable=pressure, width=25)

                brine_flux_label.grid(row=2, column=0, pady=5, padx=5)
                brine_flux_textField.grid(row=2, column=1, pady=5, padx=5)
                toolTip.bind(brine_flux_textField, 'Enter the Brine Flux for your Atmospheric ROM component Model.')

                co2_flux_label = ttk.Label(componentsSetupFrame, width=25, text="CO"+str(u'\u2082')+" Flux:")
                co2_flux_textField = tk.Entry(componentsSetupFrame, textvariable=saturation, width=25)

                co2_flux_label.grid(row=3, column=0, pady=5, padx=5)
                co2_flux_textField.grid(row=3, column=1, pady=5, padx=5)
                toolTip.bind(co2_flux_textField, 'Enter the CO'+str(u'\u2082')+' Flux for your Atmospheric ROM component Model.')

        # Changes the analysis type of the simulation
        def analysisTypeSet(analysis):
            for widget in analysisFrame.winfo_children():
                widget.destroy()

            componentVars['analysis']['size']=IntVar()
            componentVars['analysis']['seed']=IntVar()
            componentVars['analysis']['nvals']=IntVar()

            if(analysis=='Forward'):
                analysis_label = ttk.Label(analysisFrame, text="Analysis:", width=32)
                analysis_menu = tk.OptionMenu(analysisFrame, componentVars['analysis']['type'], *analysisTypes, command=analysisTypeSet)
                analysis_menu.config(width=21)
                analysis_label.grid(row=3, column=0, padx=5, sticky='e')
                analysis_menu.grid(row=3, column=1, pady=5, sticky='we')

            if(analysis=='LHS'):
                componentVars['analysis']['size'].set(30)
                componentVars['analysis']['seed'].set(random.randrange(1000))

                analysis_label = ttk.Label(analysisFrame, text="Analysis:", width=32)
                analysis_menu = tk.OptionMenu(analysisFrame, componentVars['analysis']['type'], *analysisTypes, command=analysisTypeSet)
                analysis_size_label = ttk.Label(analysisFrame, text="Size:")
                analysis_size_txtField = tk.Entry(analysisFrame, textvariable=componentVars['analysis']['size'], width=25)
                analysis_seed_label = ttk.Label(analysisFrame, text="Seed:")
                analysis_seed_txtField = tk.Entry(analysisFrame, textvariable=componentVars['analysis']['seed'], width=25)

                analysis_menu.config(width=21)
                analysis_label.grid(row=3, column=0, pady=5, padx=5, sticky='e')
                analysis_menu.grid(row=3, column=1, pady=5, sticky='we')
                analysis_size_label.grid(row=3, column=2, pady=5)
                analysis_size_txtField.grid(row=3, column=3, pady=5)
                analysis_seed_label.grid(row=3, column=4, pady=5)
                analysis_seed_txtField.grid(row=3, column=6, pady=5)

            if(analysis=='Parstudy'):
                componentVars['analysis']['nvals'].set(30)

                analysis_label = ttk.Label(analysisFrame, text="Analysis:", width=32)
                analysis_menu = tk.OptionMenu(analysisFrame, componentVars['analysis']['type'], *analysisTypes, command=analysisTypeSet)
                analysis_size_label = ttk.Label(analysisFrame, text="nvals:")
                analysis_size_txtField = tk.Entry(analysisFrame, textvariable=componentVars['analysis']['nvals'], width=25)

                analysis_menu.config(width=21)
                analysis_label.grid(row=3, column=0, padx=5, sticky='e')
                analysis_menu.grid(row=3, column=1, pady=5, sticky='we')
                analysis_size_label.grid(row=3, column=2, pady=5)
                analysis_size_txtField.grid(row=3, column=3, pady=5)

        # Modifies what values that need to be entered for the reservoir
        # thickness based on the distribution selected
        def reservoirThicknessDistriubtionChange(distType):
            for widget in reservoirThicknessFrame.winfo_children():
                widget.destroy()

            componentVars['reservoirThickness']['distriubtion'].set(distType)

            if(distType=='Fixed Value'):
                reservoirThickness_label = ttk.Label(reservoirThicknessFrame, width=25, text="Reservoir Thickness:")
                reservoirThicknessValue_label=ttk.Label(reservoirThicknessFrame, text="Value:")
                reservoirThicknessValue_menu = tk.OptionMenu(reservoirThicknessFrame, componentVars['reservoirThickness']['distriubtion'], *distributionOptions, command=reservoirThicknessDistriubtionChange)
                reservoirThicknessValue_txtField = tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['Value'])

                reservoirThicknessValue_menu.config(width=15)
                toolTip.bind(reservoirThicknessValue_menu, 'Select distribution type for Reservoir Thickness.')
                reservoirThickness_label.grid(row=0, column=0, sticky='w', padx=5)
                reservoirThicknessValue_menu.grid(row=0, column=1, sticky='ew', padx=5)
                reservoirThicknessValue_label.grid(row=0, column=2, sticky='w', padx=5)
                reservoirThicknessValue_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(reservoirThicknessValue_txtField, 'Set Thickness of Reservoir.')

            if(distType=='Uniform'):
                reservoirThickness_label = ttk.Label(reservoirThicknessFrame, width=25, text="Reservoir Thickness:")
                reservoirThicknessMin_label=ttk.Label(reservoirThicknessFrame, text="Minimum:")
                reservoirThicknessMax_label=ttk.Label(reservoirThicknessFrame, text="Maximum:")
                reservoirThicknessValue_menu = tk.OptionMenu(reservoirThicknessFrame, componentVars['reservoirThickness']['distriubtion'], *distributionOptions, command=reservoirThicknessDistriubtionChange)
                reservoirThicknessMin_textField=tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['min'])
                reservoirThicknessMax_textField=tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['max'])

                reservoirThicknessValue_menu.config(width=15)
                toolTip.bind(reservoirThicknessValue_menu, 'Select distribution type for Reservoir Thickness.')
                reservoirThickness_label.grid(row=0, column=0, sticky='w', padx=5)
                reservoirThicknessValue_menu.grid(row=0, column=1, sticky='ew', padx=5)
                reservoirThicknessMin_label.grid(row=0, column=2, sticky='e', padx=5)
                reservoirThicknessMin_textField.grid(row=0, column=3, sticky='e', padx=5)
                toolTip.bind(reservoirThicknessMin_textField, 'Set Minimum Thickness of Reservoir.')
                reservoirThicknessMax_label.grid(row=0, column=4, sticky='e', padx=5)
                reservoirThicknessMax_textField.grid(row=0, column=5, sticky='e', padx=5)
                toolTip.bind(reservoirThicknessMax_textField, 'Set Maximum Thickness of Reservoir.')

            if(distType=='Normal'):
                reservoirThickness_label = ttk.Label(reservoirThicknessFrame, width=25, text="Reservoir Thickness:")
                reservoirThicknessMean_label=ttk.Label(reservoirThicknessFrame, text="Mean:")
                reservoirThicknessStd_label=ttk.Label(reservoirThicknessFrame, text="Standard Deviation:")
                reservoirThicknessValue_menu = tk.OptionMenu(reservoirThicknessFrame, componentVars['reservoirThickness']['distriubtion'], *distributionOptions, command=reservoirThicknessDistriubtionChange)
                reservoirThicknessMean_txtField = tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['mean'])
                reservoirThicknessStd_textField = tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['std'])

                reservoirThicknessValue_menu.config(width=15)
                toolTip.bind(reservoirThicknessValue_menu, 'Select distribution type for Reservoir Thickness.')
                reservoirThickness_label.grid(row=0, column=0, sticky='w', padx=5)
                reservoirThicknessValue_menu.grid(row=0, column=1, sticky='ew', padx=5)
                reservoirThicknessMean_label.grid(row=0, column=2, sticky='e', padx=5)
                reservoirThicknessMean_txtField.grid(row=0, column=3, sticky='w', padx=5)
                toolTip.bind(reservoirThicknessMean_txtField, 'Set Mean Thickness of Reservoir.')
                reservoirThicknessStd_label.grid(row=0, column=4, sticky='e', padx=5)
                reservoirThicknessStd_textField.grid(row=0, column=5, sticky='w', padx=5)
                toolTip.bind(reservoirThicknessStd_textField, 'Set Standard Deviation Thickness of Reservoir.')

            if(distType=='Lognormal'):
                reservoirThickness_label = ttk.Label(reservoirThicknessFrame, width=25, text="Reservoir Thickness:")
                reservoirThicknessMean_label=ttk.Label(reservoirThicknessFrame, text="Sigma:")
                reservoirThicknessStd_label=ttk.Label(reservoirThicknessFrame, text="ln(mu):")
                reservoirThicknessValue_menu = tk.OptionMenu(reservoirThicknessFrame, componentVars['reservoirThickness']['distriubtion'], *distributionOptions, command=reservoirThicknessDistriubtionChange)
                reservoirThicknessMean_txtField = tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['mean'])
                reservoirThicknessStd_textField = tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['std'])

                reservoirThicknessValue_menu.config(width=15)
                toolTip.bind(reservoirThicknessValue_menu, 'Select distribution type for Reservoir Thickness.')
                reservoirThickness_label.grid(row=0, column=0, sticky='w', padx=5)
                reservoirThicknessValue_menu.grid(row=0, column=1, sticky='ew', padx=5)
                reservoirThicknessMean_label.grid(row=0, column=2, sticky='e', padx=5)
                reservoirThicknessMean_txtField.grid(row=0, column=3, sticky='w', padx=5)
                toolTip.bind(reservoirThicknessMean_txtField, 'Set Sigma Value.')
                reservoirThicknessStd_label.grid(row=0, column=4, sticky='e', padx=5)
                reservoirThicknessStd_textField.grid(row=0, column=5, sticky='w', padx=5)
                toolTip.bind(reservoirThicknessStd_textField, 'Set Ln Value.')

            if(distType=='Truncated'):
                reservoirThickness_label = ttk.Label(reservoirThicknessFrame, width=25, text="Reservoir Thickness:")
                reservoirThicknessMin_label=ttk.Label(reservoirThicknessFrame, text="Minimum:")
                reservoirThicknessMax_label=ttk.Label(reservoirThicknessFrame, text="Maximum:")
                reservoirThicknessValue_menu = tk.OptionMenu(reservoirThicknessFrame, componentVars['reservoirThickness']['distriubtion'], *distributionOptions, command=reservoirThicknessDistriubtionChange)
                reservoirThicknessMin_textField=tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['min'])
                reservoirThicknessMax_textField=tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['max'])
                reservoirThicknessMean_label=ttk.Label(reservoirThicknessFrame, text="Mean:")
                reservoirThicknessStd_label=ttk.Label(reservoirThicknessFrame, text="Standard Deviation:")
                reservoirThicknessMean_txtField = tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['mean'])
                reservoirThicknessStd_textField = tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['std'])

                reservoirThicknessValue_menu.config(width=15)
                toolTip.bind(reservoirThicknessValue_menu, 'Select distribution type for Reservoir Thickness.')
                reservoirThickness_label.grid(row=0, column=0, sticky='w', padx=5)
                reservoirThicknessValue_menu.grid(row=0, column=1, sticky='ew', padx=5)
                reservoirThicknessValue_menu.grid(row=0, column=2, sticky='ew', padx=5)
                reservoirThicknessMin_label.grid(row=0, column=3, sticky='e', padx=5)
                reservoirThicknessMin_textField.grid(row=0, column=4, sticky='e', padx=5)
                toolTip.bind(reservoirThicknessMin_textField, 'Set Minimum Thickness of Reservoir.')
                reservoirThicknessMax_label.grid(row=0, column=5, sticky='e', padx=5)
                reservoirThicknessMax_textField.grid(row=0, column=6, sticky='e', padx=5)
                toolTip.bind(reservoirThicknessMax_textField, 'Set Maximum Thickness of Reservoir.')
                reservoirThicknessMean_label.grid(row=0, column=7, sticky='e', padx=5)
                reservoirThicknessMean_txtField.grid(row=0, column=8, sticky='w', padx=5)
                toolTip.bind(reservoirThicknessMean_txtField, 'Set Mean Thickness of Reservoir.')
                reservoirThicknessStd_label.grid(row=0, column=9, sticky='e', padx=5)
                reservoirThicknessStd_textField.grid(row=0, column=10, sticky='w', padx=5)
                toolTip.bind(reservoirThicknessStd_textField, 'Set Standard Deviation Thickness of Reservoir.')

            if(distType=='Triangular'):
                reservoirThickness_label = ttk.Label(reservoirThicknessFrame, width=25, text="Reservoir Thickness:")
                reservoirThicknessValue_menu = tk.OptionMenu(reservoirThicknessFrame, componentVars['reservoirThickness']['distriubtion'], *distributionOptions, command=reservoirThicknessDistriubtionChange)
                reservoirThicknessMin_label=ttk.Label(reservoirThicknessFrame, text="C:")
                reservoirThicknessMin_textField=tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['min'])
                reservoirThicknessMax_label=ttk.Label(reservoirThicknessFrame, text="loc:")
                reservoirThicknessMax_textField=tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['max'])
                reservoirThicknessMean_label=ttk.Label(reservoirThicknessFrame, text="Scale:")
                reservoirThicknessMean_txtField = tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['std'])

                reservoirThicknessValue_menu.config(width=15)
                toolTip.bind(reservoirThicknessValue_menu, 'Select distribution type for Reservoir Thickness.')
                reservoirThickness_label.grid(row=0, column=0, sticky='w', padx=5)
                reservoirThicknessValue_menu.grid(row=0, column=1, sticky='ew', padx=5)
                reservoirThicknessMin_label.grid(row=0, column=3, sticky='e', padx=5)
                reservoirThicknessMin_textField.grid(row=0, column=4, sticky='e', padx=5)
                toolTip.bind(reservoirThicknessMin_textField, 'Set C Value.')
                reservoirThicknessMax_label.grid(row=0, column=5, sticky='e', padx=5)
                reservoirThicknessMax_textField.grid(row=0, column=6, sticky='e', padx=5)
                toolTip.bind(reservoirThicknessMax_textField, 'Set loc Value.')
                reservoirThicknessMean_label.grid(row=0, column=7, sticky='e', padx=5)
                reservoirThicknessMean_txtField.grid(row=0, column=8, sticky='w', padx=5)
                toolTip.bind(reservoirThicknessMean_txtField, 'Set Scale.')

            if(distType=='Discrete'):
                reservoirThickness_label = ttk.Label(reservoirThicknessFrame, width=25, text="Reservoir Thickness:")
                reservoirThicknessValue_menu = tk.OptionMenu(reservoirThicknessFrame, componentVars['reservoirThickness']['distriubtion'], *distributionOptions, command=reservoirThicknessDistriubtionChange)
                reservoirThicknessMin_label=ttk.Label(reservoirThicknessFrame, text="Values:")
                reservoirThicknessMin_textField=tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['values'])
                reservoirThicknessMax_label=ttk.Label(reservoirThicknessFrame, text="Weights:")
                reservoirThicknessMax_textField=tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['weights'])

                reservoirThicknessValue_menu.config(width=15)
                toolTip.bind(reservoirThicknessValue_menu, 'Select distribution type for Reservoir Thickness.')
                reservoirThickness_label.grid(row=0, column=0, sticky='w', padx=5)
                reservoirThicknessValue_menu.grid(row=0, column=1, sticky='ew', padx=5)
                reservoirThicknessMin_label.grid(row=0, column=2, sticky='e', padx=5)
                reservoirThicknessMin_textField.grid(row=0, column=3, sticky='e', padx=5)
                toolTip.bind(reservoirThicknessMin_textField, 'Set Thickness Values of Reservoir.')
                reservoirThicknessMax_label.grid(row=0, column=4, sticky='e', padx=5)
                reservoirThicknessMax_textField.grid(row=0, column=5, sticky='e', padx=5)
                toolTip.bind(reservoirThicknessMax_textField, 'Set Thickness Weighs.')

        # Adds the number of shale layers and aquifer layers based on user
        # selection during setup also adds each aquifer layer to the
        # aquifer array
        def addShaleAndAquiferComponents(i):
            for widget in shaleFrame.winfo_children():
                widget.destroy()
            for widget in aquiferFrame.winfo_children():
                widget.destroy()

            scanv.config(scrollregion=(0,0,1500, 150+((i)*30)))
            acanv.config(scrollregion=(0,0,1500, 150+((i)*30)))

            aquiferFrames=[]
            shaleFrames=[]

            shaleFrame.shaleThicknessMinComponents=[]
            shaleFrame.shaleThicknessMaxComponents=[]
            shaleFrame.shaleThicknessValueComponents=[]
            shaleFrame.shaleThicknessDistributionOptionsComponents=[]

            aquiferFrame.aquiferThicknessMinComponents=[]
            aquiferFrame.aquiferThicknessMaxComponents=[]
            aquiferFrame.aquiferThicknessValueComponents=[]
            aquiferFrame.aquiferThicknessDistributionOptionsComponents=[]

            for j in range(i-oldNumberOfShaleLayers.get()):
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']={}
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['min']=(DoubleVar())
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['max']=(DoubleVar())
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['value']=(DoubleVar())
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['mean']=(DoubleVar())
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['std']=(DoubleVar())
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['values']=StringVar()
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['weights']=StringVar()
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['distribution']=(StringVar())

                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['min'].set(1)
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['max'].set(1000)
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['value'].set(100)
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['mean'].set(0)
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['std'].set(1)
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['values'].set('12, 34')
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['weights'].set('12, 34')
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['distribution'].set(distributionOptions[0])
                shaleThickness['shale'+str(j+oldNumberOfShaleLayers.get()+1)+'Thickness']['label']="Shale "+ str(j+oldNumberOfShaleLayers.get()+1) +" Thickness: [m]"

            for h in range(i-oldNumberOfShaleLayers.get()):
                aquifers.append('aquifer'+str(h+oldNumberOfShaleLayers.get()))

                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']={}
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['min']=(DoubleVar())
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['max']=(DoubleVar())
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['value']=(DoubleVar())
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['mean']=(DoubleVar())
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['std']=(DoubleVar())
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['values']=StringVar()
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['weights']=StringVar()
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['distribution']=(StringVar())

                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['min'].set(1)
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['max'].set(1000)
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['value'].set(100)
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['mean'].set(0)
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['std'].set(1)
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['values'].set('12, 34')
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['weights'].set('12, 34')
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['distribution'].set(distributionOptions[0])
                aquiferThickness['aquifer'+str(h+oldNumberOfShaleLayers.get())+'Thickness']['label']="Aquifer "+ str(h+oldNumberOfShaleLayers.get()) +" Thickness: [m]"

            for n in range(componentVars['numberOfShaleLayers'].get()):
                shaleFrames.append(tk.Frame(shaleFrame))
                shaleFrames[n].grid(row=n, column=0, sticky='w')

                shaleThickness['shale'+str(n+1)+'Thickness']['frame']=shaleFrames[n]
                shaleThickness['shale'+str(n+1)+'Thickness']['distribution'].set(distributionOptions[0])

                ttk.Label(shaleFrames[n], width=25, text="Shale "+ str(n+1) +" Thickness: [m]").grid(row=0, column=0, sticky='w', padx=5)
                ttk.Label(shaleFrames[n], text="Value:").grid(row=0, column=2, sticky='w', padx=5)

                if n==0:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(0)))
                if n==1:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(1)))
                if n==2:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(2)))
                if n==3:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(3)))
                if n==4:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(4)))
                if n==5:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(5)))
                if n==6:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(6)))
                if n==7:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(7)))
                if n==8:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(8)))
                if n==9:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(9)))
                if n==10:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(10)))
                if n==11:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(11)))
                if n==12:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(12)))
                if n==13:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(13)))
                if n==14:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(14)))
                if n==15:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(15)))
                if n==16:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(16)))
                if n==17:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(17)))
                if n==18:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(18)))
                if n==19:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(19)))
                if n==20:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(20)))
                if n==21:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(21)))
                if n==22:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(22)))
                if n==23:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(23)))
                if n==24:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(24)))
                if n==25:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(25)))
                if n==26:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(26)))
                if n==27:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(27)))
                if n==28:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(28)))
                if n==29:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(29)))
                if n==30:
                    shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(30)))

                shaleFrame.shaleThicknessValueComponents.append(tk.Entry(shaleFrames[n], textvariable=shaleThickness['shale'+str(n+1)+'Thickness']['value']))

                shaleFrame.shaleThicknessDistributionOptionsComponents[n].config(width=15)
                toolTip.bind(shaleFrame.shaleThicknessDistributionOptionsComponents[n],'Select distribution type of shale layer.')

                shaleFrame.shaleThicknessDistributionOptionsComponents[n].grid(row=0, column=1, sticky='w', padx=5)
                shaleFrame.shaleThicknessValueComponents[n].grid(row=0, column=3, sticky='w', padx=5)
                toolTip.bind(shaleFrame.shaleThicknessValueComponents[n],'Set Thickness of shale layer.')

            for m in range(componentVars['numberOfShaleLayers'].get()-1):
                aquiferFrames.append(tk.Frame(aquiferFrame))
                aquiferFrames[m].grid(row=m, column=0, sticky='w')

                aquiferThickness['aquifer'+str(m+1)+'Thickness']['frame']=aquiferFrames[m]
                aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'].set(distributionOptions[0])

                ttk.Label(aquiferFrames[m], width=25, text="Aquifer "+ str(m+1) +" Thickness: [m]").grid(row=0, column=0, sticky='w', padx=5)
                ttk.Label(aquiferFrames[m], text="Value:").grid(row=0, column=2, sticky='w', padx=5)

                if m==0:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(0)))
                if m==1:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(1)))
                if m==2:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(2)))
                if m==3:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(3)))
                if m==4:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(4)))
                if m==5:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(5)))
                if m==6:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(6)))
                if m==7:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(7)))
                if m==8:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(8)))
                if m==9:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(9)))
                if m==10:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(10)))
                if m==11:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(11)))
                if m==12:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(12)))
                if m==13:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(13)))
                if m==14:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(14)))
                if m==15:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(15)))
                if m==16:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(16)))
                if m==17:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(17)))
                if m==18:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(18)))
                if m==19:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(19)))
                if m==20:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(20)))
                if m==21:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(21)))
                if m==22:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(22)))
                if m==23:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(23)))
                if m==24:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(24)))
                if m==25:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(25)))
                if m==26:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(26)))
                if m==27:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(27)))
                if m==28:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(28)))
                if m==29:
                    aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(29)))


                aquiferFrame.aquiferThicknessValueComponents.append(tk.Entry(aquiferFrames[m], textvariable=aquiferThickness['aquifer'+str(m+1)+'Thickness']['value']))

                aquiferFrame.aquiferThicknessDistributionOptionsComponents[m].config(width=15)
                toolTip.bind(aquiferFrame.aquiferThicknessDistributionOptionsComponents[m],'Select distribution type of aquifer layer.')

                aquiferFrame.aquiferThicknessDistributionOptionsComponents[m].grid(row=0, column=1, sticky='w', padx=5)
                aquiferFrame.aquiferThicknessValueComponents[m].grid(row=0, column=3, sticky='w', padx=5)
                toolTip.bind(aquiferFrame.aquiferThicknessValueComponents[m],'Set Thickness of aquifer layer.')

            oldNumberOfShaleLayers.set(i)

        tabControl= ttk.Notebook(self)
        controller.tabControl = tabControl
        model = ttk.Frame(tabControl, padding=10)
        modelTab = ttk.Frame(model, padding=10)
        tabControl.add(model, text="Model")
        tabControl.pack(expand=1, fill="both", padx=10, pady=5)

        d['simName'] = StringVar()
        d['simName'].set('Default')

        simName_label=ttk.Label(model, text='Enter a Name:')
        simName_label.grid(row=0, column=0, padx=5, pady=5)

        simName_txtFiled = tk.Entry(model, textvariable=d['simName'])
        simName_txtFiled.grid(row=0, column=1, padx=5, columnspan=3, pady=5)
        toolTip.bind(simName_txtFiled, 'Enter a Unique Name for your simulation.')

        modelTab.grid(row=1, column=0, columnspan=10)

        descriptionLabel = ttk.Label(model, text=""" After entering model parameters proceed to stratigraphy parameters. """, font=LABEL_FONT)
        descriptionLabel.grid(row=2, column=0, columnspan=10)

        nextButton = tk.Button(model, text='Stratigraphy Tab', command=lambda :tabControl.select('.!frame.!openiam_page.!notebook.!frame2'))
        nextButton.grid(row=3, column=11)
        toolTip.bind(nextButton, 'This button will move you to the next part of the setup.')

        modelParams_label = ttk.Label(modelTab, text="Model Parameters:", font=LABEL_FONT)
        modelParams_label.grid(row=0, column=0, sticky='w')

        componentVars['endTime'] = DoubleVar()
        componentVars['endTime'].set(50.0)
        endTime_label = ttk.Label(modelTab, text="End Time: [Years]")
        endTime_txtField = tk.Entry(modelTab, width=25, textvariable=componentVars['endTime'])
        endTime_label.grid(row=1, column=0, pady=5, padx=5, sticky='w')
        endTime_txtField.grid(row=1, column=1, padx=5, pady=5, sticky='we')
        toolTip.bind(endTime_txtField, "Number of years to run simulation.")

        componentVars['timeStep'] = DoubleVar()
        componentVars['timeStep'].set(1.0)
        timeStep_label = ttk.Label(modelTab, text="Time Step: [Years]")
        timeStep_txtField = tk.Entry(modelTab, textvariable=componentVars['timeStep'])
        timeStep_label.grid(row=2, column=0, pady=5, padx=5, sticky='w')
        timeStep_txtField.grid(row=2, column=1, padx=5, pady=5, sticky='we')
        toolTip.bind(timeStep_txtField, "How many years in each time step.")

        analysisFrame = tk.Frame(modelTab)
        analysisFrame.grid(row=3, column=0, columnspan=5, sticky='w')

        componentVars['analysis']['type'] = StringVar()
        componentVars['analysis']['type'].set(analysisTypes[0])
        analysis_label = ttk.Label(analysisFrame, text="Analysis:", width=32)
        analysis_menu = tk.OptionMenu(analysisFrame, componentVars['analysis']['type'], *analysisTypes, command=analysisTypeSet)
        analysis_menu.config(width=21)
        analysis_label.grid(row=3, column=0, pady=5, padx=5, sticky='w')
        analysis_menu.grid(row=3, column=1, pady=5, sticky='w')
        toolTip.bind(analysis_menu, 'Type of analysis to be used for simulation.')

        componentVars['logging'] = StringVar()
        componentVars['logging'].set(loggingtypes[1])
        logging_label = ttk.Label(modelTab, text="Logging:")
        logging_Menu = tk.OptionMenu(modelTab, componentVars['logging'], *loggingtypes)
        logging_label.grid(row=5, column=0, pady=5, padx=5, sticky='w')
        logging_Menu.grid(row=5, column=1, pady=5, padx=5, sticky='we')
        toolTip.bind(logging_Menu, 'What type of logging for simulation.')

        componentVars['outputDirectory'] = StringVar()
        try:
            componentVars['outputDirectory'].set(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), 'Output'))
        except:
            componentVars['outputDirectory'].set('~Documents')

        outputDirectory_label = ttk.Label(modelTab, text="Output Directory:", width=15)
        outputDirectory_txtField = tk.Entry(modelTab, textvariable=componentVars['outputDirectory'])
        outputDirectory_browse = ttk.Button(modelTab, text="Browse", command=lambda:controller.chooseOutputDir(componentVars['outputDirectory']))
        outputDirectory_label.grid(row=6, column=0, pady=5, padx=5, sticky='w')
        outputDirectory_txtField.grid(row=6, column=1, pady=5, padx=5, sticky='we')
        outputDirectory_browse.grid(row=6, column=2, pady=5, padx=5, sticky='w')
        toolTip.bind(outputDirectory_txtField, 'Location to save outputs.')
        toolTip.bind(outputDirectory_browse, 'Button to open file browser and select output directory.')

        componentVars['outputDirectoryGenerate'] = BooleanVar()
        componentVars['outputDirectoryGenerate'].set(0)
        outputDirectoryGenerate_label = ttk.Label(modelTab, text="Generate Output Directory:")
        outputDirectoryGenerate_checkbox = tk.Checkbutton(modelTab, variable=componentVars['outputDirectoryGenerate'])
        outputDirectoryGenerate_label.grid(row=7, column=0, pady=5, padx=5, sticky='w')
        outputDirectoryGenerate_checkbox.grid(row=7, column=1, pady=5, sticky='w')
        toolTip.bind(outputDirectoryGenerate_checkbox, 'Check to generate a directory titled with timestamp for outputs to be saved.')

        wellboreLocations_label = ttk.Label(modelTab, text="Wellbore Locations:", font=LABEL_FONT)
        wellboreLocations_label.grid(row=8, column=0, sticky='w')

        componentVars['xCoordinates'] = StringVar()
        componentVars['xCoordinates'].set("100, 540")
        xCoordinates_label = ttk.Label(modelTab, text="X-Coordinates:")
        xCoordinates_txtField = tk.Entry(modelTab, textvariable=componentVars['xCoordinates'])
        xCoordinates_label.grid(row=9, column=0, pady=5, padx=5, sticky='w')
        xCoordinates_txtField.grid(row=9, column=1, pady=5, sticky='we')
        toolTip.bind(xCoordinates_txtField, 'A list of X-Coordinates for Wellbore Locations.')

        componentVars['yCoordinates'] = StringVar()
        componentVars['yCoordinates'].set("100, 630")
        yCoordinates_label = ttk.Label(modelTab, text="Y-Coordinates:")
        yCoordinates_txtField = tk.Entry(modelTab, textvariable=componentVars['yCoordinates'])
        yCoordinates_label.grid(row=10, column=0, pady=5, padx=5, sticky='w')
        yCoordinates_txtField.grid(row=10, column=1, pady=5, sticky='we')
        toolTip.bind(yCoordinates_txtField, 'A list of Y-Coordinates for Wellbore Locations.')

        randomWellDomain_label=ttk.Label(modelTab, text="Random Well Bounding Box:", font=LABEL_FONT)
        randomWellDomain_label.grid(row=11, column=0, pady=5, padx=5)

        randomWellDomain=ttk.Frame(modelTab)
        randomWellDomain.grid(row=12, column=0, columnspan=15, padx=15)

        # produces a bounding box for all random wellbores to be placed within
        componentVars['RandomWellDomain']={}
        componentVars['RandomWellDomain']['xmin'] = DoubleVar()
        componentVars['RandomWellDomain']['ymin'] = DoubleVar()
        componentVars['RandomWellDomain']['xmax'] = DoubleVar()
        componentVars['RandomWellDomain']['ymax'] = DoubleVar()

        componentVars['RandomWellDomain']['xmin'].set(40)
        componentVars['RandomWellDomain']['ymin'].set(50)
        componentVars['RandomWellDomain']['xmax'].set(100)
        componentVars['RandomWellDomain']['ymax'].set(140)

        randomWellXMin_label = ttk.Label(randomWellDomain, text="X-Minimum:", width=15)
        randomWellXMin_txtField = tk.Entry(randomWellDomain, textvariable=componentVars['RandomWellDomain']['xmin'])
        randomWellYMin_label = ttk.Label(randomWellDomain, text="Y-Minimum:", width=15)
        randomWellYMin_txtField = tk.Entry(randomWellDomain, textvariable=componentVars['RandomWellDomain']['ymin'])
        randomWellXMax_label = ttk.Label(randomWellDomain, text="X-Maximum:", width=15)
        randomWellXMax_txtField = tk.Entry(randomWellDomain, textvariable=componentVars['RandomWellDomain']['xmax'])
        randomWellYMax_label = ttk.Label(randomWellDomain, text="Y-Maximum:", width=15)
        randomWellYMax_txtField = tk.Entry(randomWellDomain, textvariable=componentVars['RandomWellDomain']['ymax'])

        randomWellXMin_label.grid(row=2, column=0, pady=5, sticky='w')
        randomWellXMin_txtField.grid(row=2, column=1, pady=5, padx=5, sticky='we')
        toolTip.bind(randomWellXMin_txtField, 'Minimum X value for Bounding Box.')

        randomWellYMin_label.grid(row=2, column=2, pady=5, sticky='w')
        randomWellYMin_txtField.grid(row=2, column=3, pady=5, padx=5, sticky='we')
        toolTip.bind(randomWellYMin_txtField, 'Minimum Y value for Bounding Box.')

        randomWellXMax_label.grid(row=1, column=0, pady=5, sticky='w')
        randomWellXMax_txtField.grid(row=1, column=1, pady=5, padx=5, sticky='we')
        toolTip.bind(randomWellXMax_txtField, 'Maximum X value for Bounding Box.')

        randomWellYMax_label.grid(row=1, column=2, pady=5, sticky='w')
        randomWellYMax_txtField.grid(row=1, column=3, pady=5, padx=5, sticky='we')
        toolTip.bind(randomWellYMax_txtField, 'Maximum Y value for Bounding Box.')

        stratigraphy = ttk.Frame(tabControl, padding=10)

        descriptionLabel = ttk.Label(stratigraphy, text=""" """, font=LABEL_FONT)
        descriptionLabel.grid(row=1, column=0, columnspan=10)

        nextButton = tk.Button(stratigraphy, text='Next', command=lambda :tabControl.select('.!frame.!openiam_page.!notebook.!frame3'))
        nextButton.grid(row=2, column=11)
        toolTip.bind(nextButton, 'Click to move on to the Stratigraphy Entry form.')

        stratigraphyTab = ttk.Frame(stratigraphy, padding=10)
        stratigraphyTab.grid(row=0, column=0, columnspan=10)

        tabControl.add(stratigraphy, text="Stratigraphy Parameters")
        tabControl.pack(expand=1, fill="both")

        stratigraphy_label = ttk.Label(stratigraphyTab, text="Stratigraphy:", font=LABEL_FONT)
        stratigraphy_label.grid(row=0, column=0, sticky='w')

        layersCountFrame = tk.Frame(stratigraphyTab)
        layersCountFrame.grid(row=1,column=0, padx=15, sticky='w')

        componentVars['numberOfShaleLayers'] = IntVar()
        counts = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
        componentVars['numberOfShaleLayers'].set(counts[0])
        numberOfShaleLayers_label = ttk.Label(layersCountFrame, width=25, text="Number of Shale Layers:")
        numberOfShaleLayers_Menu = tk.OptionMenu(layersCountFrame, componentVars['numberOfShaleLayers'], *counts, command=addShaleAndAquiferComponents)
        numberOfShaleLayers_Menu.config(width=15)
        numberOfShaleLayers_label.grid(row=0, column=0)
        numberOfShaleLayers_Menu.grid(row=0, column=1, padx=15)
        toolTip.bind(numberOfShaleLayers_Menu,'Select the total number of Shale Layers for Simulation.')

        shaleBottomFrame = tk.Frame(stratigraphyTab, height=150)
        shaleBottomFrame.grid(row=2, column=0, sticky='nsew', columnspan=6, padx=5, pady=5)

        scanv = tk.Canvas(shaleBottomFrame, relief=tk.SUNKEN)
        scanv.config(width=800, height=150)
        scanv.config(scrollregion=(0,0,1500, 150))
        scanv.config(highlightthickness=0)

        sybar = tk.Scrollbar(shaleBottomFrame)
        sybar.config(command=scanv.yview)
        sxbar = tk.Scrollbar(shaleBottomFrame, orient='horizontal', command=scanv.xview)
        sxbar.pack(side=tk.BOTTOM, fill=tk.X)
        scanv.config(xscrollcommand=sxbar.set, yscrollcommand=sybar.set)
        sybar.pack(side=tk.RIGHT, fill=tk.Y)
        scanv.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

        shaleFrame=ttk.Frame(scanv)
        scanv.create_window((10,0), window=shaleFrame, anchor='nw')

        aquiferBottomFrame = tk.Frame(stratigraphyTab, height=150)
        aquiferBottomFrame.grid(row=3, column=0, sticky='nsew', columnspan=6, padx=5, pady=5)

        acanv = tk.Canvas(aquiferBottomFrame, relief=tk.SUNKEN)
        acanv.config(width=800, height=150)
        acanv.config(scrollregion=(0, 0, 1500, 150))
        acanv.config(highlightthickness=0)

        aybar = tk.Scrollbar(aquiferBottomFrame)
        aybar.config(command=acanv.yview)
        axbar = tk.Scrollbar(aquiferBottomFrame, orient='horizontal', command=acanv.xview)
        axbar.pack(side=tk.BOTTOM, fill=tk.X)
        acanv.config(xscrollcommand=axbar.set, yscrollcommand=aybar.set)
        aybar.pack(side=tk.RIGHT, fill=tk.Y)
        acanv.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

        aquiferFrame = tk.Frame(acanv)
        acanv.create_window((10,0), window=aquiferFrame, anchor='nw')

        right_frame=tk.Frame(stratigraphyTab, borderwidth=2)
        right_frame.grid(row=2, column=7, rowspan=2, columnspan=5, sticky='e')

        self.canvas = tk.PhotoImage(file="images/ShaleLayers.png")
        canvas = tk.Canvas(right_frame)
        canvas.configure(width=400, height=350)
        canvas.create_image(5, 5, anchor='nw', image=self.canvas)
        canvas.grid(row=0, column=0)

        aquiferFrames=[]
        shaleFrames=[]

        shaleFrame.shaleThicknessMinComponents=[]
        shaleFrame.shaleThicknessMaxComponents=[]
        shaleFrame.shaleThicknessValueComponents=[]
        shaleFrame.shaleThicknessDistributionOptionsComponents=[]

        aquiferFrame.aquiferThicknessMinComponents=[]
        aquiferFrame.aquiferThicknessMaxComponents=[]
        aquiferFrame.aquiferThicknessValueComponents=[]
        aquiferFrame.aquiferThicknessDistributionOptionsComponents=[]

        # creates all necessary varibles for the shale and aquifer thicknesses
        for n in range(componentVars['numberOfShaleLayers'].get()):
            shaleThickness['shale'+str(n+1)+'Thickness']={}
            shaleThickness['shale'+str(n+1)+'Thickness']['min']=(DoubleVar())
            shaleThickness['shale'+str(n+1)+'Thickness']['max']=(DoubleVar())
            shaleThickness['shale'+str(n+1)+'Thickness']['value']=(DoubleVar())
            shaleThickness['shale'+str(n+1)+'Thickness']['mean']=(DoubleVar())
            shaleThickness['shale'+str(n+1)+'Thickness']['std']=(DoubleVar())
            shaleThickness['shale'+str(n+1)+'Thickness']['values']=StringVar()
            shaleThickness['shale'+str(n+1)+'Thickness']['weights']=StringVar()
            shaleThickness['shale'+str(n+1)+'Thickness']['distribution']=(StringVar())

            shaleThickness['shale'+str(n+1)+'Thickness']['min'].set(1)
            shaleThickness['shale'+str(n+1)+'Thickness']['max'].set(1000)
            shaleThickness['shale'+str(n+1)+'Thickness']['value'].set(100)
            shaleThickness['shale'+str(n+1)+'Thickness']['mean'].set(0)
            shaleThickness['shale'+str(n+1)+'Thickness']['std'].set(1)
            shaleThickness['shale'+str(n+1)+'Thickness']['values'].set('12, 34')
            shaleThickness['shale'+str(n+1)+'Thickness']['weights'].set('12, 34')
            shaleThickness['shale'+str(n+1)+'Thickness']['distribution'].set(distributionOptions[0])
            shaleThickness['shale'+str(n+1)+'Thickness']['label']="Shale "+ str(n+1) +" Thickness: [m]"

            shaleFrames.append(tk.Frame(shaleFrame))
            shaleFrames[n].grid(row=n, column=0, sticky='w')

            shaleThickness['shale'+str(n+1)+'Thickness']['frame']=shaleFrames[n]

            ttk.Label(shaleFrames[n], width=25, text="Shale "+ str(n+1) +" Thickness: [m]").grid(row=0, column=0, sticky='w', padx=5)
            ttk.Label(shaleFrames[n], text="Value:").grid(row=0, column=2, sticky='w', padx=5)

            if n==0:
                shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(0)))
            if n==1:
                shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(1)))
            if n==2:
                shaleFrame.shaleThicknessDistributionOptionsComponents.append(tk.OptionMenu(shaleFrames[n], shaleThickness['shale'+str(n+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(2)))

            shaleFrame.shaleThicknessValueComponents.append(tk.Entry(shaleFrames[n], textvariable=shaleThickness['shale'+str(n+1)+'Thickness']['value']))

            shaleFrame.shaleThicknessDistributionOptionsComponents[n].config(width=15)
            toolTip.bind(shaleFrame.shaleThicknessDistributionOptionsComponents[n],'Select Distriubtion Type for Shale Layer.')
            shaleFrame.shaleThicknessDistributionOptionsComponents[n].grid(row=0, column=1, sticky='w', padx=5)
            shaleFrame.shaleThicknessValueComponents[n].grid(row=0, column=3, sticky='w', padx=5)
            toolTip.bind(shaleFrame.shaleThicknessValueComponents[n],'Set Thickness of shale layer.')

        for m in range(componentVars['numberOfShaleLayers'].get()-1):
            aquiferThickness['aquifer'+str(m+1)+'Thickness']={}
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['min']=(DoubleVar())
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['max']=(DoubleVar())
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['value']=(DoubleVar())
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['mean']=(DoubleVar())
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['std']=(DoubleVar())
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['values']=StringVar()
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['weights']=StringVar()
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution']=(StringVar())

            aquiferThickness['aquifer'+str(m+1)+'Thickness']['min'].set(1)
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['max'].set(1000)
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['value'].set(100)
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['mean'].set(0)
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['std'].set(1)
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['values'].set('12, 34')
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['weights'].set('12, 34')
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'].set(distributionOptions[0])
            aquiferThickness['aquifer'+str(m+1)+'Thickness']['label']="Aquifer "+ str(m+1) +" Thickness: [m]"

            aquiferFrames.append(tk.Frame(aquiferFrame))
            aquiferFrames[m].grid(row=m, column=0, sticky='w')

            aquiferThickness['aquifer'+str(m+1)+'Thickness']['frame']=aquiferFrames[m]

            ttk.Label(aquiferFrames[m], width=25, text="Aquifer "+ str(m+1) +" Thickness: [m]").grid(row=0, column=0, sticky='w', padx=5)
            ttk.Label(aquiferFrames[m], text="Value:").grid(row=0, column=2, sticky='w', padx=5)

            if m==0:
                aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(0)))
            if m==1:
                aquiferFrame.aquiferThicknessDistributionOptionsComponents.append(tk.OptionMenu(aquiferFrames[m], aquiferThickness['aquifer'+str(m+1)+'Thickness']['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(1)))

            aquiferFrame.aquiferThicknessValueComponents.append(tk.Entry(aquiferFrames[m], textvariable=aquiferThickness['aquifer'+str(m+1)+'Thickness']['value']))

            aquiferFrame.aquiferThicknessDistributionOptionsComponents[m].config(width=15)
            toolTip.bind(aquiferFrame.aquiferThicknessDistributionOptionsComponents[m],'Select distribution type for aquifer layer.')
            aquiferFrame.aquiferThicknessDistributionOptionsComponents[m].grid(row=0, column=1, sticky='w', padx=5)
            aquiferFrame.aquiferThicknessValueComponents[m].grid(row=0, column=3, sticky='w', padx=5)
            toolTip.bind(aquiferFrame.aquiferThicknessValueComponents[m],'Set Thickness of aquifer layer.')

        bottomFrame = tk.Frame(stratigraphyTab)
        bottomFrame.grid(row=4, column=0, padx=15, sticky='w', columnspan=20)

        reservoirThicknessFrame = tk.Frame(bottomFrame)
        reservoirThicknessFrame.grid(row=0, column=0, sticky='w')

        componentVars['reservoirThickness']['Value'] = DoubleVar()
        componentVars['reservoirThickness']['distriubtion']=StringVar()
        componentVars['reservoirThickness']['Value'].set(50.0)
        componentVars['reservoirThickness']['distriubtion'].set(distributionOptions[0])

        reservoirThickness_label = ttk.Label(reservoirThicknessFrame, width=25, text="Reservoir Thickness: [m]")
        reservoirThicknessValue_label=ttk.Label(reservoirThicknessFrame, text="Value:")
        reservoirThicknessValue_menu = tk.OptionMenu(reservoirThicknessFrame, componentVars['reservoirThickness']['distriubtion'], *distributionOptions, command=reservoirThicknessDistriubtionChange)
        reservoirThicknessValue_txtField = tk.Entry(reservoirThicknessFrame, textvariable=componentVars['reservoirThickness']['Value'])

        reservoirThicknessValue_menu.config(width=15)
        toolTip.bind(reservoirThicknessValue_menu, 'Select distribution type for Reservoir Thickness.')
        reservoirThickness_label.grid(row=0, column=0, sticky='w', padx=5)
        reservoirThicknessValue_menu.grid(row=0, column=1, sticky='w', padx=5)
        reservoirThicknessValue_label.grid(row=0, column=2, sticky='w', padx=5)
        reservoirThicknessValue_txtField.grid(row=0, column=3, padx=5)
        toolTip.bind(reservoirThicknessValue_txtField, 'Set Thickness of Reservoir.')

        datumFrame = tk.Frame(stratigraphyTab)
        datumFrame.grid(row=5, column=0, padx=15, sticky='w')

        componentVars['datumPressure'] = DoubleVar()
        componentVars['datumPressure'].set(101325)
        datumPressure_label = ttk.Label(datumFrame, width=25, text="Datum Pressure: [Pa]")
        datumPressure_textField = tk.Entry(datumFrame, textvariable=componentVars['datumPressure'])
        datumPressure_label.grid(row=1, column=0, padx=5)
        datumPressure_textField.grid(row=1, column=1, padx=5)
        toolTip.bind(datumPressure_textField, 'Enter pressure value.')

        addComponentTab = ttk.Frame(tabControl, padding=10)
        tabControl.add(addComponentTab, text="Add Components")
        tabControl.pack(expand=1, fill="both")

        addComponent_label = ttk.Label(addComponentTab, text="Add Component:", font=LABEL_FONT)
        addComponent_label.grid(row=0, column=0, sticky='w')

        componentName = StringVar()
        aquiferName = StringVar()
        componentName.set('')

        componentName_label = ttk.Label(addComponentTab, width=25, text="Component Name:")
        controller.componentName_textField = tk.Entry(addComponentTab, textvariable=componentName, width=29)

        componentName_label.grid(row=1, column=0, pady=5)
        controller.componentName_textField.grid(row=1, column=1, pady=5)
        toolTip.bind(controller.componentName_textField, 'Assure you give your component model a Unique Name.')

        controller.componentType = StringVar()
        controller.componentType.set(componentTypes[0])
        componentType_label = ttk.Label(addComponentTab, width=25, text="Model Type:")
        componentType_Menu = tk.OptionMenu(addComponentTab, controller.componentType, *componentTypes, command=componentTypeSetup)

        componentType_Menu.config(width=25)
        toolTip.bind(componentType_Menu, 'Select the type of component model you wish to add to your Simulation.')
        componentType_label.grid(row=2, column=0, pady=5, padx=5)
        componentType_Menu.grid(row=2, column=1, pady=5, padx=5, sticky='ew')

        connection_menu = tk.OptionMenu(addComponentTab, controller.connection, *connections)
        tabControl.connection_menu = connection_menu

        componentsSetupFrame = ttk.Frame(addComponentTab)
        tabControl.componentsSetupFrame = componentsSetupFrame
        componentsSetupFrame.grid(row=3, column=0, padx=20, columnspan=10, sticky='w')

        addComponentButton = ttk.Button(addComponentTab, text="Add Component", command=lambda:controller.add_component(controller.connection, aquiferName, tabControl, componentName, controller.componentType, connection_menu, componentsSetupFrame, controller, pressure, saturation))
        addComponentButton.grid(row=5, column=12, pady=5, padx=5)
        toolTip.bind(addComponentButton, 'When all configuration is finished click to add your component Model and switch to editing it.')

        textDescription = ttk.Label(addComponentTab, text=("""        Add each component model for the system you want to simulate.
        After specifying all component models, save the model and return
        to Dashboard to run the simulation. """), font=LABEL_FONT)
        textDescription.grid(row = 6, column=0, pady=5, padx=5, columnspan=10, sticky='w')

        saveButton = ttk.Button(self, text="Save", command=lambda:controller.populate_dictionary(shaleThickness, aquiferThickness, lookupTableParams, LUTWeights))
        saveButton.pack(side='left', padx=10, pady=5)

        cancelButton = ttk.Button(self, text="Return to Dashboard", command=showDashboard)
        cancelButton.pack(side='right', padx=10, pady=5)

        for key in componentVars:
            try:
                savedDictionary[key]=componentVars[key].get()
            except:
                savedDictionary[key] = {}

                for object in componentVars[key]:
                    savedDictionary[key][object]=componentVars[key][object].get()

        # changes the distribution of the selected aquifer layer
        def AquiferThicknessDistriubtionChange(var):
            key = "aquifer"+str(var+1)+"Thickness"

            for widget in aquiferThickness[key]['frame'].winfo_children():
                widget.destroy()

            if(aquiferThickness[key]['distribution'].get()=='Fixed Value'):
                label = ttk.Label(aquiferThickness[key]['frame'], width=25, text=aquiferThickness[key]['label'])
                Value_label=ttk.Label(aquiferThickness[key]['frame'], text="Value:")
                Value_menu = tk.OptionMenu(aquiferThickness[key]['frame'], aquiferThickness[key]['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(var))
                Value_txtField = tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['value'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Aquifer Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Value_label.grid(row=0, column=2, sticky='w', padx=5)
                Value_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(Value_txtField, 'Set Aquifer Layer Thickness.')

            if(aquiferThickness[key]['distribution'].get()=='Uniform'):
                label = ttk.Label(aquiferThickness[key]['frame'], width=25, text=aquiferThickness[key]['label'])
                Min_label=ttk.Label(aquiferThickness[key]['frame'], text="Minimum:")
                Max_label=ttk.Label(aquiferThickness[key]['frame'], text="Maximum:")
                Value_menu = tk.OptionMenu(aquiferThickness[key]['frame'], aquiferThickness[key]['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(var))
                Min_textField=tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['min'])
                Max_textField=tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['max'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Aquifer Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Min_label.grid(row=0, column=2, sticky='e', padx=5)
                Min_textField.grid(row=0, column=3, sticky='e', padx=5)
                toolTip.bind(Min_textField, 'Set Aquifer Layer Minimum Thickness.')

                Max_label.grid(row=0, column=4, sticky='e', padx=5)
                Max_textField.grid(row=0, column=5, sticky='e', padx=5)
                toolTip.bind(Max_textField, 'Set Aquifer Layer Maximum Thickness.')

            if(aquiferThickness[key]['distribution'].get()=='Normal'):
                label = ttk.Label(aquiferThickness[key]['frame'], width=25, text=aquiferThickness[key]['label'])
                Mean_label=ttk.Label(aquiferThickness[key]['frame'], text="Mean:")
                Std_label=ttk.Label(aquiferThickness[key]['frame'], text="Standard Deviation:")
                Value_menu = tk.OptionMenu(aquiferThickness[key]['frame'], aquiferThickness[key]['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(var))
                Mean_textField = tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['mean'])
                Std_textField = tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['std'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Aquifer Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Mean_label.grid(row=0, column=2, sticky='e', padx=5)
                Mean_textField.grid(row=0, column=3, sticky='e', padx=5)
                toolTip.bind(Mean_textField, 'Set Aquifer Layer Mean Thickness.')

                Std_label.grid(row=0, column=4, sticky='e', padx=5)
                Std_textField.grid(row=0, column=5, sticky='e', padx=5)
                toolTip.bind(Std_textField, 'Set Aquifer Layer Standard Deviation Thickness.')

            if(aquiferThickness[key]['distribution'].get()=='Lognormal'):
                label = ttk.Label(aquiferThickness[key]['frame'], width=25, text=aquiferThickness[key]['label'])
                Mean_label=ttk.Label(aquiferThickness[key]['frame'], text="Sigma:")
                Std_label=ttk.Label(aquiferThickness[key]['frame'], text="ln(mu):")
                Value_menu = tk.OptionMenu(aquiferThickness[key]['frame'], aquiferThickness[key]['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(var))
                Mean_txtField = tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['mean'])
                Std_textField = tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['std'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Aquifer Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Mean_label.grid(row=0, column=2, sticky='e', padx=5)
                Mean_txtField.grid(row=0, column=3, sticky='w', padx=5)
                toolTip.bind(Mean_txtField, 'Set Aquifer Layer Sigma.')

                Std_label.grid(row=0, column=4, sticky='e', padx=5)
                Std_textField.grid(row=0, column=5, sticky='w', padx=5)
                toolTip.bind(Std_textField, 'Set Aquifer Layer ln(mu).')

            if(aquiferThickness[key]['distribution'].get()=='Truncated'):
                label = ttk.Label(aquiferThickness[key]['frame'], width=25, text=aquiferThickness[key]['label'])
                Min_label=ttk.Label(aquiferThickness[key]['frame'], text="Minimum:")
                Max_label=ttk.Label(aquiferThickness[key]['frame'], text="Maximum:")
                Value_menu = tk.OptionMenu(aquiferThickness[key]['frame'], aquiferThickness[key]['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(var))
                Min_textField=tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['min'])
                Max_textField=tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['max'])
                Mean_label=ttk.Label(aquiferThickness[key]['frame'], text="Mean:")
                Std_label=ttk.Label(aquiferThickness[key]['frame'], text="Standard Deviation:")
                Mean_txtField = tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['mean'])
                Std_textField = tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['std'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Aquifer Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Min_label.grid(row=0, column=2, sticky='e', padx=5)
                Min_textField.grid(row=0, column=3, sticky='e', padx=5)
                toolTip.bind(Min_textField, 'Set Aquifer Layer Minimum Thickness.')

                Max_label.grid(row=0, column=4, sticky='e', padx=5)
                Max_textField.grid(row=0, column=5, sticky='e', padx=5)
                toolTip.bind(Max_textField, 'Set Aquifer Layer Maximum Thickness.')

                Mean_label.grid(row=0, column=6, sticky='e', padx=5)
                Mean_txtField.grid(row=0, column=7, sticky='w', padx=5)
                toolTip.bind(Mean_txtField, 'Set Aquifer Layer Mean Thickness.')

                Std_label.grid(row=0, column=8, sticky='e', padx=5)
                Std_textField.grid(row=0, column=9, sticky='w', padx=5)
                toolTip.bind(Std_textField, 'Set Aquifer Layer Standard Deviation.')

            if(aquiferThickness[key]['distribution'].get()=='Triangular'):
                label = ttk.Label(aquiferThickness[key]['frame'], width=25, text=aquiferThickness[key]['label'])
                Value_menu = tk.OptionMenu(aquiferThickness[key]['frame'], aquiferThickness[key]['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(var))
                Min_label=ttk.Label(aquiferThickness[key]['frame'], text="C:")
                Min_textField=tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['min'])
                Max_label=ttk.Label(aquiferThickness[key]['frame'], text="loc:")
                Max_textField=tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['max'])
                Mean_label=ttk.Label(aquiferThickness[key]['frame'], text="Scale:")
                Mean_txtField = tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['std'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Aquifer Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Min_label.grid(row=0, column=3, sticky='e', padx=5)
                Min_textField.grid(row=0, column=4, sticky='e', padx=5)
                toolTip.bind(Min_textField, 'Set Aquifer Layer C.')

                Max_label.grid(row=0, column=5, sticky='e', padx=5)
                Max_textField.grid(row=0, column=6, sticky='e', padx=5)
                toolTip.bind(Max_textField, 'Set Aquifer Layer loc.')

                Mean_label.grid(row=0, column=7, sticky='e', padx=5)
                Mean_txtField.grid(row=0, column=8, sticky='w', padx=5)
                toolTip.bind(Mean_txtField, 'Set Aquifer Layer Scale.')

            if(aquiferThickness[key]['distribution'].get()=='Discrete'):
                label = ttk.Label(aquiferThickness[key]['frame'], width=25, text=aquiferThickness[key]['label'])
                Value_menu = tk.OptionMenu(aquiferThickness[key]['frame'], aquiferThickness[key]['distribution'], *distributionOptions, command=lambda _:AquiferThicknessDistriubtionChange(var))
                Min_label=ttk.Label(aquiferThickness[key]['frame'], text="Values:")
                Min_textField=tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['values'])
                Max_label=ttk.Label(aquiferThickness[key]['frame'], text="Weights:")
                Max_textField=tk.Entry(aquiferThickness[key]['frame'], textvariable=aquiferThickness[key]['weights'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Aquifer Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Min_label.grid(row=0, column=2, sticky='e', padx=5)
                Min_textField.grid(row=0, column=3, sticky='e', padx=5)
                toolTip.bind(Min_textField, 'Set Aquifer Layer Thickness Values.')

                Max_label.grid(row=0, column=4, sticky='e', padx=5)
                Max_textField.grid(row=0, column=5, sticky='e', padx=5)
                toolTip.bind(Max_textField, 'Set Aquifer Layer Thickness Weights.')

        # changes the distribution of the selected shale layer
        def ShaleThicknessDistriubtionChange(var):
            key = "shale"+str(var+1)+"Thickness"

            for widget in shaleThickness[key]['frame'].winfo_children():
                widget.destroy()

            if(shaleThickness[key]['distribution'].get()=='Fixed Value'):
                label = ttk.Label(shaleThickness[key]['frame'], width=25, text=shaleThickness[key]['label'])
                Value_label=ttk.Label(shaleThickness[key]['frame'], text="Value:")
                Value_menu = tk.OptionMenu(shaleThickness[key]['frame'], shaleThickness[key]['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(var))
                Value_txtField = tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['value'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Shale Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Value_label.grid(row=0, column=2, sticky='w', padx=5)
                Value_txtField.grid(row=0, column=3, padx=5)
                toolTip.bind(Value_txtField, 'Set Shale Layer Thickness.')

            if(shaleThickness[key]['distribution'].get()=='Uniform'):
                label = ttk.Label(shaleThickness[key]['frame'], width=25, text=shaleThickness[key]['label'])
                Min_label=ttk.Label(shaleThickness[key]['frame'], text="Minimum:")
                Max_label=ttk.Label(shaleThickness[key]['frame'], text="Maximum:")
                Value_menu = tk.OptionMenu(shaleThickness[key]['frame'], shaleThickness[key]['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(var))
                Min_textField=tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['min'])
                Max_textField=tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['max'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Shale Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Min_label.grid(row=0, column=2, sticky='e', padx=5)
                Min_textField.grid(row=0, column=3, sticky='e', padx=5)
                toolTip.bind(Min_textField, 'Set Shale Layer Minimum Thickness.')

                Max_label.grid(row=0, column=4, sticky='e', padx=5)
                Max_textField.grid(row=0, column=5, sticky='e', padx=5)
                toolTip.bind(Max_textField, 'Set Shale Layer Maximum Thickness.')

            if(shaleThickness[key]['distribution'].get()=='Normal'):
                label = ttk.Label(shaleThickness[key]['frame'], width=25, text=shaleThickness[key]['label'])
                Mean_label=ttk.Label(shaleThickness[key]['frame'], text="Mean:")
                Std_label=ttk.Label(shaleThickness[key]['frame'], text="Standard Deviation:")
                Value_menu = tk.OptionMenu(shaleThickness[key]['frame'], shaleThickness[key]['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(var))
                Mean_textField = tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['mean'])
                Std_textField = tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['std'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Shale Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Mean_label.grid(row=0, column=2, sticky='e', padx=5)
                Mean_textField.grid(row=0, column=3, sticky='e', padx=5)
                toolTip.bind(Mean_textField, 'Set Shale Layer Mean Thickness.')

                Std_label.grid(row=0, column=4, sticky='e', padx=5)
                Std_textField.grid(row=0, column=5, sticky='e', padx=5)
                toolTip.bind(Std_textField, 'Set Shale Layer Standard Deviation.')

            if(shaleThickness[key]['distribution'].get()=='Lognormal'):
                label = ttk.Label(shaleThickness[key]['frame'], width=25, text=shaleThickness[key]['label'])
                Mean_label=ttk.Label(shaleThickness[key]['frame'], text="Sigma:")
                Std_label=ttk.Label(shaleThickness[key]['frame'], text="ln(mu):")
                Value_menu = tk.OptionMenu(shaleThickness[key]['frame'], shaleThickness[key]['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(var))
                Mean_txtField = tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['mean'])
                Std_textField = tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['std'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Shale Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Mean_label.grid(row=0, column=2, sticky='e', padx=5)
                Mean_txtField.grid(row=0, column=3, sticky='w', padx=5)
                toolTip.bind(Mean_txtField, 'Set Shale Layer Sigma.')

                Std_label.grid(row=0, column=4, sticky='e', padx=5)
                Std_textField.grid(row=0, column=5, sticky='w', padx=5)
                toolTip.bind(Std_textField, 'Set Shale Layer ln(mu).')

            if(shaleThickness[key]['distribution'].get()=='Truncated'):
                label = ttk.Label(shaleThickness[key]['frame'], width=25, text=shaleThickness[key]['label'])
                Min_label=ttk.Label(shaleThickness[key]['frame'], text="Minimum:")
                Max_label=ttk.Label(shaleThickness[key]['frame'], text="Maximum:")
                Value_menu = tk.OptionMenu(shaleThickness[key]['frame'], shaleThickness[key]['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(var))
                Min_textField=tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['min'])
                Max_textField=tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['max'])
                Mean_label=ttk.Label(shaleThickness[key]['frame'], text="Mean:")
                Std_label=ttk.Label(shaleThickness[key]['frame'], text="Standard Deviation:")
                Mean_txtField = tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['mean'])
                Std_textField = tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['std'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Shale Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Min_label.grid(row=0, column=2, sticky='e', padx=5)
                Min_textField.grid(row=0, column=3, sticky='e', padx=5)
                toolTip.bind(Min_textField, 'Set Shale Layer Minimum Thickness.')

                Max_label.grid(row=0, column=4, sticky='e', padx=5)
                Max_textField.grid(row=0, column=5, sticky='e', padx=5)
                toolTip.bind(Max_textField, 'Set Shale Layer Maximum Thickness.')

                Mean_label.grid(row=0, column=6, sticky='e', padx=5)
                Mean_txtField.grid(row=0, column=7, sticky='w', padx=5)
                toolTip.bind(Mean_txtField, 'Set Shale Layer Mean Thickness.')

                Std_label.grid(row=0, column=8, sticky='e', padx=5)
                Std_textField.grid(row=0, column=9, sticky='w', padx=5)
                toolTip.bind(Std_textField, 'Set Shale Layer Standard Deviation.')

            if(shaleThickness[key]['distribution'].get()=='Triangular'):
                label = ttk.Label(shaleThickness[key]['frame'], width=25, text=shaleThickness[key]['label'])
                Value_menu = tk.OptionMenu(shaleThickness[key]['frame'], shaleThickness[key]['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(var))
                Min_label=ttk.Label(shaleThickness[key]['frame'], text="C:")
                Min_textField=tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['min'])
                Max_label=ttk.Label(shaleThickness[key]['frame'], text="loc:")
                Max_textField=tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['max'])
                Mean_label=ttk.Label(shaleThickness[key]['frame'], text="Scale:")
                Mean_txtField = tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['std'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Shale Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Min_label.grid(row=0, column=3, sticky='e', padx=5)
                Min_textField.grid(row=0, column=4, sticky='e', padx=5)
                toolTip.bind(Min_textField, 'Set Shale Layer C.')

                Max_label.grid(row=0, column=5, sticky='e', padx=5)
                Max_textField.grid(row=0, column=6, sticky='e', padx=5)
                toolTip.bind(Max_textField, 'Set Shale Layer loc.')

                Mean_label.grid(row=0, column=7, sticky='e', padx=5)
                Mean_txtField.grid(row=0, column=8, sticky='w', padx=5)
                toolTip.bind(Mean_txtField, 'Set Shale Layer Scale.')

            if(shaleThickness[key]['distribution'].get()=='Discrete'):
                label = ttk.Label(shaleThickness[key]['frame'], width=25, text=shaleThickness[key]['label'])
                Value_menu = tk.OptionMenu(shaleThickness[key]['frame'], shaleThickness[key]['distribution'], *distributionOptions, command=lambda _:ShaleThicknessDistriubtionChange(var))
                Min_label=ttk.Label(shaleThickness[key]['frame'], text="Values:")
                Min_textField=tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['values'])
                Max_label=ttk.Label(shaleThickness[key]['frame'], text="Weights:")
                Max_textField=tk.Entry(shaleThickness[key]['frame'], textvariable=shaleThickness[key]['weights'])

                Value_menu.config(width=15)
                toolTip.bind(Value_menu,'Select Shale Layer Distriubtion Type.')

                label.grid(row=0, column=0, sticky='w', padx=5)
                Value_menu.grid(row=0, column=1, sticky='ew', padx=5)
                Min_label.grid(row=0, column=2, sticky='e', padx=5)
                Min_textField.grid(row=0, column=3, sticky='e', padx=5)
                toolTip.bind(Min_textField, 'Set Shale Layer Thickness Values.')

                Max_label.grid(row=0, column=4, sticky='e', padx=5)
                Max_textField.grid(row=0, column=5, sticky='e', padx=5)
                toolTip.bind(Max_textField, 'Set Shale Layer Thickness Weights.')
