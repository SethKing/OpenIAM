# Pwm is imported to allow hover text
import Pmw, sys, os

sys.path.append(os.path.join('../..','source'))

from dictionarydata import d
from dictionarydata import processingTypes
from dictionarydata import plotTypes
from dictionarydata import LABEL_FONT
from dictionarydata import DASHBOARD_FONT

from openiam.visualize.iam_post_processor import IAM_Post_Processor

try:
    # Python2
    import Tkinter as tk
    from Tkinter import ttk
    from Tkinter import StringVar
    from Tkinter import BooleanVar
    from Tkinter import IntVar
    from Tkinter import tkMessageBox as messagebox
except ImportError:
    # Python3
    import tkinter as tk
    from tkinter import ttk
    from tkinter import StringVar
    from tkinter import BooleanVar
    from tkinter import IntVar
    from tkinter import messagebox

class PostPocessor_Page(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        # Setup all variables needed for the post processing interface
        processingType = StringVar()
        plotType = StringVar()
        titleText = StringVar()
        filename = StringVar()
        subplot = BooleanVar()
        ncols = IntVar()
        simType = StringVar()
        sensitivies = IntVar()
        capturePoint = IntVar()
        outputVars = []

        processingType.set(processingTypes[0])
        plotType.set(plotTypes[0])
        titleText.set('')
        filename.set('')
        subplot.set(False)
        ncols.set(1)
        simType.set('Pearson')
        sensitivies.set(5)

        toolTip = Pmw.Balloon(parent)
        setupFrame = ttk.Frame(self)

        # Based on the processing type chosen the panel will be filled with
        # different Input parts
        def changeProcessingType(type):
            for widget in setupFrame.winfo_children():
                widget.destroy()

            chckbox = []

            processing_label = ttk.Label(self, text='Processing:', width=25)
            processing_label.grid(row=2, column=1, padx=5, pady=5)

            processing_txtField = tk.OptionMenu(self, processingType, *processingTypes, command=changeProcessingType)
            processing_txtField.grid(row=2, column=2, padx=5, pady=5, sticky='ew')
            toolTip.bind(processing_txtField, 'The only processing type available is Plotting.')

            title_label = ttk.Label(setupFrame, text='Title:', width=25)
            title_label.grid(row=4, column=0, padx=5, pady=5)

            title_txtField = tk.Entry(setupFrame, textvariable = titleText)
            title_txtField.grid(row=4, column=1, padx=5, pady=5, sticky='ew')
            toolTip.bind(title_txtField, 'Enter the title of the plot here.')

            filename_label = ttk.Label(setupFrame, text='Filename:', width=25)
            filename_label.grid(row=5, column=0, padx=5, pady=5)

            filename_txtFiled = tk.Entry(setupFrame, textvariable=filename)
            filename_txtFiled.grid(row=5, column=1, padx=5, pady=5, sticky='ew')
            toolTip.bind(filename_txtFiled, 'Enter the file name you wish to use to save the plot.  This file will be saved in the current output directory.')

            observations_frame = ttk.Frame(setupFrame)
            observations_frame.grid(row=6, column=0, padx=5, pady=5, columnspan=10)

            post_processing_plot_button = ttk.Button(observations_frame, text='Plot', command=lambda :plotSimulation(chckbox))
            post_processing_plot_button.grid(row=9, column=1, pady=15, padx=5)
            toolTip.bind(post_processing_plot_button, 'Create plots for Selected simulation.')

            if(type == 'Plotting'):
                observations=ttk.Label(observations_frame, text='Observations:', width=25)
                observations.grid(row=0, column=0, sticky='w')

                i = 1
                index = 0
                row = 1

                for output in ipp.outputs:
                    outputVar = BooleanVar()
                    outputVar.set(0)
                    outputVars.append(outputVar)

                    checkbox = ttk.Checkbutton(observations_frame, text=output, variable=outputVars[index])
                    checkbox.grid(row=row, column=i, sticky='w', pady=5, padx=5)
                    chckbox.append(checkbox)

                    i = i + 1
                    index = index + 1

                    if(i >= 4):
                        row = row + 1
                        i = 1

                plot_label = ttk.Label(setupFrame, text='Plot Type:', width=25)
                plot_label.grid(row=3, column=0, padx=5, pady=5)

                plot_menu = tk.OptionMenu(setupFrame, plotType, *plotTypes)
                plot_menu.grid(row=3, column=1, padx=5, pady=5, sticky = 'ew')
                toolTip.bind(plot_menu, 'toolTipText')

                subPlots_label=ttk.Label(setupFrame, text='Use Suplots:', width=25)
                subPlots_label.grid(row=7, column=0, padx=5, pady=5)

                subPlots_chckBox = ttk.Checkbutton(setupFrame, variable=subplot)
                subPlots_chckBox.grid(row=7, column=1, padx=5, pady=5, sticky='w')
                toolTip.bind(subPlots_chckBox, 'Check if you would like to see more than one plot per file.')

                ncols_label = ttk.Label(setupFrame, text='Number of Columns:', width=25)
                ncols_label.grid(row=8, column=0, padx=5, pady=5)

                ncols_spin = tk.Spinbox(setupFrame, from_=1, to=10, textvariable=ncols)
                ncols_spin.grid(row=8, column=1, padx=5, pady=5, sticky='ew')
                toolTip.bind(ncols_spin, 'Enter the number of columns you would like your plots to be seperated into.')

                if(ipp.analysis_type == 'LHS' or ipp.analysis_type == 'lhs'):
                    toolTip.bind(processing_txtField, 'Select the type of processing you wish to do.')
                    return

            if(type != 'Plotting'):
                capturePoint.set(len(ipp.time_array)-1)
                capturepoint_label=ttk.Label(setupFrame, text="Capture Point:", width=20)
                capturepoint_label.grid(row=0, column=0, padx=5, pady=5, sticky='w')

                capturepoint_spin = tk.Spinbox(setupFrame, from_=0, to=len(ipp.time_array)-1, textvariable=capturePoint)
                capturepoint_spin.grid(row=0, column=1, padx=5, pady=5, sticky='ew')
                toolTip.bind(capturepoint_spin, 'Select the time Step you wish to plot.')

            if(type == 'Correlation Coefficients'):
                capturepoint_label=ttk.Label(setupFrame, text="Type:", width=20)
                capturepoint_label.grid(row=1, column=0, padx=5, pady=5, sticky='w')

                type_menu = tk.OptionMenu(setupFrame, simType, *['Pearson', 'Spearman'])
                type_menu.grid(row=1, column=1, padx=5, pady=5, sticky='ew')
                toolTip.bind(type_menu, 'Select the time Type.')

                excludes=ttk.Label(observations_frame, text='Excludes:', width=25)
                excludes.grid(row=0, column=0, sticky='w')

                i = 1
                index = 0
                row = 1

                for output in ipp.outputs:
                    outputVar = BooleanVar()
                    outputVar.set(0)
                    outputVars.append(outputVar)

                    checkbox = ttk.Checkbutton(observations_frame, text=output, variable=outputVars[index])
                    checkbox.grid(row=row, column=i, sticky='w', pady=5, padx=5)
                    chckbox.append(checkbox)

                    i = i + 1
                    index = index + 1

                    if(i >= 4):
                        row = row + 1
                        i = 1

            if(type.find('Sensitivity')!=-1):
                outputs=ttk.Label(observations_frame, text='Outputs:', width=25)
                outputs.grid(row=0, column=0, sticky='w')

                i = 1
                index = 0
                row = 1

                for output in ipp.outputs:
                    outputVar = BooleanVar()
                    outputVar.set(0)
                    outputVars.append(outputVar)

                    checkbox = ttk.Checkbutton(observations_frame, text=output, variable=outputVars[index])
                    checkbox.grid(row=row, column=i, sticky='w', pady=5, padx=5)
                    chckbox.append(checkbox)

                    i = i + 1
                    index = index + 1

                    if(i >= 4):
                        row = row + 1
                        i = 1

            if(type=='Multi Sensitivity Coefficients'):
                sensitvities_label=ttk.Label(setupFrame, text="Number of sensitivies:", width=20)
                sensitvities_label.grid(row=3, column=0, padx=5, pady=5, sticky='w')

                sensitvities_spin = tk.Spinbox(setupFrame, from_=1, to=100, textvariable=sensitivies)
                sensitvities_spin.grid(row=3, column=1, padx=5, pady=5, sticky='ew')
                toolTip.bind(sensitvities_spin, 'Select the number of sensitivies you wish to plot.')

        # Open sim allows the user to select a previously run simulation and
        # get back a list of possible post processing plots as well as stats
        def openSim():
            for widget in setupFrame.winfo_children():
                widget.destroy()

            from tkinter.filedialog import askdirectory
            fileDialog = tk.Tk()
            fileDialog.withdraw()

            processingTypes = []

            try:
                dirname = askdirectory(initialdir=simulation.get(), title="Choose Directory containing Outputs.")
            except:
                fileDialog.destroy()
            else:
                if(dirname!=''):
                    simulation.set(dirname)

                fileDialog.destroy()

            global ipp
            try:
                ipp = IAM_Post_Processor(dirname)
            except:
                messagebox.showerror('Error', 'The directory you have selected is not a valid Open IAM output Directory.')
                return

            if(ipp.analysis_type == 'Parstudy' or ipp.analysis_type == 'LHS' or ipp.analysis_type == 'lhs'):
                toolTipText = 'Choose Plot type you wish to use.'
                if(len(plotTypes) == 1):
                    plotTypes.append('Time Series Stats')
                    plotTypes.append('Time Series and Stats')

                if(ipp.atm_cmp):
                    if(len(plotTypes) == 3):
                        plotTypes.append('Atm Plume Single')
                        plotTypes.append('Atm Plume Ensemble')

            processingType.set('Plotting')
            add_post_processing_types()
            changeProcessingType('Plotting')

        # Based on what outputs were selected in the opened simulation
        # this will add different options of plots and stats
        def add_post_processing_types():
            toolTipText = 'The only Plot type available is Time Series.'
            setupFrame.grid(row=3, column=1, padx=5, pady=5, columnspan=20)

            processing_label = ttk.Label(self, text='Processing:', width=25)
            processing_label.grid(row=2, column=1, padx=5, pady=5)

            processing_txtField = tk.OptionMenu(self, processingType, *processingTypes, command=changeProcessingType)
            processing_txtField.grid(row=2, column=2, padx=5, pady=5, sticky='ew')
            toolTip.bind(processing_txtField, 'The only processing type available is Plotting.')

            plot_label = ttk.Label(setupFrame, text='Plot Type:', width=25)
            plot_label.grid(row=3, column=0, padx=5, pady=5)

            plot_menu = tk.OptionMenu(setupFrame, plotType, *plotTypes)
            plot_menu.grid(row=3, column=1, padx=5, pady=5, sticky = 'ew')
            toolTip.bind(plot_menu, toolTipText)

            title_label = ttk.Label(setupFrame, text='Title:', width=25)
            title_label.grid(row=4, column=0, padx=5, pady=5)

            title_txtField = tk.Entry(setupFrame, textvariable = titleText)
            title_txtField.grid(row=4, column=1, padx=5, pady=5, sticky='ew')
            toolTip.bind(title_txtField, 'Enter the title of the plot here.')

            filename_label = ttk.Label(setupFrame, text='Filename:', width=25)
            filename_label.grid(row=5, column=0, padx=5, pady=5)

            filename_txtFiled = tk.Entry(setupFrame, textvariable=filename)
            filename_txtFiled.grid(row=5, column=1, padx=5, pady=5, sticky='ew')
            toolTip.bind(filename_txtFiled, 'Enter the file name you wish to use to save the plot.  This file will be saved in the current output directory.')
            observations_frame = ttk.Frame(setupFrame)
            observations_frame.grid(row=6, column=0, padx=5, pady=5, columnspan=10)

            observations=ttk.Label(observations_frame, text='Observations:', width=15)
            observations.grid(row=0, column=0, sticky='w', pady=5, padx=5)

            subPlots_label=ttk.Label(setupFrame, text='Use Suplots:', width=25)
            subPlots_label.grid(row=7, column=0, padx=5, pady=5)

            subPlots_chckBox = ttk.Checkbutton(setupFrame, variable=subplot)
            subPlots_chckBox.grid(row=7, column=1, padx=5, pady=5, sticky='w')
            toolTip.bind(subPlots_chckBox, 'Check if you would like to see more than one plot per file.')

            ncols_label = ttk.Label(setupFrame, text='Number of Columns:', width=25)
            ncols_label.grid(row=8, column=0, padx=5, pady=5)

            ncols_spin = tk.Spinbox(setupFrame, from_=1, to=10, textvariable=ncols)
            ncols_spin.grid(row=8, column=1, padx=5, pady=5, sticky='ew')
            toolTip.bind(ncols_spin, 'Enter the number of columns you would like your plots to be seperated into.')

            post_processing_plot_button = ttk.Button(setupFrame, text='Plot', command=lambda :plotSimulation(chckbox))
            post_processing_plot_button.grid(row=9, column=1, pady=15, padx=5)
            toolTip.bind(post_processing_plot_button, 'Create plots for Selected simulation.')

            if(ipp.analysis_type == 'LHS' or ipp.analysis_type == 'lhs' and len(processingTypes) <= 1):
                processingTypes.append('Correlation Coefficients')
                processingTypes.append('Sensitivity Coefficients')
                processingTypes.append('Multi Sensitivity Coefficients')
                processingTypes.append('Time Series Sensitivity')
                toolTip.bind(processing_txtField, 'Select the type of processing you wish to do.')
                return

        def plotSimulation(chckbox):
            outputs = []

            if(titleText.get() == ''):
                messagebox.showerror("Error", "You must enter a Title for the Plot.")
                return
            if(filename.get() == ''):
                messagebox.showinfo("warning", "You will need to enter a file name to save the image directly.")
                return

            for box in chckbox:
                if(box.instate(['selected'])):
                    outputs.append(box['text'])

            if(processingType.get() == 'Plotting'):
                if(plotType.get() == 'Time Series'):
                    ipp.plotter(outputs, 'TimeSeries', title=titleText.get(), subplot={'use':subplot.get(), 'ncols':ncols.get()}, savefig=filename.get())
                    ipp.plotter(outputs, 'TimeSeries', title=titleText.get(), subplot={'use':subplot.get(), 'ncols':ncols.get()})
                if(plotType.get() == 'Time Series Stats'):
                    ipp.plotter(outputs, 'TimeSeriesStats', title=titleText.get(), subplot={'use':subplot.get(), 'ncols':ncols.get()}, savefig=filename.get())
                    ipp.plotter(outputs, 'TimeSeriesStats', title=titleText.get(), subplot={'use':subplot.get(), 'ncols':ncols.get()})
                if(plotType.get() == 'Time Series and Stats'):
                    ipp.plotter(outputs, 'TimeSeriesAndStats', title=titleText.get(), subplot={'use':subplot.get(), 'ncols':ncols.get()}, savefig=filename.get())
                    ipp.plotter(outputs, 'TimeSeriesAndStats', title=titleText.get(), subplot={'use':subplot.get(), 'ncols':ncols.get()})
                if(plotType.get() == 'Atm Plume Single'):
                    figName = 'atmplumes_single_plots/'+filename.get()+'_{time_index}.png'
                    ipp.plotter(outputs, 'AtmPlumeSingle', savefig=figName)
                if(plotType.get() == 'Atm Plume Ensemble'):
                    figName = 'atmplumes_ensemble_plots/'+filename.get()+'_{time_index}.png'
                    ipp.plotter(outputs, 'AtmPlumeEnsemble', savefig=figName)

            if(processingType.get() == 'Correlation Coefficients'):
                ipp.sensitivity_analysis('CorrelationCoeff', outputs, {'savefig': filename.get()+'.png', 'capture_point':capturePoint.get()})
            if(processingType.get() == 'Sensitivity Coefficients'):
                ipp.sensitivity_analysis('SensitivityCoeff', outputs, {'savefig': filename.get()+'.png', 'capture_point':capturePoint.get()})
            if(processingType.get() == 'Multiple Sensitivity Coefficients'):
                ipp.sensitivity_analysis('MultiSensitivities', outputs, {'savefig': filename.get()+'.png', 'capture_point':capturePoint.get()})
            if(processingType.get() == 'Time Series Sensitivity'):
                ipp.sensitivity_analysis('TimeSeriesSensitivity', outputs, {'savefig': filename.get()+'.png', 'capture_point':capturePoint.get()})

        header = ttk.Label(self, text='Post Processor', font=LABEL_FONT)
        header.grid(row=0, column=3, padx=5, pady=5)

        selection_label = ttk.Label(self, text='Folder:', width=30)
        selection_label.grid(row=1, column=0, padx=5, pady=5)

        simulation = StringVar()
        simulation.set('/media/joel/Data/NRAP/openiam-a0-5-2')

        selection_txtField = tk.Entry(self, textvariable=simulation)
        selection_txtField.grid(row=1, column=1, padx=5, pady=5)
        toolTip.bind(selection_txtField, 'Enter the output directory of the simulation you wish to use for Post Processing.')

        selection_button = ttk.Button(self, text='Browse', command=openSim)
        selection_button.grid(row=1, column=2, padx=5, pady=5, sticky='ew')
        toolTip.bind(selection_button, 'Click to Browse for directory of the simulation you wish to use for Post Processing.')

        returnButton = ttk.Button(self, text="Return to Dashboard", command=controller.showDashboard)
        returnButton.grid(row=12, column=2, pady=15, padx=5)
        toolTip.bind(returnButton, 'Clicking this Acknowledges that you understand the Disclaimer and Accept it.')
