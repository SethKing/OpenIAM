# Pmw allows for hover text
# webbrowser allows me to open a webpage link
# pickle is used to write binary files and read binary files
import pickle, Pmw, webbrowser

from OpenIAM_Page import OpenIAM_Page
from PostPocessor_Page import PostPocessor_Page
from dictionarydata import d
from dictionarydata import LABEL_FONT
from dictionarydata import DASHBOARD_FONT

try:
    # Python2
    import Tkinter as tk
    from Tkinter import ttk
    from Tkinter import tkMessageBox
    from Tkinter import StringVar
    import tkFont
except ImportError:
    # Python3
    import tkinter as tk
    from tkinter import ttk
    from tkinter import messagebox
    from tkinter import StringVar

class Dashboard_Page(tk.Frame):
    def __init__(self, parent, controller):
        self.root = parent
        tk.Frame.__init__(self, parent)
        toolTip = Pmw.Balloon(parent)

        dict={}

        # This method links to the manual on Edx's website for uses to download
        def showManual():
            new=2

            url="http://google.com"
            webbrowser.open(url, new=new)

        # This method creates a popup window to display all references
        def showReferences():
            messagebox.showinfo("References", 'Fill in References Here')

        # This method creates a popup window to display all acknowledgements
        def showAcknowledgements():
            messagebox.showinfo("Acknowledgements", "Fill in AcknowledgeMents Here")

        # This method is used to reload a saved simulation from a binary file
        # utilizing the pickle package
        def openSim():
            from tkinter.filedialog import askopenfilename
            fileDialog = tk.Tk()
            fileDialog.withdraw()

            # First the open file dialog will appear and show only open iam files
            # this try will catch any error with the opening of the file
            # if an error occurs the file dialog is destroyed else the file is
            # read into a dictionary and passed to the dictiondata file to be
            # a globally accessible file.
            try:
                parametersfilename = askopenfilename(initialdir='.', title="Choose Simulation File",filetypes=[("Open IAM Files", "*.OpenIAM")])
            except:
                fileDialog.destroy()
            else:
                if parametersfilename:
                    # This try only happens if the file name selected is not
                    # blank if any error with the file read or the dictionary
                    # load happens then the error message is displayed and the
                    # file dialog is destroyed
                    try:
                        with open(parametersfilename, 'rb') as file:
                            dict=pickle.load(file)
                    except:
                        messagebox.showerror('Error with File', 'The file you are attempting to open has a problem.')

                    controller.load_sim(dict)
                fileDialog.destroy()

        tool_label = tk.Label(self, padx=5, pady=5, text=('NRAP-IAM-CS v2 - Main Page'), font=LABEL_FONT, bg="light blue")
        tool_label.grid(row=0, column=0, columnspan=2, sticky='ew')

        left_frame = tk.Frame(self, borderwidth=2)
        left_frame.grid(row=1, column=0, sticky='w')

        model_frame=tk.Frame(left_frame, borderwidth=2)
        model_frame.grid(row=1, column=0)

        buttons_frame = ttk.Frame(model_frame, borderwidth=2)
        buttons_frame.pack(side='top')

        enter_params_button = ttk.Button(buttons_frame, text="Enter Parameters", command=lambda:controller.show_frame(OpenIAM_Page))
        enter_params_button.grid(row=1, column=0, padx=5)
        toolTip.bind(enter_params_button, 'Begin Entering Parameters')

        open_saved_sim_button = ttk.Button(buttons_frame, text="Load Simulation", command=openSim)
        open_saved_sim_button.grid(row=1, column=1, padx=5)
        toolTip.bind(open_saved_sim_button, 'Browse to open existing simulation file.')

        post_processing_button = ttk.Button(buttons_frame, text='Post Processing', command=lambda:controller.show_frame(PostPocessor_Page))
        post_processing_button.grid(row=1, column=2, padx=5)
        toolTip.bind(post_processing_button, 'Begin Post Processing to graph results.')

        description_label = tk.Label(model_frame, padx=5, pady=5, text=(
        """NRAP-IAM-CS v2 is an open-source Integrated Assessment Model (IAM)
        for phase II of the National Risk Assessment Partnership (NRAP).
        The goal of this software is to go beyond risk assessment into
        risk management and containment assurance."""))
        description_label.pack(side = 'bottom')

        runStyle = ttk.Style()
        runStyle.configure('Bold.TButton', font=('Times', '20', 'bold'), background='light blue')

        runSim_button = ttk.Button(left_frame, text='RUN SIMULATION', width=30, style='Bold.TButton', command=controller.run_simulation)
        runSim_button.grid(row=2, column=0)

        logo_frame = tk.Frame(left_frame, borderwidth=2)
        logo_frame.grid(row=3, column=0)

        self.nrap_logo = tk.PhotoImage(file="images/NRAP_logo.png")
        nrap_canvas = tk.Canvas(logo_frame)
        nrap_canvas.configure(width=525, height=200)
        nrap_canvas.create_image(250,100, image=self.nrap_logo)
        nrap_canvas.grid(row=0, column=0, columnspan=5)

        self.pacificnorthwest_logo = tk.PhotoImage(file="images/DOE-LABS_NRAP.gif")
        pacificnorthwest_canvas = tk.Canvas(logo_frame)
        pacificnorthwest_canvas.configure(width=900, height=90)
        pacificnorthwest_canvas.create_image(435, 50, image=self.pacificnorthwest_logo)
        pacificnorthwest_canvas.grid(row=1, column=0)

        right_frame=tk.Frame(self, borderwidth=2)
        right_frame.grid(row=1, column=1)

        self.well_logo = tk.PhotoImage(file="images/wellbore.gif")
        well_canvas = tk.Canvas(right_frame)
        well_canvas.configure(width=250, height=250)
        well_canvas.create_image(0, 0, anchor='nw', image=self.well_logo)
        well_canvas.grid(row=0, column=0)

        label_frame = tk.Frame(right_frame)
        label_frame.grid(row=1, column=0)

        version_label = ttk.Label(label_frame, text=('Version: 2018-10-a0.5.2'))
        version_label.grid(row=0, column=0, padx=5, pady=5, sticky='w')
        toolTip.bind(version_label, 'Software Version')

        contact_label = tk.Label(label_frame, padx=5, pady=5, text=('Main Contact: Seth King'))
        contact_label.grid(row=1, column=0, sticky='w')
        toolTip.bind(contact_label, 'Name of Main Contact for Project')

        email_label = tk.Label(label_frame, padx=5, pady=5, text=('E-mail: seth.king@netl.doe.gov'))
        email_label.grid(row=2, column=0, sticky='w')
        toolTip.bind(email_label, 'E-Mail of Main Contact for Project')

        acknowledge_button = ttk.Button(label_frame, text='Acknowledgements', command=showAcknowledgements)
        acknowledge_button.grid(row=3, column=0, sticky='w')
        toolTip.bind(acknowledge_button, 'Link to Acknowledgements')

        reference_button = ttk.Button(label_frame, text='References', command=showReferences)
        reference_button.grid(row=4, column=0, sticky='w')
        toolTip.bind(reference_button, 'Link to References')

        usermanual_button = ttk.Button(label_frame, text='User Manual', command=showManual)
        usermanual_button.grid(row=5, column=0, sticky='w')
        toolTip.bind(usermanual_button, 'Link to User Manual')
