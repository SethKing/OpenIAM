# Pmw allows for hover text
import Pmw

from Dashboard import Dashboard_Page
from dictionarydata import LABEL_FONT
from dictionarydata import DASHBOARD_FONT

try:
    # Python2
    import Tkinter as tk
    from Tkinter import ttk
except ImportError:
    # Python3
    import tkinter as tk
    from tkinter import ttk

class Disclaimer_Page(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        toolTip = Pmw.Balloon(parent)

        self.grid_columnconfigure(1, weight=1)

        notice_label = ttk.Label(self, text=('NOTICE TO USERS:'), font=LABEL_FONT)
        notice_label.grid(row=0, column=0)

        disclaimer_label = ttk.Label(self, text=(
        """        Neither the United States Government, nor any of their employees, make any warranty, express or implied, or assumes any legal liablility
        or responsibility for the accuracy, completeness, or usefulness of any information, apparatus, product, or process disclosed, or represents
        that its use would not infringe privately owned rights. Reference herein to any specific commercial product, process, or service by
        trade name, trademark, manufacturer, or otherwise does not necessarily constitute or impoly its endorsement, recommendation, or favoring
        by the United States Government or any agency thereof.  The views and opinions of authers expressed herein do not necessarily state or
        reflect those of the United States Government or any agency thereof."""))
        disclaimer_label.grid(row=1, column=0, pady=5)

        okButton = ttk.Button(self, text="OK", command=lambda: controller.show_frame(Dashboard_Page))
        okButton.grid(row=2, column=0, pady=15)
        toolTip.bind(okButton, 'Clicking this Acknowledges that you understand the Disclaimer and Accept it.')
