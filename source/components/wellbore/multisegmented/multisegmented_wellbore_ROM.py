"""
The module contains the solution class for the multisegmented_well_model.

The solution is based on the CeliaNordbottenCourtDobossy2011 paper
"Field-scale application of a semi-analytical model
for estimation of CO2 and brine leakage along old wells"

Author: Veronika S. Vasylkivska
Date: 06/25/2015
"""
import logging
import os, sys
import numpy as np
import scipy.misc as scm
from scipy.interpolate import interp1d
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from multisegmented import units


class Parameters(object):
    def __init__(self):
        self.numberOfShaleLayers = None

        self.shaleThickness = None
        self.aquiferThickness = None
        self.reservoirThickness = None

        # Permeability
        self.shalePermeability = None
        self.aquiferPermeability = None

        # Land surface pressure
        self.datumPressure = None

        # Input pressure, CO2 saturation and time points
        self.pressure = None
        self.CO2saturation = None
        self.timePoint = None

        # Amount of CO2 present in aquifers before simulation
        self.prevCO2Mass = None

        # Time step in days
        self.timeStep = None

        # Density
        self.brineDensity = None
        self.brineDensityForm = "Constant"     # constant or functional
        self.CO2Density = None
        self.CO2DensityForm = "Constant"       # constant or functional

        # Viscosity
        self.brineViscosity = None
        self.brineViscosityForm = "Constant"     # constant or functional
        self.CO2Viscosity = None
        self.CO2ViscosityForm = "Constant"       # constant or functional

        # Residual saturation and compressibility
        self.brineResidualSaturation = None
        self.compressibility = None

        # Well radius
        self.wellRadius = None
        self.flowArea = None


class Solution(object):

    def __init__(self, parameters):
        """ Create an instance of the Solution class. """

        # Setup the parameters that define a solution
        self.parameters = Parameters()
        self.parameters = parameters

        # Reserve a space for the solution pieces
        self.CO2LeakageRates = None
        self.brineLeakageRates = None
        self.g = 9.80665  # acceleration due to gravity

    def get_depth_for_pressure(self):
        """ Calculate depth of all layers for the pressure calculations. """
        # The deepest shale has the first thickness in the array
        shaleThickness = self.parameters.shaleThickness
        aquiferThickness = self.parameters.aquiferThickness
        reservoirThickness = self.parameters.reservoirThickness

        # Get the sum of shale layers thicknesses
        shaleThicknessSum = sum(shaleThickness)

        # Number of aquifer includes reservoir so
        # number of aquifers = (number of shales - 1) + 1
        aquiferNum = self.parameters.numberOfShaleLayers

        # Get the sum of aquifer thicknesses
        if aquiferNum == 2:
            aquiferThicknessSum = aquiferThickness
        else:
            aquiferThicknessSum = sum(aquiferThickness)

        # Get the depth of the bottom of the reservoir
        self.reservoirBottom = (shaleThicknessSum + aquiferThicknessSum +
                                reservoirThickness)

        # We need to know the depths to the bottom of each aquifer
        # Initialize depth array for depths of the bottoms of the aquifers
        self.depth = np.zeros((aquiferNum,))

        self.depth[0] = self.reservoirBottom
        self.depth[1] = self.depth[0] - (
            shaleThickness[0] + reservoirThickness)

        if aquiferNum > 2:
            for ind in range(2,aquiferNum):
              self.depth[ind] = self.depth[ind-1] - (
                  shaleThickness[ind-1] + aquiferThickness[ind-2])

    def setup_initial_conditions(self):
        """ Setup initial conditions at the abandoned well"""

        # Determine depths of the aquifers
        self.get_depth_for_pressure()

        # Setup initial hydrostatic pressure
        # datumPressure is atmospheric pressure or some reference pressure
        # at the top of the upper shale layer
        self.initialPressure = self.parameters.datumPressure + (
            self.parameters.brineDensity*self.g*self.depth)

        pressureTopReservoir = (
            self.initialPressure[0] - self.parameters.brineDensity*self.g*self.parameters.reservoirThickness)

        if pressureTopReservoir > self.parameters.pressure:
            logging.warning("Hydrostatic pressure at the top of reservoir exceeds supplied pressure.\n"+
                            "This could explain negative leakage rates.")

        self.nSL = self.parameters.numberOfShaleLayers

        # Setup initial masses of CO2 in each aquifer and reservoir
        self.CO2Mass = self.parameters.prevCO2Mass

        # Setup array for the thickness of aquifers and reservoir
        self.thicknessH = np.ones(self.nSL)
        self.thicknessH[0] = self.parameters.reservoirThickness
        self.thicknessH[1:self.nSL] = self.parameters.aquiferThickness

        # Setup initial interface height h at the leaking well
        self.interface = np.zeros(self.nSL)
        for j in range(self.nSL):
            self.interface[j] = self.parameters.CO2saturation[j]*self.thicknessH[j]

        # Setup saturation in aquifers
        self.CO2SaturationAq = np.zeros(self.nSL-1)

    def find(self):
        # Setup initial pressure, saturations, masses and interface height
        self.setup_initial_conditions()

        self.find_lambda()

        # Initialize matrix for system of equations
        M = np.zeros((self.nSL,self.nSL))
        F = np.zeros(self.nSL)

        # Initialize constant factors for the matrix and RHS
        KbarPlus = (
            self.parameters.flowArea*self.parameters.shalePermeability)
        KbarMinus = np.zeros(self.nSL)
        KbarMinus[1:self.nSL] = (
            self.parameters.flowArea*(
            self.parameters.shalePermeability[0:self.nSL-1]))

        CPlus = KbarPlus/self.parameters.shaleThickness
        CMinus = np.zeros(self.nSL)
        CMinus[1:self.nSL] = (KbarMinus[1:self.nSL]/
            self.parameters.shaleThickness[0:self.nSL-1])

        CO2GravityPlus = KbarPlus*self.g*self.parameters.CO2Density
        CO2GravityMinus = KbarMinus*self.g*self.parameters.CO2Density

        brineGravityPlus = KbarPlus*self.g*self.parameters.brineDensity
        brineGravityMinus = KbarMinus*self.g*self.parameters.brineDensity

        # Parameters for injection well function
        compressibility = self.parameters.compressibility

        # Parameters for leaking well function
        r1 = self.parameters.wellRadius
        u1Scalar = np.ones(self.nSL)
        u1Scalar[1:self.nSL] = r1**2*compressibility/(
            4*self.parameters.aquiferPermeability)

        timeStep = self.parameters.timeStep*units.day()
        t = self.parameters.timePoint*units.day()
        self.CO2LeakageRates = np.zeros(self.nSL)
        self.brineLeakageRates = np.zeros(self.nSL)
        self.CO2Q = np.zeros(self.nSL)
        self.brineQ = np.zeros(self.nSL)

        GLeak = np.zeros(self.nSL)
        fourPi = 4*np.pi

        permxThickness = np.zeros(self.nSL)
        permxThickness[1:self.nSL] = self.thicknessH[1:self.nSL]*self.parameters.aquiferPermeability

        self.get_mobility_for_aquifers()

        leakt = 0.92*t
        uLeak = u1Scalar/(self.effectiveMobility*leakt)
        GLeak = well_function(uLeak)

        self.get_mobility_for_shales()

        # Get factors for the matrix
        LPlusC = CPlus*self.CO2MobilityShale
        LMinusC = np.zeros(self.nSL)
        LMinusC[1:self.nSL] = (
            CMinus[1:self.nSL]*self.CO2MobilityShale[0:self.nSL-1])

        LPlusB = CPlus*self.brineMobilityShale
        LMinusB = np.zeros(self.nSL)
        LMinusB[1:self.nSL] = (
            CMinus[1:self.nSL]*self.brineMobilityShale[0:self.nSL-1])

        # Get factors for the RHS
        FPlusC = CO2GravityPlus*self.CO2MobilityShale
        FMinusC = np.zeros(self.nSL)
        FMinusC[1:self.nSL] = CO2GravityMinus[1:self.nSL]*(
            self.CO2MobilityShale[0:self.nSL-1])

        FPlusB = brineGravityPlus*self.brineMobilityShale
        FMinusB = np.zeros(self.nSL)
        FMinusB[1:self.nSL] = brineGravityMinus[1:self.nSL]*(
            self.brineMobilityShale[0:self.nSL-1])

        adjterm = ((self.thicknessH-self.interface)*self.parameters.brineDensity+
            self.interface*self.parameters.CO2Density)*self.g/self.parameters.shaleThickness  # term needed to adjust the pressure at the bottom to the pressure at the top
        adjtermPlusC = adjterm*self.CO2MobilityShale*KbarPlus
        adjtermPlusB = adjterm*self.brineMobilityShale*KbarPlus
        adjtermMinusC = np.zeros(self.nSL)
        adjtermMinusC[1:self.nSL] = adjterm[0:self.nSL-1]*self.CO2MobilityShale[0:self.nSL-1]*KbarMinus[1:self.nSL]
        adjtermMinusB = np.zeros(self.nSL)
        adjtermMinusB[1:self.nSL] = adjterm[0:self.nSL-1]*self.brineMobilityShale[0:self.nSL-1]*KbarMinus[1:self.nSL]

        # Coefficient next to the well function in the pressure equation
        denomS = fourPi*permxThickness*self.effectiveMobility

        ind = 0  # index of the first row/column
        M[ind,ind] = 1
        M[ind,ind+1] = 0

        F[ind] = self.parameters.pressure+(
            self.thicknessH[0]-self.interface[0])*self.parameters.brineDensity*self.g-(
            self.interface[0]*self.parameters.CO2Density*self.g)

        ind = self.nSL-1  # index of the last row/column
        tempC1 = GLeak[ind]*(LPlusC[ind] + LPlusB[ind])/denomS[ind]
        tempC2 = GLeak[ind]*(LMinusC[ind] + LMinusB[ind])/denomS[ind]
        M[ind,ind] = 1 + (tempC1 + tempC2)
        M[ind,ind-1] = -tempC2
        tempF1 = GLeak[ind]*(FPlusC[ind] + FPlusB[ind])/denomS[ind]
        tempF2 = GLeak[ind]*(FMinusC[ind] + FMinusB[ind])/denomS[ind]
        tempF3 = (LPlusC[ind] + LPlusB[ind])*(
            GLeak[ind]*self.parameters.datumPressure)/denomS[ind]
        temp4 = GLeak[ind]*(adjtermMinusC[ind] + adjtermMinusB[ind])/denomS[ind]
        temp5 = GLeak[ind]*(adjtermPlusC[ind] + adjtermPlusB[ind])/denomS[ind]

        F[ind] = self.initialPressure[ind] + (tempF1 - tempF2 + tempF3 - temp4 + temp5)

        for ind in range(1,self.nSL-1):
            tempC1 = GLeak[ind]*(LPlusC[ind] + LPlusB[ind])/denomS[ind]
            tempC2 = GLeak[ind]*(LMinusC[ind] + LMinusB[ind])/denomS[ind]

            M[ind,ind] = 1 + (tempC1 + tempC2)
            M[ind,ind-1] = -tempC2
            M[ind,ind+1] = -tempC1
            tempF1 = GLeak[ind]*(FPlusC[ind] + FPlusB[ind])/denomS[ind]

            tempF2 = GLeak[ind]*(FMinusC[ind] + FMinusB[ind])/denomS[ind]
            temp4 = GLeak[ind]*(adjtermMinusC[ind] + adjtermMinusB[ind])/denomS[ind]
            temp5 = GLeak[ind]*(adjtermPlusC[ind] + adjtermPlusB[ind])/denomS[ind]
            F[ind] = self.initialPressure[ind] + (tempF1 - tempF2 - temp4 + temp5)

        # Find pressure as solution of linear system
        pressure = np.linalg.solve(M, F)

        # Find change in pressure to calculate fluxes
        deltaP = np.zeros(self.nSL)
        pressureLPlusBottom = pressure[1:self.nSL]
        pressureLMinusTop = pressure[0:self.nSL] - (
            self.thicknessH-self.interface)*self.parameters.brineDensity*self.g-(
            self.interface*self.parameters.CO2Density*self.g)
        deltaP[0:self.nSL-1] = pressureLMinusTop[0:self.nSL-1] - pressureLPlusBottom
        deltaP[self.nSL-1] = (pressureLMinusTop[self.nSL-1] -
                                            self.parameters.datumPressure)

        self.CO2Q[:] = (LPlusC*deltaP - FPlusC)

        self.brineQ[:] = (LPlusB*deltaP - FPlusB)

        # Update CO2 volume
        self.CO2Mass = self.CO2Mass + (
            self.CO2Q[0:self.nSL-1]-self.CO2Q[1:self.nSL])*timeStep

        self.CO2Mass = np.maximum(self.CO2Mass,np.zeros(self.nSL-1))

        self.get_interface(t,self.parameters.CO2saturation)
        for j in range(self.nSL-1):
            self.CO2SaturationAq[j] = self.interface[j+1]/self.thicknessH[j+1]

        self.CO2LeakageRates = self.CO2Q*self.parameters.CO2Density
        self.brineLeakageRates = self.brineQ*self.parameters.brineDensity

    def get_mobility_for_aquifers(self):
        CO2Permeability = np.minimum(np.ones(self.nSL)*(
            1-self.parameters.brineResidualSaturation),self.interface/self.thicknessH)
        brinePermeability = 1/(1-self.parameters.brineResidualSaturation)*(
            1-self.parameters.brineResidualSaturation-CO2Permeability)

        self.CO2MobilityAq = CO2Permeability/self.parameters.CO2Viscosity
        self.brineMobilityAq = brinePermeability/self.parameters.brineViscosity
        self.effectiveMobility = (self.interface*self.CO2MobilityAq+
            (self.thicknessH-self.interface)*self.brineMobilityAq)/(
            self.thicknessH)

        # For use in well functions
        for ind in range(0,self.nSL):
            if self.interface[ind] == 0:
                self.effectiveMobility[ind] = 1/self.parameters.brineViscosity

    def get_mobility_for_shales(self):
        CO2Permeability = np.zeros(self.nSL)
        brinePermeability = np.ones(self.nSL)
        for ind in range(0,self.nSL):
            if self.interface[ind] > 0.3*self.thicknessH[ind]:
                CO2Permeability[ind] = min(1-self.parameters.brineResidualSaturation,
                    self.interface[ind]/self.thicknessH[ind])

                brinePermeability[ind] = 1/(
                    1-self.parameters.brineResidualSaturation)*(
                    1-self.parameters.brineResidualSaturation-CO2Permeability[ind])

            elif self.interface[ind] > 0:
                CO2Permeability[ind] = min(
                    1-self.parameters.brineResidualSaturation,
                    self.interface[ind]/self.thicknessH[ind])
                brinePermeability[ind] = 1/(
                    1-self.parameters.brineResidualSaturation)*(
                    1-self.parameters.brineResidualSaturation-CO2Permeability[ind])

            else:
                CO2Permeability[ind] = 0.
                brinePermeability[ind] = 1.

        self.CO2MobilityShale = CO2Permeability/self.parameters.CO2Viscosity
        self.brineMobilityShale = brinePermeability/(
            self.parameters.brineViscosity)

    def find_lambda(self):
        # Lambda is a constant in the formula for the outer egde of the plume
        # It is constant for each aquifer in the system unless
        # brine and CO2 have different relative permeabilities, residual
        # saturations or viscosities
        # For simple calculations, the mobility ratio Lambda is calculated at
        # the endpoint relative permeability values for the two phases.

        # Maximum value for CO2 relative permeability
        CO2Permeability = 1 - self.parameters.brineResidualSaturation
        # Maximum value for brine relative permeability
        brinePermeability = 1.

        brineViscosity = self.parameters.brineViscosity
        CO2Viscosity = self.parameters.CO2Viscosity

        self.Lambda = np.ones(self.nSL)*CO2Permeability*brineViscosity/(
                            brinePermeability*CO2Viscosity)

    def update_lambda(self):
        Sres = self.parameters.brineResidualSaturation
        CO2Permeability = np.minimum(np.ones(self.nSL)*(
            1-Sres),self.interface/self.thicknessH)
        brinePermeability = 1-CO2Permeability

        brineViscosity = self.parameters.brineViscosity
        CO2Viscosity = self.parameters.CO2Viscosity

        self.Lambda = CO2Permeability*brineViscosity/(
                            brinePermeability*CO2Viscosity)

    def get_interface(self, t, CO2saturation):
        Sres = self.parameters.brineResidualSaturation

        for j in range(1,self.nSL):
            if self.CO2Mass[j-1] > 0:
                x = 2*np.pi*self.thicknessH[j]*(1-Sres)*(self.parameters.wellRadius**2)/(self.CO2Mass[j-1])
                if x < 2/self.Lambda[j]:
                    self.interface[j] = (1-Sres)*self.thicknessH[j]
                elif x >= 2*self.Lambda[j]:
                    self.interface[j] = 0.
                else:
                    self.interface[j] = 1/(self.Lambda[j]-1)*(
                        np.sqrt(2*self.Lambda[j]/x)-1)*self.thicknessH[j]

def read_data(filename):
    """ Routine used for reading pressure and saturation data files."""
    # Check whether the file with given name exists
    if os.path.isfile(filename):
        data = np.genfromtxt(filename)
        return data

def well_function(x):
    """
    Return the approximation of the well function with even number
    of terms in expansion. Expansions with an odd number of terms
    diverge to plus infinity without crossing zero.
    """
    W = np.zeros(len(x))
    for i, u in list(enumerate(x)):
        if u <= 1.0:
            W[i] = (-0.577216-np.log(u)+u-u**2/(2*scm.factorial(2))+
                u**3/(3*scm.factorial(3))-u**4/(4*scm.factorial(4))+
                u**5/(5*scm.factorial(5))-u**6/(6*scm.factorial(6))+
                u**7/(7*scm.factorial(7))-u**8/(8*scm.factorial(8)))
        elif u <= 9:
            uu = np.linspace(1.0,9.0,num=9)
            Wu = np.array([0.219, 0.049, 0.013, 0.0038, 0.0011,
                       0.00036, 0.00012, 0.000038, 0.000012])
            Wfun = interp1d(uu, Wu, kind='linear')
            W[i] = Wfun(u)
        else:
            W[i] = 0.000001
    return W

if __name__ == "__main__":
    x = [5.0, 6, 4]
    W = well_function(x)
    print(W)