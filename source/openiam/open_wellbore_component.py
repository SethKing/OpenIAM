# -*- coding: utf-8 -*-
"""
The Open Wellbore model is a lookup table reduced order model based on the
drift-flux approach, see :cite:`RN1899`. This model treats the leakage of |CO2|
up an open wellbore or up an open (i.e., uncemented) casing/tubing. The lookup
table is populated using T2Well/ECO2N Ver. 1.0 :cite:`RN1900`, which treats
the non-isothermal flow of |CO2| and brine up an open wellbore, allows for the
phase transition of |CO2| from supercritical to gaseous, with Joule-Thompson
cooling, and considers exsolution of |CO2| from the brine phase.

By default, when used within the user interface the Open Wellbore is connected
to the upper aquifer (aquifer2). For user-defined scenarios
the *LeakTo* keyword can be used to specify either the name of the aquifer
(e.g., 'aquifer1') |CO2| leaks to or 'atmosphere' for
leakage to the atmosphere. The default value is 'aquifer2'.

Component model input definitions:

* **logNormalizedTransmissivity** [|log10|] (-1 to 1) - reservoir
  transmissivity log normalized by depth average transmissivity (default 0.0)

* **brineSalinity** [-] (0 to 0.2) - brine salinity (mass fraction) (default 0.1)

* **wellRaduis** [m] (0.025 to 0.25) - radius of the wellbore (default 0.05 m)

* **wellTop** [m] (0 to 500) - depth of well top (default 500 m).
  *Linked to Stratigraphy.*

* **reservoirDepth** [m] (1000 to 4000) - depth of reservoir/ well base
  (default 2000 m).  *Linked to Stratigraphy.*

The possible outputs from the Open Wellbore component are leakage rates
of |CO2| and brine to aquifer and atmosphere. The names of the
observations are of the form:

* **CO2_aquifer** and **CO2_atm** [kg/s] - |CO2| leakage rates.

* **brine_aquifer** and **brine_atm** [kg/s] - brine leakage rates.

"""

import numpy as np
import sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import matplotlib.pyplot as plt

try:
    from openiam import SystemModel, ComponentModel
except ImportError as err:
        print('Unable to load IAM class module: '+str(err))

try:
    import components.wellbore.open as owmodel
    import components.wellbore.open.open_wellbore_ROM as owrom
except ImportError as err:
    print('\nERROR: Unable to load ROM for Open wellbore component\n')
    sys.exit()


class OpenWellbore(ComponentModel):
    def __init__(self, name, parent, header_file_dir=None):
        """
        Constructor method of OpenWellbore class

        :param name: name of component model
        :type name: str

        :param parent: the SystemModel object that the component model
            belongs to
        :type parent: SystemModel object

        :param header_file_dir: path to the directory that contains
            the look up tables
        :type workdir: str

        :returns: OpenWellbore class object
        """

        # Set up keyword arguments of the 'model' method provided by the system model
        model_kwargs = {'time_point': 365.25, 'time_step': 365.25}   # default value of 365.25 days

        if header_file_dir is None:
            model_kwargs['header_file_dir'] = owmodel.__path__[0]
        else:
            model_kwargs['header_file_dir'] = header_file_dir

        super(OpenWellbore, self).__init__(name, parent,
             model=self.model, model_kwargs=model_kwargs)

        # Define output dictionary labels
        self.output_labels = ['CO2_atm','CO2_aquifer','brine_atm','brine_aquifer']

        # Set default parameters of the component model
        self.initPressure = 0.
        self.add_default_par('wellRadius', value=0.05)
        self.add_default_par('wellTop', value = 500.0)
        self.add_default_par('reservoirDepth', value = 2000.0)
        self.add_default_par('logNormalizedTransmissivity', value = 0.0)
        self.add_default_par('brineSalinity', value = 0.1)

        # Instantiate solution
        self.sol = owrom.Solution(model_kwargs['header_file_dir'])
        self.leak_layer = 0

        self.default_out = {}

    def model(self, p, header_file_dir='.', time_point=365.25, time_step=365.25,
              pressure=0.0, CO2saturation=0.0):
        """
        Return CO2 and brine leakage rates corresponding to the provided input.

        :param p: dictionary of input parameters
        :type p: dict

        :param header_file_dir: path to header files
        :type header_file_dir: str

        :param time_point: current time point in days (at which the output is
            to be provided); by default its value is 365.25 (1 year in days)
        :type time_point: float

        :param time_step: difference between current and previous time points
            in days; by default its value is 365.25 (1 year in days)
        :type time_point: float

        :param pressure: pressure at the bottom of leaking well
            at the current time point, in Pa; by default its value is 0.0
        :type pressure: float

        :param saturation: CO2 saturation at the bottom of leaking well;
            by default its value is 0.0
        :type saturation: float

        :returns: dictionary of observations (leakage rates, etc.) of Open
            wellbore model; keys:
            ['CO2_rate','brine_rate']

        """
        # Return the initial state of the model if requested
        if time_point == 0.0:
            # Setup initial pressure
            self.initPressure = pressure
            # For time point 0 all outputs of the model are zeros
            out = self.default_out.copy()
            out.update(dict(list(zip(self.output_labels, len(self.output_labels)*[0.0]))))
            return out

        # Obtain the default values of the parameters from dictionary of default parameters
        actual_p = dict([(k,v.value) for k,v in self.default_pars.items()])
        # Update default values of parameters with the provided ones
        actual_p.update(p)

        # Units
        megaPascal = 1.0e+6

        # Set parameters
        wellTop = actual_p['wellTop']
        reservoirDepth = actual_p['reservoirDepth']
        logNormalizedTransmissivity = actual_p['logNormalizedTransmissivity']
        brineSalinity = actual_p['brineSalinity']
        deltaP = pressure-self.initPressure

        # Define array of input parameters
        inputArray = np.array([wellTop, reservoirDepth, logNormalizedTransmissivity,
                               brineSalinity, deltaP/megaPascal, CO2saturation])

        # Find solution
        self.sol.find(inputArray)

        # Rescale solution by constant dependent on the wellbore radius
        wellRadius = actual_p['wellRadius']
        originalRadius = 0.05
        solScalar = (wellRadius/originalRadius)**2
        CO2LeakageRates = solScalar*self.sol.CO2LeakageRates
        brineLeakageRates = solScalar*self.sol.brineLeakageRates

        # Return dictionary of leakage rates
        out = self.default_out.copy()
        out.update(dict(list(zip(self.output_labels, np.concatenate([CO2LeakageRates,brineLeakageRates])))))
        out['CO2_aquifer{}'.format(self.leak_layer)] = out['CO2_aquifer']
        out['brine_aquifer{}'.format(self.leak_layer)] = out['brine_aquifer']
        return out

    def connect_with_system(self, component_data, name2obj_dict,
                            welllocs=None, num_pathways=1):
        # Parameters
        if ('Parameters' in component_data) and (component_data['Parameters']):
            for key in component_data['Parameters']:
                if type(component_data['Parameters'][key]) != dict:
                    component_data['Parameters'][key] = {'value': component_data['Parameters'][key],
                                                         'vary': False}
                self.add_par(key, **component_data['Parameters'][key])

        if ('DynamicParameters' in component_data) and (component_data['DynamicParameters']):
            for key in component_data['DynamicParameters']:
                self.add_dynamic_kwarg(key, component_data['DynamicParameters'][key])

        strata = name2obj_dict['strata']
        if 'numberOfShaleLayers' in strata.deterministic_pars:
            num_shale_layers = strata.deterministic_pars['numberOfShaleLayers'].value
        elif 'numberOfShaleLayers' in strata.default_pars:
            num_shale_layers = strata.default_pars['numberOfShaleLayers'].value
        else:
            num_shale_layers = 3
        if 'LeakTo' in component_data:
            leak_to = component_data['LeakTo'].lower()
        else:
            leak_to = 'aquifer2'
        if 'aquifer' in leak_to:
            self.leak_layer = int(leak_to[7:])
        else:
            self.leak_layer = num_shale_layers - 1
        # Make model connections
        if 'Connection' in component_data:
            connection = None
            try:
                connection = name2obj_dict[component_data['Connection']]
            except KeyError:
                pass
            system_inputs = ['pressure', 'CO2saturation']
            for sinput in system_inputs:
                connection.add_obs_to_be_linked(sinput)
                self.add_kwarg_linked_to_obs(sinput, connection.linkobs[sinput])

        res_depth = 'strata.shale1Thickness '
        for il in range(1, num_shale_layers):
            res_depth += ('+ strata.aquifer{il}Thickness + strata.shale{ilp1}Thickness '
                          .format(il=il, ilp1=il+1))
        self.add_composite_par('reservoirDepth', res_depth)

        if leak_to == 'atmosphere':
            self.add_par('wellTop', value=0.0, vary=False)
        else:
            welltop = '0 '
            for il in range(self.leak_layer, num_shale_layers):
                welltop += ('+ strata.aquifer{il}Thickness + strata.shale{ilp1}Thickness '
                            .format(il=il, ilp1=il+1))
            self.add_composite_par('wellTop', welltop)

        for il in range(1, num_shale_layers):
            aq = 'aquifer{}'.format(il)
            self.default_out['CO2_' + aq] = 0.0
            self.default_out['brine_' + aq] = 0.0
        return


if __name__ == "__main__":
    try:
        from openiam import SimpleReservoir
    except ImportError as err:
        print('Unable to load IAM class module: '+str(err))
    # Create system model
    num_years = 50.
    time_array = 365.25*np.arange(0.0,num_years+1)
    sm_model_kwargs = {'time_array': time_array} # time is given in days
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # Add reservoir component
    sres = sm.add_component_model_object(SimpleReservoir(name='sres', parent=sm))

    # Add parameters of reservoir component model
    sres.add_par('numberOfShaleLayers', value=3, vary=False)
    sres.add_par('shale1Thickness', min=500.0, max=550., value=525.0)
    sres.add_par('shale2Thickness', min=510.0, max=550., value=525.0)
    # Shale 3 has a fixed thickness of 1.5 m
    sres.add_par('shale3Thickness', min=1.0, max=2.0, value=1.5)
    # Aquifer 1 (thief zone has a fixed thickness of 1.0)
    sres.add_par('aquifer1Thickness', value=1.0, vary=False)
    # Aquifer 2 (shallow aquifer) has a fixed thickness of 200
    sres.add_par('aquifer2Thickness', value=200.0, vary=False)
    # Reservoir has a fixed thickness of 51.2
    sres.add_par('reservoirThickness', value=51.2, vary=False)

    # Add observations of reservoir component model
    # When add_obs method is called, system model automatically
    # (if no other options of the method is used) adds a list of observations
    # with name sres.obsnm_0, sres.obsnm_1,.... The final index is determined by the
    # number of time points in system model time_array attribute.
    # For more information, see the docstring of add_obs of ComponentModel class.
    sres.add_obs('pressure')
    sres.add_obs('CO2saturation')
    sres.add_obs_to_be_linked('pressure')
    sres.add_obs_to_be_linked('CO2saturation')

    # Add open wellbore component
    ow = sm.add_component_model_object(OpenWellbore(name='ow',parent=sm))

    # Add parameters of open wellbore component
    ow.add_par('logNormalizedTransmissivity', min=-1.0, max=-1.0, value=-1.0)
    ow.add_par('brineSalinity', min=0.0, max=0.0, value=0.0)

    # Add keyword arguments of the open wellbore component model
    ow.add_kwarg_linked_to_obs('pressure', sres.linkobs['pressure'])
    ow.add_kwarg_linked_to_obs('CO2saturation', sres.linkobs['CO2saturation'])

    # Add composite parameter of open wellbore component
    ow.add_composite_par('reservoirDepth', expr='sres.shale1Thickness' +
        '+sres.shale2Thickness + sres.shale3Thickness' +
        '+sres.aquifer1Thickness+ sres.aquifer2Thickness')
    ow.add_composite_par('wellTop', expr='sres.shale1Thickness' +
        '+ sres.aquifer2Thickness')

    # Add observations of the open wellbore component
    ow.add_obs('CO2_aquifer')
    ow.add_obs('CO2_atm')
    ow.add_obs('brine_aquifer')
    ow.add_obs('brine_atm')

    sm.forward()
    # Since the observations at the particular time points are different variables,
    # method collect_observations_as_time_series creates lists of
    # values of observations belonging to a given component (e.g. cw) and having the same
    # common name (e.g. 'CO2_aquifer1', etc) but differing in indices.
    # More details are given in the docstring and documentation to the method
    # collect_observations_as_time_series of SystemModel class.
    pressure = sm.collect_observations_as_time_series(sres,'pressure')
    CO2saturation = sm.collect_observations_as_time_series(sres,'CO2saturation')
    print('------------------------------------------------------------------')
    print('Pressure', pressure, sep='\n')
    print('------------------------------------------------------------------')
    print('CO2 saturation', CO2saturation, sep='\n')

    CO2leakrates_aq1 = sm.collect_observations_as_time_series(ow,'CO2_aquifer')
    CO2leakrates_atm = sm.collect_observations_as_time_series(ow,'CO2_atm')
    print('------------------------------------------------------------------')
    print('CO2 leakage rates to aquifer', CO2leakrates_aq1, sep='\n')
    print('------------------------------------------------------------------')
    print('CO2 leakage rates to atmosphere', CO2leakrates_atm, sep='\n')

    brine_leakrates_aq1 = sm.collect_observations_as_time_series(ow,'CO2_aquifer')
    brine_leakrates_atm = sm.collect_observations_as_time_series(ow,'CO2_atm')
    print('------------------------------------------------------------------')
    print('Brine leakage rates to aquifer', brine_leakrates_aq1, sep='\n')
    print('------------------------------------------------------------------')
    print('Brine leakage rates to atmosphere', brine_leakrates_atm, sep='\n')
    print('------------------------------------------------------------------')

    plt.figure(1)
    plt.plot(sm.time_array/365.25, CO2leakrates_aq1, color='#000066', linewidth=1)
    plt.xlabel('Time, t (years)')
    plt.ylabel('Leakage rates, q (kg/s)')
    plt.title(r'Leakage of CO$_2$: Shallow aquifer')
    plt.show()
