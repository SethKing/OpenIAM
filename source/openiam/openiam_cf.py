# -*- coding: utf-8 -*-
"""
NRAP OpenIAM Control File reader code
Reads YAML formatted control file and runs IAM analysis.
Use: python openiam_cf.py --file ControlFileName.yaml

Created on Wed Oct 11 11:05:53 2017

@author: Seth King
AECOM supporting NETL
Seth.King@NETL.DOE.GOV
"""
import argparse
import collections
from datetime import datetime
import logging
import numpy as np
import os
import pickle
import matplotlib.pyplot as plt
import random
import shutil
import sys
import yaml
source_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(source_dir)
import openiam as iam
import openiam.visualize as iam_vis

# The following line creates a parser to parse arguments from the command line to the code.
parser = argparse.ArgumentParser(description='YAML control file IAM reader')

# Create a group of mutually exclusive command line arguments.
# parser will make sure that only one of the arguments in the mutually
# exclusive group is present on the command line.
group = parser.add_mutually_exclusive_group(required=False)

# lhs_sample_size and parstudy_divisions are mutually exclusive.
# --lhs and --parstudy are the keys used to indicate which of the parameters
# are given in the command line.
group.add_argument('--lhs', type=int,
                   dest='lhs_sample_size', default=20,
                   help='Latin Hypercube Sampling mode: enter number of samples')
group.add_argument('--parstudy', type=int,
                   dest='parstudy_divisions', default=3,
                   help='Parameter study mode: enter number of parameter partitions')

# Parameter ncpus has default value 1, i.e. by default all simulations are run
# sequentially. If a user wants to run simulations in paraller, he/she has to
# specify the number of cpus to use.
parser.add_argument('--ncpus', type=int, default=5,
                    help='Number of processors to use to run concurrent simulations')

# Parameter file sets the control file to read in.
parser.add_argument('--file', type=str, dest='yaml_cf_name',
                    default='../../examples/ControlFile_ex1.yaml',
                    help='IAM Control File Name')
parser.add_argument('--binary', type=bool, dest='binary_file',
                    default=False, help='Set to true for binary control file')
args = parser.parse_args()

output_header = '''# OpenIAM version: {iam_version}
# Runtime: {now}
'''

pathway_components = ['LookupTableReservoir',
                      'SimpleReservoir',
                      'MultisegmentedWellbore',
                      'CementedWellbore',
                      'OpenWellbore',
                      'AlluviumAquifer',
                      'DeepAlluviumAquifer']


def main(yaml_filename):
    """
    Reads in yaml data control file to create OpenIAM model and run it.

    :param yaml_filename: yaml formatted OpenIAM control file name
    :type filename: str

    :returns: None
    """
    start_time = datetime.now()
    now = start_time.strftime('%Y-%m-%d_%H.%M.%S')
    # Load yaml file data
    if args.binary_file:
        with open(yaml_filename, 'rb') as cf:
            yaml_data = pickle.load(cf)
    else:
        with open(yaml_filename, 'r') as yaml_cf:
            yaml_data = yaml.load(yaml_cf)
    model_data = yaml_data['ModelParams']

    if 'Analysis' not in model_data:
        model_data['Analysis'] = 'forward'
    if type(model_data['Analysis']) == str:
        analysis = model_data['Analysis'].lower()
        analysis_dict = {}
    elif type(model_data['Analysis']) == dict:
        analysis_dict = model_data['Analysis']
        analysis = analysis_dict.pop('type').lower()

    if 'OutputDirectory' in model_data:
        out_dir = os.path.abspath(model_data['OutputDirectory'])
        if not os.path.exists(os.path.dirname(out_dir)):
            os.mkdir(os.path.dirname(out_dir))
        out_dir = out_dir.format(datetime=now)
        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
        model_data['OutputDirectory'] = out_dir
        logging_file_name = os.path.join(out_dir, 'IAM_log.log')
        analysis_log = os.path.join(out_dir, 'Analysis.log')
        outfile = os.path.join(out_dir, '{analysis}_results.txt'.format(analysis=analysis))
    else:
        logging_file_name = 'IAM_log.log'
        analysis_log = 'analysis.log'
        outfile = '{analysis}_results.txt'.format(analysis=analysis)

    # If logging level not specified, set default
    if 'Logging' not in model_data:
        model_data['Logging'] = 'Info'
    log_dict = {'All': logging.NOTSET,
                'Debug': logging.DEBUG,
                'Info': logging.INFO,
                'Warning': logging.WARNING,
                'Error': logging.ERROR,
                'Critical': logging.CRITICAL}

    log_level = log_dict[model_data['Logging']]

    logging.basicConfig(level=log_level,
                        format='from %(module)s %(funcName)s - %(levelname)s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=logging_file_name,
                        filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    logging.info('\n' + output_header.format(iam_version=iam.__version__, now=now))

    if 'EndTime' in model_data:
        num_years = model_data['EndTime']
    else:
        num_years = 1
    if 'TimeStep' in model_data:
        time_step = model_data['TimeStep']
    else:
        time_step = 1
    time_array = 365.25*np.arange(0.0, num_years+time_step, time_step)
    sm_model_kwargs = {'time_array': time_array}  # time is given in days
    sm = iam.SystemModel(model_kwargs=sm_model_kwargs)

    strata = sm.add_component_model_object(iam.Stratigraphy(name='strata', parent=sm))

    if 'Stratigraphy' in yaml_data:
        strat_params = yaml_data['Stratigraphy']
        for parameter in strat_params:
            if type(strat_params[parameter]) != dict:
                strat_params[parameter] = {'value': strat_params[parameter],
                                           'vary': False}
            strata.add_par(parameter, **(strat_params[parameter]))

    # Sort model into order according to connections
    comp_list = []
    sm.collectors = {}

    for comp_model in model_data['Components']:
        comp_data = yaml_data[comp_model]
        if comp_model in comp_list:
            continue
        if 'connection' in comp_data:
            comp_data['Connection'] = comp_data['connection']
        if 'Connection' in comp_data:
            if comp_data['Connection'] == 'none':
                comp_data.pop('Connection', None)
        if 'Connection' in comp_data:
            connect = comp_data['Connection']
            if connect in comp_list:
                comp_list.append(comp_model)
            else:
                comp_list.append(connect)
                comp_list.append(comp_model)
        else:
            comp_list.append(comp_model)

    # Find Wellbore locations from model data or wellbore components
    if 'WellboreLocations' not in yaml_data:
        yaml_data['WellboreLocations'] = {'coordx': [],
                                          'coordy': []}
    for comp in comp_list:
        comp_data = yaml_data[comp]
        if 'Locations' in comp_data:
            well_loc = comp_data['Locations']
            yaml_data['WellboreLocations']['coordx'] += well_loc['coordx']
            yaml_data['WellboreLocations']['coordy'] += well_loc['coordy']

    # Use pathway_components list to find leakage pathway and create
    #    component models for leakage pathways
    path_list = []
    num_pathways = 1
    ad_connect = []
    comp_list2 = []
    for comp_model in comp_list:
        comp_data = yaml_data[comp_model]
        if 'number' in comp_data:
            comp_data['Number'] = comp_data['number']
        if 'Number' in comp_data:
            num_pathways = comp_data['Number']
        if 'type' in comp_data:
            comp_data['Type'] = comp_data['type']
        if comp_data['Type'] in pathway_components:
            path_list.append(comp_model)
        comp_type = getattr(iam, comp_data['Type'])
        if hasattr(comp_type, 'adapters') and comp_type.adapters:
            path_list.append('adapter')
            ad_connect.append([comp_data['Connection'],
                               yaml_data[comp_model]['AquiferName']])
            comp_list2.append('adapter')
        if hasattr(comp_type, 'system_collected_inputs') and comp_type.system_collected_inputs:
            sci = comp_type.system_collected_inputs
            if 'Connection' in comp_data:
                for sinput in sci:
                    sm.collectors[sinput] = {'parent': comp_model,
                                             'Connection': comp_data['Connection'],
                                             'argument': sci[sinput],
                                             'data': []}
        comp_list2.append(comp_model)

    # Generate Random wellbore locations if needed.
    n_given_wells = len(yaml_data['WellboreLocations']['coordx'])
    n_random_wells = num_pathways - n_given_wells
    if 'RandomWellDomain' not in yaml_data:
        yaml_data['RandomWellDomain'] = {'min': (-100, -100),
                                         'max': (100, 100)}
    if 'xmin' in yaml_data['RandomWellDomain']:
        yaml_data['RandomWellDomain']['min'] = (yaml_data['RandomWellDomain']['xmin'],
                                                yaml_data['RandomWellDomain']['ymin'])
        yaml_data['RandomWellDomain']['max'] = (yaml_data['RandomWellDomain']['xmax'],
                                                yaml_data['RandomWellDomain']['ymax'])
    wmin = yaml_data['RandomWellDomain']['min']
    wmax = yaml_data['RandomWellDomain']['max']
    if 'WellSeed' in yaml_data['RandomWellDomain']:
        well_seed = yaml_data['RandomWellDomain']['WellSeed']
    else:
        well_seed = random.randint(500, 1100)
    random.seed(well_seed)
    yaml_data['RandomWellDomain']['WellSeed'] = well_seed
    for i in range(n_random_wells):
        xy = (random.uniform(wmin[0], wmax[0]), random.uniform(wmin[1], wmax[1]))
        yaml_data['WellboreLocations']['coordx'].append(xy[0])
        yaml_data['WellboreLocations']['coordy'].append(xy[1])
        logging.debug('Placing Random well at ({xy[0]}, {xy[1]})'.format(xy=xy))
    random.seed()

    # Create component list with all pathways
    comps = comp_list2
    comp_list = []
    for comp_model in comps:
        if comp_model in path_list:
            if comp_model == 'adapter':
                ad_connect_name_base = ad_connect.pop(0)
                for i in range(num_pathways):
                    ad_name = 'adapter_{0:03}'.format(i)
                    ad_connect_name = ad_connect_name_base[0] + '_{0:03}'.format(i)
                    yaml_data[ad_name] = {'Type': 'RateToMassAdapter',
                                          'Connection': ad_connect_name,
                                          'AquiferName': ad_connect_name_base[1]}
                    comp_list.append(ad_name)
            else:
                for i in range(num_pathways):
                    comp_name = comp_model + '_{0:03}'.format(i)
                    comp_data = yaml_data[comp_model].copy()
                    if 'Connection' in comp_data:
                        connect = comp_data['Connection']
                        if connect in path_list:
                            comp_data['Connection'] = connect + '_{0:03}'.format(i)
                    comp_data['locX'] = yaml_data['WellboreLocations']['coordx'][i]
                    comp_data['locY'] = yaml_data['WellboreLocations']['coordy'][i]
                    yaml_data[comp_name] = comp_data
                    comp_list.append(comp_name)
        else:
            comp_list.append(comp_model)

    # Create dictionary to map component names to objects
    name2obj_dict = {'strata': strata}
    # Create dictionary of component models with outputs and lists of output names
    output_list = {}
    # Create list of component models
    components = []

    for i, component_name in enumerate(comp_list):
        component_data = yaml_data[component_name]
        comp_type = getattr(iam, component_data['Type'])
        components.append(sm.add_component_model_object(
                          comp_type(name=component_name, parent=sm)))
        name2obj_dict[component_name] = components[-1]

        if hasattr(components[-1], 'connect_with_system'):
            components[-1].connect_with_system(component_data,
                                               name2obj_dict,
                                               welllocs=yaml_data['WellboreLocations'],
                                               num_pathways=num_pathways)
            # Outputs
            if 'Outputs' in component_data:
                comp_outputs = component_data['Outputs']
                output_list[components[-1]] = comp_outputs
                for output in comp_outputs:
                    components[-1].add_obs(output)

            continue

        # Parameters
        if ('Parameters' in component_data) and (component_data['Parameters']):
            for key in component_data['Parameters']:
                if type(component_data['Parameters'][key]) != dict:
                        component_data['Parameters'][key] = {
                                'value': component_data['Parameters'][key],
                                'vary': False
                                }
                components[-1].add_par(key, **component_data['Parameters'][key])

        if ('DynamicParameters' in component_data) and (component_data['DynamicParameters']):
            for key in component_data['DynamicParameters']:
                components[-1].add_dynamic_kwarg(key, component_data['DynamicParameters'][key])

        if hasattr(components[-1], 'needs_locXY') and components[-1].needs_locXY:
            try:
                components[-1].locX = component_data['locX']
                components[-1].locY = component_data['locY']
            except IndexError:
                print('Well location unknown, placing at (100, 100)')
                components[-1].locX = 100
                components[-1].locY = 100

        if hasattr(components[-1], 'needsXY'):
            if components[-1].needsXY:
                components[-1].model_kwargs['x'] = yaml_data['WellboreLocations']['coordx'][:num_pathways]
                components[-1].model_kwargs['y'] = yaml_data['WellboreLocations']['coordy'][:num_pathways]

        # Outputs
        if 'Outputs' in component_data:
            comp_outputs = component_data['Outputs']
            output_list[components[-1]] = comp_outputs
            for output in comp_outputs:
                components[-1].add_obs(output)

        # Make model connections
        if 'Connection' in component_data:
            connection = None
            try:
                connection = name2obj_dict[component_data['Connection']]
            except KeyError:
                pass

            if hasattr(components[-1], 'system_inputs'):
                for sinput in components[-1].system_inputs:
                    # TODO add adapter linked obs for system_inputs here
                    connection.add_obs_to_be_linked(sinput)
                    components[-1].add_kwarg_linked_to_obs(sinput, connection.linkobs[sinput])

            if (hasattr(components[-1], 'system_collected_inputs') and
               components[-1].system_collected_inputs):
                collectors = sm.collectors
                for collect in collectors:
                    cdict = collectors[collect]
                    argument = cdict['argument'].format(aquifer_name=component_data['AquiferName'])
                    connect = cdict['Connection']
                    for i in range(num_pathways):
                        aname = 'adapter_{0:03}'.format(i)
                        cname = connect + '_{0:03}'.format(i)
                        adapter = name2obj_dict[aname]
                        connector = name2obj_dict[cname]
                        if(argument in adapter.linkobs):
                            collectors[collect]['data'].append(adapter.linkobs[argument])
                        else:
                            collectors[collect]['data'].append(connector.linkobs[argument])
                sci = components[-1].system_collected_inputs
                for sinput in sci:
                    components[-1].add_kwarg_linked_to_collection(sinput,
                                                                  collectors[sinput]['data'])

            if hasattr(components[-1], 'composite_inputs'):
                for key in components[-1].composite_inputs:
                    components[-1].add_composite_par(key,
                        components[-1].composite_inputs[key].format(driver=connection.name,
                                                                    selfm=components[-1].name,
                                                                    strata='strata'))
        # End Connection if statement
        if hasattr(components[-1], 'system_params'):
            for sparam in components[-1].system_params:
                connect = None
                if sparam in strata.pars:
                    connect = strata.pars
                elif sparam in strata.deterministic_pars:
                    connect = strata.deterministic_pars
                elif sparam in strata.default_pars:
                    connect = strata.default_pars
                else:
                    print('Unable to find parameter ' + sparam)

                components[-1].add_par_linked_to_par(sparam, connect[sparam])
    # End Component model loop

    # Setup Analysis
    if analysis == 'lhs':
        if 'siz' not in analysis_dict:
            analysis_dict['size'] = args.lhs_sample_size
        if 'seed_size' in analysis_dict:
            seed_size = [int(ss) for ss in analysis_dict.pop('seed_size').strip('()').split(',')]
            analysis_dict['seed'] = random.randint(*seed_size)
        if 'seed' not in analysis_dict:
            analysis_dict['seed'] = random.randint(500, 1100)
    elif analysis == 'parstudy':
        if 'nvals' not in analysis_dict:
            analysis_dict['nvals'] = args.parstudy_divisions
    model_data['Analysis_type'] = analysis
    model_data['Analysis_dict'] = analysis_dict

    # End setup
    calc_start_time = datetime.now()
    setup_time = calc_start_time - start_time
    logging.debug('Model setup time: {0}'.format(setup_time))
    # Run Analysis
    if analysis == 'forward':
        results = sm.forward()
        s = None
    elif analysis == 'lhs':
        s = sm.lhs(**analysis_dict)
        results = s.run(cpus=args.ncpus, verbose='progress', logfile=analysis_log)
        # New line in console
        print("")
    elif analysis == 'parstudy':
        s = sm.parstudy(**analysis_dict)
        results = s.run(cpus=args.ncpus, verbose='progress', logfile=analysis_log)
        # New line in console
        print("")
    else:
        raise ValueError('Analysis type {atype} not found\nNo analysis ran'.format(atype=analysis))
    yaml_data['Results'] = results
    yaml_data['s'] = s
    yaml_data['sm'] = sm
    yaml_data['output_list'] = output_list
    yaml_data['components'] = components
    yaml_data['time_array'] = time_array

    # End Analysis
    calc_end_time = datetime.now()
    calc_time = calc_end_time - calc_start_time
    logging.debug('Model analysis time: {0}'.format(calc_time))
    if 'OutputDirectory' in model_data:
        # Copy input YAML file
        shutil.copy2(yaml_filename, out_dir)
        if analysis == 'forward':
            for output_component in list(output_list.keys()):
                for output_name in output_list[output_component]:
                    outfile = os.path.join(out_dir,
                                           output_component.name + '.' +
                                           output_name + '.txt')
                    np.savetxt(outfile,
                               sm.collect_observations_as_time_series(output_component, output_name))
        elif analysis == 'lhs' or analysis == 'parstudy':
            outfile = os.path.join(out_dir, '{analysis}_results.txt'.format(analysis=analysis))
            analysisfile = os.path.join(out_dir, '{analysis}_statistics.txt'.format(analysis=analysis))
            s.savetxt(outfile)
            s.savestats(analysisfile)
        else:
            pass
        outfile = os.path.join(out_dir, 'output_dump.pkl')
        with open(outfile, 'wb') as output_dump:
#            output_dump.write(output_header.format(iam_version=iam.__version__,
#                                                   now=now))
            pickle.dump(yaml_data, output_dump)
        timefile = os.path.join(out_dir, 'time_series.csv')
        np.savetxt(timefile, time_array/365.25, fmt='%.2f', delimiter=',', header='Time (in years)')

    if 'Analysis' in yaml_data:
        if analysis != 'lhs':
            logging.warning('Sensitivity analysis only available for lhs sampling')
        else:
            analysis_dict = yaml_data['Analysis']
            if 'CorrelationCoeff' in analysis_dict:
                corrcoeff_dict = {'capture_point': len(time_array)-1,
                                  'excludes': [],
                                  'ctype': 'pearson',
                                  'plot': True,
                                  'printout': False,
                                  'plotvals': True,
                                  'figsize': (15, 15),
                                  'title': 'Pearson Correlation Coefficients at time {cp}',
                                  'xrotation': 90,
                                  'savefig': 'correlation_coefficients.png',
                                  'outfile': 'correlation_coefficients.csv'}
                if type(analysis_dict['CorrelationCoeff']) == dict:
                    corrcoeff_dict.update(analysis_dict['CorrelationCoeff'])
                if 'OutputDirectory' in model_data:
                    corrcoeff_dict['savefig'] = os.path.join(model_data['OutputDirectory'],
                                                             corrcoeff_dict['savefig'])
                    corrcoeff_dict['outfile'] = os.path.join(model_data['OutputDirectory'],
                                                             corrcoeff_dict['outfile'])
                iam_vis.correlations_at_time(s, **corrcoeff_dict)
            if 'SensitivityCoeff' in analysis_dict:
                if type(analysis_dict['SensitivityCoeff']) == dict:
                    sens_dict = analysis_dict['SensitivityCoeff']
                    obs = sens_dict.pop('Outputs')
                else:
                    obs = analysis_dict['SensitivityCoeff']
                    sens_dict = {}
                if type(obs) == str:
                    obs = [obs]
                res_obs = resolve_obs_names(obs, output_list)
                if 'capture_point' in sens_dict:
                    capture_point = sens_dict.pop('capture_point')
                else:
                    capture_point = len(time_array)-1
                if not isinstance(capture_point, collections.Iterable):
                    capture_points = [capture_point]
                else:
                    capture_points = capture_point
                for capture_point in capture_points:
                    cp_obs = ['{ob}_{cp}'.format(ob=ob, cp=capture_point)
                              for ob in res_obs]
                    sens_dict_full = {'title': '{ob} Sensitivity Coefficients',
                                      'ylabel': None,
                                      'savefig': '{ob}_sensitivity.png',
                                      'outfile': '{ob}_sensitivity.txt'}
                    sens_dict_full.update(sens_dict)
                    for ob in cp_obs:
                        if 'OutputDirectory' in model_data:
                            sens_dict_full['savefig'] = os.path.join(model_data['OutputDirectory'],
                                                                     sens_dict_full['savefig'])
                            sens_dict_full['outfile'] = os.path.join(model_data['OutputDirectory'],
                                                                     sens_dict_full['outfile'])
                        sens_dict2 = {}
                        for key, value in list(sens_dict_full.items()):
                            if type(value) == str:
                                v = value.format(ob=ob, cp=capture_point)
                            else:
                                v = value
                            sens_dict2[key] = v
                        sensitivities = s.rbd_fast(obsname=ob, print_to_console=False)
                        iam_vis.simple_sensitivities_barplot(sensitivities, sm,
                                                             **sens_dict2)
            if 'MultiSensitivities' in analysis_dict:
                if type(analysis_dict['MultiSensitivities']) == dict:
                    sens_dict = analysis_dict['MultiSensitivities']
                    obs = sens_dict.pop('Outputs')
                else:
                    obs = analysis_dict['MultiSensitivities']
                    sens_dict = {}
                if type(obs) == str:
                    obs = [obs]
                res_obs = resolve_obs_names(obs, output_list)
                if 'capture_point' in sens_dict:
                    capture_point = sens_dict.pop('capture_point')
                else:
                    capture_point = len(time_array)-1
                if not isinstance(capture_point, collections.Iterable):
                    capture_points = [capture_point]
                else:
                    capture_points = capture_point
                for capture_point in capture_points:

                    cp_obs = ['{ob}_{cp}'.format(ob=ob, cp=capture_point)
                              for ob in res_obs]
                    sens_dict_full = {'title': 'Sensitivity Coefficients',
                                      'ylabel': None,
                                      'savefig': 'Sensitivity_Analysis.png',
                                      'outfile': 'Sensitivity_Analysis.csv'}
                    sens_dict_full.update(sens_dict)
                    if 'OutputDirectory' in model_data:
                        sens_dict_full['savefig'] = os.path.join(model_data['OutputDirectory'],
                                                                 sens_dict_full['savefig']
                                                                 .format(ob=ob, cp=capture_point))
                        sens_dict_full['outfile'] = os.path.join(model_data['OutputDirectory'],
                                                                 sens_dict_full['outfile']
                                                                 .format(ob=ob, cp=capture_point))
                    iam_vis.multi_sensitivities_barplot(cp_obs, sm, s,
                                                        **sens_dict_full)
            if 'TimeSeriesSensitivity' in analysis_dict:
                if type(analysis_dict['TimeSeriesSensitivity']) == dict:
                    sens_dict = analysis_dict['TimeSeriesSensitivity']
                    obs = sens_dict.pop('Outputs')
                else:
                    obs = analysis_dict['TimeSeriesSensitivity']
                    sens_dict = {}
                if type(obs) == str:
                    obs = [obs]
                res_obs = resolve_obs_names(obs, output_list)
                if 'capture_point' in sens_dict:
                    capture_point = sens_dict.pop('capture_point')
                else:
                    capture_point = len(time_array)-1
                sens_dict_full = {'title': '{ob} Time Sensitivity Coefficients',
                                  'ylabel': None,
                                  'num_sensitivities': 5,
                                  'savefig': '{ob}_Sensitivity_Analysis.png',
                                  'outfile': '{ob}_Sensitivity_Analysis.csv'}
                sens_dict_full.update(sens_dict)
                for ob in res_obs:
                    if 'OutputDirectory' in model_data:
                        sens_dict_full['savefig'] = os.path.join(model_data['OutputDirectory'],
                                                                 sens_dict_full['savefig'])
                        sens_dict_full['outfile'] = os.path.join(model_data['OutputDirectory'],
                                                                 sens_dict_full['outfile'])
                    sens_dict = {}
                    for key, value in list(sens_dict_full.items()):
                        if type(value) == str:
                            v = value.format(ob=ob)
                        else:
                            v = value
                        sens_dict[key] = v
                    iam_vis.time_series_sensitivities(ob, sm, s, time_array,
                                                      capture_point=capture_point,
                                                      **sens_dict)

    if 'Plots' in yaml_data:
        plots = yaml_data['Plots']
        for p in plots:
            plt.figure(p, figsize=(13, 8))
            savefig = None
            title = None
            if 'Title' in plots[p]:
                title = plots[p]['Title']
            subplot = {'use': False}
            if 'subplot' in plots[p]:
                subplot = plots[p]['subplot']
                if 'use' not in subplot:
                    subplot['use'] = True
            if 'OutputDirectory' in model_data:
                savefig = os.path.join(model_data['OutputDirectory'], p)
            if 'Data' in plots[p]:
                # Keep Data keyword for backward compatibility
                plots[p]['TimeSeries'] = plots[p]['Data']
            if 'TimeSeries' in plots[p]:
                iam_vis.time_series_plot(plots[p]['TimeSeries'], sm, s, output_list,
                                         name=p, analysis=analysis, savefig=savefig,
                                         title=title, subplot=subplot,
                                         plot_type=['real'])
            if 'TimeSeriesStats' in plots[p]:
                iam_vis.time_series_plot(plots[p]['TimeSeriesStats'], sm, s, output_list,
                                         name=p, analysis=analysis, savefig=savefig,
                                         title=title, subplot=subplot,
                                         plot_type=['stats'])
            if 'TimeSeriesAndStats' in plots[p]:
                iam_vis.time_series_plot(plots[p]['TimeSeriesAndStats'], sm, s, output_list,
                                         name=p, analysis=analysis, savefig=savefig,
                                         title=title, subplot=subplot,
                                         plot_type=['real', 'stats'])
            if 'AtmPlumeSingle' in plots[p]:
                satm = find_atm_comp(components)
                if 'savefig' in plots[p]['AtmPlumeSingle']:
                    plots[p]['AtmPlumeSingle']['savefig'] = os.path.join(model_data['OutputDirectory'],
                                                                         plots[p]['AtmPlumeSingle']['savefig'])
                if not os.path.exists(os.path.dirname(plots[p]['AtmPlumeSingle']['savefig'])):
                    os.mkdir(os.path.dirname(plots[p]['AtmPlumeSingle']['savefig']))
                iam_vis.map_plume_plot_single(s, satm, time_array, **plots[p]['AtmPlumeSingle'])
            if 'AtmPlumeEnsemble' in plots[p]:
                satm = find_atm_comp(components)
                if 'savefig' in plots[p]['AtmPlumeEnsemble']:
                    plots[p]['AtmPlumeEnsemble']['savefig'] = os.path.join(model_data['OutputDirectory'],
                                                                           plots[p]['AtmPlumeEnsemble']['savefig'])
                if not os.path.exists(os.path.dirname(plots[p]['AtmPlumeEnsemble']['savefig'])):
                    os.mkdir(os.path.dirname(plots[p]['AtmPlumeEnsemble']['savefig']))
                iam_vis.map_plume_plot_ensemble(s, satm, time_array, **plots[p]['AtmPlumeEnsemble'])
    total_time = datetime.now() - start_time
    logging.info('\nAnalysis Completed at {0}'.format(datetime.now().strftime('%Y-%m-%d_%H.%M.%S')))
    logging.info('Total run time: {0}'.format(total_time))
    
    # Remove all handlers from the logger for proper work in the consecutive runs
    while logging.getLogger('').handlers:
        logging.getLogger('').handlers.pop()
    return True


def find_atm_comp(components):
    for comp in components[::-1]:
        if type(comp) == iam.AtmosphericROM:
            return comp
    else:
        logging.warning('Unable to find Atmospheric ROM for plume plots')
    return


def resolve_obs_names(obs, output_list):
    '''
    Takes in base observation name and returns list of appended cm.obs for
    all component model names that have observations matching the base name.
    '''
    resolved_names = []
    for ob in obs:
        for output_component in list(output_list.keys()):
            if ob in output_list[output_component]:
                resolved_names.append('.'.join([output_component.name, ob]))
    return resolved_names

if __name__ == "__main__":
    __spec__ = None
    if args.yaml_cf_name == 'test':
        print('Main Control File:')
        main('../../examples/ControlFile_ex1.yaml')
        plt.close('all')
        print('Example 2 Control File:')
        main('../../examples/ControlFile_ex2.yaml')
        plt.close('all')
        print('Aquifer Example:')
        main('../../examples/ControlFile_ex3.yaml')
        plt.close('all')
        print('OpenWellbore Example:')
        main('../../examples/ControlFile_ex4.yaml')
        plt.close('all')
        print('parstudy Example:')
        main('../../examples/ControlFile_ex5.yaml')
        print('Test done!')
    else:
        main(args.yaml_cf_name)
