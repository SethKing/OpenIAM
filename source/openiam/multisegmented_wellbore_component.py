# -*- coding: utf-8 -*-
"""
The Multisegmented Wellbore component estimates the leakage rates of brine and
|CO2| along wells with the presence of overlying aquifers or thief zones.
The model is based on work of Nordbotten et al.,
:cite:`C2011`. Further reading can be found in :cite:`N2005a`, :cite:`C2009a`,
:cite:`N2009`, :cite:`N2011a`.

The model is focused on flow across relatively large distances and does not take
into account discrete features of the flow paths such as fractures, cracks,
etc. It assumes that leakage is occurring in the annulus between the
outside of the casing and borehole. This area is assigned an “effective”
permeability of the flow path. The permeability is applied over a length
along the well that corresponds to the thickness of a shale formation.
Each well is characterized by an effective permeability assigned to each
segment of the well that crosses an individual formation. For example, if
a well crosses N permeable formations, then it is characterized by
N different permeability values. The model utilizes the one-dimensional
multiphase version of Darcy’s law to represent flow along a leaky well.

In the IAM control file, the type name for the multisegmented wellbore component is
MultisegmentedWellbore. The description of the component's parameters are
provided below. Note that the names of parameters coincide with those used by
``model`` method of the MultisegmentedWellbore class.

* **logWellPerm** [|log10| |m^2|] (-17 to -9) - logarithm of well permeability
  along shale layer (default -13). Logarithm of well permeability along shale 3,
  for example, can be defined by logWell3Perm. Permeability of the well
  along the shale layers not defined by user will be assigned a default value.

* **logAquPerm** [|log10| |m^2|] (-14 to -9) - logarithm of aquifer
  permeability (default -11). Logarithm of aquifer 1 permeability, for example,
  can be defined by logAqu1Perm. Aquifer permeability not defined by user will
  be assigned a default value.

* **brineDensity** [|kg/m^3|] (900 to 1500) - density of brine phase (default
  1000 |kg/m^3|).

* **CO2Density** [|kg/m^3|] (100 to 1500) - density of |CO2| phase (default
  479 |kg/m^3|).

* **brineViscosity** [|Pa*s|] (1.0e-4 to 5.0e-3) - viscosity of brine phase
  (default 2.535e-3 |Pa*s|).

* **CO2Viscosity** [|Pa*s|] (1.0e-6 to 1.0e-4)  - viscosity of |CO2| phase
  (default 3.95e-5 |Pa*s|).

* **brineResSaturation** [-] (0 to 0.7) - residual saturation of brine phase
  (default 0.1).

* **compressibility** [|Pa^-1|] (5.0e-11 to 1.0e-9) - compressibility of brine
  and |CO2| phases (assumed to be the same for both phases)
  (default 5.1e-11 |Pa^-1|).

* **wellRadius** [m] (0.01 to 0.5) - radius of leaking well (default 0.05 m).

* **numberOfShaleLayers** [-] (3 to 30) - number of shale layers in the
  system (default 3). The shale units must be separated by an aquifer.
  *Linked to Stratigraphy.*

* **shaleThickness** [m] (1 to 1000) - thickness of shale layers (default
  250 m). Thickness of shale layer 1, for example, can be defined by
  shale1Thickness; otherwise, shale layers for which the thickness is not
  defined will be assigned a default thickness.
  *Linked to Stratigraphy.*

* **aquiferThickness** [m] (1 to 1000) - thickness of aquifers (default 100 m).
  Thickness of aquifer 1, for example, can be defined by aquifer1Thickness;
  otherwise, aquifers for which the thickness is not defined will be assigned
  a default thickness.
  *Linked to Stratigraphy.*

* **reservoirThickness** [m] (1 to 1000) - thickness of reservoir (default 50 m).
  *Linked to Stratigraphy.*

* **datumPressure** [Pa] (80,000 to 300,000) - pressure at the top of the
  system (default 101,325 Pa).
  *Linked to Stratigraphy.*

The possible outputs from the Multisegmented Wellbore component are
leakage rates of |CO2| and brine to each of the aquifers in the system and
atmosphere. The names of the observations are of the form:

* **CO2_aquifer1**, **CO2_aquifer2**,..., **CO2_atm** [kg/s] - |CO2| leakage rates.

* **brine_aquifer1**, **brine_aquifer2**,..., **brine_atm** [kg/s] - brine leakage rates.

* **mass_CO2_aquifer1**, **mass_CO2_aquifer2**,..., **mass_CO2_aquiferN** [kg] - mass of the |CO2| leaked into the aquifer.

"""
import logging
import numpy as np
import sys
import os
import matplotlib.pyplot as plt
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

try:
    from openiam import SystemModel, ComponentModel
except ImportError as err:
    print('Unable to load IAM class module: '+str(err))

try:
    import components.wellbore.multisegmented.multisegmented_wellbore_ROM as mswrom
except ImportError as err:
    print('\nERROR: Unable to load ROM for multisegmented wellbore component\n')
    sys.exit()

class MultisegmentedWellbore(ComponentModel):
    def __init__(self, name, parent):
        """
        Constructor method of MultisegmentedWellbore class

        :param name: name of component model
        :type name: str

        :param parent: the SystemModel object that the component model
            belongs to
        :type parent: SystemModel object

        :returns: MultisegmentedWellbore class object
        """
        # Set up keyword arguments of the 'model' method provided by the system model
        model_kwargs = {'time_point': 365.25, 'time_step': 365.25}   # default value of 365.25 days

        super(MultisegmentedWellbore, self).__init__(
              name, parent, model=self.model, model_kwargs=model_kwargs)

        # Set default parameters of the component model
        self.add_default_par('numberOfShaleLayers', value=3)
        self.add_default_par('shaleThickness', value=250.0)
        self.add_default_par('logWellPerm', value=-13.0)
        self.add_default_par('aquiferThickness', value=100.0)
        self.add_default_par('logAquPerm', value=-11.0)
        self.add_default_par('reservoirThickness', value=50.0)
        self.add_default_par('datumPressure', value=101325.0)
        self.add_default_par('brineDensity', value=1000.0)
        self.add_default_par('CO2Density', value=479.0)
        self.add_default_par('brineViscosity', value=2.535e-3)
        self.add_default_par('CO2Viscosity', value=3.95e-5)
        self.add_default_par('brineResSaturation', value=0.1)
        self.add_default_par('compressibility', value=5.1e-11)
        self.add_default_par('wellRadius', value=0.05)

        # Define dictionary of boundaries
        self.pars_bounds = dict()
        self.pars_bounds['numberOfShaleLayers'] = [3, 30]
        self.pars_bounds['shaleThickness'] = [1.0, 1000.0]
        self.pars_bounds['logWellPerm'] = [-17.0, -9.0]
        self.pars_bounds['aquiferThickness'] = [1.0, 1000.0]
        self.pars_bounds['logAquPerm'] = [-14.0, -9.0]
        self.pars_bounds['reservoirThickness'] = [1.0, 1000.0]
        self.pars_bounds['datumPressure'] = [8.0e+4, 3.0e+5]
        self.pars_bounds['brineDensity'] = [900.0, 1500.0]
        self.pars_bounds['CO2Density'] = [100.0, 1500.0]
        self.pars_bounds['brineViscosity'] = [1.0e-4, 5.0e-3]
        self.pars_bounds['CO2Viscosity'] = [1.0e-6, 1.0e-4]
        self.pars_bounds['brineResSaturation'] = [0.0, 0.7]
        self.pars_bounds['compressibility'] = [5.0e-11, 1.0e-9]
        self.pars_bounds['wellRadius'] = [0.01, 0.5]

        # By default the smallest number of aquifers the system can have is 2,
        # so we add two accumulators by default. Extra will be added as needed,
        # once the system knows how many there are
        for i in range(2):
            self.add_accumulator('mass_CO2_aquifer'+str(i+1), sim=0.0)
            self.add_accumulator('CO2_saturation_aquifer'+str(i+1), sim=0.0)
        self.num_accumulators = 2

        logging.debug('MultisegmentedWellbore created with name {name}'.format(name=name))

    def check_input_parameters(self, p):
        """
        Check whether input parameters fall within specified boundaries.

        :param p: input parameters of component model
        :type p: dict
        """
        logging.debug('Input parameters of {name} component are {p}'.format(name=self.name,p=p))

        for key, val in p.items():
            if (key[0:5] == 'shale' and key[-9:] == 'Thickness'):
                if ((val < self.pars_bounds['shaleThickness'][0])or
                    (val > self.pars_bounds['shaleThickness'][1])):
                    logging.warning('Parameter ' + key + ' is out of boundaries.')
            elif key[0:7] == 'aquifer' and key[-9:] == 'Thickness':
                if ((val < self.pars_bounds['aquiferThickness'][0])or
                    (val > self.pars_bounds['aquiferThickness'][1])):
                    logging.warning('Parameter ' + key + ' is out of boundaries.')
            elif key[0:7] == 'logWell' and key[-4:] == 'Perm':
                if ((val < self.pars_bounds['logWellPerm'][0])or
                    (val > self.pars_bounds['logWellPerm'][1])):
                    logging.warning('Parameter ' + key + ' is out of boundaries.')
            elif key[0:6] == 'logAqu' and key[-4:] == 'Perm':
                if ((val < self.pars_bounds['logAquPerm'][0])or
                    (val > self.pars_bounds['logAquPerm'][1])):
                    logging.warning('Parameter ' + key + ' is out of boundaries.')
            elif key in self.pars_bounds:
                if ((val < self.pars_bounds[key][0])or
                    (val > self.pars_bounds[key][1])):
                    logging.warning('Parameter ' + key + ' is out of boundaries.')
            else:
                logging.warning('Parameter {key} not recognized as a MultisegmentedWellbore input parameter.'
                                .format(key=key))

    def model(self, p, time_point=365.25, time_step=365.25,
              pressure=0.0, CO2saturation=0.0):
        """
        Return |CO2| and brine leakage rates corresponding to the provided input.
        Note that the names of parameters contained in the input dictionary
        coincide with those defined in this module's docstring.

        :param p: input parameters of multisegmented wellbore model
        :type p: dict

        :param pressure: pressure at the bottom of leaking well, in Pa;
            by default its value is 0.0
        :type pressure: float

        :param CO2saturation: saturation of |CO2| phase at the bottom
            of leaking well; by default its value is 0.0
        :type CO2saturation: float

        :param time_point: time point in days at which the leakage rates are
            to be calculated; by default its value is 365.25 (1 year in days)
        :type time_point: float

        :param time_step: difference between the current and previous
            time points in days; by default its value is 365.25 (1 year in days)
        :type time_point: float

        :param kwargs: additional keyword arguments for the model
        :type kwargs: dict (if provided)

        :returns: out - dictionary of observations of multisegmented wellbore
            model; keys:
            ['CO2_aquifer1','CO2_aquifer2',...,'CO2_atm',
            'brine_aquifer1','brine_aquifer2',...,'brine_atm',
            'mass_CO2_aquifer1','mass_CO2_aquifer2',...]
        """
        # Obtain the default values of the parameters from dictionary of default parameters
        actual_p = dict([(k,v.value) for k,v in self.default_pars.items()])
        # Update default values of parameters with the provided ones
        actual_p.update(p)
        inputParameters = mswrom.Parameters()

        nSL = int(actual_p['numberOfShaleLayers'])
        inputParameters.numberOfShaleLayers = nSL

        # Add extra accumulators after the number of aquifers became known
        num_extra_accumulators = nSL-1 - self.num_accumulators
        if num_extra_accumulators != 0: # if component has no enough accumulators
            for j in range(num_extra_accumulators):
                self.add_accumulator('mass_CO2_aquifer'+str(j+3), sim=0.0)
                self.add_accumulator('CO2_saturation_aquifer'+str(j+3), sim=0.0)

            self.num_accumulators = nSL - 1

        # Check whether the initial state of the system is requested
        # Then if yes, return the state without proceeding further
        if time_point == 0.0:

            # Create dictionary of leakage rates
            out = dict()
            for i in range(nSL-1):
                out['CO2_aquifer'+str(i+1)] = 0.0
                out['brine_aquifer'+str(i+1)] = 0.0
                out['mass_CO2_aquifer'+str(i+1)] = 0.0
                self.accumulators['mass_CO2_aquifer'+str(i+1)].sim = 0.0
                self.accumulators['CO2_saturation_aquifer'+str(i+1)].sim = 0.0
            out['CO2_atm'] = 0.0
            out['brine_atm'] = 0.0
            self.prevCO2Mass = 0.0
            return out

        inputParameters.shaleThickness = actual_p['shaleThickness']*np.ones(nSL)
        inputParameters.shalePermeability = 10**actual_p['logWellPerm']*np.ones(nSL)
        inputParameters.aquiferThickness = actual_p['aquiferThickness']*np.ones((nSL-1))
        inputParameters.aquiferPermeability = 10**actual_p['logAquPerm']*np.ones((nSL-1))

        # Set up shale, aquifer and reservoir thickness parameters
        for i in range(nSL):
            if 'shale'+str(i+1)+'Thickness' in p:
                inputParameters.shaleThickness[i] = p['shale'+str(i+1)+'Thickness']
        for i in range(nSL-1):
            if 'aquifer'+str(i+1)+'Thickness' in p:
                inputParameters.aquiferThickness[i] = p['aquifer'+str(i+1)+'Thickness']
        inputParameters.reservoirThickness = actual_p['reservoirThickness']

        # Set up shale permeability
        for i in range(nSL):
            if 'logWell'+str(i+1)+'Perm' in p:
                inputParameters.shalePermeability[i] = 10**p['logWell'+str(i+1)+'Perm']

        for i in range(nSL-1):
            if 'logAqu'+str(i+1)+'Perm' in p:
                inputParameters.shalePermeability[i] = 10**p['logAqu'+str(i+1)+'Perm']

        # Set up land surface pressure
        inputParameters.datumPressure = actual_p['datumPressure']

        # Set up brine and CO2 density
        inputParameters.brineDensity = actual_p['brineDensity']
        inputParameters.CO2Density = actual_p['CO2Density']

        # Set up brine and CO2 viscosity
        inputParameters.brineViscosity = actual_p['brineViscosity']
        inputParameters.CO2Viscosity = actual_p['CO2Viscosity']

        # Set up residual saturation and compressibility
        inputParameters.brineResidualSaturation = actual_p['brineResSaturation']
        inputParameters.compressibility = actual_p['compressibility']

        # Set up well radius parameter
        inputParameters.wellRadius = actual_p['wellRadius']
        inputParameters.flowArea = np.pi*inputParameters.wellRadius**2

        # Get parameters from keyword arguments of the 'model' method
        inputParameters.timeStep = time_step         # in days
        inputParameters.timePoint = time_point       # in days
        inputParameters.pressure = pressure

        inputParameters.prevCO2Mass = np.zeros(nSL-1)
        inputParameters.CO2saturation = np.zeros(nSL)
        inputParameters.CO2saturation[0] = CO2saturation

        for i in range(nSL-1):
            inputParameters.prevCO2Mass[i] = (
                self.accumulators['mass_CO2_aquifer'+str(i+1)].sim)
            inputParameters.CO2saturation[i+1] = (
                self.accumulators['CO2_saturation_aquifer'+str(i+1)].sim)

        # Create solution object with defined input parameters
        sol = mswrom.Solution(inputParameters)

        # Find solution corresponding to the inputParameters
        sol.find()

        # Create dictionary of leakage rates
        out = dict()
        for i in range(nSL-1):
            out['CO2_aquifer'+str(i+1)] = sol.CO2LeakageRates[i]
            out['brine_aquifer'+str(i+1)] = sol.brineLeakageRates[i]
            # Convert mass in cubic meters to kg
            out['mass_CO2_aquifer'+str(i+1)] = sol.CO2Mass[i]*inputParameters.CO2Density
            # Keep mass in cubic meters in accumulators
            self.accumulators['mass_CO2_aquifer'+str(i+1)].sim = sol.CO2Mass[i]
            self.accumulators['CO2_saturation_aquifer'+str(i+1)].sim = sol.CO2SaturationAq[i]
        out['CO2_atm'] = sol.CO2LeakageRates[inputParameters.numberOfShaleLayers-1]
        out['brine_atm'] = sol.brineLeakageRates[inputParameters.numberOfShaleLayers-1]

        # Return dictionary of outputs
        return out

    def reset(self):
        pass

    # Attributes for system connections
    system_inputs = ['pressure',
                     'CO2saturation']
    system_params = ['shale1Thickness',
                     'shale2Thickness',
                     'shale3Thickness',
                     'aquifer1Thickness',
                     'aquifer2Thickness',
                     'reservoirThickness',
                     'datumPressure']

def read_data(filename):
    """
    Routine used for reading data files.

    Read data from file and create numpy.array if file exists.
    """
    # Check whether the file with given name exists
    if os.path.isfile(filename):
        data = np.genfromtxt(filename)
        return data


if __name__ == "__main__":
    try:
        from openiam import SimpleReservoir
    except ImportError as err:
        print('Unable to load IAM class module: '+str(err))

    __spec__ = None

    logging.basicConfig(level=logging.WARNING)
    # Define keyword arguments of the system model
    num_years = 50
    num_obs = num_years+1
    time_array = 365.25*np.arange(0.0,num_years+1)
    sm_model_kwargs = {'time_array': time_array} # time is given in days

    # Create system model
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # Add reservoir component
    sres = sm.add_component_model_object(SimpleReservoir(name='sres', parent=sm))

    # Add parameters of reservoir component model
    sres.add_par('numberOfShaleLayers', value=3, vary=False)
    sres.add_par('injRate', value=0.1, vary=False)
    sres.add_par('reservoirThickness', value=40.0, vary=False)
    sres.add_par('shale1Thickness', min=30.0, max=150., value=55.0)
    sres.add_par('shale2Thickness', min=10.0, max=150., value=20.0)
    sres.add_par('shale3Thickness', min=10.0, max=150., value=25.0)
    sres.add_par('aquifer1Thickness', min=10.0, max=150., value=51.0)
    sres.add_par('aquifer2Thickness', min=10.0, max=150., value=28.0)

    # Add observations of reservoir component model
    sres.add_obs_to_be_linked('pressure')
    sres.add_obs_to_be_linked('CO2saturation')
    sres.add_obs('pressure')
    sres.add_obs('CO2saturation')
    sres.add_obs('mass_CO2_reservoir')

    # Add multisegmented wellbore component
    ms = sm.add_component_model_object(MultisegmentedWellbore(name='ms',parent=sm))
    ms.add_par('wellRadius', min=0.01, max=0.02, value=0.015)

    # Add linked parameters: common to both components
    ms.add_par_linked_to_par('numberOfShaleLayers',
                             sres.deterministic_pars['numberOfShaleLayers'])
    ms.add_par_linked_to_par('shale1Thickness', sres.pars['shale1Thickness'])
    ms.add_par_linked_to_par('shale2Thickness', sres.pars['shale2Thickness'])
    ms.add_par_linked_to_par('shale3Thickness', sres.pars['shale3Thickness'])
    ms.add_par_linked_to_par('aquifer1Thickness', sres.pars['aquifer1Thickness'])
    ms.add_par_linked_to_par('aquifer2Thickness', sres.pars['aquifer2Thickness'])
    ms.add_par_linked_to_par('reservoirThickness',
                             sres.deterministic_pars['reservoirThickness'])
    ms.add_par_linked_to_par('datumPressure',
                             sres.default_pars['datumPressure'])

    # Add keyword arguments linked to the output provided by reservoir model
    ms.add_kwarg_linked_to_obs('pressure', sres.linkobs['pressure'])
    ms.add_kwarg_linked_to_obs('CO2saturation', sres.linkobs['CO2saturation'])

    # Add observations of multisegmented wellbore component model
    # When add_obs method is called, system model automatically
    # (if no other options of the method is used) adds a list of observations
    # with names ms.obsnm_0, ms.obsnm_1,.... The final index is determined by the
    # number of time points in system model time_array attribute.
    # For more information, see the docstring of add_obs of ComponentModel class.
    ms.add_obs('CO2_aquifer1')
    ms.add_obs('CO2_aquifer2')
    ms.add_obs('brine_aquifer1')
    ms.add_obs('brine_aquifer2')
    ms.add_obs('mass_CO2_aquifer1')
    ms.add_obs('mass_CO2_aquifer2')

    # Run system model using current values of its parameters
    sm.forward()  # system model is run deterministically

    print('------------------------------------------------------------------')
    print('                  Forward method illustration ')
    print('------------------------------------------------------------------')
    # Since the observations at the particular time points are different variables,
    # method collect_observations_as_time_series creates lists of
    # values of observations belonging to a given component (e.g. cw) and having the same
    # common name (e.g. 'CO2_aquifer1', etc) but differing in indices.
    # More details are given in the docstring and documentation to the method
    # collect_observations_as_time_series of SystemModel class.
    print('pressure', sm.collect_observations_as_time_series(sres,'pressure'), sep='\n')
    print('------------------------------------------------------------------')
    print('CO2saturation', sm.collect_observations_as_time_series(sres,'CO2saturation'), sep='\n')
    print('------------------------------------------------------------------')
    print('CO2_aquifer1', sm.collect_observations_as_time_series(ms,'CO2_aquifer1'), sep='\n')
    print('------------------------------------------------------------------')
    print('CO2_aquifer2', sm.collect_observations_as_time_series(ms,'CO2_aquifer2'), sep='\n')
    print('------------------------------------------------------------------')
    print('mass_CO2_reservoir', sm.collect_observations_as_time_series(sres,'mass_CO2_reservoir'), sep='\n')
    print('------------------------------------------------------------------')
    print('mass_CO2_aquifer1', sm.collect_observations_as_time_series(ms,'mass_CO2_aquifer1'), sep='\n')
    print('------------------------------------------------------------------')
    print('mass_CO2_aquifer2', sm.collect_observations_as_time_series(ms,'mass_CO2_aquifer2'), sep='\n')

    print('------------------------------------------------------------------')
    print('                          UQ illustration ')
    print('------------------------------------------------------------------')

    import random
    num_samples = 50
    ncpus = 1
    # Draw Latin hypercube samples of parameter values
    seed = random.randint(500,1100)
#    s = sm.lhs(siz=num_samples,seed=345)
    s = sm.lhs(siz=num_samples,seed=seed)

    # Run model using values in samples for parameter values
    results = s.run(cpus=ncpus,verbose=False)

    # Extract results from stochastic simulations
    CO2_aquifer1 = np.ones((num_samples,len(time_array)))
    CO2_aquifer2 = np.ones((num_samples,len(time_array)))
    brine_aquifer1 = np.ones((num_samples,len(time_array)))
    brine_aquifer2 = np.ones((num_samples,len(time_array)))
    mass_CO2_aquifer1 = np.ones((num_samples,len(time_array)))
    mass_CO2_aquifer2 = np.ones((num_samples,len(time_array)))
    mass_CO2_reservoir = np.ones((num_samples,len(time_array)))

    for ind in range(len(time_array)):
        CO2_aquifer1[:,ind] = s.recarray['ms.CO2_aquifer1_{}'.format(ind)]
        CO2_aquifer2[:,ind] = s.recarray['ms.CO2_aquifer2_{}'.format(ind)]
        brine_aquifer1[:,ind] = s.recarray['ms.brine_aquifer1_{}'.format(ind)]
        brine_aquifer2[:,ind] = s.recarray['ms.brine_aquifer2_{}'.format(ind)]
        mass_CO2_aquifer1[:,ind] = s.recarray['ms.mass_CO2_aquifer1_{}'.format(ind)]
        mass_CO2_aquifer2[:,ind] = s.recarray['ms.mass_CO2_aquifer2_{}'.format(ind)]
        mass_CO2_reservoir[:,ind] = s.recarray['sres.mass_CO2_reservoir_{}'.format(ind)]

    # Plot results
    fig = plt.figure(figsize=(18,7))
    ax = fig.add_subplot(231)
    fraction = np.zeros((num_samples,len(time_array)))
    fraction[:,1:] = (mass_CO2_aquifer1 + mass_CO2_aquifer2)[:,1:]/(
        mass_CO2_aquifer1 + mass_CO2_aquifer2 + mass_CO2_reservoir)[:,1:]
    for j in range(num_samples):
        plt.semilogy(time_array[1:]/365.25, fraction[j,1:], color='#1B4F72', linewidth=1)
    plt.xlabel('Time, t (years)', fontsize=14)
    plt.ylabel(r'Fraction value', fontsize=14)
    plt.title(r'Mass of CO$_2$ leaked to mass of CO$_2$ injected', fontsize=18)
    plt.tick_params(labelsize=12)
    plt.xlim([0,50])
    ax.get_yaxis().set_label_coords(-0.13,0.5)

    ax = fig.add_subplot(232)
    for j in range(num_samples):
        plt.semilogy(time_array/365.25, CO2_aquifer1[j], color='#000066', linewidth=1)
    plt.xlabel('Time, t (years)', fontsize=14)
    plt.ylabel('Leakage rates, q (kg/s)', fontsize=14)
    plt.title(r'Leakage of CO$_2$: aquifer 1', fontsize=18)
    plt.tick_params(labelsize=12)
    plt.xlim([0,50])
    ax.get_yaxis().set_label_coords(-0.13,0.5)

    ax = fig.add_subplot(233)
    for j in range(num_samples):
        plt.semilogy(time_array/365.25, CO2_aquifer2[j], color='#0000AA', linewidth=1)
    plt.xlabel('Time, t (years)', fontsize=14)
    plt.ylabel('Leakage rates, q (kg/s)', fontsize=14)
    plt.title(r'Leakage of CO$_2$: aquifer 2', fontsize=18)
    plt.tick_params(labelsize=12)
    plt.xlim([0,50])
    ax.get_yaxis().set_label_coords(-0.13,0.5)

    ax = fig.add_subplot(234)
    for j in range(num_samples):
        plt.semilogy(time_array/365.25, brine_aquifer1[j], color='#663300', linewidth=1)
    plt.xlabel('Time, t (years)', fontsize=14)
    plt.ylabel('Leakage rates, q (kg/s)', fontsize=14)
    plt.title(r'Leakage of brine: aquifer 1', fontsize=18)
    plt.tight_layout()
    plt.tick_params(labelsize=12)
    plt.xlim([0,50])
    ax.get_yaxis().set_label_coords(-0.14,0.5)

    ax = fig.add_subplot(235)
    for j in range(num_samples):
        plt.semilogy(time_array/365.25, brine_aquifer2[j], color='#CC9900', linewidth=1)
    plt.xlabel('Time, t (years)', fontsize=14)
    plt.ylabel('Leakage rates, q (kg/s)', fontsize=14)
    plt.title(r'Leakage of brine: aquifer 2', fontsize=18)
    plt.tight_layout()
    plt.tick_params(labelsize=12)
    plt.xlim([0,50])
    plt.ylim([1.0e-16,1.0e-6])
    ax.get_yaxis().set_label_coords(-0.14,0.5)

    to_save = False
    if to_save:
        plt.savefig('LeakageRatesCombinedPlotVsTime2.png',dpi=300)