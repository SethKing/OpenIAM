# -*- coding: utf-8 -*-
"""
The Carbonate Aquifer component model is a reduced-order model that can be
used to predict the impact that carbon dioxide (|CO2|) and brine leaks from a
|CO2| storage reservoir might have on overlying aquifers. The model predicts
the size of “impact plumes” according to nine water quality metrics, see
:cite:`RN1087`, :cite:`RN631`, :cite:`RN1605`.

Although the carbonate aquifer ROM was developed using site-specific data
from the Edwards aquifer, the model accepts aquifer characteristics as
variable inputs and, therefore, may have more broad applicability. Careful
consideration should be given to the hydrogeochemical character of the
aquifer before using this model at a new site. Guidelines and examples are
presented in :cite:`RN1635`.

The size of “impact plumes” are calculated using two alternative
definitions of “impact” which should be selected by user: 1) changes that cause an
exceedance of a drinking water standard or maximum contaminant level (MCL);
and 2) changes that are above and beyond “natural background variability” in
the aquifer, :cite:`RN1608`.

Component model input definitions:

* **ithresh** [-] (1 or 2) - threshold, either 1: MCL or 2: No-impact
  (default 2)

* **rmin** [m] (0 to 100) - maximum distance between leaks for them to be
  considered one leak (default 15 m)

* **perm_var** [|log10| |m^4|] (0.017 to 1.89) - logarithm of permeability
  variance (default 0.9535)

* **corr_len** [m] (1 to 3.95) - correlation length (default 2.475 m)

* **aniso** [-] (1.1 to 49.1) - anisotropy factor, ratio of horizontal to
  vertical permeability (default 25.1)

* **mean_perm** [|log10| |m^2|] (-13.8 to -10.3) - logarithm of mean
  permeability (default -12.05)

* **hyd_grad** [-] (2.88e-4 to 1.89e-2) - horizontal hydraulic gradient
  (default 9.59e-03)

* **calcite_ssa** [|m^2|/g] (0 to 1.e-2) - calcite surface area
  (default 5.5e-03 |m^2|/g)

* **organic_carbon** [-] (0 to 1.e-2) - organic carbon volume fraction
  (default 5.5e-03)

* **benzene_kd** [|log10| K_oc] (1.49 to 1.73) - benzene distribution
  coefficient (default 1.61)

* **benzene_decay** [|log10| day] (0.15 to 2.84) - benzene decay constant
  (default 0.595)

* **nap_kd** [|log10| K_oc] (2.78 to 3.18) - naphthalene distribution
  coefficient (default 2.98)

* **nap_decay** [|log10| day] (-0.85 to 2.04) - naphthalene decay constant
  (default 0.595)

* **phenol_kd** [|log10| K_oc] (1.21 to 1.48) - phenol distribution coefficient
  (default 1.35)

* **phenol_decay** [|log10| day] (-1.22 to 2.06) - phenol decay constant
  (default 0.42 |log10| day)

* **cl** [|log10| Molality] (0.1 to 6.025) - brine salinity (default 0.776)

* **logf** [integer] (0 to 1) - log transform of output plume volume:
  0=linear, 1=log (default 0)

* **aqu_thick** [m] (100 to 500) - aquifer thickness (default 300 m).
  *Linked to Stratigraphy.*

Possible observations from the Carbonate Aquifer component are:

* **pH** [|m^3|] - volume of aquifer below pH threshold.
* **Flux** [kg/s] - |CO2| flux to atmosphere.
* **dx** [m] - width of impacted aquifer volume in x-direction.
* **dy** [m] - width of impacted aquifer volume in y-direction.
* **TDS** [|m^3|] - volume of aquifer above TDS threshold in mg/L.
* **As** [|m^3|] - volume of aquifer above arsenic threshold in μg/L.
* **Pb** [|m^3|] - volume of aquifer above lead threshold in μg/L.
* **Cd** [|m^3|] - volume of aquifer above cadmium threshold in μg/L.
* **Ba** [|m^3|] - volume of aquifer above barium threshold in μg/L.
* **Benzene** [|m^3|] - volume of aquifer above benzene threshold.
* **Naphthalene** [|m^3|] - volume of aquifer above naphthalene threshold.
* **Phenol** [|m^3|] - volume of aquifer above phenol threshold.

"""
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import matplotlib.pyplot as plt
import logging

try:
    from openiam import SystemModel, ComponentModel
except ImportError as err:
    print('Unable to load IAM class module: '+str(err))

try:
    import components.aquifer as aqmodel
except ImportError as err:
    print('\nERROR: Unable to load ROM for aquifer component\n')
    sys.exit()

import numpy as np
import ctypes
from sys import platform

class CarbonateAquifer(ComponentModel):
    def __init__(self, name, parent, model_args=[], model_kwargs={}, workdir=None, header_file_dir=None):
        """
        Constructor method of CarbonateAquifer class

        :param name: name of component model
        :type name: str

        :param parent: the SystemModel object that the component model
            belongs to
        :type parent: SystemModel object

        :param model_args: additional optional parameters of the component
            model; by default, model_args is an empty list []
        :type model_args: [float]

        :param model_kwargs: additional optional keyword arguments of the
            component model; by default, model_kwargs is an empty dictionary {}.
        :type model_kwargs: dict

        :param workdir: name of directory to use for model runs (serial run
            case)
        :type workdir: str

        :param header_file_dir: name of directory with Fortran DLL
        :type header_file_dir: str

        :returns: CarbonateAquifer class object
        """
        if header_file_dir is None:
            model_kwargs['header_file_dir'] = aqmodel.__path__[0]
            # print(aqmodel.__path__[0])
        else:
            model_kwargs['header_file_dir'] = header_file_dir

        super(CarbonateAquifer, self).__init__(name, parent, model=self.model, model_args=model_args, model_kwargs=model_kwargs, workdir=workdir)

        # Placeholder for keyword arguments of the 'model' method:
        # to let the system model know that this component needs the specified keyword arguments
        self.model_kwargs['time_point'] = 365.25  # default value of 365.25 days
        self.model_kwargs['time_step'] = 365.25   # default value of 365.25 days

        # Set default parameters of the component model
        self.add_default_par('ithresh', value=2)
        self.add_default_par('rmin', value=15.0)
        self.add_default_par('perm_var', value=0.9535)
        self.add_default_par('corr_len', value=2.475)
        self.add_default_par('aniso', value=25.1)
        self.add_default_par('mean_perm', value=-12.05)
        self.add_default_par('aqu_thick', value=300.)
        self.add_default_par('hyd_grad', value=9.59e-3)
        self.add_default_par('calcite_ssa', value=5.5e-03)
        self.add_default_par('organic_carbon', value=5.5e-03)
        self.add_default_par('benzene_kd', value=1.61)
        self.add_default_par('benzene_decay', value=1.5)
        self.add_default_par('nap_kd', value=2.98)
        self.add_default_par('nap_decay', value=0.595)
        self.add_default_par('phenol_kd', value=1.35)
        self.add_default_par('phenol_decay', value=0.42)
        self.add_default_par('cl', value=0.776)
        self.add_default_par('logf', value=0)

        # Define dictionary of boundaries
        self.pars_bounds = dict()
        self.pars_bounds['ithresh'] = [1, 2]
        self.pars_bounds['rmin'] = [0, 100]
        self.pars_bounds['perm_var'] = [0.017, 1.89]
        self.pars_bounds['corr_len'] = [1.0, 3.95]
        self.pars_bounds['aniso'] = [1.1, 49.1]
        self.pars_bounds['mean_perm'] = [-13.8, -10.3]
        self.pars_bounds['aqu_thick'] = [100., 500.]
        self.pars_bounds['hyd_grad'] = [2.88e-4, 1.89e-2]
        self.pars_bounds['calcite_ssa'] = [0, 1.e-2]
        self.pars_bounds['organic_carbon'] = [0, 1.e-2]
        self.pars_bounds['benzene_kd'] = [1.49, 1.73]
        self.pars_bounds['benzene_decay'] = [0.15, 2.84]
        self.pars_bounds['nap_kd'] = [2.78, 3.18]
        self.pars_bounds['nap_decay'] = [-0.85, 2.04]
        self.pars_bounds['phenol_kd'] = [1.21, 1.48]
        self.pars_bounds['phenol_decay'] = [-1.22, 2.06]
        self.pars_bounds['cl'] = [0.1, 6.025]
        self.pars_bounds['logf'] = [0, 1]

        # Define dictionary of temporal data limits
        self.temp_data_bounds = dict()
        self.temp_data_bounds['co2_rate'] = ['CO2 Rate', 0., 0.5]
        self.temp_data_bounds['brine_rate'] = ['Brine Rate', 0., 0.075]
        self.temp_data_bounds['co2_mass'] = ['CO2 Mass', 0., 2.e+9]
        self.temp_data_bounds['brine_mass'] = ['Brine Mass', 0., 2.e+8]

        logging.debug('CarbonateAquifer created with name {name}'.format(name=name))

    def check_input_parameters(self, p):
        """
        Check whether input parameters fall within specified boundaries.

        :param p: input parameters of component model
        :type p: dict
        """
        logging.debug('Input parameters of {name} component are {p}'.format(name=self.name,p=p))

        for key, val in p.items():
            if key in self.pars_bounds:
                if ((val < self.pars_bounds[key][0])or
                    (val > self.pars_bounds[key][1])):
                    logging.warning('Parameter ' + key + ' is out of bounds.')
            else:
                logging.warning('Parameter {key} not recognized as a Carbonate Aquifer input parameter.'
                                .format(key=key))

    def check_temporal_inputs(self, time, temp_inputs):
        """
        Check whether temporal data fall within specified boundaries.

        :param temp_inputs: temporal input data of component model
        :type temp_inputs: dict
        """
        for key, vals in temp_inputs.items():
            for val in vals:
                if ((val < self.temp_data_bounds[key][1]) or
                   (val > self.temp_data_bounds[key][2])):
                    logging.warning(' {0} {1} is outside the model range {2} to {3} at time t = {4}.'
                                    .format(self.temp_data_bounds[key][0],
                                            val,
                                            self.temp_data_bounds[key][1],
                                            self.temp_data_bounds[key][2],
                                            time))

    # carbonate aquifer model function
    def model(self, p, x=[], y=[], co2_rate=[], brine_rate=[],
              co2_mass=[], brine_mass=[], time_point=365.25, time_step=365.25,
              header_file_dir=None):
        """
        Return volume of impacted aquifer based on several metrics.

        :param p: input parameters of carbonate aquifer model
        :type p: dict

        :param x: horizontal coordinate of leaking well (m)
        :type x: [float]

        :param y: horizontal coordinate of leaking well (m)
        :type y: [float]

        :param co2_rate: |CO2| leakage rate, kg/s
        :type co2_rate: [float]

        :param brine_rate: brine leakage rate, kg/s
        :type brine_rate: [float]

        :param co2_mass: cumulative |CO2| mass leaked, kg
        :type co2_mass: [float]

        :param brine_mass: cumulative brine mass leaked, kg
        :type brine_mass: [float]

        :param time_point: time point in days at which the leakage rates are
            to be calculated; by default its value is 365.25 (1 year in days)
        :type time_point: float

        :param time_step: difference between the current and previous
            time points in days; by default its value is 365.25 (1 year in days)
        :type time_step: float
        
        :param header_file_dir: path to the directory that contains
            the compiled Fortran library 
        :type workdir: str

        :returns: vol - dictionary of observations of carbonate aquifer impacted volumes
            model; keys:
            ['pH','Flux','dx','dy','TDS','As','Pb','Cd','Ba','Benzene','Napthalene','Phenol']

        """

        # Obtain the default values of the parameters from dictionary of default parameters
        actual_p = dict([(k,v.value) for k,v in self.default_pars.items()])

        # Update default values of parameters with the provided ones
        actual_p.update(p)

        # determine number of leaks
        # x,y,co2_rate,brine_rate,co2_mass,brine_mass must all be same length
        nleak = len(x)

        # Check whether pressure and saturation inputs satisfy the model requirements
        self.check_temporal_inputs(time_point, dict(list(zip(
            ['co2_rate', 'brine_rate'], [co2_rate, brine_rate]))))

        # convert rates and masses to correct units
        co2_rate_conv = [val * 1000. for val in co2_rate] # kg/s -> g/s
        brine_rate_conv = [val * 1000. for val in brine_rate]
        co2_mass_conv = [val * 1e-6 for val in co2_mass] # kg -> kTon
        brine_mass_conv = [val * 1e-6 for val in brine_mass]

        # get time in days from keyword arguments, convert to years
        time = time_point/365.25

        # 1:MCL, 2:No-Impact
        ithresh = actual_p['ithresh']

        # This is not defined in AIM
        rmin = actual_p['rmin']

        # aquifer parameters
        aquifer = np.array([actual_p['perm_var'],actual_p['corr_len'],actual_p['aniso'],actual_p['mean_perm'],
            actual_p['aqu_thick']/100.,actual_p['hyd_grad']])
        # ROM expects aquifer thickness in hundreds of meters

        # geochemical parameters
        chem = np.array([actual_p['calcite_ssa'],actual_p['organic_carbon'],actual_p['benzene_kd'],
            actual_p['nap_kd'],actual_p['phenol_kd'],actual_p['benzene_decay'],actual_p['nap_decay'],
            actual_p['phenol_decay']])

        # log brine salinity
        cl = []
        for i in range(nleak):
            cl.append(actual_p['cl'])

        # log transform of output plume volume 0=linear, 1=log
        logf = actual_p['logf']*np.ones((12,), dtype=np.int)

        # Specify name of and path to dynamic library
        if platform == "linux" or platform == "linux2":
            # linux
            library_name = "carbonate.so"
        elif platform == "darwin":
            # OS X
            library_name = "carbonate.dylib"
        elif platform == "win32":
            # Windows...
            library_name = "carbonate.dll"
        library = os.sep.join([header_file_dir,'carbonate',library_name])

        # Load the dynamic library
        external_lib = ctypes.cdll.LoadLibrary(os.path.join(os.getcwd(),library))

        # Specify Fortran function name
        functionName = "gw_plumes"

        # Get needed function as attribute of the library
        function = getattr(external_lib, functionName)

        # Define C classes
        INT = ctypes.c_int
        DOUBLE = ctypes.c_double
        EightDoubles = DOUBLE*8
        SixDoubles = DOUBLE*6
        TwelveDoubles = DOUBLE*12
        LEAKS = DOUBLE*nleak
        TwelveInts = INT*12

        # Set argument types for value and array pointers
        function.argtypes = [ctypes.POINTER(INT),
                             ctypes.POINTER(DOUBLE),
                             ctypes.POINTER(SixDoubles),
                             ctypes.POINTER(DOUBLE),
                             ctypes.POINTER(INT),
                             ctypes.POINTER(LEAKS),
                             ctypes.POINTER(LEAKS),
                             ctypes.POINTER(LEAKS),
                             ctypes.POINTER(LEAKS),
                             ctypes.POINTER(LEAKS),
                             ctypes.POINTER(LEAKS),
                             ctypes.POINTER(LEAKS),
                             ctypes.POINTER(TwelveDoubles),
                             ctypes.POINTER(EightDoubles),
                             ctypes.POINTER(TwelveInts)]
        function.restype = None

        # Define values of the input parameters that will be passed to Fortran function
        par_ithresh = INT(ithresh)
        par_rmin = DOUBLE(rmin)
        par_aquifer = SixDoubles(*aquifer)
        par_time = DOUBLE(time)
        par_nleak = INT(nleak)
        par_x = LEAKS(*x)
        par_y = LEAKS(*y)
        par_cl = LEAKS(*cl)
        par_co2rate = LEAKS(*co2_rate_conv)
        par_co2mass = LEAKS(*co2_mass_conv)
        par_brinerate = LEAKS(*brine_rate_conv)
        par_brinemass = LEAKS(*brine_mass_conv)
        par_chem = EightDoubles(*chem)
        par_logf = TwelveInts(*logf)

        out_vol = TwelveDoubles()    # initialize output array

        # change to the directory where the dynamic library is located
        currentWorkDir = os.getcwd()
        targetWorkDir = os.path.join(os.getcwd(),header_file_dir)
        os.chdir(targetWorkDir)

        # call the Fortran function
        function(par_ithresh,par_rmin,par_aquifer,par_time,par_nleak,
                 par_x,par_y,par_cl,par_co2rate,par_co2mass,
                 par_brinerate,par_brinemass,out_vol,par_chem,par_logf)

        # return to original directory
        os.chdir(currentWorkDir)

        # return a dictionary of the output volumes
        labels = ['pH','Flux','dx','dy','TDS','As','Pb','Cd','Ba','Benzene','Naphthalene','Phenol']
        vol = dict(list(zip(labels,out_vol)))
        return vol

    # Attributes for system connections
    system_params = ['{aquifer_name}Thickness']
    system_collected_inputs = {'co2_rate': 'CO2_{aquifer_name}',
                        'brine_rate': 'brine_{aquifer_name}',
                        'co2_mass': 'mass_CO2_{aquifer_name}',
                        'brine_mass': 'mass_brine_{aquifer_name}'}
    adapters = ['RateToMass']
    needsXY = True

    def connect_with_system(self, component_data, name2obj_dict,
                            welllocs=None, num_pathways=1):
        """
        Code to add carbonate aquifer to system model for control file interface.

        :param component_data: Dictionary of component data including 'Connection'
        :type component_data: dict

        :param name2obj_dict: Dictionary to map component names to objects
        :type name2obj: dict

        :param welllocs: dictionary of x and y coordinates of leaking wellbores,
            contains keys 'coordx' and 'coordy'
        :type welllocs: dict

        :param num_pathways: number of leakage pathways
        :type num_pathways: int

        :returns: None
        """
        if ('Parameters' in component_data) and (component_data['Parameters']):
            for key in component_data['Parameters']:
                if type(component_data['Parameters'][key]) != dict:
                    component_data['Parameters'][key] = {'value': component_data['Parameters'][key],
                                                         'vary': False}
                self.add_par(key, **component_data['Parameters'][key])

        if ('DynamicParameters' in component_data) and (component_data['DynamicParameters']):
            for key in component_data['DynamicParameters']:
                self.add_dynamic_kwarg(key, component_data['DynamicParameters'][key])

        self.model_kwargs['x'] = welllocs['coordx'][:num_pathways]
        self.model_kwargs['y'] = welllocs['coordy'][:num_pathways]

        # Make model connections
        if 'Connection' in component_data:
            collectors = self._parent.collectors
            for collect in collectors:
                cdict = collectors[collect]
                argument = cdict['argument'].format(aquifer_name=component_data['AquiferName'])
                connect = cdict['Connection']
                for i in range(num_pathways):
                    aname = 'adapter_{0:03}'.format(i)
                    cname = connect + '_{0:03}'.format(i)
                    adapter = name2obj_dict[aname]
                    connector = name2obj_dict[cname]
                    if(argument in adapter.linkobs):
                        collectors[collect]['data'].append(adapter.linkobs[argument])
                    else:
                        collectors[collect]['data'].append(connector.linkobs[argument])
            system_collected_inputs = {'co2_rate': 'CO2_{aquifer_name}',
                                       'brine_rate': 'brine_{aquifer_name}',
                                       'co2_mass': 'mass_CO2_{aquifer_name}',
                                       'brine_mass': 'mass_brine_{aquifer_name}'}
            for sinput in system_collected_inputs:
                self.add_kwarg_linked_to_collection(sinput,
                                                    collectors[sinput]['data'])
        # End Connection if statement

        sparam = '{aq}Thickness'.format(aq=component_data['AquiferName'])
        strata = name2obj_dict['strata']
        connect = None
        if sparam in strata.pars:
            connect = strata.pars
        elif sparam in strata.deterministic_pars:
            connect = strata.deterministic_pars
        elif sparam in strata.default_pars:
            connect = strata.default_pars
        if not connect:
            sparam = 'aquiferThickness'
            if sparam in strata.pars:
                connect = strata.pars
            elif sparam in strata.deterministic_pars:
                connect = strata.deterministic_pars
            elif sparam in strata.default_pars:
                connect = strata.default_pars
            else:
                print('Unable to find parameter ' + sparam)

        self.add_par_linked_to_par('aqu_thick', connect[sparam])


if __name__ == "__main__":
    # Create system model
    time_array = 365.25*np.arange(0.0,2.0)
    sm_model_kwargs = {'time_array': time_array} # time is given in days
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # Add carbonate aquifer model object and define parameters
    ca = sm.add_component_model_object(CarbonateAquifer(name='ca',parent=sm))
    ca.add_par('perm_var', value=1.24e+00)
    ca.add_par('corr_len', value=2.01E+00)
    ca.add_par('aniso', value=4.36e+01)
    ca.add_par('mean_perm', value=-1.14E+01)
    ca.add_par('aqu_thick', value=4.71E+02)
    ca.add_par('hyd_grad', value=1.63e-02)
    ca.add_par('calcite_ssa', value=4.13E-03)
    ca.add_par('cl', value=4.59E+00)
    ca.add_par('organic_carbon', value=5.15e-03)
    ca.add_par('benzene_kd', value=1.53e+00)
    ca.add_par('nap_kd', value=2.78e+00)
    ca.add_par('phenol_kd', value=1.29e+00)
    ca.add_par('benzene_decay', value=1.58e+00)
    ca.add_par('nap_decay', value=3.65e-01)
    ca.add_par('phenol_decay', value=2.61e-01)
    ca.model_kwargs['x'] = [0.]
    ca.model_kwargs['y'] = [0.]
    ca.model_kwargs['co2_rate'] = [1.90e-02] # kg/s
    ca.model_kwargs['co2_mass'] = [6.00e+05] # kg
    ca.model_kwargs['brine_rate'] = [4.62e-03] # kg/s
    ca.model_kwargs['brine_mass'] = [1.46e+05] # kg

    # Add observations (output) from the carbonate aquifer model
    # When add_obs method is called, system model automatically
    # (if no other options of the method is used) adds a list of observations
    # with names ca.obsnm_0, ca.obsnm_1,.... The final index is determined by the
    # number of time points in system model time_array attribute.
    # For more information, see the docstring of add_obs of ComponentModel class.
    ca.add_obs('pH')
    ca.add_obs('Flux')
    ca.add_obs('dx')
    ca.add_obs('dy')
    ca.add_obs('TDS')
    ca.add_obs('As')
    ca.add_obs('Pb')
    ca.add_obs('Cd')
    ca.add_obs('Ba')
    ca.add_obs('Benzene')
    ca.add_obs('Naphthalene')
    ca.add_obs('Phenol')

    # Run the system model
    sm.forward()

    # Print the observations
    # Since the observations at the particular time points are different variables,
    # method collect_observations_as_time_series creates lists of
    # values of observations belonging to a given component (e.g. ca) and having the same
    # common name (e.g. 'pH', 'Flux', etc) but differing in indices.
    # More details are given in the docstring and documentation to the method
    # collect_observations_as_time_series of SystemModel class.
    print('pH:', sm.collect_observations_as_time_series(ca,'pH'))
    print('Flux:', sm.collect_observations_as_time_series(ca,'Flux'))
    print('dx:', sm.collect_observations_as_time_series(ca,'dx'))
    print('dy:', sm.collect_observations_as_time_series(ca,'dy'))
    print('TDS:', sm.collect_observations_as_time_series(ca,'TDS'))
    print('As:', sm.collect_observations_as_time_series(ca,'As'))
    print('Pb:', sm.collect_observations_as_time_series(ca,'Pb'))
    print('Cd:', sm.collect_observations_as_time_series(ca,'Cd'))
    print('Ba:', sm.collect_observations_as_time_series(ca,'Ba'))
    print('Benzene:', sm.collect_observations_as_time_series(ca,'Benzene'))
    print('Naphthalene:', sm.collect_observations_as_time_series(ca,'Naphthalene'))
    print('Phenol:', sm.collect_observations_as_time_series(ca,'Phenol'))

    # Expected output
#    ('pH', array([ 0.,  0.]))
#    ('Flux', array([ 0.,  0.]))
#    ('dx', array([ 97.81492584,  97.81492584]))
#    ('dy', array([ 61.16387968,  61.16387968]))
#    ('TDS', array([ 18940957.05166385,  18925088.43222359]))
#    ('As', array([ 318247.57621476,  318247.57621476]))
#    ('Pb', array([ 0.,  0.]))
#    ('Cd', array([ 2301435.20549617,  2301637.01148604]))
#    ('Ba', array([ 0.,  0.]))
#    ('Benzene', array([ 36434231.34681176,  36434231.34681176]))
#    ('Naphthalene', array([ 391423.9405283,  391423.9405283]))
#    ('Phenol', array([ 30248015.99834153,  30248015.99834153]))
