# -*- coding: utf-8 -*-
"""
The Alluvium Aquifer component modelis a reduced order model which can be used
to predict the changes in diluted groundwater chemistry if CO2 and brine were to
leak into an overlying alluvium aquifer similar to the High Plains aquifer,
Haskel County, Kansas, USA. The protocol allows uncertainty and variability in
aquifer heterogeneity, fluid transport and geochemical reactions to be
collectively evaluated to assess potential changes in groundwater pH, total
dissolved solids (TDS), As, Ba, Cd, Pb, benzene, naphthalene, and phenol
concentrations by developing a scaling function that can be applied to correct
the output from the hydrology ROM for geochemical reactions.

Although the alluvium aquifer ROM was developed using site-specific data
from the High Plains aquifer, the model accepts aquifer characteristics as
variable inputs and, therefore, may have more broad applicability. Careful
consideration should be given to the hydrogeochemical character of the
aquifer before using this model at a new site.

The Alluvium Aquifer will reject a number of simulation results, typically
around 20 to 30 percent of results will be rejected.  When results are rejected
the model will return NaN (Not a Number) values.  Rejected results will need to
be filtered out for analysis.  For more information on the Alluvium Aquifer
model development see Carroll et al. :cite:`Carroll2016`.

Component model input definitions:

* **co2_flux** [kg/s] (0 to 0.5001) - CO2 flux(default 0.25005 kg/s)

* **co2_mass** [|log10| (kg)] (2.23 to 9.058) - Cumulative CO2 mass(default
  5.644 |log10| (kg))

* **sandFraction** [-] (0.35 to 0.65) - Sand volume fraction(default 0.5)

* **correlationLengthX** [m] (200 to 2500) - Correlation length in
  X-direction(default 1350 m)

* **correlationLengthZ** [m] (0.5 to 25) - Correlation length in
  Z-direction(default 12.75 m)

* **permeabilityClay** [|log10| (|m^2|)] (-18 to -15) - Permeability in
  clay(default -16.5 |log10| (|m^2|))

* **NaMolality** [|log10| (Molality)] (-3 to 1) - Sodium molality(default -1
  |log10| (Molality))

* **PbMolality** [|log10| (Molality)] (-8.5 to -5) - Lead molality(default
  -6.75 |log10| (Molality))

* **benzeneMolality** [|log10| (Molality)] (0 to 1) - Benzene molality(default
  0.5 |log10| (Molality))

* **tMitigation** [years] (1 to 200) - Mitigation time(default 100.5 years)

* **brn_flux** [kg/s] (0 to 0.075) - Brine flux(default 0.0375 kg/s)

* **brn_mass** [|log10| (kg)] (4.5 to 8.124) - Cumulative brine mass(default
  6.312 |log10| (kg))

* **simtime** [years] (1 to 200) - Simulation Time(default 5.0 years)

* **CEC** [meq/100g] (0.1 to 40) - The cation exchange capacity
  (meq/100)(default 20.05 meq/100g)

* **Asbrine** [|log10| (Molality)] (-9 to -5) - Arsenic concentration in the
  leaking brine (mol/L)(default -7 |log10| (Molality))

* **Babrine** [|log10| (Molality)] (-5.1 to -2.3) - Barium concentration in
  the leaking brine (mol/L)(default -3.7 |log10| (Molality))

* **Cdbrine** [|log10| (Molality)] (-9 to -6) - Cadmium concentration in the
  leaking brine (mol/L)(default -7.5 |log10| (Molality))

* **Pbbrine** [|log10| (Molality)] (-8.5 to -5) - Lead concentration in the
  leaking brine (mol/L)(default -6.75 |log10| (Molality))

* **Benzene_brine** [|log10| (Molality)] (-8.8927 to -4.8927) - Benzene
  concentration in the leaking brine (mol/L)(default -6.8927 |log10| (Molality))

* **Benzene_kd** [L/kg] (-4.5 to 0.69) - Benzene distribution coefficient
  (L/kg)(default -1.905 L/kg)

* **Benzene_decay** [1/s] (-6.1 to 0) - Benzene degradation constant
  (1/s)(default -3.05 1/s)

* **PAH_brine** [mol/L] (-10 to -4.1) - Naphthalene concentration in the
  leaking brine (mol/L)(default -7.05 mol/L)

* **PAH_kd** [L/kg] (-3.1 to 1.98) - Naphthalene distribution coefficient
  (L/kg)(default -0.56 L/kg)

* **PAH_decay** [1/s] (-6.45 to 0) - Naphthalene degradation constant
  (1/s)(default -3.225 1/s)

* **phenol_brine** [mol/L] (-10 to -3.7) - Phenol concentration in the leaking
  brine (mol/L)(default -6.85 mol/L)

* **phenol_kd** [L/kg] (-6 to 0.15) - Phenol distribution coefficient
  (L/kg)(default -2.925 L/kg)

* **phenol_decay** [1/s] (-5.62999999999999 to 0) - Phenol degradation
  constant (1/s)(default -2.815 1/s)

* **porositySand** [-] (0.25 to 0.5) - Porosity [-](default 0.375)

* **densitySand** [kg/|m^3|] (1500 to 2500) - Density [kg/|m^3|](default 2000
  kg/|m^3|)

* **VG_mSand** [-] (0.52 to 0.79) - Van Genuchten m [-](default 0.655)

* **VG_alphaSand** [1/m] (-4.69 to -3.81) - Van Genuchten alpha [1/m] (default
  -4.25 1/m)

* **permeabilitySand** [|m^2|] (-14 to -10) - Permeability [|m^2|](default -12
  |m^2|)

* **Clbrine** [mol/L] (-2 to 0.73) - Chlorine concentration in the leaking
  brine (mol/L)(default -0.635 mol/L)

* **calcitevol** [-] (0.035 to 0.2) - Volume fraction of calcite [-](default
  0.1175)

* **V_goethite** [-] (0 to 0.2) - Volume fraction of goethite [-](default 0.1)

* **V_illite** [-] (0 to 0.3) - Volume fraction of illite [-](default 0.15)

* **V_kaolinite** [-] (0 to 0.2) - Volume fraction of kaolinite [-](default
  0.1)

* **V_smectite** [-] (0 to 0.5) - Volume fraction of smectite [-](default 0.25)

Possible observations from the Carbonate Aquifer component are:

* **TDS** [|m^3|] - volume of aquifer above TDS threshold in mg/L (TDS > 1300 mg/L).
* **pH** [|m^3|] - volume of aquifer below pH threshold (pH < 7.0 ).
* **As** [|m^3|] - volume of aquifer above arsenic threshold in μg/L (arsenic > 9.3 μg/L).
* **Ba** [|m^3|] - volume of aquifer above barium threshold in μg/L (barium > 140 μg L/L).
* **Cd** [|m^3|] - volume of aquifer above cadmium threshold in μg/L (cadmium > 0.25 μg/L).
* **Pb** [|m^3|] - volume of aquifer above lead threshold in μg/L (lead  > 0.63 μg/L).
* **Benzene** [|m^3|] - volume of aquifer above benzene threshold (benzene  > 0.03 μg/L).
* **Naphthalene** [|m^3|] - volume of aquifer above naphthalene threshold (napthalene  > 0.2 μg/L).
* **Phenol** [|m^3|] - volume of aquifer above phenol threshold (phenol  > 0.003 μg/L).
"""
# Created on June 7, 2018
# @author: Kayyum Mansoor
# mansoor1@llnl.gov
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import numpy as np
import matplotlib.pyplot as plt
import logging

try:
    from openiam import SystemModel, ComponentModel
except ImportError as err:
    print('Unable to load IAM class module: '+str(err))

try:
    import components.aquifer as aqmodel
    import components.aquifer.alluvium as aamodel
    import components.aquifer.alluvium.alluvium_aquifer_ROM as aarom
except ImportError as err:
    print('\nERROR: Unable to load ROM for aquifer component\n')
    sys.exit()


class AlluviumAquifer(ComponentModel):
    def __init__(self, name, parent, model_args=[], model_kwargs={}, workdir=None, header_file_dir=None):
        """
        Constructor method of AlluviumAquifer class

        :param name: name of component model
        :type name: str

        :param parent: the SystemModel object that the component model
            belongs to
        :type parent: SystemModel object

        :param model_args: additional optional parameters of the component
            model; by default, model_args is an empty list []
        :type model_args: [float]

        :param model_kwargs: additional optional keyword arguments of the
            component model; by default, model_kwargs is an empty dictionary {}.
        :type model_kwargs: dict

        :param workdir: name of directory to use for model runs (serial run
            case)
        :type workdir: str

        :param header_file_dir: name of directory with Fortran DLL
        :type header_file_dir: str

        :returns: AlluviumAquifer class object
        """
        if header_file_dir is None:
            model_kwargs['header_file_dir'] = aamodel.__path__[0]
            # print(aqmodel.__path__[0])
        else:
            model_kwargs['header_file_dir'] = header_file_dir

        super(AlluviumAquifer, self).__init__(name, parent, model=self.model, model_args=model_args, model_kwargs=model_kwargs, workdir=workdir)

        # Placeholder for keyword arguments of the 'model' method:
        # to let the system model know that this component needs the specified keyword arguments
        self.model_kwargs['time_point'] = 365.25  # default value of 365.25 days
        self.model_kwargs['time_step'] = 365.25   # default value of 365.25 days

        self.add_default_par('co2_flux',value=0.001)
        self.add_default_par('co2_mass',value=4.385)
        self.add_default_par('sandFraction',value=0.422)
        self.add_default_par('correlationLengthX',value=361.350)
        self.add_default_par('correlationLengthZ',value=17.382)
        self.add_default_par('permeabilityClay',value=-16.340)
        self.add_default_par('NaMolality',value=0.121)
        self.add_default_par('PbMolality',value=-5.910)
        self.add_default_par('benzeneMolality',value=0.109)
        self.add_default_par('tMitigation',value=87.914)
        self.add_default_par('brn_flux',value=0.022)
        self.add_default_par('brn_mass',value=5.930)
        self.add_default_par('simtime',value=1.0)
        self.add_default_par('CEC',value=32.073)
        self.add_default_par('Asbrine',value=-5.397)
        self.add_default_par('Babrine',value=-3.397)
        self.add_default_par('Cdbrine',value=-8.574)
        self.add_default_par('Pbbrine',value=-7.719)
        self.add_default_par('Benzene_brine',value=-8.610)
        self.add_default_par('Benzene_kd',value=-3.571)
        self.add_default_par('Benzene_decay',value=-2.732)
        self.add_default_par('PAH_brine',value=-7.118)
        self.add_default_par('PAH_kd',value=-0.985)
        self.add_default_par('PAH_decay',value=-3.371)
        self.add_default_par('phenol_brine',value=-6.666)
        self.add_default_par('phenol_kd',value=-1.342)
        self.add_default_par('phenol_decay',value=-3.546)
        self.add_default_par('porositySand',value=0.468)
        self.add_default_par('densitySand',value=2165.953)
        self.add_default_par('VG_mSand',value=0.627)
        self.add_default_par('VG_alphaSand',value=-4.341)
        self.add_default_par('permeabilitySand',value=-12.430)
        self.add_default_par('Clbrine',value=-0.339)
        self.add_default_par('calcitevol',value=0.165)
        self.add_default_par('V_goethite',value=0.004)
        self.add_default_par('V_illite',value=0.006)
        self.add_default_par('V_kaolinite',value=0.004)
        self.add_default_par('V_smectite',value=0.010)

        # Define dictionary of boundaries
        self.pars_bounds = dict()
        self.pars_bounds['sandFraction'] = [0.35,0.65]
        self.pars_bounds['correlationLengthX'] = [200,2500]
        self.pars_bounds['correlationLengthZ'] = [0.5,25.0]
        self.pars_bounds['permeabilityClay'] = [-18.0,-15.0]
        self.pars_bounds['NaMolality'] = [-3.0,1.0]
        self.pars_bounds['PbMolality'] = [-8.5,-5.0]
        self.pars_bounds['benzeneMolality'] = [0.0,1.0]
        self.pars_bounds['tMitigation'] = [1.0,200.0]
        self.pars_bounds['co2_flux'] = [0,0.5001]
        self.pars_bounds['co2_mass'] = [2.23,9.058]
        self.pars_bounds['brn_flux'] = [0,0.075]
        self.pars_bounds['brn_mass'] = [4.5,8.124]
        self.pars_bounds['simtime'] = [1.0,200.0]
        self.pars_bounds['CEC'] = [0.1,40.0]
        self.pars_bounds['Asbrine'] = [-9.0,-5.0]
        self.pars_bounds['Babrine'] = [-5.1,-2.3]
        self.pars_bounds['Cdbrine'] = [-9.0,-6.0]
        self.pars_bounds['Pbbrine'] = [-8.5,-5.0]
        self.pars_bounds['Benzene_brine'] = [-8.8927,-4.8927]
        self.pars_bounds['Benzene_kd'] = [-4.5,0.69]
        self.pars_bounds['Benzene_decay'] = [-6.1,0.0]
        self.pars_bounds['PAH_brine'] = [-10.0,-4.1]
        self.pars_bounds['PAH_kd'] = [-3.1,1.98]
        self.pars_bounds['PAH_decay'] = [-6.45,0]
        self.pars_bounds['phenol_brine'] = [-10.0,-3.7]
        self.pars_bounds['phenol_kd'] = [-6.0,0.15]
        self.pars_bounds['phenol_decay'] = [-5.62999999999999,0.0]
        self.pars_bounds['porositySand'] = [0.25,0.5]
        self.pars_bounds['densitySand'] = [1500,2500]
        self.pars_bounds['VG_mSand'] = [0.52,0.79]
        self.pars_bounds['VG_alphaSand'] = [-4.69,-3.81]
        self.pars_bounds['permeabilitySand'] = [-14.0,-10.0]
        self.pars_bounds['Clbrine'] = [-2.0,0.73]
        self.pars_bounds['calcitevol'] = [0.035,0.2]
        self.pars_bounds['V_goethite'] = [0,0.2]
        self.pars_bounds['V_illite'] = [0,0.3]
        self.pars_bounds['V_kaolinite'] = [0,0.2]
        self.pars_bounds['V_smectite'] = [0,0.5]

        # Define dictionary of temporal data limits
        self.temp_data_bounds = dict()
        self.temp_data_bounds['co2_rate'] = [0., 0.5]
        self.temp_data_bounds['brine_rate'] = [0., 0.075]
        self.temp_data_bounds['co2_mass'] = [2.23, 9.058]
        self.temp_data_bounds['brine_mass'] = [4.5, 8.124]
        self.temp_data_bounds['simtime'] = [1,200]

        logging.debug('AlluviumAquifer created with name {name}'.format(name=name))
        
    def check_input_parameters(self, p):
        """
        Check whether input parameters fall within specified boundaries.

        :param p: input parameters of component model
        :type p: dict
        """
        logging.debug('Input parameters of {name} component are {p}'.format(name=self.name,p=p))
        
        for key, val in p.items():
            if key in self.pars_bounds:
                if ((val < self.pars_bounds[key][0])or
                    (val > self.pars_bounds[key][1])):
                    logging.warning('Parameter ' + key + ' is out of bounds.')
            else:
                logging.warning('Parameter {key} not recognized as a Carbonate Aquifer input parameter.'
                                .format(key=key))

    def check_temporal_inputs(self, time, temp_inputs):
        """
        Check whether temporal data fall within specified boundaries.

        :param temp_inputs: temporal input data of component model
        :type temp_inputs: dict
        """
        for key, val in temp_inputs.items():
            if ((val < self.temp_data_bounds[key][0]) or
               (val > self.temp_data_bounds[key][1])):
                logging.warning(' {0} {1} is outside the model range {2} to {3} at time t = {4}.'
                                .format(key,val,self.temp_data_bounds[key][0],self.temp_data_bounds[key][1],time))

    def normalize_temp_inpar(self, temp_inputs):
        """
        Normalize (0-1) temporal input paramters

        :param temp_inputs: temporal input data of component model
        :type temp_inputs: dict
        
        """
        k,v=list(temp_inputs.keys())[0], list(temp_inputs.values())[0]
        nv=(v - self.temp_data_bounds[k][0])/(self.temp_data_bounds[k][1] - self.temp_data_bounds[k][0])
        return  nv

    def normalize_inpar(self, temp_inputs):
        """
        Normalize (0-1) input paramters

        :param temp_inputs: temporal input data of component model
        :type temp_inputs: dict
        """
        k,v=list(temp_inputs.keys())[0], list(temp_inputs.values())[0]
        nv=(v - self.pars_bounds[k][0])/(self.pars_bounds[k][1] - self.pars_bounds[k][0])
        return  nv
        
    def model(self, p, co2_rate=[], brine_rate=[],
              co2_mass=[], brine_mass=[], time_point=365.25, time_step=365.25,
              header_file_dir=None):

        """
        Return volume of impacted aquifer based on several metrics.

        :param p: input parameters of carbonate aquifer model
        :type p: dict

        :param co2_rate: CO2 leakage rate, kg/s
        :type co2_rate: [float]

        :param brine_mass: Brine leakage rate, kg/s
        :type brine_mass: [float]

        :param co2_mass: Cumulative CO2 mass leaked, |log10|kg
        :type co2_mass: [float]

        :param brine_mass: Cumulative brine mass leaked, |log10|kg
        :type brine_mass: [float]

        :param time_point: time point in days at which the leakage rates are
            to be calculated; by default its value is 365.25 (1 year in days)
        :type time_point: float

        :param time_step: difference between the current and previous
            time points in days; by default its value is 365.25 (1 year in days)
        :type time_step: float

        :param kwargs: additional keyword arguments for the model
        :type kwargs: dict (if provided)

        :returns: vol - dictionary of observations of carbonate aquifer impacted volumes
            model; keys:
            ['TDS','pH','As','Pb','Cd','Ba','Benzene','Naphthalene','Phenol']

        """
        # Check assign defailt values
        actual_p = dict([(k,v.value) for k,v in self.default_pars.items()])
        actual_p.update(p)

        # normalize input paramters
        for key, val in actual_p.items():  p[key]=self.normalize_inpar(dict([(key, val)]))

        # Check whether co2, brine, rates and mass inputs satisfy the model requirements
        # Conditional statement used to supressess excessive error messsages
        if co2_mass <=0: co2_mass=1e-12
        if brine_mass <=0: brine_mass=1e-12
           
        if (((time_point/365.25 >= 1) and (np.log10(co2_mass) >= self.temp_data_bounds['co2_mass'][0])) or 
            ((time_point/365.25 >= 1)  and (np.log10(brine_mass) >= self.temp_data_bounds['brine_mass'][0]))):
            self.check_temporal_inputs(time_point, dict(list(zip(['co2_rate', 'brine_rate','co2_mass','brine_mass','simtime'], [co2_rate, brine_rate, np.log10(co2_mass), np.log10(brine_mass), time_point/365.25 ]))))
        
        #apply nominal value for brine/bo2
        p['norm_co2_rate']=self.normalize_temp_inpar(dict([("co2_rate", co2_rate)]))
        p['norm_brine_rate']=self.normalize_temp_inpar(dict([("brine_rate", brine_rate)]))
        p['norm_co2_mass']=self.normalize_temp_inpar(dict([("co2_mass", np.log10(co2_mass))]))
        p['norm_brine_mass']=self.normalize_temp_inpar(dict([("brine_mass", np.log10(brine_mass))]))

        # Update default values of parameters with the provided ones
        actual_p.update(p)

        # normalize simtime
        actual_p['simtime']=self.normalize_inpar(dict([("simtime", time_point/365.25)]))

        oparam = np.array([actual_p['norm_co2_rate'], actual_p['norm_co2_mass'], actual_p['sandFraction'],
                actual_p['correlationLengthX'], actual_p['correlationLengthZ'],
                actual_p['permeabilityClay'], actual_p['NaMolality'], actual_p['PbMolality'],
                actual_p['benzeneMolality'], actual_p['tMitigation'], actual_p['norm_brine_rate'],
                actual_p['norm_brine_mass'], actual_p['simtime'], actual_p['CEC'], actual_p['Asbrine'],
                actual_p['Babrine'], actual_p['Cdbrine'], actual_p['Pbbrine'],
                actual_p['Benzene_brine'], actual_p['Benzene_kd'], actual_p['Benzene_decay'],
                actual_p['PAH_brine'], actual_p['PAH_kd'], actual_p['PAH_decay'],
                actual_p['phenol_brine'], actual_p['phenol_kd'], actual_p['phenol_decay'],
                actual_p['porositySand'], actual_p['densitySand'], actual_p['VG_mSand'],
                actual_p['VG_alphaSand'], actual_p['permeabilitySand'], actual_p['Clbrine'],
                actual_p['calcitevol'], actual_p['V_goethite'], actual_p['V_illite'],
                actual_p['V_kaolinite'], actual_p['V_smectite']])    

        labels = ['TDS','pH','As','Pb','Cd','Ba','Benzene','Naphthalene','Phenol']

        if (((time_point/365.25 >= 1) and (np.log10(co2_mass) >= self.temp_data_bounds['co2_mass'][0])) or 
            ((time_point/365.25 >= 1)  and (np.log10(brine_mass) >= self.temp_data_bounds['brine_mass'][0]))):

            self.sol = aarom.Solution('.')
            self.sol.find(oparam)
            out = dict(list(zip(labels, self.sol.Outputs)))

        else:
            out = dict(list(zip(labels, np.zeros(len(labels)))))

        return out

    # Attributes for system connections
    adapters = ['RateToMass']
    needsXY = False

    def connect_with_system(self, component_data, name2obj_dict,
                            welllocs=None, num_pathways=1):
        """
        Code to add carbonate aquifer to system model for control file interface.

        :param component_data: Dictionary of component data including 'Connection'
        :type component_data: dict

        :param name2obj_dict: Dictionary to map component names to objects
        :type name2obj: dict

        :param welllocs: dictionary of x and y coordinates of leaking wellbores,
            contains keys 'coordx' and 'coordy'
        :type welllocs: dict

        :param num_pathways: number of leakage pathways
        :type num_pathways: int

        :returns: None
        """
        if ('Parameters' in component_data) and (component_data['Parameters']):
            for key in component_data['Parameters']:
                if type(component_data['Parameters'][key]) != dict:
                    component_data['Parameters'][key] = {'value': component_data['Parameters'][key],
                                                         'vary': False}
                self.add_par(key, **component_data['Parameters'][key])

        if ('DynamicParameters' in component_data) and (component_data['DynamicParameters']):
            for key in component_data['DynamicParameters']:
                self.add_dynamic_kwarg(key, component_data['DynamicParameters'][key])

        # Make model connections
        if 'Connection' in component_data:
            connection = None
            try:
                connection = name2obj_dict[component_data['Connection']]
            except KeyError:
                pass
            pathway_i = int(component_data['Connection'][-3:])
            adapter = name2obj_dict['adapter_{0:03}'.format(pathway_i)]
            if 'AquiferName' in component_data:
                aq_name = component_data['AquiferName']
            else:
                aq_name = 'aquifer1'
            connection.add_obs_to_be_linked('CO2_{aquifer_name}'.format(aquifer_name=aq_name))
            adapter.add_obs_to_be_linked('mass_CO2_{aquifer_name}'.format(aquifer_name=aq_name))
            connection.add_obs_to_be_linked('brine_{aquifer_name}'.format(aquifer_name=aq_name))
            adapter.add_obs_to_be_linked('mass_brine_{aquifer_name}'.format(aquifer_name=aq_name))
            self.add_kwarg_linked_to_obs('co2_rate',
                                         connection.linkobs['CO2_{aquifer_name}'.format(
                                                 aquifer_name=aq_name)])
            self.add_kwarg_linked_to_obs('co2_mass',
                                         adapter.linkobs['mass_CO2_{aquifer_name}'.format(
                                                 aquifer_name=aq_name)])
            self.add_kwarg_linked_to_obs('brine_rate',
                                         connection.linkobs['brine_{aquifer_name}'.format(
                                                 aquifer_name=aq_name)])
            self.add_kwarg_linked_to_obs('brine_mass',
                                         adapter.linkobs['mass_brine_{aquifer_name}'.format(
                                                 aquifer_name=aq_name)])


if __name__ == "__main__":
    # Create system model
    time_array = 365.25*np.arange(0.0,2.0)
    sm_model_kwargs = {'time_array': time_array} # time is given in days
    sm = SystemModel(model_kwargs=sm_model_kwargs)
    
    # Add alluvium aquifer model object and define parameters
    aa = sm.add_component_model_object(AlluviumAquifer(name='aa',parent=sm))

    aa.add_par('sandFraction', value=0.422)
    aa.add_par('correlationLengthX', value=361.350)
    aa.add_par('correlationLengthZ', value=17.382)
    aa.add_par('permeabilityClay', value=-16.340)
    aa.add_par('NaMolality', value=0.121)
    aa.add_par('PbMolality', value=-5.910)
    aa.add_par('benzeneMolality', value=0.109)
    aa.add_par('tMitigation', value=87.914)
    aa.add_par('simtime', value=1.995)
    aa.add_par('CEC', value=32.073)
    aa.add_par('Asbrine', value=-5.397)
    aa.add_par('Babrine', value=-3.397)
    aa.add_par('Cdbrine', value=-8.574)
    aa.add_par('Pbbrine', value=-7.719)
    aa.add_par('Benzene_brine', value=-8.610)
    aa.add_par('Benzene_kd', value=-3.571)
    aa.add_par('Benzene_decay', value=-2.732)
    aa.add_par('PAH_brine', value=-7.118)
    aa.add_par('PAH_kd', value=-0.985)
    aa.add_par('PAH_decay', value=-3.371)
    aa.add_par('phenol_brine', value=-6.666)
    aa.add_par('phenol_kd', value=-1.342)
    aa.add_par('phenol_decay', value=-3.546)
    aa.add_par('porositySand', value=0.468)
    aa.add_par('densitySand', value=2165.953)
    aa.add_par('VG_mSand', value=0.627)
    aa.add_par('VG_alphaSand', value=-4.341)
    aa.add_par('permeabilitySand', value=-12.430)
    aa.add_par('Clbrine', value=-0.339)
    aa.add_par('calcitevol', value=0.165)
    aa.add_par('V_goethite', value=0.004)
    aa.add_par('V_illite', value=0.006)
    aa.add_par('V_kaolinite', value=0.004)
    aa.add_par('V_smectite', value=0.010)

    aa.model_kwargs['co2_rate'] = 1.90e-02 # kg/s
    aa.model_kwargs['co2_mass'] = 6.00e+05 # kg
    aa.model_kwargs['brine_rate'] = 4.62e-03 # kg/s
    aa.model_kwargs['brine_mass'] = 1.46e+05 # kg

    # Add observations (output) from the alluvium aquifer model
    # When add_obs method is called, system model automatically
    # (if no other options of the method is used) adds a list of observations
    # with names aa.obsnm_0, aa.obsnm_1,.... The final index is determined by the
    # number of time points in system model time_array attribute.
    # For more information, see the docstring of add_obs of ComponentModel class.

    aa.add_obs('TDS')
    aa.add_obs('pH')
    aa.add_obs('As')
    aa.add_obs('Pb')
    aa.add_obs('Cd')
    aa.add_obs('Ba')
    aa.add_obs('Benzene')
    aa.add_obs('Naphthalene')
    aa.add_obs('Phenol')

    # Run the system model
    sm.forward()

    # Print the observations
    # Since the observations at the particular time points are different variables,
    # method collect_observations_as_time_series creates lists of
    # values of observations belonging to a given component (e.g. ca) and having the same
    # common name (e.g. 'pH', 'TDS', etc) but differing in indices.
    # More details are given in the docstring and documentation to the method
    # collect_observations_as_time_series of SystemModel class.
    print('TDS:', sm.collect_observations_as_time_series(aa,'TDS'))
    print('pH:', sm.collect_observations_as_time_series(aa,'pH'))
    print('As:', sm.collect_observations_as_time_series(aa,'As'))
    print('Pb:', sm.collect_observations_as_time_series(aa,'Pb'))
    print('Cd:', sm.collect_observations_as_time_series(aa,'Cd'))
    print('Ba:', sm.collect_observations_as_time_series(aa,'Ba'))
    print('Benzene:', sm.collect_observations_as_time_series(aa,'Benzene'))
    print('Naphthalene:', sm.collect_observations_as_time_series(aa,'Naphthalene'))
    print('Phenol:', sm.collect_observations_as_time_series(aa,'Phenol'))

    # Expected output
    # ('TDS', array([       0.        ,  6878026.69860782]))
    # ('pH', array([  0.00000000e+00,   5.74256378e+09]))
    # ('As', array([  0.00000000e+00,   3.64713147e+08]))
    # ('Pb', array([  0.00000000e+00,   6.26222833e+09]))
    # ('Cd', array([  0.00000000e+00,   2.14112532e+09]))
    # ('Ba', array([  0.00000000e+00,   6.04804066e+09]))
    # ('Benzene', array([ 0.        ,  8.42570834]))
    # ('Naphthalene', array([ 0.        ,  0.01776546]))
    # ('Phenol', array([ 0.        ,  1.90527474]))
