# -*- coding: utf-8 -*-
"""
The reservoir data interpolator estimates pressure and |CO2| saturation
at the location of interest using precomputed lookup tables.
The calculations are based on linear interpolation for irregular grids.
"""

import csv
import logging
import numpy as np
np.set_printoptions(threshold=np.nan)
import os
import scipy.spatial.qhull as qhull
import sys
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

try:
    from openiam import SystemModel
    from openiam.grid import interp_weights
    from openiam.grid import interpolate
except ImportError as err:
        print('Unable to load IAM class module: '+str(err))


def _blit_draw(self, artists, bg_cache):
    """
    Patch that allows for title and axes changes during animation.

    The original code is taken from:
    https://stackoverflow.com/questions/17558096/animated-title-in-matplotlib
    """
    # Handles blitted drawing, which renders only the artists given instead
    # of the entire figure.
    updated_ax = []
    for a in artists:
        if a.axes not in bg_cache:
            bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.figure.bbox)
        a.axes.draw_artist(a)
        updated_ax.append(a.axes)

    # After rendering all the needed artists, blit each axes individually.
    for ax in set(updated_ax):
        ax.figure.canvas.blit(ax.figure.bbox)

# Patch to handle title changes
matplotlib.animation.Animation._blit_draw = _blit_draw

class AnimatedScatterPlot(object):
    def __init__(self, interp_obj, obs_nm='pressure'):
        """
        Constructor method of AnimatedScatterPlot class.

        Create object that would animate the time evolution of the indicated data
        associated with the supplied interpolator.

        :param interp_obj: the ReservoirDataInterpolator object whose data
            is needed to be plotted
        :type interp_obj: ReservoirDataInterpolator object

        :param obs_nm: name of observational data to be shown as animation;
            possible values: name of the observation varying (at least) in time
        :type obs_nm: str

        :returns: AnimatedScatterPlot class object
        """
        self.num_time_points = len(interp_obj.time_points)
        self.num_points = len(interp_obj.points[:,0])

        self.time_points = interp_obj.time_points
        self.points = interp_obj.points
        self.interp_obj = interp_obj
        self.obs_nm = obs_nm

        self.data = interp_obj.data
        self.colorbar_title = interp_obj.default_units

        self.plot_title = interp_obj.title_names

        self.stream = list(range(0, self.num_time_points))

        # Setup the figure and axes
        self.fig, self.ax = plt.subplots(figsize=(8,8))

        # Set the x and y limits
        self.ax.axis([np.min(self.points[:,0]), np.max(self.points[:,0]),
                      np.min(self.points[:,1]), np.max(self.points[:,1])])
        self.ax.set_xlabel('x')
        self.ax.set_ylabel('y')
        self.ax.set_aspect('equal', 'datalim')

        # Determine min and max values of observations
        data_min = min([np.min(self.data[self.obs_nm][key]) for key in self.data[self.obs_nm]])
        data_max = max([np.max(self.data[self.obs_nm][key]) for key in self.data[self.obs_nm]])

        # For the colors range we take the min and max of data for all time points
        self.scat = self.ax.scatter(x=self.points[:,0], y=self.points[:,1],
                                    c=np.linspace(data_min, data_max, self.num_points),
                                    animated=True)
        # Add colorbar
        plt.colorbar(self.scat, ax=self.ax, label=self.colorbar_title[self.obs_nm])

        # Plot data at the first available time point
        self.scat.set_array(self.data[self.obs_nm][0])

        # Add title of the scatter plot as text
        self.title = self.ax.text(.5, 1.05, self.plot_title[self.obs_nm].format(0),
                                  transform = self.ax.transAxes,
                                  va='center', ha='center', size='large')

        # Setup FuncAnimation
        self.ani = animation.FuncAnimation(self.fig, self.update, self.stream,
                                           init_func = self.setup, blit=True,
                                           interval=250, save_count=self.num_time_points)

    def setup(self):
        """ Return initial state of the plot."""
        return self.scat, self.title

    def update(self, ind):
        """ Update the scatter plot."""

        # Set colors of the scatter plot
        self.scat.set_array(self.data[self.obs_nm][ind])

        # Set description of data
        self.title.set_text(self.plot_title[self.obs_nm].format(self.time_points[ind]))

        # Return the updated artists for FuncAnimation to draw.
        return self.scat, self.title


class ReservoirDataInterpolator(object):
    def __init__(self, name, parent, header_file_dir, time_file, data_file,
                 signature, default_values={}):
        """
        Constructor method of ReservoirDataInterpolator

        :param name: name of interpolator
        :type name: str

        :param parent: name of the system model interpolator belongs to
        :type parent: SystemModel object

        :param header_file_dir: location (directory) of the reservoir
            simulation data that will be used for interpolation
        :type header_file_dir: string

        :param time_file: name of *.csv file to read information about time
            points (in years) at which observations (e.g., pressure and |CO2|
            saturation) were calculated
        :type filename: string

        :param data_file: name of *.csv file to read observation (e.g.,
            pressure and |CO2| saturation) data from
        :type filename: string

        :param signature: dictionary of parameters and their corresponding values
            associated with the given simulation data file
        :type signature: dict()

        :param default_values: dictionary of default values of observations
            which are not provided in the data_file. By default,
            it is assumed that data provided in the data_file is enough,
            thus, by default, the parameter is an empty dictionary.
            The values provided are assumed to be constant for all time steps
            and all spatial points.
        type default_values: dict()

        :returns: ReservoirDataInterpolator class object
        """
        # Setup attributes of the ReservoirDataInterpolator class object
        self.name = name             # name of interpolator
        self.parent = parent         # a system model interpolator belongs to
        self.header_file_dir = header_file_dir  # path to the directory with simulation data files
        self.time_file = time_file   # name of file with time points data
        self.data_file = data_file   # name of file with pressure and saturation data
        self.signature = signature   # dictionary of parameters with values defining this interpolator
        self.default_values = default_values  # values (if any) specified for the interpolator observation

        # Setup default units of measurements for possible observations
        # (mainly for plotting purposes).
        # The dictionary can be updated later when we figure out whether any
        # additional observations are possible/needed; or dictionary can be updated
        # in script
        self.default_units = {'pressure': 'Pa',
                              'CO2saturation': '',
                              'salinity': '',
                              'temperature': r'$^\circ$C',
                              'area': r'm$^2$'}

        # Setup possible titles for different observations (for use in show_data method)
        self.title_names = {'pressure': 'Pressure',
                            'CO2saturation': r'CO$_2$ saturation',
                            'salinity': 'Salinity',
                            'temperature': 'Temperature',
                            'area': 'Cell area'}
        for key in self.title_names:
            self.title_names[key] = self.title_names[key]+' at time t = {:05.2f} years'

        # Create models for for each output type for each time step
        self.create_data_interpolators()

        # Log message about creating the interpolator
        logging.debug('ReservoirDataInterpolator created with name {name}'.format(name=name))

    def create_data_interpolators(self):
        """
        Setup attributes storing applicable observation data.

        Create data attribute of the class object for use in the model method of the class.
        """
        # Obtain data from csv files
        self.time_points = np.genfromtxt(os.path.join(self.header_file_dir,
            self.time_file), delimiter=",", dtype='f8')      # in years

        # Obtain headers of the data file with observations
        with open(os.path.join(self.header_file_dir, self.data_file), "r" ) as f:
            reader = csv.reader(f)
            self.data_headers = next(reader)
            logging.debug('Data file headers: {}'.format(self.data_headers))

        data = np.genfromtxt(os.path.join(self.header_file_dir,
                                          self.data_file),
                             delimiter=",", dtype='f8', skip_header=1)

        # Determine number of time points in the data
        self.num_data_time_points = len(self.time_points)

        # Setup data
        self.data = {}

        # Initialize list of constant in space observations
        constant_obs = {}

        # Loop over all observation names in the header except the first two (x and y)
        for ind, nm in enumerate(self.data_headers[2:]):
            # Find index of the last occurence of underscore in the column name
            underscore_ind = nm.rfind('_')

            if underscore_ind == -1:
                # If there is no underscore in the column name
                # then the observation is constant for all time points
                constant_obs[nm] = ind+2   # save index of the column
            else:
                # If there is an underscore in the name of the observation
                # find base name of the observation and time index
                obs_nm = nm[0:underscore_ind]                  # name of observation

                try:
                    # Try is added to check whether symbols after last underscore represent number
                    time_ind = int(nm[(underscore_ind+1):])-1  # time index of observation
                    # Check whether the obs_nm is already added to the data dictionary
                    if not obs_nm in self.data:
                        # Initialize dictionary corresponding to obs_nm
                        self.data[obs_nm] = {}
                    self.data[obs_nm][time_ind] = data[:,ind+2]
                except ValueError:
                    # If symbols after underscore are not numbers (e.g. cell_area)
                    # assume that the data in the column is time independent
                    constant_obs[nm] = ind+2  # save index of the column

        for nm,ind in constant_obs.items():
            if nm in self.data:   # nm observation is both specified as time dependent and independent
                logging.warning(
                    ' '.join(['Observation {} is specified as time',
                              'independent in the column {} and as time',
                              'varying in another column(s). The column',
                              'corresponding to the time independent data',
                              'will be used for calculations.']).format(nm,ind+1))
            self.data[nm] = {-1: data[:,ind]}  # -1 corresponds to the data constant in time but not in space

        for nm,val in self.default_values.items():
            if nm in self.data:
                logging.warning(
                    ' '.join(['Default value is specified for {} as argument',
                              'of the ReservoirDataInterpolator constructor',
                              'method. Observation is also defined in the data',
                              'file. Value provided to the constructor method',
                              'will be used for calculations.']).format(nm))
            self.data[nm] = {-2: val}   # -2 corresponds to data constant both in space and time

        # Setup x,y points
        self.points = data[:,0:2]
        self.num_xy_points = len(self.points)

        # Determine triangulation for the data points
        self.triangulation = qhull.Delaunay(self.points)

    def data_units(self, obs_nm):
        """
        Return measurement units of the provided observations.

        :param obs_nm: name of the observation for which units of measurements
            are to be provided.
        :type obs_nm: str

        :returns: units of measurements as str. For observation not
            in the list of possible known names, the string 'Unknown' is returned.
        """
        if obs_nm in self.default_units:
            return self.default_units[obs_nm]
        else:
            return 'Unknown'

    def show_data(self, time=None, obs_to_show='all'):
        """
        Show data linked to the interpolator.

        :param time: time point (in years) at which the data need to be shown. If time
            point does not coincide with any of the time points
            associated with the interpolator only the extent of the domain
            is shown. If no time point is provided, i.e., time is None
            (by default), an animation is created.
        :type time: float

        :param obs_to_show: list of names of observations to be shown; by default,
            all data linked to interpolator is shown.
        :type kwargs: list()
        """
        # Create a list of observation names to show
        if obs_to_show=='all':
            obs_nms = list(self.data.keys())
        else:
            obs_nms = obs_to_show

        # Loop over all observations to be shown on plots
        for nm in obs_nms:
            unchanging_plot = True
            if -2 in self.data[nm]:     # obs is constant in space and time
                data_colors = self.data[nm][-2]*np.ones(self.num_xy_points)
            elif -1 in self.data[nm]:   # obs is constant in time but not space
                data_colors = self.data[nm][-1]
            else:
                if time is None:
                    if not hasattr(self, 'sca_plt'):
                        self.sca_plt = {}
                    self.sca_plt[nm] = AnimatedScatterPlot(self, obs_nm=nm)
                    unchanging_plot = False
                else:
                    # Find index of time array point
                    ind = np.where(self.time_points==time)[0]
                    if len(ind)==1:
                        data_colors = self.data[nm][ind[0]][:]
                    else:
                        # Show extent of the data
                        plt.figure(figsize=(8,8))
                        plt.plot(self.points[:,0],self.points[:,1],'o')
                        plt.title('Boundaries of the domain')
                        plt.xlabel('x [m]')
                        plt.ylabel('y [m]')
                        plt.axes().set_aspect('equal', 'datalim')
                        plt.show()
                        logging.warning('Data is not available at time t = {} years'.format(time))
                        return

            # Plot observation
            if unchanging_plot:
                plt.figure(figsize=(8,8))
                plt.scatter(self.points[:,0], self.points[:,1], c=data_colors)
                plt.xlabel('x [m]')
                plt.ylabel('y [m]')
                plt.axes().set_aspect('equal', 'datalim')
                plt.colorbar(label=self.data_units(nm))
                t = 0 if time is None else time
                plt.title(self.title_names[nm].format(t))
                plt.show()

    def __call__(self, time, vtx=None, wts=None):
        """
        Return observation data at the point of interest.

        :param time: time point (in days) for which observations (e.g.,
            pressure and saturation) are to be calculated
        :type time: float

        :param vtx: array of indices of simplex vertices which enclose
            the point of interest; array should have a shape (1,3); indices
            do not exceed the number of data points
        :type vtx: numpy.array of int

        :param wts: array of weights of data at simplex vertices which enclose
            the point of interest; array should have a shape (1,3); weights
            values should be between 0 and 1, and sum up to 1
        :type wts: numpy.array of floats

        :returns: out - dictionary of observations; keys depend on the data
            provided in the data files. Possible keys:
            ['pressure','CO2saturation', 'salinity', 'temperature', 'area'].
        """
        # Create dictionary of output
        out = dict()

        # Convert days to years for interpolation
        dtime = time/365.25

        # Check whether time point is within the acceptable range
        if (dtime < self.time_points[0]) or (dtime>self.time_points[-1]):
            range_str = ', '.join((str(self.time_points[0]),str(self.time_points[-1])))

            logging.error(' '.join(['Time point t = {} years provided by',
                'system model is beyond the available time range [{}] of',
                'the data in {}.']).format(dtime,range_str,self.data_file))

            raise ValueError('Interpolation is requested at the time '+
                             'point outside the available range.')

        # Check whether a single point is requested
        if vtx is not None and wts is not None:            
            # Check whether weights are reasonable; if they are not, then it means
            # that the point at which we need to interpolate is outside the data range
            if np.any(abs(wts)>1.0):
                logging.warning('Input array of weights has invalid values: '+
                                'some or all values are greater than 1.')

            for ind in range(self.num_data_time_points):
                for nm in self.data:
                    if -2 in self.data[nm]:     # obs is constant in space and time
                        out[nm] = np.array([self.data[nm][-2]])
                    elif -1 in self.data[nm]:   # obs is constant in time but not space
                        out[nm] = interpolate(self.data[nm][-1], vtx, wts)
                    else:                       # obs varies in space and time
                        # If time point coincides with one of the data time points
                        if dtime == self.time_points[ind]:
                            out[nm] = interpolate(self.data[nm][ind], vtx, wts)
                        # If time point is between the data time points
                        elif ((self.time_points[ind]<dtime) and (dtime<self.time_points[ind+1])):
                            out1 = interpolate(self.data[nm][ind], vtx, wts)
                            out2 = interpolate(self.data[nm][ind+1], vtx, wts)
                            delta_t = self.time_points[ind+1]-self.time_points[ind]

                            # Interpolate between two data points
                            out[nm] = (out2-out1)/(delta_t)*(dtime-self.time_points[ind])+out1

        else:
            for ind in range(self.num_data_time_points):
                for nm in self.data:
                    if -2 in self.data[nm]:     # obs is constant in space and time
                        out[nm] = self.data[nm][-2]*np.ones(self.num_xy_points)
                    elif -1 in self.data[nm]:   # obs is constant in time but not space
                        out[nm] = self.data[nm][-1]
                    else:
                        # If time point coincides with one of the data time points
                        if dtime == self.time_points[ind]:
                            out[nm] = self.data[nm][ind]
                        elif ((self.time_points[ind]<dtime) and (dtime<self.time_points[ind+1])):
                            out1 = self.data[nm][ind]
                            out2 = self.data[nm][ind+1]

                            delta_t = self.time_points[ind+1]-self.time_points[ind]

                            # Interpolate between two data points
                            out[nm] = (out2-out1)/(delta_t)*(dtime-self.time_points[ind])+out1
        return out


if __name__ == "__main__":
    # The code below tests only interpolator and plotting/animation
    # capabilities but it needs a system model for the parent parameter.
    # Thus, we need to create a system model first.

    # Define logging level
    logging.basicConfig(level=logging.WARNING)

    # Create system model
    sm = SystemModel()

    # Create interpolator
    int1 = sm.add_interpolator(ReservoirDataInterpolator(name='int1', parent=sm,
        header_file_dir=os.path.join('..','components','reservoir',
                                     'lookuptables','Kimb_54_sims'),
        time_file='time_points.csv',
        data_file='Reservoir_data_sim02.csv',
        signature={'logResPerm': -13.3, 'reservoirPorosity': 0.25, 'logShalePerm': -18.7},
        default_values={'salinity': 0.1, 'temperature': 50.0}),
        intr_family='reservoir')

    # Print signature of the interpolator
    logging.debug('Signature of created interpolator is {}'.format(int1.signature))

    # Setup location of interest
    locX,locY = [37478.0,48333.0]

    # Calculate weights of the location of interest
    vertices, weights = interp_weights(int1.triangulation, np.array([[locX,locY]]))

    for t in 365.25*10.*np.arange(11):
        out = int1(t,vertices,weights)
        print(' '.join(['At time t = {} years pressure is {} MPa',
                        'and CO2 saturation is {}.']).format(
                        t/365.25, out['pressure'][0]/1.0e+6, out['CO2saturation'][0]))

    # Close all figures
    plt.close("all")

    # Show all data at 120 years
    int1.show_data(120.0)

    logging.warning("If running code in IPython make sure to switch to the interactive mode to see the animation.")
    # Show all data of interpolator int1 for available time points
    int1.show_data(obs_to_show=['pressure','CO2saturation'])

    to_save_figure = False
    # In order to save the animated figure ffmpeg should be installed.
    # If using Anaconda, in Anaconda prompt enter
    # conda install -c conda-forge ffmpeg
    # Worked on Windows 7
    # Advice is taken from: https://stackoverflow.com/questions/13316397/matplotlib-animation-no-moviewriters-available/14574894#14574894
    if to_save_figure:
        for obs_nm in int1.sca_plt:
            int1.sca_plt[obs_nm].ani.save(obs_nm+'_anim.mp4')