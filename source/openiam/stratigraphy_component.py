# -*- coding: utf-8 -*-
"""
The Stratigraphy component is a component containing parameters describing
the structure of the storage reservoir system. The stratigraphy component
allows the number of shale (or aquitard) layers to be set, thus, setting the
total number of layers in the system. Each shale or aquifer layer can
take on the default thickness for that layer type or be assigned a user defined value
with *shale#Thickness* or *aquifer#Thickness* keywords where # is replaced
by an index of the layer (e.g., *shale1Thickness*). Layers are numbered from
the bottom upward: shale 1 is the layer above the storage reservoir,
and, with N shale layers total, shale N is the surface layer.

.. image:: images/ShaleLayers.png
   :align: center
   :scale: 100%
   :alt: Layer ordering

The description of the component's parameters is provided below.

* **numberOfShaleLayers** [-] (3 to 30) - number of shale layers in the
  system (default 3). The shale units must be separated by an aquifer.

* **shaleThickness** [m] (1 to 1000) - thickness of shale layers (default
  250 m). Thickness of shale layer 1, for example, can be defined by
  shale1Thickness; otherwise, shale layers for which the thickness is not
  defined will be assigned a default thickness.

* **aquiferThickness** [m] (1 to 1000) - thickness of aquifers (default 100 m).
  Thickness of aquifer 1, for example, can be defined by aquifer1Thickness;
  otherwise, aquifers for which the thickness is not defined will be assigned
  a default thickness.

* **reservoirThickness** [m] (1 to 1000) - thickness of reservoir (default 50 m).

* **datumPressure** [Pa] (80,000 to 300,000) - pressure at the top of the
  system (default 101,325 Pa).

* **depth** [m] (5 to 30,000) - depth to the top of reservoir (default 950 m).
"""

import logging
import numpy as np
import sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

try:
    from openiam import SystemModel, ComponentModel
except ImportError as err:
    print('Unable to load IAM class module: '+str(err))


class Stratigraphy(ComponentModel):
    def __init__(self, name, parent):
        """
        Constructor method of Container Class.
        """

        super(Stratigraphy, self).__init__(name, parent, model='',
             model_args=[], model_kwargs={}, workdir=None)

        # Set default parameters of the component model
        self.add_default_par('numberOfShaleLayers', value=3)
        self.add_default_par('shaleThickness', value=250.0)
        self.add_default_par('aquiferThickness', value=100.0)
        self.add_default_par('reservoirThickness', value=50.0)
        self.add_default_par('datumPressure', value=101325.0)
        self.add_default_par('depth', value=950.0)   # depth to the top of the reservoir

        # Define dictionary of boundaries
        self.pars_bounds = dict()
        self.pars_bounds['numberOfShaleLayers'] = [3, 30]
        self.pars_bounds['shaleThickness'] = [1.0, 1500.0]
        self.pars_bounds['aquiferThickness'] = [1.0, 1000.0]
        self.pars_bounds['reservoirThickness'] = [1.0, 1000.0]
        self.pars_bounds['datumPressure'] = [8.0e+4, 3.0e+5]
        self.pars_bounds['depth'] = [5.0, 30000.0 ]

        # Indicate that the component should not be run
        self.run_frequency = 0

        logging.debug('Stratigraphy created with name {name}'.format(name=name))

    def check_input_parameters(self, p):
        """
        Check whether input parameters fall within specified boundaries.

        :param p: input parameters of component model
        :type p: dict
        """
        logging.debug('Input parameters of {name} component are {p}'.format(name=self.name,p=p))

        for key, val in p.items():
            if (not key in self.gridded_pars):
                if (key[0:5] == 'shale' and key[-9:] == 'Thickness'):
                    if ((val < self.pars_bounds['shaleThickness'][0])or
                        (val > self.pars_bounds['shaleThickness'][1])):
                        logging.warning('Parameter ' + key + ' is out of boundaries.')
                elif key[0:7] == 'aquifer' and key[-9:] == 'Thickness':
                    if ((val < self.pars_bounds['aquiferThickness'][0])or
                        (val > self.pars_bounds['aquiferThickness'][1])):
                        logging.warning('Parameter ' + key + ' is out of boundaries.')
                elif key[0:5] == 'depth':
                    if ((val < self.pars_bounds['depth'][0])or
                        (val > self.pars_bounds['depth'][1])):
                        logging.warning('Parameter ' + key + ' is out of boundaries.')
                elif key in self.pars_bounds:
                    if ((val < self.pars_bounds[key][0])or
                        (val > self.pars_bounds[key][1])):
                        logging.warning('Parameter ' + key + ' is out of boundaries.')
                else:
                    logging.warning('Parameter {key} not recognized as a Stratigraphy input parameter.'
                                    .format(key=key))

if __name__ == "__main__":
    try:
        from openiam import SimpleReservoir
    except ImportError as err:
        print('Unable to load IAM class module: '+str(err))

    logging.basicConfig(level=logging.WARNING)
    # Define keyword arguments of the system model
    num_years = 50
    time_array = 365.25*np.arange(0.0,num_years+1) # time is in days
    sm_model_kwargs = {'time_array': time_array}
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    strata = sm.add_component_model_object(Stratigraphy(name='strata', parent=sm))

    # Add parameters of reservoir component model
    strata.add_par('numberOfShaleLayers', value=3, vary=False)
    strata.add_par('shale1Thickness', min=30.0, max=50., value=40.0)
    strata.add_par('shale2Thickness', min=40.0, max=60., value=50.0)

    # Add reservoir component
    sres = sm.add_component_model_object(SimpleReservoir(name='sres', parent=sm))

    # Add parameters of reservoir component model
    sres.add_par_linked_to_par('numberOfShaleLayers',
                         strata.deterministic_pars['numberOfShaleLayers'])
    sres.add_par('injRate', min=0.4, max=0.6, value=0.5)
    sres.add_par_linked_to_par('shale1Thickness', strata.pars['shale1Thickness'])
    sres.add_par_linked_to_par('shale2Thickness', strata.pars['shale2Thickness'])
    sres.add_par_linked_to_par('shale3Thickness', strata.default_pars['shaleThickness'])

    sres.add_par_linked_to_par('aquifer1Thickness', strata.default_pars['aquiferThickness'])
    sres.add_par_linked_to_par('aquifer2Thickness', strata.default_pars['aquiferThickness'])
    sres.add_par_linked_to_par('aquifer3Thickness', strata.default_pars['aquiferThickness'])

    sres.add_par_linked_to_par('reservoirThickness', strata.default_pars['reservoirThickness'])

    sres.add_par_linked_to_par('datumPressure', strata.default_pars['datumPressure'])

    sres.add_obs('pressure')

    # Run system model using current values of its parameters
    sm.forward()

    print('------------------------------------------------------------------')
    print('                  Forward method illustration ')
    print('------------------------------------------------------------------')

    # Print pressure
    print('Pressure', sm.collect_observations_as_time_series(sres,'pressure'), sep='\n')