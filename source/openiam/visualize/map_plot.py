# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 12:16:47 2018

@author: Seth King
AECOM supporting NETL
Seth.King@NETL.DOE.GOV
"""
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import os
import sys
source_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(source_dir)


class Plume(object):
    """
    Object to package ellipse data together
    Does not include rotation, must be oriented
    with x and y directions.
    """
    def __init__(self, x, y, dx, dy):
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy

    def plot(self, ax, alpha=0.2):
        ax.plot(self.x, self.y, 'r.', label='Source')
        e1 = mpatches.Ellipse((self.x, self.y), self.dx*2, self.dy*2, alpha=alpha)
        ax.add_artist(e1)

        ax.annotate("{0:.1f}m".format(self.dx),
                    xy=(self.x, self.y),
                    xytext=(self.x, self.y+1))
        return


def make_plume_plots(plumes, time=None, alpha=0.2, extent=None, savefig=None, receptors=[]):
    """
    Takes in list of plume objects and plot them on a figure.
    :param plumes: list of plume objects to plot.
    :typle plumes: lst
    :param time: Time step to display on figure title.
    :type time: float, int, str
    :param alpha: alpha (opacity) value for plumes.
    :type alpha: float in [0, 1]
    :param extent: list of lists of (min, max) extent of the form
        [[xmin, xmax], [ymin, ymax]].  If not given will estimate from first sample.
    :type extent: lst
    :param savefig: Filename to save figure to.  If not given, no file will be saved.
    :type savefig: str
    :param receptors: location of receptors to plot
    :type receptors: lst
    :returns: None
    """
    fig, ax = plt.subplots(1, 1, figsize=(15, 10))
    plt.grid()
    plt.axis('equal')
    max_x = -9e99
    min_x = 9e99
    max_y = -9e99
    min_y = 9e99
    max_dx = 0
    for r in receptors:
        plt.plot(r[0], r[1], 'k.', label='Receptor')
        if r[0] > max_x:
            max_x = r[0]
        if r[0] < min_x:
            min_x = r[0]
        if r[1] > max_y:
            max_y = r[1]
        if r[1] < min_y:
            min_y = r[1]
    for p in plumes:
        p.plot(ax, alpha=alpha)
        if p.x > max_x:
            max_x = p.x
        if p.x < min_x:
            min_x = p.x
        if p.y > max_y:
            max_y = p.y
        if p.y < min_y:
            min_y = p.y
        if p.dx > max_dx:
            max_dx = p.dx
    if not extent:
        extent = [(min_x - 1.5*max_dx, max_x + 1.5*max_dx),
                  (min_y - 1.5*max_dx, max_y + 1.5*max_dx)]
    ax.set(xlim=extent[0],
           ylim=extent[1])
    if time is not None:
        plt.title('Map View of $CO_2$ release\nat time {0} years'.format(time))
    plt.xlabel('x $(m)$')
    plt.ylabel('y $(m)$')
    l, h = ax.get_legend_handles_labels()
    ptch = mpatches.Patch(alpha=alpha, label="Critical Areas")
    plt.legend(handles=[l[0], l[-1], ptch])
    if savefig:
        plt.savefig(savefig)
    else:
        plt.show()
    plt.close(fig)
    return extent


def make_grid(extent, npoints=(100, 100)):
    """
    Makes a grid over the given extent.

    :param extent: list of lists of (min, max) extent of the form
        [[xmin, xmax], [ymin, ymax]]
    :type extent: lst
    :param npoints: tuple of number of points in x and y direction (nx, ny).
    :type npoints: tuple
    :returns: ndarray(fl64) of size [nx*ny, 2] with (x, y) grid points.
    """
    if hasattr(npoints, '__iter__'):
        npoints_x = npoints[0]
        npoints_y = npoints[-1]
    else:
        npoints_x = npoints
        npoints_y = npoints
    x = np.linspace(extent[0][0], extent[0][1], num=npoints_x)
    y = np.linspace(extent[1][0], extent[1][1], num=npoints_y)
    xx, yy = np.meshgrid(x, y)
    xy = np.column_stack((xx.flatten(), yy.flatten()))
    return xy


def get_plumes(s, time_index=0, sample=0, dr_factor=3, atm_name='satm'):
    """
    Reads atmosphere plumes from sample data set and returns list of plume objects.
    :param s: Sample data set from completed analysis run.
    :type s: matk.sampleset
    :param time_index: Index of time array to produce plumes for.
    :type time_index: int
    :param sample: Realization number to compute plumes from.
    :type sample: int
    :param dr_factor: Padding factor to multiply radius by when calculating extent.
    :type dr_factor: float
    :param atm_name: name of atmosphere component model
    :type atm_name: str
    :returns: ([plumes], extent) List of plume objects and recommended extent to plot them on.
    """
    num_source_real = s.recarray['{atm}.num_sources_{time}'.format(time=time_index, atm=atm_name)]
    plumes = []
    max_x = -9e99
    min_x = 9e99
    max_y = -9e99
    min_y = 9e99
    max_dx = 0
    for source in range(int(num_source_real[sample])):
        x = s.recarray['{atm}.x_new_s{source:03}_{time}'.format(source=source,
                       time=time_index,
                       atm=atm_name)][sample]
        y = s.recarray['{atm}.y_new_s{source:03}_{time}'.format(source=source,
                       time=time_index,
                       atm=atm_name)][sample]
        cd = s.recarray['{atm}.critical_distance_s{source:03}_{time}'.format(source=source,
                        time=time_index,
                        atm=atm_name)][sample]
        plumes.append(Plume(x, y, cd, cd))
        if x > max_x:
            max_x = x
        if x < min_x:
            min_x = x
        if y > max_y:
            max_y = y
        if y < min_y:
            min_y = y
        if cd > max_dx:
            max_dx = cd
    extent = [(min_x - dr_factor*max_dx, max_x + dr_factor*max_dx),
              (min_y - dr_factor*max_dx, max_y + dr_factor*max_dx)]
    return (plumes, extent)


def map_plume_plot_single(s, satm, time_array, sample=0, savefig=None, extent=None):
    """
    Maps atmosphere plumes from sample data set for single realization sample.
    :param s: Sample data set from completed analysis run.
    :type s: matk.sampleset
    :param satm: Atmosphere rom component instance used in system model
    :type satm: openiam.AtmosphericRom
    :param time_array: Time array the system model used to generate the sample set
    :type time_array: list or np.array
    :param sample: Realization number to compute plumes from.
    :type sample: int
    :param savefig: Filename to save figure to.  If not given, no file will be saved.
        Include keys {time_index} or {time} for time series plots.
    :type savefig: str
    :returns: None
    """
    plumes, ex = get_plumes(s, time_index=len(time_array)-1, sample=sample, atm_name=satm.name)
    if not extent:
        extent = ex
    for time_index, time in enumerate(time_array/365.25):
        plumes, ex = get_plumes(s, time_index=time_index, sample=sample, atm_name=satm.name)
        receptors = list(zip(satm.model_kwargs['x_receptor'], satm.model_kwargs['y_receptor']))
        savefig_ti = None
        if savefig:
            savefig_ti = savefig.format(time_index=time_index, time=time)
        make_plume_plots(plumes, time=time, alpha=0.2, extent=extent, savefig=savefig_ti, receptors=receptors)
    return


def ellipse_to_grid(grid_xy, center_xy, dxdy):
    """
    Returns a boolean grid of the same size as grid_xy with 1's
    where the ellipse is and zeros everywhere else.

    :param grid_xy: Array of size [nx*ny, 2] with (x,y) pairs of coordinates.
    :type grid_xy: ndarray(fl64)
    :param center_xy: tuple of (x, y) coordinates for center of ellipse.
    :type center_xy: tuple
    :param dxdy: tuple of major and minor axis of ellipse (dx, dy).
    :typle dxdy: tuple
    :returns: ndarray(fl64) Boolean grid values of shape [nx*ny]
    """
    ellipse = np.zeros_like(grid_xy[:, 0])
    etf = ((grid_xy[:, 0]-center_xy[0])/dxdy[0])**2 + ((grid_xy[:, 1]-center_xy[1])/dxdy[1])**2 <= 1
    ellipse[etf] = 1
    return ellipse


def prob_plume_grid(s, satm, time_index, extent=None, xy=None):
    """
    Reads plumes for all realizations at a time step and return grid of existence probabilities.
    :param s: Sample data set from completed analysis run.
    :type s: matk.sampleset
    :param satm: Atmosphere rom component instance used in system model
    :type satm: openiam.AtmosphericRom
    :param time_index: Index of time array to produce plumes for.
    :type time_index: int
    :param extent: list of lists of (min, max) extent of the form
        [[xmin, xmax], [ymin, ymax]].  If not given will estimate from first sample.
    :type extent: lst
    :param xy: Array of size [nx*ny, 2] with (x,y) pairs of coordinates.
        If not given, will be created from extent.
    :type xy: ndarray(fl64)
    :returns: (plumes_grid, xy) Probabilities of plumes on a grid, and the xy grid coordinates.
    """
    if not extent:
            plumes, extent = get_plumes(s, time_index, sample=0, atm_name=satm.name)
    if xy is None:
        xy = make_grid(extent, npoints=(200, 200))
    output = np.zeros_like(xy[:, 0])
    num_samples = s.recarray.shape[0]
    for sample in range(num_samples):
        el = np.zeros_like(xy[:, 0])
        plumes, _ = get_plumes(s, time_index, sample=sample, atm_name=satm.name)
        for p in plumes:
            el = np.logical_or(el, ellipse_to_grid(xy, (p.x, p.y), (p.dx, p.dy)))
        output += el
    return (output/num_samples, xy)


def plot_grid_plumes(xy, plumes_grid, savefig=None, time=None, figsize=(15, 10)):
    """
    Plots grid of probabilities of plumes.
    :param xy: Array of size [nx*ny, 2] with (x,y) pairs of coordinates.
    :type xy: ndarray(fl64)
    :param plumes_grid: Array of size [nx*ny] with probabilities of plume existing at (x, y) coordinate.
    :type plumes_grid: ndarray(fl64)
    :param savefig: Filename to save figure to.  If not given, no file will be saved.
    :type savefig: str
    :param time: Time step to print to figure title.
    :type time: float, int, str
    :param figsize: tuple of figure size.
    :type figsize: tuple
    :returns: None
    """
    plt.figure(figsize=figsize)
    plt.clf()
    plt.tricontourf(xy[:, 0], xy[:, 1], plumes_grid, levels=np.linspace(0, 1.001, num=10))
    plt.axis('equal')
    plt.colorbar(format='%.1f')
    plt.xlabel('x $(m)$')
    plt.ylabel('y $(m)$')
    title = 'Map View of Probablility\nof Critical Distance Existance'
    if time is not None:
        title += '\nfor time t={time} years'.format(time=time)
    plt.title(title)
    if savefig:
        plt.savefig(savefig)
        plt.close()


def map_plume_plot_ensemble(s, satm, time_array, extent=None, xy=None, savefig=None, figsize=(15, 10)):
    """
    Reads plumes for all realizations at all time steps and plots the probabilities of exisitence.
    :param s: Sample data set from completed analysis run.
    :type s: matk.sampleset
    :param satm: Atmosphere rom component instance used in system model
    :type satm: openiam.AtmosphericRom
    :param time_array: Time array the system model used to generate the sample set
    :type time_array: list or np.array
    :param extent: list of lists of (min, max) extent of the form
        [[xmin, xmax], [ymin, ymax]].  If not given will estimate from first sample.
    :type extent: lst
    :param xy: Array of size [nx*ny, 2] with (x,y) pairs of coordinates.
        If not given, will be created from extent.
    :type xy: ndarray(fl64)
    :param savefig: Filename to save figure to.  If not given, no file will be saved.
        Include keys {time_index} or {time} for time series plots.
    :type savefig: str
    :param figsize: tuple of figure size.
    :type figsize: tuple
    :returns: (plumes_grid, xy) Probabilities of plumes on a grid, and the xy grid coordinates.
    """
    savefig_ti = None
    for time_index, time in enumerate(time_array[1:]):
        plume_grid, xy = prob_plume_grid(s, satm, time_index+1, extent=extent, xy=xy)
        if savefig:
            savefig_ti = savefig.format(time_index=time_index+1, time=time)
        plot_grid_plumes(xy, plume_grid, savefig=savefig_ti, time=time/365.25, figsize=figsize)
    return


if __name__ == '__main__':
    from openiam import SimpleReservoir, OpenWellbore, AtmosphericROM
    from matk import pyDOE
    from openiam import SystemModel
    # Define keyword arguments of the system model
    num_years = 5
    time_array = 365.25*np.arange(0.0, num_years+1)
    sm_model_kwargs = {'time_array': time_array}  # time is given in days

    # Create system model
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # Create randomly located Leaky well locations
    # within box defined by xmin,xmax,ymin,ymax
    xymins = np.array([200., 550.])
    xymaxs = np.array([600., 700.])
    well_xys = xymins + pyDOE.lhs(2, samples=5)*(xymaxs-xymins)

    sress = []
    mss = []
    ow = []
    co2_leakrates_collector = []
    for i, crds in enumerate(well_xys):
        # Add reservoir components
        sress.append(sm.add_component_model_object(
                     SimpleReservoir(name='sres'+str(i), parent=sm,
                                     injX=0., injY=0.,
                                     locX=crds[0], locY=crds[1])))

        # Add parameters of reservoir component model
        sress[-1].add_par('numberOfShaleLayers', value=3, vary=False)
        sress[-1].add_par('injRate', value=0.05, vary=False)
        sress[-1].add_par('shale1Thickness', min=300.0, max=500., value=400.0)
        sress[-1].add_par('aquifer1Thickness', min=20.0, max=60., value=50.0)
        sress[-1].add_par('shale2Thickness', min=700.0, max=900., value=800.0)
        sress[-1].add_par('aquifer2Thickness', min=60.0, max=80., value=75.0)
        sress[-1].add_par('shale3Thickness', min=150.0, max=250., value=200.0)
        sress[-1].add_par('logResPerm', min=-12.5, max=-11.5, value=-12.)

        # Add observations of reservoir component model to be used by the next component
        sress[-1].add_obs_to_be_linked('pressure')
        sress[-1].add_obs_to_be_linked('CO2saturation')

        # Add observations of reservoir component model
        sress[-1].add_obs('pressure')
        sress[-1].add_obs('CO2saturation')

        # Add open wellbore component
        ow.append(sm.add_component_model_object(OpenWellbore(name='ow'+str(i), parent=sm)))

        # Add parameters of open wellbore component
        ow[-1].add_par('wellRadius', min=0.01, max=0.02, value=0.015)
        ow[-1].add_par('logNormalizedTransmissivity', min=-1.0, max=1.0, value=0.0)
        ow[-1].add_par('brineSalinity', value=0.1, vary=False)
        ow[-1].add_par('wellTop', value=0.0, vary=False)

        # Add keyword arguments of the open wellbore component model
        ow[-1].add_kwarg_linked_to_obs('pressure', sress[-1].linkobs['pressure'])
        ow[-1].add_kwarg_linked_to_obs('CO2saturation', sress[-1].linkobs['CO2saturation'])

        # Add composite parameter of open wellbore component
        ow[-1].add_composite_par('reservoirDepth', expr='sres0.shale1Thickness' +
                                 '+sres0.shale2Thickness + sres0.shale3Thickness' +
                                 '+sres0.aquifer1Thickness+ sres0.aquifer2Thickness')

        # Add observations of multisegmented wellbore component model
        ow[-1].add_obs_to_be_linked('CO2_atm')
        ow[-1].add_obs('CO2_aquifer')
        ow[-1].add_obs('brine_aquifer')
        ow[-1].add_obs('CO2_atm')
        ow[-1].add_obs('brine_atm')

        # Create Collector for leakrates
        co2_leakrates_collector.append(ow[-1].linkobs['CO2_atm'])

    # Add Atm ROM
    satm = sm.add_component_model_object(AtmosphericROM(name='satm', parent=sm))
    satm.add_par('T_amb', min=13.0, max=15, value=18.0, vary=False)
    satm.add_par('P_amb', min=0.99, max=1.02, value=1.01E+00, vary=False)
    satm.add_par('V_wind', min=3.0, max=8.0, value=5.0, vary=False)
    satm.add_par('C0_critical', value=0.01, vary=False)

    satm.model_kwargs['x_receptor'] = [200, 250, 400, 350]
    satm.model_kwargs['y_receptor'] = [580, 660, 525, 600]

    satm.model_kwargs['x_coor'] = well_xys[:, 0]
    satm.model_kwargs['y_coor'] = well_xys[:, 1]

    satm.add_kwarg_linked_to_collection('co2_leakrate', co2_leakrates_collector)

    # Add Observations for receptors
    for i, r in enumerate(satm.model_kwargs['x_receptor']):
        satm.add_obs('outflag_r{0:03}'.format(i))
    n_sources = len(satm.model_kwargs['x_coor'])
    for i in range(n_sources):
        satm.add_obs('x_new_s{0:03}'.format(i))
        satm.add_obs('y_new_s{0:03}'.format(i))
        satm.add_obs('critical_distance_s{0:03}'.format(i))

    satm.add_obs('num_sources')

    import random
    num_samples = 50
    ncpus = 1
    # Draw Latin hypercube samples of parameter values
    s = sm.lhs(siz=num_samples, seed=random.randint(500, 1100))

    # Run model using values in samples for parameter values
    results = s.run(cpus=ncpus, verbose=False)
    # Map single result
    if not os.path.exists('atm_plumes'):
        os.mkdir('atm_plumes')
    savefig = os.path.join('atm_plumes', 'atm_forward_plume_t{time_index}.png')
    map_plume_plot_single(s, satm, time_array, savefig=savefig, extent=[[100, 700], [450, 800]])

    # Map lhs results
    savefig = os.path.join('atm_plumes', 'atm_prob_plume_t{time_index}.png')
    map_plume_plot_ensemble(s, satm, time_array, savefig=savefig, extent=[[100, 700], [450, 800]])
