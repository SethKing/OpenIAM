# -*- coding: utf-8 -*-
"""
Code to create different time-series visualizations for the OpenIAM.
Created on Wed Feb 14 15:01:05 2018

@author: Seth King
AECOM supporting NETL
Seth.King@NETL.DOE.GOV
"""
import numpy as np
import matplotlib.pyplot as plt
from matk.sampleset import percentile, mean
import math
import matplotlib.cbook
import warnings
# Ignore futurewarning from numpy about record array subarray copies vs views.
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=matplotlib.cbook.mplDeprecation)

y_label_dict = {'pressure': 'Reservoir pressure at wellbore (Pa)',
                'CO2saturation': 'Reservoir $CO_2$ saturation at wellbore',
                'mass_CO2_reservoir': 'Mass of $CO_2$ in the reservoir (kg)',
                'CO2_aquifer': 'Leakage rate of $CO_2$ (kg/s)',
                'CO2_aquifer1': 'Leakage rate of $CO_2$ (kg/s)',
                'CO2_aquifer2': 'Leakage rate of $CO_2$ (kg/s)',
                'CO2_atm': 'Leakage rate of $CO_2$ (kg/s)',
                'brine_aquifer': 'Leakage rate of brine (kg/s)',
                'brine_aquifer1': 'Leakage rate of brine (kg/s)',
                'brine_aquifer2': 'Leakage rate of brine (kg/s)',
                'brine_atm': 'Leakage rate of brine (kg/s)',
                'mass_CO2_aquifer1': 'mass of $CO_2$ leaked (kg)',
                'mass_CO2_aquifer2': 'mass of $CO_2$ leaked (kg)',
                'pH': 'Volume of aquifer below pH threshold $(m^3)$',
                'TDS': 'Volume of aquifer above the TDS threshold in mg/L $(m^3)$',
                'Benzene': 'Volume of the aquifer above benzene threshold $(m^3)$',
                'Naphthalene': 'Volume of the aquifer above the naphthalene threshold $(m^3)$',
                'Phenol': 'Volume of the aquifer above the phenol threshold $(m^3)$',
                'As': 'Volume of the aquifer above the arsenic threshold $(m^3)$',
                'Pb': 'Volume of the aquifer above the lead threshold $(m^3)$',
                'Cd': 'Volume of the aquifer above the cadmium threshold $(m^3)$',
                'Ba': 'Volume of the aquifer above the barium threshold $(m^3)$',
                'Flux': '$CO_2$ flux to atmosphere',
                }


def time_series_plot(output_names, sm, s, output_list, name=None,
                     analysis='forward', savefig=None, title=None,
                     subplot={'use': False}, plot_type=['real']):
    """
    Makes a time series plots of statistic on data in output_names.
    Highlights percentiles, mean, and median values.

    :param output_names: List of observation names to match with component models and plot.
    :type output_names: list

    :param sm: OpenIAM System model for which plots are created
    :type sm: openiam.SystemModel object

    :param s: SampleSet with results of analysis performed on the system model.
        None for forward analysis.
    :type s: matk.SampleSet object

    :param output_list: Dictionary mapping observations to component models
    :type output_list: dict

    :param name: Figure Name to be used/created.
    :type name: str

    :param analysis: Type of OpenIAM system analysis performed ('forward', 'lhs', or 'parstudy')
    :type analysis: str

    :param savefig: Filename to save resulting figure to. No file saved if None.
    :type savefig: str

    :param title: Optional Title for figure
    :type title: str

    :param subplot: Dictionary for subplot controls, use=True will use subplotting (boolean default=False),
        ncols=n will use n columns of subplots (positive integer default 1)
        comp.var_name = sub_title will title the subplots of comp.var_name subtitle (string default=comp.var_name)
    :type subplot: dict

    :param plot_type: List of 'real' and/or 'stats' plot types to produce.
        'real' plots realization values
        'stats' plots quartiles, mean, and median values
    :type plot_type: list of 'real' and/or 'stats'

    :return: None
    """
    time = sm.time_array/365.25
    default_title = ' '
    fig = plt.gcf()
    # Find number of subplots
    nplots = 0
    for pt_value in output_names:
        for output_component in list(output_list.keys()):
            if pt_value in output_list[output_component]:
                nplots += 1
    single_plot = False
    if not subplot['use']:
        ncols = 1
        single_plot = True
    elif 'ncols' in subplot:
        ncols = subplot['ncols']
    else:
        ncols = 1
    nrows = math.ceil(float(nplots)/ncols)
    if single_plot:
        nrows = 1
    subplot_n = 1
    color = 1
    for pt_value in output_names:
        default_title += pt_value + ' '
        for output_component in list(output_list.keys()):
            if pt_value in output_list[output_component]:
                plt.subplot(nrows, ncols, subplot_n)
                if not single_plot:
                    subplot_n += 1
                full_obs_nm = '.'.join([output_component.name, pt_value])
                if analysis == 'forward':
                    values = sm.collect_observations_as_time_series(output_component, pt_value)
                    label = output_component.name + '.' + pt_value
                    line_style = '-'
                    # raise IOError('Stats not available for forward model')
                elif analysis == 'lhs' or analysis == 'parstudy':
                    ind_list = list(range(len(time)))
                    obs_names = [full_obs_nm + '_{0}'.format(indd) for indd in ind_list]
                    obs_percentiles = percentile(s.recarray[obs_names], [0, 25, 50, 75, 100])
                    obs_means = mean(s.recarray[obs_names])
                    values = np.array([s.recarray[full_obs_nm+'_'+str(indd)] for indd in ind_list])
                    label = 'Simulated Values'
                    line_style = 'C0'
                else:
                    pass
                if 'real' in plot_type:
                    plt.plot(time, values, line_style, label=label)
                if 'stats' in plot_type:
                    plt.fill_between(time, obs_percentiles[3, :],
                                     obs_percentiles[4, :], alpha=0.2,
                                     label='Upper quartile')
                    plt.fill_between(time, obs_percentiles[1, :],
                                     obs_percentiles[3, :], alpha=0.4,
                                     label='Middle quartile'.format(full_obs_nm))
                    plt.fill_between(time, obs_percentiles[0, :],
                                     obs_percentiles[1, :], alpha=0.2,
                                     label='Lower quartile'.format(full_obs_nm))
                    plt.plot(time, obs_percentiles[2, :], color='C{0}'.format(color % 10),
                             label='Median Value')
                    plt.plot(time, obs_means, color='C{0}'.format((color+1) % 10),
                             label='Mean Value')

                plt.xlabel('Time, t (years)')
                plt.grid(alpha=0.15)
                if pt_value in y_label_dict:
                    plt.ylabel(y_label_dict[pt_value])
                sub_title = full_obs_nm
                if full_obs_nm in subplot:
                    sub_title = subplot[full_obs_nm]
                if not single_plot:
                    plt.title(sub_title)
                else:
                    color += 2
    fig.subplots_adjust(left=0.10, right=0.85, hspace=0.3)
    ax = plt.gca()
    handles, labels = ax.get_legend_handles_labels()
    handle_list, label_list = [], []
    for handle, label in zip(handles, labels):
        if label not in label_list:
            handle_list.append(handle)
            label_list.append(label)
    fig.legend(handle_list, label_list, loc=7)
    if title:
        plt.suptitle(title)
    else:
        plt.suptitle(default_title)
    if savefig:
        try:
            plt.savefig(savefig)
        except ValueError:
            # User has specified plot with a '.' in name but no extension.  Add .png as output format.
            savefig += '.png'
            plt.savefig(savefig)
    else:
        plt.show()
    return
