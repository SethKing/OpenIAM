# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 07:15:37 2018

@author: Seth King
AECOM supporting NETL
Seth.King@NETL.DOE.GOV
"""
import collections
import numpy as np
import os
import matplotlib.pyplot as plt
import random
import sys
import warnings
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
from matk.sampleset import corr
from openiam import SystemModel, SimpleReservoir
from openiam import OpenWellbore, CarbonateAquifer, RateToMassAdapter
warnings.simplefilter(action='ignore', category=FutureWarning)


def array2string(array):
    return np.array2string(array, separator=',', max_line_width=999999).strip('[]')


def correlations_at_time(sampleset, capture_point=1, excludes=[], ctype='pearson', plot=True,
                         printout=False, plotvals=True, figsize=(15, 15), title=None,
                         xrotation=90, savefig=None, outfile=None):
    """
    Calculate correlation coefficients of parameters and responses at capture_point time-step

    :param sampleset: Sampleset from sampling openiam.SystemModel with observations (has been ran)
    :type sampleset: matk.SampleSet

    :param capture_point: Time point to calculate correlation coefficients on
    :type capture_point: int

    :param exclude: Observation base names to exclude from calculations
    :type exclude: list of strings

    :param ctype: Type of correlation coefficient (pearson by default, spearman also available)
    :type ctype: str

    :param plot: If True, plot correlation matrix
    :type plot: bool

    :param printout: If True, print correlation matrix with row and column headings
    :type printout: bool

    :param plotvals: If True, print correlation coefficients on plot matrix
    :type plotvals: bool

    :param figsize: Width and height of figure in inches
    :type figsize: tuple(fl64,fl64)

    :param title: Title of plot
    :type title: str

    :param xrotation: Rotation for x axis tick labels (observations)
    :type xrotation: int, float, or str

    :param savefig: Filename for figure to be saved as, not saved if None
    :type savefig: str

    :param outfile: Filename for csv file output of correlation coeff, not saved if None
    :type outfile: str

    :returns: ndarray(fl64) -- Correlation coefficients
    """
    if not isinstance(capture_point, collections.Iterable):
        capture_points = [capture_point]
    else:
        capture_points = capture_point
    for capture_point in capture_points:
        resp_names = []
        for name in sampleset.responses._names:
            if name.endswith('_{cp}'.format(cp=capture_point)):
                resp_names.append(name)
        if type(excludes) == 'str':
            excludes = [excludes]
        exc_names = ['{ex}_{cp}'.format(ex=exc, cp=capture_point) for exc in excludes]
        resp_names = [nm for nm in resp_names if nm not in exc_names]
        rc2 = sampleset.responses.recarray[resp_names]
        corr_coeff = corr(sampleset.samples.recarray, rc2, 
                          type=ctype, plot=plot, printout=printout,
                          plotvals=plotvals, figsize=figsize,
                          title=title.format(cp=capture_point),
                          xrotation=xrotation, 
                          filename=savefig.format(cp=capture_point),
                          adjust_dict={'left': 0.2,
                                       'bottom': 0.2,
                                       'right': 0.95,
                                       'top': 0.95})
        if savefig:
            plt.close()
        if outfile:
            with open(outfile.format(cp=capture_point), 'w') as ofile:
                ofile.write(' , ' + ', '.join(resp_names) + '\n')
                for i, par in enumerate(sampleset.samples.names):
                    ofile.write(par + ', ' + array2string(corr_coeff[i]) + '\n')
    return corr_coeff


def top_sensitivities(sensitivities, num_sensitivities=5):
    """
    Returns a list of arguments that can be used to sort parameters
    and sensitivities from largest to smallest impact.

    :param sensitivities: Dictionary of sensitivities with 'S1' entry
    :type sensitivities: dict

    :param num_sensitivities: Number of top sensitivities to return
    :type num_sensitivities: int

    :returns sorted_args: array of indices to sort sensitivities by
    :type sorted_args: np.array of size [num_sensitivities]
    """
    sorted_args = np.flipud(np.argsort(sensitivities['S1']))
    if num_sensitivities:
        sorted_args = sorted_args[:num_sensitivities]
    return sorted_args


def time_series_sensitivities(obs_base_name, sm, lhs_sample, time_array,
                              title=None, ylabel=None, num_sensitivities=5,
                              savefig=None, capture_point=None, outfile=None):
    """
    Plot a time_series of top sensitivity coefficients for the observation

    :param obs_base_name: Base observation name to append time index to for sensitivity analysis
    :type obs_base_name: str

    :param sm: System model
    :type sm: openiam.SystemModel object

    :param lhs_sample: Sample set for system model with observations (has been ran)
    :type lhs_sample: matk.SampleSet object

    :param time_array: Time values for system model (in days)
    :type time_array: np.array

    :param title: title for figure
    :type title: str

    :param ylabel: ylabel for figure
    :type ylabel: str

    :param num_sensitivities: Number of top sensitivities to plot
    :type num_sensitivities: int

    :param savefig: Filename for figure to be saved as; figure is not saved if savefig is None
    :type savefig: str

    :param capture_point: Time Index to use for top sensitivity calculation. If None, use end time point
    :type capture_point: int

    :param outfile: Filename for csv file output of top sensitivities; output is not saved if None
    :type outfile: str

    :returns: raw_sensitivities
    """
    if capture_point:
        obs_name = obs_base_name + '_{0}'.format(capture_point)
    else:
        num_time = len(time_array)
        # Find top sensitivites at end time
        obs_name = obs_base_name + '_{0}'.format(num_time-1)
    end_sensitivities = lhs_sample.rbd_fast(obsname=obs_name, print_to_console=False)
    array_args = top_sensitivities(end_sensitivities,
                                   num_sensitivities=num_sensitivities)
    raw_sensitivities = []
    for time_i, time_point in enumerate(time_array[1:]):
        obs_name = obs_base_name + '_{0}'.format(time_i + 1)
        sensitivities = np.array(lhs_sample.rbd_fast(obsname=obs_name,
                                                     print_to_console=False)['S1'])
        raw_sensitivities.append(sensitivities[array_args])
    raw_sensitivities = np.array(raw_sensitivities)
    last_top = np.zeros_like(raw_sensitivities[:, 0])
    last_bottom = np.zeros_like(raw_sensitivities[:, 0]) 
    plt.figure(figsize=(15, 12))
    plt.xlim([time_array[1]/365.25, time_array[-1]/365.25])
    # plt.ylim([0, 1.025])
    color = 1
    for pi, param_i in enumerate(array_args):
        tsens_pos = raw_sensitivities[:, pi].copy()
        tsens_pos[tsens_pos < 0] = 0
        tsens_pos = tsens_pos + last_top
        tsens_neg = raw_sensitivities[:, pi].copy()
        tsens_neg[tsens_neg > 0] = 0
        tsens_neg = tsens_neg + last_bottom
        plt.fill_between(time_array[1:]/365.25,
                         last_top,
                         tsens_pos,
                         label=sm.parnames[param_i],
                         color='C{0}'.format(color % 10),
                         alpha=0.3)
        plt.fill_between(time_array[1:]/365.25,
                         tsens_neg,
                         last_bottom,
                         color='C{0}'.format(color % 10),
                         alpha=0.3)
        color += 1
        last_top = tsens_pos
        last_bottom = tsens_neg
    plt.legend(loc=4)
    plt.grid(alpha=0.15)
    plt.xlabel('Time $(y)$')
    if ylabel is not None: # the previous version of the code won't work for empty label since empty string also evaluates to False
        plt.ylabel(ylabel)
    else:
        plt.ylabel('First order Sensitivity')
    if title is not None:  # see comment above for y-axis label
        plt.title(title.format(ob=obs_base_name))
    else:
        plt.title('{ob} Parameter Sensitivity over time'.format(ob=obs_base_name))
    if savefig:
        plt.savefig(savefig, dpi=300)
        plt.close()
    if outfile:
        with open(outfile, 'w') as ofile:
            ofile.write(' , ' + array2string(time_array[1:]/365.25) + '\n')
            for pi, param_i in enumerate(array_args):
                ofile.write(sm.parnames[param_i] + ',')
                ofile.write(array2string(raw_sensitivities[:, pi]) + '\n')
    return raw_sensitivities


def multi_sensitivities_barplot(obs_names, system_model, lhs_sample, title=None,
                                ylabel=None, savefig=None, outfile=None):
    """
    Calculates sensitivities and makes side-by-side bar chart out of values

    :param obs_names: list of observation names to calculate sensitivites on
    :type obs_names: list

    :param system_model: System model sensitivities were ran on.
    :type system_model: openiam.SystemModel

    :param lhs_sample: Sample set for system model with observations (has been ran)
    :type lhs_sample: matk.SampleSet object

    :param title: title for figure
    :type title: str

    :param ylabel: ylabel for figure
    :type ylabel: str

    :param savefig: Filename for figure to be saved as; figure is not saved if savefig is None
    :type savefig: str

    :param outfile: Filename for csv file output of sensitivities; output is not saved if None
    :type outfile: str

    :returns: raw_sensitivities
    """
    if type(obs_names) == str:
        obs_names = [obs_names]
    total_obs = len(obs_names)
    total_params = len(system_model.parnames)
    width = 0.8
    fig = plt.figure(figsize=(15, 15))
    ax = plt.subplot(1, 1, 1)
    raw_sensitivities = {}
    for obs_i, obs_name in enumerate(obs_names):
        sens = lhs_sample.rbd_fast(obsname=obs_name, print_to_console=False)['S1']
        x_values = [total_obs*x + width*obs_i for x in range(total_params)]
        plt.bar(x_values, sens, label=obs_name)
        raw_sensitivities[obs_name] = np.array(sens)
    x_values = [total_obs*x + 0.8*(total_obs/2) for x in range(total_params)]
    ax.set_xticks(x_values)
    ax.set_xticklabels(system_model.parnames, rotation=90)
    if title is None:
        title = 'Sensitivity'
    plt.title(title)
    if ylabel is None:
        ylabel = 'First Order Sensitivity'
    plt.ylabel(ylabel)
    plt.yscale('log')
    plt.subplots_adjust(left=0.08, right=0.8, bottom=0.3)
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(handles, labels, loc=7)
    plt.grid(axis='y', linestyle='--', linewidth=1, alpha=0.3)
    if savefig:
        plt.savefig(savefig, dpi=300)
        plt.close()
    if outfile:
        with open(outfile, 'w') as ofile:
            ofile.write(' ,' + ', '.join(system_model.parnames) + '\n')
            for obs_name in obs_names:
                ofile.write(obs_name + ',' +
                            array2string(raw_sensitivities[obs_name]) + '\n')
    return raw_sensitivities


def simple_sensitivities_barplot(sensitivities, system_model,
                                 title=None, ylabel=None, savefig=None,
                                 outfile=None):
    """
    Makes simple bar chart out of sensitivity values

    :param sensitivities: dictionary of sensitivity output from SAlib analysis
    :type sensitivities: dict

    :param system_model: System model sensitivity analysis were ran for.
    :type system_model: openiam.SystemModel

    :param title: title for figure
    :type title: str

    :param ylabel: ylabel for figure
    :type ylabel: str

    :param savefig: Filename for figure to be saved as; figure is not saved if None
    :type savefig: str

    :param outfile: Filename for text file output of sensitivies; output is not saved if None
    :type outfile: str

    :returns: sensitivities['S1']
    """
    plt.figure(figsize=(10, 10))
    ax = plt.subplot(1, 1, 1)
    x_vals = list(range(len(system_model.parnames)))
    plt.bar(x_vals, sensitivities['S1'])
    ax.set_xticks(x_vals)
    ax.set_xticklabels(system_model.parnames, rotation=90)
    plt.subplots_adjust(bottom=0.3)
    if title is None:
        title = 'Sensitivity'
    plt.title(title)
    if ylabel is None:
        ylabel = 'First Order Sensitivity'
    plt.ylabel(ylabel)
    plt.yscale('log')
    plt.grid(axis='y', linestyle='--', linewidth=1, alpha=0.3)
    if savefig:
        plt.savefig(savefig, dpi=300)
        plt.close()
    if outfile:
        with open(outfile, 'w') as ofile:
            for i, par in enumerate(system_model.parnames):
                ofile.write(par + ':  {0}\n'.format(sensitivities['S1'][i]))
    return sensitivities['S1']


def stacked_sensitivities_barplot(sensitivities, names, system_model,
                                  title=None, ylabel=None, savefig=None):
    """
    Makes simple bar chart out of sensitivity values

    :param sensitivities: list of dictionaries of sensitivity output from SAlib analysis
    :type sensitivities: lst

    :param names: List of names for sensitivity labels
    :type names: lst

    :param system_model: System model sensitivities were ran on.
    :type system_model: openiam.SystemModel

    :param title: title for figure
    :type title: str

    :param ylabel: ylabel for figure
    :type ylabel: str

    :param savefig: Filename for figure to be saved as, not saved if None
    :type savefig: str

    :returns: None
    """
    # TODO this doesn't look good.  Subset parameters somehow.
    plt.figure(figsize=(10, 10))
    ax = plt.subplot(1, 1, 1)
    x_vals = list(range(len(sensitivities)))
    bottom = np.array([0 for x in x_vals])
    for i, param in enumerate(system_model.parnames):
        y = np.array([x['S1'][i] for x in sensitivities])
        plt.bar(x_vals, y, bottom=bottom, label=param)
        bottom = bottom + y
    ax.set_xticks(x_vals)
    ax.set_xticklabels(names, rotation=90)
    plt.subplots_adjust(bottom=0.3, right=0.65)
    if title is None:
        title = 'Sensitivity'
    plt.title(title)
    if ylabel is None:
        ylabel = 'First Order Sensitivity'
    plt.ylabel(ylabel)
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.yscale('log')
    plt.grid(axis='y', linestyle='--', linewidth=1, alpha=0.3)
    if savefig:
        plt.savefig(savefig, dpi=300)
    return

if __name__ == "__main__":

    # Define keyword arguments of the system model
    num_years = 50
    time_array = 365.25*np.arange(0.0, num_years+1)
    sm_model_kwargs = {'time_array': time_array}  # time is given in days

    # Create system model
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # Add reservoir component
    sres = sm.add_component_model_object(SimpleReservoir(name='sres', parent=sm))

    # Add parameters of reservoir component model
    sres.add_par('numberOfShaleLayers', value=3, vary=False)
    sres.add_par('shale1Thickness', min=900.0, max=1100., value=1000.0)
    sres.add_par('shale2Thickness', min=900.0, max=1100., value=1000.0)
    # Shale 3 has a fixed thickness of 11.2 m
    sres.add_par('shale3Thickness', value=11.2, vary=False)
    # Aquifer 1 (thief zone has a fixed thickness of 22.4)
    sres.add_par('aquifer1Thickness', value=22.4, vary=False)
    # Aquifer 2 (shallow aquifer) has a fixed thickness of 19.2
    sres.add_par('aquifer2Thickness', value=500, vary=False)
    # Reservoir has a fixed thickness of 51.2
    sres.add_par('reservoirThickness', value=51.2, vary=False)

    # Add observations of reservoir component model
    sres.add_obs('pressure')
    sres.add_obs('CO2saturation')
    sres.add_obs_to_be_linked('pressure')
    sres.add_obs_to_be_linked('CO2saturation')

    # Add open wellbore component
    ow = sm.add_component_model_object(OpenWellbore(name='ow', parent=sm))

    # Add parameters of open wellbore component
    ow.add_par('wellRadius', min=0.01, max=0.02, value=0.015)
    ow.add_par('logNormalizedTransmissivity', min=-1.0, max=1.0, value=0.0)
    ow.add_par('brineSalinity', value=0.1, vary=False)

    # Add keyword arguments of the open wellbore component model
    ow.add_kwarg_linked_to_obs('pressure', sres.linkobs['pressure'])
    ow.add_kwarg_linked_to_obs('CO2saturation', sres.linkobs['CO2saturation'])

    # Add composite parameter of open wellbore component
    ow.add_composite_par('reservoirDepth', expr='sres.shale1Thickness' +
                         '+sres.shale2Thickness + sres.shale3Thickness' +
                         '+sres.aquifer1Thickness+ sres.aquifer2Thickness')
    ow.add_composite_par('wellTop', expr='sres.shale3Thickness' +
                         '+sres.aquifer2Thickness')

    # Add observations of multisegmented wellbore component model
    ow.add_obs_to_be_linked('CO2_aquifer')
    ow.add_obs_to_be_linked('brine_aquifer')
    ow.add_obs_to_be_linked('brine_atm')
    ow.add_obs_to_be_linked('CO2_atm')
    ow.add_obs('CO2_aquifer')
    ow.add_obs('brine_aquifer')
    ow.add_obs('CO2_atm')  # zero since well top is in aquifer
    ow.add_obs('brine_atm')  # zero since well top is in aquifer

    # Add adapter that transforms leakage rates to accumulated mass
    adapt = sm.add_component_model_object(RateToMassAdapter(name='adapt', parent=sm))
    adapt.add_kwarg_linked_to_collection('CO2_aquifer',
                                         [ow.linkobs['CO2_aquifer'],
                                          ow.linkobs['CO2_atm']])
    adapt.add_kwarg_linked_to_collection('brine_aquifer',
                                         [ow.linkobs['brine_aquifer'], ow.linkobs['brine_atm']])
    adapt.add_obs_to_be_linked('mass_CO2_aquifer')
    adapt.add_obs_to_be_linked('mass_brine_aquifer')
    adapt.add_obs('mass_CO2_aquifer')
    adapt.add_obs('mass_brine_aquifer')

    # add carbonate aquifer model object and define parameters
    ca = sm.add_component_model_object(CarbonateAquifer(name='ca', parent=sm))
    ca.add_par('perm_var', min=0.017, max=1.89, value=0.9535)
    ca.add_par('corr_len', min=1.0, max=3.95, value=2.475)
    ca.add_par('aniso', min=1.1, max=49.1, value=25.1)
    ca.add_par('mean_perm', min=-13.8, max=-10.3, value=-12.05)
    ca.add_par('aqu_thick', min=100., max=500., value=300.)
    ca.add_par('hyd_grad', min=2.88e-4, max=1.89e-2, value=9.59e-3)
    ca.add_par('calcite_ssa', min=0, max=1.e-2, value=5.5e-03)
    ca.add_par('organic_carbon', min=0, max=1.e-2, value=5.5e-03)
    ca.add_par('benzene_kd', min=1.49, max=1.73, value=1.61)
    ca.add_par('benzene_decay', min=0.15, max=2.84, value=1.5)
    ca.add_par('nap_kd', min=2.78, max=3.18, value=2.98)
    ca.add_par('nap_decay', min=-0.85, max=2.04, value=0.595)
    ca.add_par('phenol_kd', min=1.21, max=1.48, value=1.35)
    ca.add_par('phenol_decay', min=-1.22, max=2.06, value=0.42)
    ca.add_par('cl', min=0.1, max=6.025, value=0.776)
    ca.model_kwargs['x'] = [100.]
    ca.model_kwargs['y'] = [100.]

    CO2_rate_obs_list = []
    brine_rate_obs_list = []
    CO2_mass_obs_list = []
    brine_mass_obs_list = []
    CO2_rate_obs_list.append(ow.linkobs['CO2_aquifer'])
    brine_rate_obs_list.append(ow.linkobs['brine_aquifer'])
    CO2_mass_obs_list.append(adapt.linkobs['mass_CO2_aquifer'])
    brine_mass_obs_list.append(adapt.linkobs['mass_brine_aquifer'])

    # Add aquifer component's keyword argument co2_rate linked to the collection created above
    ca.add_kwarg_linked_to_collection('co2_rate', CO2_rate_obs_list)
    ca.add_kwarg_linked_to_collection('brine_rate', brine_rate_obs_list)
    ca.add_kwarg_linked_to_collection('co2_mass', CO2_mass_obs_list)
    ca.add_kwarg_linked_to_collection('brine_mass', brine_mass_obs_list)

    # Add observations (output) from the carbonate aquifer model
    ca.add_obs('pH')
    ca.add_obs('TDS')

    num_samples = 1000
    ncpus = 5
    # Draw Latin hypercube samples of parameter values
    lhs_sample = sm.lhs(siz=num_samples, seed=random.randint(500, 1100))

    lhs_sample.run(cpus=ncpus, verbose=False)

    # Run RBD_fast sensitivity analysis on mass of CO2 leaked into aquifer
    mass_CO2_sensitvity = lhs_sample.rbd_fast(obsname='adapt.mass_CO2_aquifer_50')
    simple_sensitivities_barplot(mass_CO2_sensitvity,
                                 sm,
                                 title='RBD-Fast Sensitivity\nfor Mass of $CO_2$ Leaked into the Aquifer',
                                 ylabel='First Order Sensitivity\n(Calculated using RBD-Fast Method)',
                                 savefig='RBD-fast_co2_leak_plot.png')

    # Add capture point so sensitivity is calculated at time other than ending
    capture_point = 24  # in years
    cp_index = np.argmin(np.abs(time_array - capture_point*365.25))
    sensitivity_obs = 'ca.pH'
    sen_obs_name = sensitivity_obs + '_{0}'.format(cp_index)

    # Run sensitivity analysis on aquifer pH plume size at capture point
    ph_sensitivity = lhs_sample.rbd_fast(obsname=sen_obs_name)

    simple_sensitivities_barplot(ph_sensitivity,
                                 sm,
                                 title='First Order Sensitivity\nfor pH Plume in Aquifer',
                                 ylabel='First Order Sensitivity\n(Calculated using RBD-Fast Method)',
                                 savefig='RBD-fast_ph_Plume_plot.png',
                                 outfile='pH_sensitivities.txt')

    time_series_sensitivities('adapt.mass_CO2_aquifer', sm, lhs_sample, time_array,
                              savefig='RBD-fast_MassCO2_time_series.png',
                              outfile='mass_CO2_aquifer_time_sens.csv')

    time_series_sensitivities('ca.pH', sm, lhs_sample, time_array,
                              savefig='RBD-fast_pH_plume_vol_time_series.png',
                              outfile='pH_time_sens.csv')

    multi_sensitivities_barplot(['sres.pressure_50',
                                 'ow.CO2_aquifer_50',
                                 'ow.brine_aquifer_50',
                                 'adapt.mass_CO2_aquifer_50',
                                 'adapt.mass_brine_aquifer_50',
                                 'ca.pH_50',
                                 'ca.TDS_50'],
                                sm, lhs_sample,
                                savefig='multi-bar-sensitivities.png',
                                outfile='multi_sens.csv')

    corr_coeffs = correlations_at_time(lhs_sample, capture_point=50,
                                       excludes=['ow.CO2_atm', 'ow.brine_atm'],
                                       plot=True, figsize=(15, 15),
                                       printout=False, xrotation=90,
                                       title='Pearson Correlation Coefficents at 50 years',
                                       savefig='Corr_coeff_at_time_50.png',
                                       outfile='Corr_coeffs.csv')
