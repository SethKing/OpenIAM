# -*- coding: utf-8 -*-
"""
Post processing analysis and plots for OpenIAM
"""
import collections
import itertools
import os
import pickle
import matplotlib.pyplot as plt
import sys
sys.path.append(os.path.join('..', '..'))
import openiam
try:
    from .time_series import time_series_plot
    from .map_plot import map_plume_plot_single, map_plume_plot_ensemble
    from .sensitivity_analysis import correlations_at_time, simple_sensitivities_barplot, multi_sensitivities_barplot, time_series_sensitivities
except ModuleNotFoundError:
    from time_series import time_series_plot
    from map_plot import map_plume_plot_single, map_plume_plot_ensemble
    from sensitivity_analysis import correlations_at_time, simple_sensitivities_barplot, multi_sensitivities_barplot, time_series_sensitivities


class IAM_Post_Processor(object):
    """
    Post processor to build plots of OpenIAM simulation results and
    compute sensitivity analysis and correlation coeffiecents.

    Attributes:
    analysis_type - 'forward', 'lhs', or 'parstudy' openiam simulation type
    outputs - list of outputs valid for plotting
    atm_cmp - boolean if atmospheric components exist for atm map plots
    """
    def __init__(self, folder):
        """
        Constructor method for Post processor class

        :param folder: Path to output folder for OpenIAM simulation results
        :type folder: str

        :returns: IAM_Post_Processor class object
        """
        self.folder = folder
        self.load_folder(folder)
        self.get_outputs()
        self.analysis_type = self.sim_data['ModelParams']['Analysis_type']
        self.components = self.sim_data['components']
        self.time_array = self.sim_data['time_array']
        self.atm_cmp = bool(self.find_atm_comp())
        return

    def load_folder(self, folder):
        """
        Method to load OpenIAM results into post processor object
        """
        output_filename = os.path.join(folder, 'output_dump.pkl')
        if not(os.path.exists(output_filename)):
            print(output_filename)
            raise ValueError('IAM Output file not found is specified folder')
        with open(output_filename, 'rb') as fi:
            self.sim_data = pickle.load(fi)
        return

    def get_outputs(self):
        """
        Populates self.outputs attribure with a list of outputs from simulation that can be plotted.
        """
        output_list = self.sim_data['output_list']
        self.outputs = []
        for ov in itertools.chain(*output_list.values()):
            if ov not in self.outputs:
                self.outputs.append(ov)
        return

    def plotter(self, outputs, plottype, name=None, title=None, savefig=None, subplot={'use': False}):
        """
        Method for plotting simulation results

        :param outputs: List of output names to plot
        :type outputs: list

        :param plottype: String of plottype. Values can be: TimeSeries, 
                         TimeSeriesStats, TimeSeriesAndStats, AtmPlumeSingle,
                         or AtmPlumeEnsemble
        :type plottype: str

        :param name: Name of the figure to create
        :type name: str

        :param title: Title to print on the plot
        :type title: str

        :param savefig: Filename to save the file as. Name only, saved to output folder.
        :type savefig: str

        :param subplot: Dictionary for subplot controls, use=True will use subplotting (boolean default=False),
                        ncols=n will use n columns of subplots (positive integer default 1)
                        comp.var_name = sub_title will title the subplots of comp.var_name subtitle (string default=comp.var_name)
        :type subplot: dict
        """
        if not name:
            name = ' '.join(outputs)
        plt.figure(name, figsize=(13, 8))
        if savefig:
            savefig = os.path.join(self.folder, savefig)
            if not os.path.exists(os.path.dirname(savefig)):
                os.mkdir(os.path.dirname(savefig))
        if plottype == 'TimeSeries':
            time_series_plot(outputs, self.sim_data['sm'], self.sim_data['s'],
                             self.sim_data['output_list'],
                             name=name,
                             analysis=self.analysis_type,
                             savefig=savefig,
                             title=title,
                             subplot=subplot,
                             plot_type=['real'])
        elif plottype == 'TimeSeriesStats':
            time_series_plot(outputs, self.sim_data['sm'], self.sim_data['s'],
                             self.sim_data['output_list'],
                             name=name,
                             analysis=self.analysis_type,
                             savefig=savefig,
                             title=title,
                             subplot=subplot,
                             plot_type=['stats'])
        elif plottype == 'TimeSeriesAndStats':
            time_series_plot(outputs, self.sim_data['sm'], self.sim_data['s'],
                             self.sim_data['output_list'],
                             name=name,
                             analysis=self.analysis_type,
                             savefig=savefig,
                             title=title,
                             subplot=subplot,
                             plot_type=['real', 'stats'])
        elif plottype == 'AtmPlumeSingle':
            satm = self.find_atm_comp()
            map_plume_plot_single(self.sim_data['s'],
                                  satm,
                                  self.time_array,
                                  savefig=savefig,
                                  sample=0,
                                  extent=None)
        elif plottype == 'AtmPlumeEnsemble':
            satm = self.find_atm_comp()
            map_plume_plot_ensemble(self.sim_data['s'],
                                    satm,
                                    self.time_array,
                                    extent=None,
                                    xy=None,
                                    savefig=savefig,
                                    figsize=(15, 10))

    def find_atm_comp(self):
        """
        Method to find AtmosphericROM component of a system model
        """
        for comp in self.components[::-1]:
            if type(comp) == openiam.AtmosphericROM:
                return comp
        else:
            return False
        
    def sensitivity_analysis(self, sensitivity_type, outputs, sensitivity_dict):
        """
        Method for computing and plotting correlation or sensitivity coeffiecents
        
        :param sensitivity_type: Type of Sensitivity Analysis to perform.  Valid inputs are
                                 CorrelationCoeff, SensitivityCoeff, MultiSensitivities, TimeSeriesSensitivity
        :type sensitivity_type: str
        
        :param outputs: List of output names to perform sensitivity analysis on.
        :type outputs: list

        :param sensitivity_dict: Dictionary for sensitivity analysis options
        :type sensitivity_dict: dict
        """
        if sensitivity_type == 'CorrelationCoeff':
            corrcoeff_dict = {'capture_point': len(self.time_array)-1,
                              'excludes': [],
                              'ctype': 'pearson',
                              'plot': True,
                              'printout': False,
                              'plotvals': True,
                              'figsize': (15, 15),
                              'title': 'Pearson Correlation Coefficients at time {cp}',
                              'xrotation': 90,
                              'savefig': 'correlation_coefficients.png',
                              'outfile': 'correlation_coefficients.csv'}
            corrcoeff_dict.update(sensitivity_dict)
            if corrcoeff_dict['savefig']:
                corrcoeff_dict['savefig'] = os.path.join(self.folder,
                                                         corrcoeff_dict['savefig'])
            if corrcoeff_dict['outfile']:
                corrcoeff_dict['outfile'] = os.path.join(self.folder,
                                                         corrcoeff_dict['outfile'])
            correlations_at_time(self.sim_data['s'], **corrcoeff_dict)
        elif sensitivity_type == 'SensitivityCoeff':
            if type(outputs) == str:
                outputs = [outputs]
            res_obs = self.resolve_obs_names(outputs)
            if 'capture_point' in sensitivity_dict:
                capture_point = sensitivity_dict.pop('capture_point')
            else:
                capture_point = len(self.time_array)-1
            if not isinstance(capture_point, collections.Iterable):
                capture_points = [capture_point]
            else:
                capture_points = capture_point
            for capture_point in capture_points:
                cp_obs = ['{ob}_{cp}'.format(ob=ob, cp=capture_point)
                          for ob in res_obs]
                sens_dict_full = {'title': '{ob} Sensitivity Coefficients',
                                  'ylabel': None,
                                  'savefig': '{ob}_sensitivity.png',
                                  'outfile': '{ob}_sensitivity.txt'}
                sens_dict_full.update(sensitivity_dict)
                for ob in cp_obs:
                    if sens_dict_full['savefig']:
                        sens_dict_full['savefig'] = os.path.join(self.folder,
                                                                 sens_dict_full['savefig'])
                    if sens_dict_full['outfile']:
                        sens_dict_full['outfile'] = os.path.join(self.folder,
                                                                 sens_dict_full['outfile'])
                    sens_dict2 = {}
                    for key, value in list(sens_dict_full.items()):
                        if type(value) == str:
                            v = value.format(ob=ob, cp=capture_point)
                        else:
                            v = value
                        sens_dict2[key] = v
                    sensitivities = self.sim_data['s'].rbd_fast(obsname=ob, print_to_console=False)
                    simple_sensitivities_barplot(sensitivities, self.sim_data['sm'],
                                                 **sens_dict2)
        elif sensitivity_type == 'MultiSensitivities':
            if type(outputs) == str:
                outputs = [outputs]
            res_obs = self.resolve_obs_names(outputs)
            if 'capture_point' in sensitivity_dict:
                capture_point = sensitivity_dict.pop('capture_point')
            else:
                capture_point = len(self.time_array)-1
            if not isinstance(capture_point, collections.Iterable):
                capture_points = [capture_point]
            else:
                capture_points = capture_point
            for capture_point in capture_points:
                cp_obs = ['{ob}_{cp}'.format(ob=ob, cp=capture_point)
                          for ob in res_obs]
                sens_dict_full = {'title': 'Sensitivity Coefficients',
                                  'ylabel': None,
                                  'savefig': 'Sensitivity_Analysis.png',
                                  'outfile': 'Sensitivity_Analysis.csv'}
                sens_dict_full.update(sensitivity_dict)
                if sens_dict_full['savefig']:
                    sens_dict_full['savefig'] = os.path.join(self.folder,
                                                             sens_dict_full['savefig'])
                if sens_dict_full['outfile']:
                    sens_dict_full['outfile'] = os.path.join(self.folder,
                                                             sens_dict_full['outfile'])
                multi_sensitivities_barplot(cp_obs,
                                            self.sim_data['sm'],
                                            self.sim_data['s'],
                                            **sens_dict_full)
        elif sensitivity_type == 'TimeSeriesSensitivity':
            if type(outputs) == str:
                outputs = [outputs]
            res_obs = self.resolve_obs_names(outputs)
            if 'capture_point' in sensitivity_dict:
                capture_point = sensitivity_dict.pop('capture_point')
            else:
                capture_point = len(self.time_array)-1
            sens_dict_full = {'title': '{ob} Time Sensitivity Coefficients',
                              'ylabel': None,
                              'num_sensitivities': 5,
                              'savefig': '{ob}_Sensitivity_Analysis.png',
                              'outfile': '{ob}_Sensitivity_Analysis.csv'}
            sens_dict_full.update(sensitivity_dict)
            for ob in res_obs:
                if sens_dict_full['savefig']:
                    sens_dict_full['savefig'] = os.path.join(self.folder,
                                                             sens_dict_full['savefig'])
                if sens_dict_full['outfile']:
                    sens_dict_full['outfile'] = os.path.join(self.folder,
                                                             sens_dict_full['outfile'])
                sens_dict = {}
                for key, value in list(sens_dict_full.items()):
                    if type(value) == str:
                        v = value.format(ob=ob)
                    else:
                        v = value
                    sens_dict[key] = v
                time_series_sensitivities(ob,
                                          self.sim_data['sm'],
                                          self.sim_data['s'],
                                          self.time_array,
                                          capture_point=capture_point,
                                          **sens_dict)
        else:
            raise IOError('sensitivity_type not reconized')

    def resolve_obs_names(self, obs):
        '''
        Takes in base observation name and returns list of appended cm.obs for
        all component model names that have observations matching the base name.
        '''
        output_list = self.sim_data['output_list']
        resolved_names = []
        for ob in obs:
            for output_component in list(output_list.keys()):
                if ob in output_list[output_component]:
                    resolved_names.append('.'.join([output_component.name, ob]))
        return resolved_names
