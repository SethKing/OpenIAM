# -*- coding: utf-8 -*-
"""
The Atmospheric model is meant to be used for performing scoping studies for |CO2|
dispersion after leakage out of the ground. The employed method is an
extension of the nomograph approach of Britter and McQuaid (1988)
:cite:`BritterMcQuaid1988` developed for estimating dense gas plume length from
a single or multiple leakage sources. The method is very fast
and, therefore, amenable to general system-level geologic carbon sequestration
(GCS) risk assessment. The method is conservative: it assumes the wind
could be from any direction and handles multiple sources by a simple
superposition approach :cite:`ZHANG2016323`.

The model is intended to be use for large |CO2| leakage rates
(e.g., leakage from an open wellbore).  It may not be suitable for
very small leakage rate, as, in general, small release rates
(e.g., less than 1.e-5 kg/s) do not form a dense gas release due to ambient
mixing. The inputs to the model are leakage rate(s) from leaky well(s),
location(s) of leaky well(s), ambient conditions (wind speed), and receptor
locations (home or business locations where people are present). The outputs
from the model are flags at receptors indicating whether the |CO2| concentration
at the location exceeds a pre-defined critical value, and the critical
downwind distance from the sources.

Within the control file interface, receptor locations can be specified with
the *receptors* keyword argument assigned a full path (including a name) to
a csv file containing x- and y-coordinates of the receptors.  Alternatively,
the *x_receptor* and *y_receptor* keywords can be assigned a list of x- and
y-coordinates of the receptors, respectively.

Component model input definitions:

* **T_amb** [C] (5 to 40) - ambient temperature (default: 15.0 C)
* **P_amb** [atmosphere] (0.7 to 1.08)- ambient pressure (default: 1.0)
* **V_wind** [m/s]  (1.e-10 to 20)- wind velocity (default 5 m/s)
* **C0_critical** [-] (0.002 to 0.1) - critical concentration, dimensionless (default 0.01)
* **T_source** [C] (5 to 50)  - released |CO2| temperature in C (default 15.0 C)
* **x_receptor** [m] - x-coordinate of receptor
* **y_receptor** [m] - y-coordinate of receptor

Possible observations from the Atmospheric Model component are:

* **out_flag_r###** [-] - count of critical distances receptor is within from original leak points.
  Here, ### is a receptor number starting at 000.
* **num_sources** [-] - number of sources. The possible maximum is a number of leakage points.
  Could be less as leakages can potentially coalesce.
* **x_new_s###** [m] - x-coordinate of leakage source.
* **y_new_s###** [m] - y-coordinate of leakage source.
* **critical_distance_s###** [m] - critical downwind distance from each source.

"""
import ctypes
import logging
import numpy as np
import os
import sys
from sys import platform
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

try:
    from openiam import SystemModel, ComponentModel
except ImportError as err:
    print('Unable to load IAM class module: '+str(err))

try:
    import components.atmosphere as atmModel
except ImportError as err:
    print('\nERROR: Unable to load ROM for atmosperic ROM component\n')
    sys.exit()


class AtmosphericROM(ComponentModel):
    def __init__(self, name, parent, model_args=[], model_kwargs={},
                 workdir=None, header_file_dir=None):
        """
        Constructor method of AtmosphericROM class

        :param name: name of component model
        :type name: str

        :param parent: the SystemModel object that the component model
            belongs to
        :type parent: SystemModel object

        :param model_args: additional optional parameters of the component
            model; by default model_args is empty list []
        :type model_args: [float]

        :param model_kwargs: additional optional keyword arguments of the
            component model; by default model_kwargs is empty dictionary {}.
        :type model_kwargs: dict

        :param workdir: name of directory to use for model runs (serial run
            case)
        :type workdir: str

        :param header_file_dir: name of directory with Fortran DLL
        :type header_file_dir: str

        :returns: AtmosphericROM class object
        """
        if header_file_dir is None:
            model_kwargs['header_file_dir'] = atmModel.__path__[0]
        else:
            model_kwargs['header_file_dir'] = header_file_dir

        super(AtmosphericROM, self).__init__(name, parent,
                                             model=self.model,
                                             model_args=model_args,
                                             model_kwargs=model_kwargs,
                                             workdir=workdir)

        # Placeholder for keyword arguments of the 'model' method:
        # to let the system model know that this component needs the specified keyword arguments
        self.model_kwargs['time_point'] = 365.25  # default value of 365.25 days
        self.model_kwargs['time_step'] = 365.25   # default value of 365.25 days

        # Set default values for user defined parameters of the component model
        self.add_default_par('T_amb', value=15.0)
        self.add_default_par('P_amb', value=1.0)
        self.add_default_par('V_wind', value=5.0)
        self.add_default_par('C0_critical', value=0.01)
        self.add_default_par('T_source', value=15.0)
        self.model_kwargs['x_receptor'] = [0.0]
        self.model_kwargs['y_receptor'] = [0.0]

        # Define dictionary of model parameters boundaries
        self.pars_bounds = dict()
        self.pars_bounds['T_amb'] = [5.0, 40.0]
        self.pars_bounds['P_amb'] = [0.7, 1.08]
        self.pars_bounds['V_wind'] = [1.e-10, 20.0]
        self.pars_bounds['C0_critical'] = [0.002, 0.1]
        self.pars_bounds['T_source'] = [5.0, 50.0]

    # should not this be something written once but all component models can use?
    def check_input_parameters(self, p):
        """
        Check whether input parameters fall within specified boundaries.

        :param p: input parameters of component model
        :type p: dict
        """
        logging.debug('Input parameters {}'.format(p))

        for key, val in p.items():
            if key in self.pars_bounds:
                if ((val < self.pars_bounds[key][0])or
                    (val > self.pars_bounds[key][1])):
                        logging.warning(' Parameter {} is out of boundaries.'.format(key))
            else:
                logging.warning('Parameter {key} not recognized as an Atmospheric Model input parameter.'
                                .format(key=key))

    # atmospheric model function
    def model(self, p, x_coor=[], y_coor=[], co2_leakrate=[],  time_point=365.25, time_step=365.25,
              x_receptor=[], y_receptor=[], header_file_dir=None):

        """
        Return flag at receptors if |CO2| concentration is above critical values.

        :param p: dictionary of input parameters
        :type p: dict

        :param header_file_dir: path to header files
        :type header_file_dir: str

        :param time_point: current time point in days (at which the output is
            to be provided); by default, its value is 365.25 (1 year in days)
        :type time_point: float

        :param time_step: difference between current and previous time points
            in days; by default, its value is 365.25 (1 year in days)
        :type time_point: float

        :param x_coor: horizontal x-coordinate of leaking well (m)
        :type x_coor: [float]

        :param y_coor: horizontal y-coordinate of leaking well (m)
        :type y_coor: [float]

        :param co2_leakrate: CO2 leakage rate, kg/s
        :type co2_leakrate: [float]

        :returns: dictionary of locations and flags at receptors; keys:['out_flag']
                  new source locations if co2 leakage sources are combined;
                  keys:['x_new', 'y_new']; and critical downwind
                  distance: ['critical_distance']
        """

        logging.debug('{sr} model call input {p}, leak {leak}'.format(sr=self.name, p=p, leak=co2_leakrate))

        # Obtain the default values of the parameters from dictionary of default parameters
        actual_p = dict([(k, v.value) for k, v in self.default_pars.items()])
        # Update default values of parameters with the provided ones
        actual_p.update(p)

        # Set parameters
        T_amb = actual_p['T_amb']
        P_amb = actual_p['P_amb']
        V_wind = actual_p['V_wind']
        C0_critical = actual_p['C0_critical']
        T_source = actual_p['T_source']

        N_receptor = len(x_receptor)

        input_array = np.array([T_amb, P_amb, V_wind,
                                C0_critical, T_source])
        Ninput = np.size(input_array)

        # determine number of leaks
        # x,y,co2_rate,brine_rate,co2_mass,brine_mass must all be same length
        nleak = len(x_coor)

        # get time in days from keyword arguments, convert to years
        Nstep = int(time_point/time_step)

        # Set up library
        if platform == "linux" or platform == "linux2":
            # linux
            library_name = "atmdisrom.so"
        elif platform == "darwin":
            # OS X
            library_name = "atmdisrom.dylib"
        elif platform == "win32":
            # Windows...
            library_name = "atmdisrom.dll"

        library = os.sep.join([header_file_dir, library_name])
        # Specify function name
        functionName = "atmdisrom"

        # Define c classes
        INT = ctypes.c_int
        DOUBLE = ctypes.c_double
        LEAKS = DOUBLE*nleak
        RECEPTORS = DOUBLE*N_receptor
        NDOUBLE = DOUBLE*Ninput
        NINT = INT*N_receptor

        # Load DLL
        external_lib = ctypes.cdll.LoadLibrary(os.path.join(os.getcwd(), library))

        # Get needed function as attribute of the library
        function = getattr(external_lib, functionName)

        # Set argument types for value and pointers
        function.argtypes = [ctypes.POINTER(INT),  # nleak
                             ctypes.POINTER(INT),  # n_receptor
                             ctypes.POINTER(INT),  # n_step
                             ctypes.POINTER(NDOUBLE),  # input_array
                             ctypes.POINTER(LEAKS),  # x_coor
                             ctypes.POINTER(LEAKS),  # y_coor
                             ctypes.POINTER(RECEPTORS),  # x_receptor
                             ctypes.POINTER(RECEPTORS),	 # y_receptor
                             ctypes.POINTER(LEAKS),  # co2_leakrate
                             ctypes.POINTER(NINT),  # output_array
                             ctypes.POINTER(INT),  # num_source
                             ctypes.POINTER(LEAKS),  # combined source location, x
                             ctypes.POINTER(LEAKS),  # combined source location, y
                             ctypes.POINTER(LEAKS),  # critical downwind distance
                             ctypes.POINTER(INT)]  # message if using B&M method is valid
        function.restype = None

        par_nleak = INT(nleak)
        par_nreceptor = INT(N_receptor)
        par_nstep = INT(Nstep)
        par_inputarray = NDOUBLE(*input_array)
        par_x = LEAKS(*x_coor)
        par_y = LEAKS(*y_coor)
        par_co2rate = LEAKS(*co2_leakrate)
        par_xre = RECEPTORS(*x_receptor)
        par_yre = RECEPTORS(*y_receptor)

        out_flag = NINT()    # initialize output array
        num_source = INT()
        x_new = LEAKS()
        y_new = LEAKS()
        critical_distance = LEAKS()
        log_message = INT()

        function(par_nleak, par_nreceptor, par_nstep, par_inputarray,
                 par_x,  par_y, par_xre, par_yre, par_co2rate, out_flag,
                 num_source, x_new, y_new, critical_distance, log_message)

        out = dict()

        if (log_message == 1):
            logging.warning('Dense gas criteria is not satisfied, results from B&M may not be valid')
        if (log_message == 2):
            logging.warning('Alpha value in the B&M is out of valid range, results may not be correct')
        if (log_message == 3):
            logging.warning('Dense gas criteria is not satisfied, alpha value in the B&M is out of valid range, results may not be correct')

        for i, outflag_i in enumerate(out_flag):
            out['outflag_r{0:03}'.format(i)] = outflag_i   # extract value from the float type of output

        out['num_sources'] = num_source.value

        for i, x_new_i in enumerate(x_new):
            out['x_new_s{0:03}'.format(i)] = x_new_i

        for i, y_new_i in enumerate(y_new):
            out['y_new_s{0:03}'.format(i)] = y_new_i
        for i, critical_distance_i in enumerate(critical_distance):
            out['critical_distance_s{0:03}'.format(i)] = critical_distance_i
        # Return output dictionary
        return out

    system_collected_inputs = {'co2_leakrate': 'CO2_atm'}

    def connect_with_system(self, component_data, name2obj_dict,
                            welllocs=None, num_pathways=1):
        """
        Add AtmosphericROM to system model for control file interface.

        :param component_data: Dictionary of component data including 'Connection'
        :type component_data: dict

        :param name2obj_dict: Dictionary to map component names to objects
        :type name2obj: dict

        :param welllocs: dictionary of x and y coordinates of leaking wellbores,
            contains keys 'coordx' and 'coordy'
        :type welllocs: dict

        :param num_pathways: number of leakage pathways
        :type num_pathways: int

        :returns: None
        """
        if ('Parameters' in component_data) and (component_data['Parameters']):
            for key in component_data['Parameters']:
                if type(component_data['Parameters'][key]) != dict:
                    component_data['Parameters'][key] = {'value': component_data['Parameters'][key],
                                                         'vary': False}
                self.add_par(key, **component_data['Parameters'][key])

        if ('DynamicParameters' in component_data) and (component_data['DynamicParameters']):
            for key in component_data['DynamicParameters']:
                self.add_dynamic_kwarg(key, component_data['DynamicParameters'][key])

        self.model_kwargs['x_coor'] = welllocs['coordx'][:num_pathways]
        self.model_kwargs['y_coor'] = welllocs['coordy'][:num_pathways]

        if ('receptors' in component_data) and (component_data['receptors']):
            xy_data = np.genfromtxt(component_data['receptors'],
                                    delimiter=',')
            self.model_kwargs['x_receptor'] = xy_data[:, 0]
            self.model_kwargs['y_receptor'] = xy_data[:, 1]
        elif ('x_receptor' in component_data) and (component_data['x_receptor']):
            self.model_kwargs['x_receptor'] = component_data['x_receptor']
            self.model_kwargs['y_receptor'] = component_data['y_receptor']

        # Make model connections
        if 'Connection' in component_data:
            collectors = self._parent.collectors
            for collect in collectors:
                cdict = collectors[collect]
                argument = cdict['argument']
                connect = cdict['Connection']
                for i in range(num_pathways):
                    cname = connect + '_{0:03}'.format(i)
                    connector = name2obj_dict[cname]
                    connector.add_obs_to_be_linked(argument)
                    collectors[collect]['data'].append(connector.linkobs[argument])
            system_collected_inputs = {'co2_leakrate': 'CO2_atm'}
            for sinput in system_collected_inputs:
                self.add_kwarg_linked_to_collection(sinput,
                                                    collectors[sinput]['data'])
        # End Connection if statement
        if 'Outputs' in component_data:
            if 'outflag' in component_data['Outputs']:
                component_data['Outputs'].remove('outflag')
                for ir in range(len(self.model_kwargs['x_receptor'])):
                    component_data['Outputs'].append('outflag_r{0:03}'.format(ir))
            source_outputs = ['x_new', 'y_new', 'critical_distance']
            for source in source_outputs:
                if source in component_data['Outputs']:
                    component_data['Outputs'].remove(source)
                    for ip in range(num_pathways):
                        component_data['Outputs'].append(source + '_s{0:03}'.format(ip))
        return

if __name__ == "__main__":
    # Create system model
    time_array = 365.25*np.arange(0.0, 2.0)
    sm_model_kwargs = {'time_array': time_array}  # time is given in days
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # input parameters by user
    satm = sm.add_component_model_object(AtmosphericROM(name='satm', parent=sm))
    satm.add_par('T_amb', value=20.0)
    satm.add_par('P_amb', value=1.01E+00)
    satm.add_par('V_wind', value=5.0)
    satm.add_par('C0_critical', value=0.01)

    # input parameters passed by other models
    satm.model_kwargs['x_coor'] = [0.1, 100.2]
    satm.model_kwargs['y_coor'] = [0.2, 100.3]
    satm.model_kwargs['co2_leakrate'] = [1.90e-01, 0.1]  # kg/s

    satm.model_kwargs['x_receptor'] = [100., 10]
    satm.model_kwargs['y_receptor'] = [110., 10]

    n_receptors = len(satm.model_kwargs['x_receptor'])
    n_sources = len(satm.model_kwargs['co2_leakrate'])
    # Add observations (output) from the atmospheric ROM model
    for i in range(n_receptors):
        satm.add_obs('outflag_r{0:03}'.format(i))

    for i in range(n_sources):
        satm.add_obs('x_new_s{0:03}'.format(i))
        satm.add_obs('y_new_s{0:03}'.format(i))
        satm.add_obs('critical_distance_s{0:03}'.format(i))

    satm.add_obs('num_sources')

    sm.forward()

    num_sources = sm.collect_observations_as_time_series(satm, 'num_sources')

    # Print the observations
    for i in range(n_receptors):
        print('Receptor_flag_r{0:03}'.format(i),
              satm.obs['outflag_r{0:03}_1'.format(i)].sim)

    print('Number of sources', num_sources[0])
    for i in range(num_sources[0]):
        print('New_Source_x{0:03}'.format(i),
              satm.obs['x_new_s{0:03}_1'.format(i)].sim)
        print('New_Source_y{0:03}'.format(i),
              satm.obs['y_new_s{0:03}_1'.format(i)].sim)
        print('New_Source_r{0:03}'.format(i),
              satm.obs['critical_distance_s{0:03}_1'.format(i)].sim)
