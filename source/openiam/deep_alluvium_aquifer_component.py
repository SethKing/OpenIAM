# -*- coding: utf-8 -*-
"""
The Deep Alluvium Aquifer component model is a reduced order model which can be
used to predict the changes in diluted groundwater chemistry if |CO2| and brine
were to leak into an overlying deep alluvium aquifer similar located below the
Kimberlina site, in California's southern San Joaquin Valley. The protocol
allows uncertainty and variability in aquifer heterogeneity, fluid transport,
and potential |CO2| and brine leakage rates from abandoned or damaged oil and gas
wells to be collectively evaluated to assess potential changes in groundwater
pH, total dissolved solids (TDS), and changes in the aquifer pressure resulting
from leakage.

Although the deep alluvium aquifer ROM was developed using site-specific data
from the LLNL's Kimberlin Model (version 1.2), the model accepts aquifer
characteristics as variable inputs and, therefore, may have more broad
applicability. Careful consideration should be given to the hydrogeochemical
character of the aquifer before using this model at a new site.

Model was created using the py-earth python package :cite:`pyearth2013`.  
Simulation data used to build this model created by Mansoor et. al. :cite:`DAA2018`.

Component model input definitions:

* **brine_rate** * [kg/s] (0 to 0.017) - Brine flux (default 0.00030 kg/s)

* **brine_mass** * [|log10| (kg)] (2.337 to 6.939) - Cumulative brine mass (default 4.928 |log10| (kg))

* **co2_rate** * [kg/s] (0 to 0.385) - |CO2| flux (default 0.045 kg/s)

* **co2_mass** * [|log10| (kg)] (0.001 to 9.210) - Cumulative |CO2| mass (default 7.214 |log10| (kg))

* **logK_sand1** * [|log10| (|m^2|)] (-12.92 to -10.92) - Permeability in layer 1 10-546 m (default -11.91 |log10| (|m^2|))

* **logK_sand2** * [|log10| (|m^2|)] (-12.72 to -10.72) - Permeability in layer 2 546-1225 m (default -11.71 |log10| (|m^2|))

* **logK_sand3** * [|log10| (|m^2|)] (-12.70 to -10.70) - Permeability in layer 3 1225-1411 m (default -11.69 |log10| (|m^2|))

* **logK_caprock** * [|log10| (|m^2|)] (-16.70 to -14.70) - Caprock Permeability 0-5 m (default -15.70 |log10| (|m^2|))

* **correlationLengthX** * [m] (200 to 2000 m) - Correlation length in X-direction (default 1098.235 m)

* **correlationLengthZ** * [m] (10 to 150 m) - Correlation length in Z-direction (default 79.827 m)

* **sandFraction** * [-] (0.70 to 0.90) - Sand volume fraction (default 0.800)

* **groundwater_gradient** * [-] (0.0010 to 0.0017) - Regional groundwater gradient (dh/dx=change in hydraulic head/distance) (default 0.0013)

* **leak_depth** * [-] (424.4 to 1341.5 m) - Depth of leakage interval (default 883.3 m)

Observations from the Deep Alluvium Aquifer component are:

* **TDS_volume** [|m^3|] - volume of plume above baseline TDS change in mg/L (change in TDS > 100 mg/L).
* **TDS_dx** [m] - length of plume above baseline TDS change in mg/L (change in TDS > 100 mg/L).
* **TDS_dy** [m] - width of plume above baseline TDS change in mg/L (change in TDS > 100 mg/L).
* **TDS_dz** [m] - height of plume above baseline TDS change in mg/L (change in TDS > 100 mg/L).

* **Pressure_volume** [|m^3|] - volume of plume above baseline pressure change in Pa (change in pressure > 500 Pa).
* **Pressure_dx** [m] - length of plume above baseline pressure change in Pa (change in pressure > 500 Pa).
* **Pressure_dy** [m] - width of plume above baseline pressure change in Pa (change in pressure > 500 Pa).
* **Pressure_dz** [m] - height of plume above baseline pressure change in Pa (change in pressure > 500 Pa).

* **pH_volume** [|m^3|] - volume of plume below pH threshold (pH < 6.75).
* **pH_dx** [m] - length of plume below pH threshold (pH < 6.75).
* **pH_dy** [m] - width of plume below pH threshold (pH < 6.75).
* **pH_dz** [m] - height of plume below pH threshold (pH < 6.75).
"""
# Created on Jul 16, 2018
# @author: Kayyum Mansoor
# mansoor1@llnl.gov

import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
#sys.path.append('..')
import matplotlib.pyplot as plt
import logging

try:
    from openiam import SystemModel, ComponentModel
except ImportError as err:
    print('Unable to load IAM class module: '+str(err))

try:
    import components.aquifer as aqmodel
    import components.aquifer.deep_alluvium as daamodel
    import components.aquifer.deep_alluvium.deep_alluvium_aquifer_ROM as daarom
except ImportError as err:
    print('\nERROR: Unable to load ROM for aquifer component\n')
    sys.exit()

import numpy as np
from sys import platform

class DeepAlluviumAquifer(ComponentModel):
    def __init__(self, name, parent, model_args=[], model_kwargs={}, workdir=None, header_file_dir=None):
        """
        Constructor method of DeepAlluviumAquifer class

        :param name: name of component model
        :type name: str

        :param parent: the SystemModel object that the component model
            belongs to
        :type parent: SystemModel object

        :param model_args: additional optional parameters of the component
            model; by default, model_args is an empty list []
        :type model_args: [float]

        :param model_kwargs: additional optional keyword arguments of the
            component model; by default, model_kwargs is an empty dictionary {}.
        :type model_kwargs: dict

        :param workdir: name of directory to use for model runs (serial run
            case)
        :type workdir: str

        :param header_file_dir: name of directory with Fortran DLL
        :type header_file_dir: str

        :returns: DeepAlluviumAquifer class object
        """
        if header_file_dir is None:
            model_kwargs['header_file_dir'] = daamodel.__path__[0]
            # print(aqmodel.__path__[0])
        else:
            model_kwargs['header_file_dir'] = header_file_dir

    
        super(DeepAlluviumAquifer, self).__init__(name, parent, model=self.model, model_args=model_args, model_kwargs=model_kwargs, workdir=workdir)

        # Placeholder for keyword arguments of the 'model' method:
        # to let the system model know that this component needs the specified keyword arguments
        self.model_kwargs['time_point'] = 365.25  # default value of 365.25 days
        self.model_kwargs['time_step'] = 365.25   # default value of 365.25 days

        self.add_default_par('time', value=20)
        self.add_default_par('brine_rate', value=3.21903E-05)
        self.add_default_par('brine_mass', value=4.71081307)
        self.add_default_par('co2_rate', value=0.060985038)
        self.add_default_par('co2_mass', value=6.737803184)
        self.add_default_par('logK_sand1', value=-11.92098495)
        self.add_default_par('logK_sand2', value=-11.7198002)
        self.add_default_par('logK_sand3', value=-11.70137252)
        self.add_default_par('logK_caprock', value=-15.69758676)
        self.add_default_par('correlationLengthX', value=1098.994284)
        self.add_default_par('correlationLengthZ', value=79.8062668)
        self.add_default_par('sandFraction', value=0.800121364)
        self.add_default_par('groundwater_gradient', value=0.001333374)
        self.add_default_par('leak_depth', value=885.5060281)

        # Define dictionary of boundaries
        self.pars_bounds = dict()

        self.pars_bounds['logK_sand1'] = [-12.92,-10.92]
        self.pars_bounds['logK_sand2'] = [-12.72,-10.72]
        self.pars_bounds['logK_sand3'] = [-12.7,-10.7]
        self.pars_bounds['logK_caprock'] = [-16.699,-14.699]
        self.pars_bounds['correlationLengthX'] = [200,2000]
        self.pars_bounds['correlationLengthZ'] = [10,150]
        self.pars_bounds['sandFraction'] = [0.7,0.9]
        self.pars_bounds['groundwater_gradient'] = [0.001000,0.001667]
        self.pars_bounds['leak_depth'] = [424.36,1341.48]

        # Define dictionary of temporal data limits
        self.temp_data_bounds = dict()
        self.temp_data_bounds['brine_rate'] = [0.00000, 0.01700]
        self.temp_data_bounds['brine_mass'] = [2.37684, 6.93934]
        self.temp_data_bounds['co2_rate'] = [0.00000, 0.38498]
        self.temp_data_bounds['co2_mass'] = [0.00107, 9.20977]
        self.temp_data_bounds['time'] = [0,200]

        logging.debug('DeepAlluviumAquifer created with name {name}'.format(name=name))
        
    def check_input_parameters(self, p):
        """
        Check whether input parameters fall within specified boundaries.

        :param p: input parameters of component model
        :type p: dict
        """
        logging.debug('Input parameters of {name} component are {p}'.format(name=self.name,p=p))
        
        for key, val in p.items():
            if key in self.pars_bounds:
                if ((val < self.pars_bounds[key][0])or
                    (val > self.pars_bounds[key][1])):
                    logging.warning('Parameter ' + key + ' is out of bounds.')
            else:
                logging.warning('Parameter {key} not recognized as a Deep Alluvium Aquifer input parameter.'
                                .format(key=key))

    def check_temporal_inputs(self, time, temp_inputs):
        """
        Check whether temporal data fall within specified boundaries.

        :param temp_inputs: temporal input data of component model
        :type temp_inputs: dict
        """
        for key, val in temp_inputs.items():
            if ((val < self.temp_data_bounds[key][0]) or
               (val > self.temp_data_bounds[key][1])):
                logging.warning(' {0} {1} is outside the model range {2} to {3} at time t = {4}.'
                                .format(key,val,self.temp_data_bounds[key][0],self.temp_data_bounds[key][1],time))

    # deep alluvium aquifer model function
    def model(self, p, co2_rate=[], brine_rate=[],
              co2_mass=[], brine_mass=[], time_point=365.25, time_step=365.25,
              header_file_dir=None):

        """
        Return volume of impacted aquifer based on several metrics.

        :param p: input parameters of carbonate aquifer model
        :type p: dict

        :param co2_rate: CO2 leakage rate, kg/s
        :type co2_rate: [float]

        :param brine_mass: Brine leakage rate, kg/s
        :type brine_mass: [float]

        :param co2_mass: Cumulative CO2 mass leaked, |log10|kg
        :type co2_mass: [float]

        :param brine_mass: Cumulative brine mass leaked, |log10|kg
        :type brine_mass: [float]

        :param time_point: time point in days at which the leakage rates are
            to be calculated; by default its value is 365.25 (1 year in days)
        :type time_point: float

        :param time_step: difference between the current and previous
            time points in days; by default its value is 365.25 (1 year in days)
        :type time_step: float

        :param kwargs: additional keyword arguments for the model
        :type kwargs: dict (if provided)

        :returns: vol - dictionary of observations of carbonate aquifer impacted volumes
            model; keys:
            ['TDS','pH','As','Pb','Cd','Ba','Benzene','Naphthalene','Phenol']

        """
        # Check assign defailt values
        actual_p = dict([(k,v.value) for k,v in self.default_pars.items()])
        actual_p.update(p)

        # Check whether co2, brine, rates and mass inputs satisfy the model requirements
        # Conditional statement used to supressess excessive error messsages
        if co2_mass <=0: co2_mass=1e-2
        if brine_mass <=0: brine_mass=1e-2
           
        if (((time_point/365.25 >= 1) and (np.log10(co2_mass) >= self.temp_data_bounds['co2_mass'][0])) or 
            ((time_point/365.25 >= 1)  and (np.log10(brine_mass) >= self.temp_data_bounds['brine_mass'][0]))):
            self.check_temporal_inputs(time_point, dict(list(zip(['co2_rate', 'brine_rate','co2_mass','brine_mass','time'], [co2_rate, brine_rate, np.log10(co2_mass), np.log10(brine_mass), time_point/365.25 ]))))

        # apply nominal value for brine/bo2
        p['co2_rate']=co2_rate
        p['brine_rate']=brine_rate
        p['co2_mass']=co2_mass 
        p['brine_mass']=brine_mass

        # assign time 
        actual_p['time']=dict([("time", time_point/365.25)])

        # Update default values of parameters with the provided ones
        actual_p.update(p)
		
        inputArray = np.array([actual_p['time'], actual_p['brine_rate'], np.log10(actual_p['brine_mass']), 
                               actual_p['co2_rate'], np.log10(actual_p['co2_mass']), 
                               actual_p['logK_sand1'], actual_p['logK_sand2'], actual_p['logK_sand3'],
                               actual_p['logK_caprock'], actual_p['correlationLengthX'], actual_p['correlationLengthZ'], 
                               actual_p['sandFraction'], actual_p['groundwater_gradient'], actual_p['leak_depth'] ])
                               
                               
        labels = ['TDS_volume','TDS_dx','TDS_dy','TDS_dz',
                 'Pressure_volume','Pressure_dx','Pressure_dy','Pressure_dz',
                 'pH_volume','pH_dx','pH_dy','pH_dz']

        if (((time_point/365.25 > 0) and (np.log10(co2_mass) >= self.temp_data_bounds['co2_mass'][0])) or 
            ((time_point/365.25 > 0)  and (np.log10(brine_mass) >= self.temp_data_bounds['brine_mass'][0]))):

            self.sol = daarom.Solution('.')
            self.sol.find(inputArray)
            out = dict(list(zip(labels, self.sol.Outputs)))

        else:
            out = dict(list(zip(labels, np.zeros(len(labels)))))
        return out

    # Attributes for system connections
    adapters = ['RateToMass']
    needsXY = False

    def connect_with_system(self, component_data, name2obj_dict,
                            welllocs=None, num_pathways=1):
        """
        Code to add carbonate aquifer to system model for control file interface.

        :param component_data: Dictionary of component data including 'Connection'
        :type component_data: dict

        :param name2obj_dict: Dictionary to map component names to objects
        :type name2obj: dict

        :param welllocs: dictionary of x and y coordinates of leaking wellbores,
            contains keys 'coordx' and 'coordy'
        :type welllocs: dict

        :param num_pathways: number of leakage pathways
        :type num_pathways: int

        :returns: None
        """
        if ('Parameters' in component_data) and (component_data['Parameters']):
            for key in component_data['Parameters']:
                if type(component_data['Parameters'][key]) != dict:
                    component_data['Parameters'][key] = {'value': component_data['Parameters'][key],
                                                         'vary': False}
                self.add_par(key, **component_data['Parameters'][key])

        if ('DynamicParameters' in component_data) and (component_data['DynamicParameters']):
            for key in component_data['DynamicParameters']:
                self.add_dynamic_kwarg(key, component_data['DynamicParameters'][key])

        # Make model connections
        if 'Connection' in component_data:
            connection = None
            try:
                connection = name2obj_dict[component_data['Connection']]
            except KeyError:
                pass
            pathway_i = int(component_data['Connection'][-3:])
            adapter = name2obj_dict['adapter_{0:03}'.format(pathway_i)]
            if 'AquiferName' in component_data:
                aq_name = component_data['AquiferName']
            else:
                aq_name = 'aquifer1'
            connection.add_obs_to_be_linked('CO2_{aquifer_name}'.format(aquifer_name=aq_name))
            adapter.add_obs_to_be_linked('mass_CO2_{aquifer_name}'.format(aquifer_name=aq_name))
            connection.add_obs_to_be_linked('brine_{aquifer_name}'.format(aquifer_name=aq_name))
            adapter.add_obs_to_be_linked('mass_brine_{aquifer_name}'.format(aquifer_name=aq_name))
            self.add_kwarg_linked_to_obs('co2_rate',
                                         connection.linkobs['CO2_{aquifer_name}'.format(
                                                 aquifer_name=aq_name)])
            self.add_kwarg_linked_to_obs('co2_mass',
                                         adapter.linkobs['mass_CO2_{aquifer_name}'.format(
                                                 aquifer_name=aq_name)])
            self.add_kwarg_linked_to_obs('brine_rate',
                                         connection.linkobs['brine_{aquifer_name}'.format(
                                                 aquifer_name=aq_name)])
            self.add_kwarg_linked_to_obs('brine_mass',
                                         adapter.linkobs['mass_brine_{aquifer_name}'.format(
                                                 aquifer_name=aq_name)])


if __name__ == "__main__":
    # Create system model
    time_array = 365.25*np.arange(0.0,2.0)
    sm_model_kwargs = {'time_array': time_array} # time is given in days
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # Add deep alluvium aquifer model object and define parameters
    daa = sm.add_component_model_object(DeepAlluviumAquifer(name='daa',parent=sm))
    
    daa.add_par('logK_sand1', value=-11.92098495)
    daa.add_par('logK_sand2', value=-11.7198002)
    daa.add_par('logK_sand3', value=-11.70137252)
    daa.add_par('logK_caprock', value=-15.69758676)
    daa.add_par('correlationLengthX', value=1098.994284)
    daa.add_par('correlationLengthZ', value=79.8062668)
    daa.add_par('sandFraction', value=0.800121364)
    daa.add_par('groundwater_gradient', value=0.001333374)
    daa.add_par('leak_depth', value=885.5060281)

    #aa.model_kwargs['x'] = [0.]
    #aa.model_kwargs['y'] = [0.]
    daa.model_kwargs['brine_rate'] = 3.21903E-05 # kg/s
    daa.model_kwargs['brine_mass'] = 10**4.71081307 # kg
    daa.model_kwargs['co2_rate'] = 0.060985038 # kg/s
    daa.model_kwargs['co2_mass'] = 10**6.737803184 # kg

    # Add observations (output) from the deep alluvium aquifer model
    # When add_obs method is called, system model automatically
    # (if no other options of the method is used) adds a list of observations
    # with names aa.obsnm_0, aa.obsnm_1,.... The final index is determined by the
    # number of time points in system model time_array attribute.
    # For more information, see the docstring of add_obs of ComponentModel class.

    daa.add_obs('TDS_volume')
    daa.add_obs('TDS_dx')
    daa.add_obs('TDS_dy')
    daa.add_obs('TDS_dz')

    daa.add_obs('Pressure_volume')
    daa.add_obs('Pressure_dx')
    daa.add_obs('Pressure_dy')
    daa.add_obs('Pressure_dz')

    daa.add_obs('pH_volume')
    daa.add_obs('pH_dx')
    daa.add_obs('pH_dy')
    daa.add_obs('pH_dz')

    # Run the system model
    sm.forward()

    # Print the observations
    # Since the observations at the particular time points are different variables,
    # method collect_observations_as_time_series creates lists of
    # values of observations belonging to a given component (e.g. ca) and having the same
    # common name (e.g. 'pH', 'TDS', etc) but differing in indices.
    # More details are given in the docstring and documentation to the method
    # collect_observations_as_time_series of SystemModel class.
    print('{:15}'.format('TDS_volume'), sm.collect_observations_as_time_series(daa,'TDS_volume'))
    print('{:15}'.format('TDS_dx'), sm.collect_observations_as_time_series(daa,'TDS_dx'))
    print('{:15}'.format('TDS_dy'), sm.collect_observations_as_time_series(daa,'TDS_dy'))
    print('{:15}'.format('TDS_dz'), sm.collect_observations_as_time_series(daa,'TDS_dz'))

    print('{:15}'.format('Pressure_volume'), sm.collect_observations_as_time_series(daa,'Pressure_volume'))
    print('{:15}'.format('Pressure_dx'), sm.collect_observations_as_time_series(daa,'Pressure_dx'))
    print('{:15}'.format('Pressure_dy'), sm.collect_observations_as_time_series(daa,'Pressure_dy'))
    print('{:15}'.format('Pressure_dz'), sm.collect_observations_as_time_series(daa,'Pressure_dz'))

    print('{:15}'.format('pH_volume'), sm.collect_observations_as_time_series(daa,'pH_volume'))
    print('{:15}'.format('pH_dx'), sm.collect_observations_as_time_series(daa,'pH_dx'))
    print('{:15}'.format('pH_dy'), sm.collect_observations_as_time_series(daa,'pH_dy'))
    print('{:15}'.format('pH_dz'), sm.collect_observations_as_time_series(daa,'pH_dz'))

#    # Expected output
#    TDS_volume      [      0.         2436214.13519926]
#    TDS_dx          [  0.         121.07792897]
#    TDS_dy          [  0.         139.62493095]
#    TDS_dz          [  0.         257.07734713]
#    Pressure_volume [      0.         2945213.33234456]
#    Pressure_dx     [  0.         176.71076959]
#    Pressure_dy     [  0.         152.39886469]
#    Pressure_dz     [  0.        260.9642596]
#    pH_volume       [      0.         2656675.37737679]
#    pH_dx           [ 0.         95.07087112]
#    pH_dy           [ 0.         88.08772333]
#    pH_dz           [  0.         249.01291277]
