# -*- coding: utf-8 -*-
"""
The Lookup Table Reservoir component model is a reduced order model based on
interpolation of data from a set of lookup tables. The lookup tables are based
on the full-physics scale simulations. Each lookup table is determined by a particular
set of M input model parameters which define a signature of the given set of lookup
tables.

In the IAM control file, the type name for the Lookup Table Reservoir component
is LookupTableReservoir. The component's parameters depend on the M input model
parameters used to create lookup tables data. The minimum and maximum values
of lookup table parameters determine boundaries of component parameters.
Moreover, the component parameters values as a set can only be one of the
combination of values that went into one of the lookup table linked to the
component. The minimum and maximum values of lookup table parameters determine boundaries
of component parameters. Moreover, the input parameters values as a set can only
be one of the combination of parameters that was used to obtain one of the data sets
linked to the component.

In the IAM control file a *FileDirectory* parameter must be specified. It
indicates the directory where files with the simulation data for the lookup tables
are located. A *TimeFile* parameter is a name of the .csv file that stores the
time points (in years) at which the results in the tables are provided.
If *TimeFile* is not specified then, by default, the name of the file
with time data is assumed to be *time_points.csv*. The time file
must be located in the directory specified by *FileDirectory*.

A *ParametersFilename* parameter can also be specified. It defines the
names and values of lookup table parameters that were used to create
the given set of lookup tables. Additionally, it lists the names of the .csv files
containing simulation data for each of the lookup table in the set.
By default, the name of the file with parameters data is assumed to be named
*parameters_and_filenames.csv*. The parameters file should be in a comma
separated values format. The first M entries in the first row of the file
are the names of the lookup table parameters which were varied for different
realizations; the (M+1)st entry is a word *filename*. Each subsequent row
of the parameters file contains the M values of the lookup table parameters
followed by the name of file (lookup table) with the corresponding realization
data. The provided filename must match with one of the files in the *FileDirectory*.

The user should make sure that the information provided in *ParametersFilename*
file on parameters and simulation data files is accurate and complete.
In general, a given parameter of the Lookup Table Reservoir component
can have any random name. At the same time the (random) names specified by user
in the *ParametersFilename* file should be the same names that the user would use
in the control file for the description of the Lookup Table Reservoir
component parameters. Due to the way the lookup tables are produced,
each parameter of the reservoir component can only take on certain
deterministic values. The possible values of a given parameter should be
listed after the *values:* keyword followed by the list in square brackets ('[').
The weights for each parameter can be specified with the *weights:* keyword
followed by the list of weights for each value in square brackets. The weights
should sum to 1, otherwise, they will be normalized. If no weights are provided
all values are assumed to be equally likely to occur.

Simulation data files (listed in *ParametersFilename*
file) in a comma separated values format contain the reservoir simulation data,
e.g., pressure and |CO2| saturation, varying over time. The data is used to build
the Lookup Table Reservoir component output. Each realization file begins with a header line
that is ignored by the code. Each subsequent row of the file represents a particular
spatial location. The first and the second columns are the x- and y-coordinates
of the location, respectively. The subsequent columns contain reservoir simulation data
at the location defined in the first two columns. The names of the columns
should represent the data in them and have the form *base.obs.nm_#* where
*base.obs.nm* is the name of observation as used in the system model and *#*
is an index of the time point at which the given observation is provided.
For example, if the column contains pressure data at the first time point,
its name should be *pressure_1*; if the column contains saturation data
at the 12th time point, its name should be *CO2saturation_12*. The order of
the columns in the lookup table except the first two x and y columns is arbitrary.
If some reservoir simulation data does not vary in time then the column name
should indicate it: its name should not contain underscore symbol *_* and
time indices, e.g., column with name #temperature# would indicate that the
provided temperature data is constant in time.

The Lookup Table Reservoir component produces the output using interpolation
in space and time within the spatio-temporal domain defined by the lookup tables
simulation model setup. Possible observations from the Lookup Table Reservoir component are:

* **pressure** [Pa] - pressure at top of the reservoir at the wellbore location(s).

* **CO2saturation** - |CO2| saturation at the top of the reservoir at the
  wellbore location(s).

In addition, the component can return any other type of observations provided
in the lookup tables.
"""
import numpy as np
import sys
import os
import logging
import matplotlib.pyplot as plt
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

try:
    from openiam import SystemModel, ComponentModel, ReservoirDataInterpolator
    from openiam.grid import interp_weights
except ImportError as err:
    print('Unable to load IAM class module: '+str(err))


class LookupTableReservoir(ComponentModel):
    def __init__(self, name, parent, intr_family=None,
                 locX=None, locY=None,
                 file_directory=None, time_file='time_points.csv',
                 parameter_filename='parameters_and_filenames.csv'):
        """
        Constructor method of LookupTableReservoir class.

        :param name: name of component model
        :type name: str

        :param parent: the SystemModel object that the component model
            belongs to
        :type parent: SystemModel object

        :param intr_family: name of interpolator family which belong to the parent
            system model and whose data will be used for reservoir observations
            (e.g., pressure and saturation) calculations
        :type intr_family: str

        :param locX: x-coordinate of the location at which reservoir observations
            (e.g., pressure and |CO2| saturation) are to be calculated.
            By default, it is None, which means that the whole grid data is requested
        :type locX: float

        :param locY: y-coordinate of the location at which reservoir observations
            (e.g., pressure and |CO2| saturation) are to be calculated.
            By default, it is None, which means that the whole grid data is requested
        :type locY: float

        :returns: LookupTableReservoir class object
        """
        # Set up keyword arguments of the 'model' method provided by the system model
        model_kwargs = {'time_point': 365.25}  # default value of 365.25 days

        super(LookupTableReservoir, self).__init__(
              name, parent, model=self.model, model_kwargs=model_kwargs)

        # Setup attributes related to interpolator family
        self.intr_family = intr_family
        self.linked_to_intr_family = False

        self.file_directory = file_directory
        self.time_file = time_file
        self.parameter_filename = parameter_filename

        # Set up location attributes
        if locX is not None and locY is not None:
            self.locX = locX
            self.locY = locY
            self.grid_obs_requested = False
        else:
            # Add flag indicating whether whole grid data is needed
            self.grid_obs_requested = True

        # If interpolator family argument is provided, connect to it,
        # otherwise create an 'empty' reservoir component
        if intr_family:
            self.link_to_interpolators()

        logging.debug('LookupTableReservoir created with name {name}'.format(name=name))

        return

    def connect_with_system(self, component_data, name2obj_dict, **kwargs):
        """
        Code to add LookupTableReservoir to system model for control file interface.

        :param component_data: Dictionary of component data including 'Connection'
        :type component_data: dict

        :param name2obj_dict: Dictionary to map component names to objects
        :type name2obj: dict

        :returns: None
        """
        if 'FileDirectory' in component_data:
            self.file_directory = component_data['FileDirectory']
        else:
            logging.error('FileDirectory must be provided for LookupTableReservoir Component')
            raise(IOError)

        if 'TimeFile' in component_data:
            self.time_file = component_data['TimeFile']
        else:
            self.time_file = 'time_points.csv'

        if 'ParameterFilename' in component_data:
            self.parameter_filename = component_data['ParameterFilename']
        else:
            self.parameter_filename = 'parameters_and_filenames.csv'

        # TODO  Decide what to do for control file interface in the situations when there are several components linked to
        # the same lookup table reservoir component but requesting gridded and scalar observations
        self.locX = component_data['locX']
        self.locY = component_data['locY']
        self.grid_obs_requested = False

        # If 'reservoir' interpolator family was not created before,
        # we need to create it first, then link it to the reservoir component(s)
        if 'reservoir' not in self._parent.interpolators:
            # Specify the name of interpolator family to be created to make it explicit
            self.build_and_link_interpolators(intr_family='reservoir')
            for par_name in component_data['Parameters']:
                values = component_data['Parameters'][par_name]['Values']
                nv = len(values)

                if 'Weights' not in component_data['Parameters'][par_name]:
                    weights = [1.0/nv for v in values]
                else:
                    weights = component_data['Parameters'][par_name]['Weights']

                self.add_par(par_name, value=self.default_pars[par_name], vary=True,
                             discrete_vals=(values, weights))
                logging.debug('Reservoir parameter {name} created '.format(name=par_name) +
                              'with values {values} and weights {weights}'.format(values=values, weights=weights))
        else:
            # If there are several reservoir components
            # that use the same interpolator family,
            # we need to make sure that we linked the parameters of those
            # to the parameters of the first created reservoir component
            init_res_comp = self._parent.interp_creators['reservoir']
            self.link_to_interpolators(intr_family='reservoir')
            for par_name in init_res_comp.pars:
                self.add_par_linked_to_par(par_name, init_res_comp.pars[par_name])

        return

    def build_and_link_interpolators(self, file_directory=None, time_file=None,
                            parameter_filename=None, intr_family=None, default_values={}):
        """
        Builds interpolators for component model to use.

        :param file_directory: Directory that contains the time file,
            the parameters file, and all realization files.
        :type file_directory: str

        :param time_file: filename of CSV file containing each of the time steps in years.
        :type time_file: str

        :param parameter_filename: filename of *.csv file containing information
            on the parameter names, values and name of file containing
            the corresponding realization data
        :type parameter_filename: str

        :param intr_family: name of interpolator family whose data is used
            for pressure and saturation calculations
        :type intr_family: str

        :param default_values: dictionary of default values of observations
            which are not provided in the lookup tables. By default,
            it is assumed that data provided in the lookup tables is enough,
            thus, by default, the parameter is an empty dictionary.
            The values provided are assumed to be constant for all time steps
            and all spatial points.
        type default_values: dict()

        :returns: None
        """
        if file_directory:
            self.file_directory = file_directory
        if time_file:
            self.time_file = time_file
        if parameter_filename:
            self.parameter_filename = parameter_filename
        if intr_family:
            self.intr_family = intr_family
        else:
            self.intr_family = 'reservoir'

        # Read file with signatures of interpolators and names of files with the corresponding data
        signature_data = np.genfromtxt(os.path.join(self.file_directory,
                                                    self.parameter_filename),
                                       delimiter=",", dtype='str')

        # The first row (except the last element) of the file contains names of the parameters.
        # The last element of the first row is supposed to be named 'filename'
        # The last column is for the filenames containing data for a particular realization
        par_names = signature_data[0,0:-1]
        num_pars = len(par_names)

        num_interpolators = signature_data.shape[0]-1  # -1 since the first line is a header

        # Create and add interpolators to the system model
        for ind in range(num_interpolators):
            signature = {par_names[j]: float(signature_data[ind+1,j]) for j in range(num_pars)}

            self._parent.add_interpolator(ReservoirDataInterpolator(name='int'+str(ind+1),
                parent=self._parent,
                header_file_dir=self.file_directory,
                time_file=self.time_file,
                data_file=signature_data[ind+1, -1],
                signature=signature,
                default_values=default_values),
                intr_family=self.intr_family,
                creator=self)

            logging.debug('Signature of the created interpolator is {}'.format(signature))
            #sm.add_interpolator(intpr,intr_family=self.intr_family)

        logging.debug('All interpolators are created')
        # Link just created interpolators to the reservoir component
        self.link_to_interpolators()
        return

    def check_input_parameters(self, p):
        """
        Check whether input parameters fall within specified boundaries.

        :param p: input parameters of component model
        :type p: dict
        """
        logging.debug('Input parameters {}'.format(p))
        logging.debug('Checking input parameters...')

        if not self.linked_to_intr_family:
            logging.error('LookupTableReservoir appears to not be linked to interpolator family.')
            raise AssertionError('Application cannot proceed further. '+
                                 'Reservoir component is not linked to '+
                                 'any interpolator family.')

        # For lookup table reservoir component we need to make sure that the
        # signature created with input parameters coincide with the signature
        # of one of the interpolators used by component

        # Create signature based on default parameter values
        param_signature = dict([(k,v.value) for k,v in self.default_pars.items()])

        # Check whether there are any parameters not belonging to the signature
        for key in p:
            if key not in param_signature:
                logging.warning('Parameter {key} not recognized as a LookupTableReservoir input parameter.'
                                .format(key=key))

        # Update default signature with values of input parameters p
        param_signature.update(p)

        # Check for the same signature among all connected interpolators
        signature_found = False
        for interpr_nm,interpr in self._parent.interpolators[self.intr_family].items():
            # Compare signature of interpolator with the provided input parameters
            if interpr.signature == param_signature:
                signature_found = True
                break

        if not signature_found:
            logging.error('Signature of input parameters do not coincide '+
                'with signatures of connected interpolators.')

    def link_to_interpolators(self, intr_family=None):
        """
        Link the component to the interpolators to be used.

        The method links the component to the interpolators to be used, check
        interpolation weights, and sets the default parameter values.

        :param intr_family: name of interpolation family to be used.
        :type intr_family: str

        :returns: None
        """
        # Check whether interpolators family intr_family exists
        if intr_family:
            self.intr_family = intr_family
        else:
            intr_family = self.intr_family

        if intr_family in self._parent.interpolators:
            # Get the first interpolator in the family
            int_nm, interpr = next(iter(self._parent.interpolators[self.intr_family].items()),0)

            if not self.grid_obs_requested:

                # Get the vertices and weights of simplex enclosing location (locX, locY)
                self.tri_vertices, self.tri_weights = interp_weights(
                        interpr.triangulation, np.array([[self.locX,self.locY]]))

                # Setup machine epsilon to replace comparison with zero
                machine_eps = np.finfo(np.float64).eps  # 2.220446049250313e-16

                # Check whether weights are reasonable; if they are not, then it means
                # that the point at which we need to interpolate is outside the data range.
                # The first if condition checks whether any of the weights are negative up to machine epsilon.
                # The second condition checks whether any of the weights exceed 1 up to machine epsilon.
                if np.any(self.tri_weights<-machine_eps) or np.any(self.tri_weights>(1+machine_eps)):
                    logging.warning('Location at which interpolation is requested is outside '+
                                    'the data domain.')
                    raise ValueError('Interpolation is requested at the location '+
                                     'outside the data domain.')

            # The default values of the component parameters are defined
            # by the signature of the first interpolator
            for key in list(interpr.signature.keys()):
                # Set default parameters of the component model
                self.add_default_par(key, value=interpr.signature[key])

            # Setup link flag
            self.linked_to_intr_family = True

            # Log creating the component
            logging.debug('LookupTableReservoir created with name {name}'.format(name=self.name))
        else:
            # Show useful message
            logging.error('Tried linking reservoir component to nonexistent family of interpolators.')
            raise KeyError('Family of interpolators '+intr_family+' does not exist.')
        return

    def model(self, p, time_point=365.35):
        """
        Return pressure and |CO2| saturation at the bottom of leaking well.

        :param p: input parameters of reservoir model determined
            by the signature of interpolators
        :type p: dict

        :param time_point: time point (in days) for which the pressure and
            saturation are to be calculated; by default, its value is 365.25 days
            (1 year in days)
        :type time_point: float

        :returns: out - dictionary of observations of lookup table reservoir
            component model; keys: ['pressure','CO2saturation']
        """
        # Obtain the default values of the parameters from dictionary of default parameters
        actual_p = dict([(k,v.value) for k,v in self.default_pars.items()])

        # Update default values of parameters with the provided ones
        actual_p.update(p)

        # Create dictionary of leakage rates
        out = dict()
        for interpr_key,interpr in self._parent.interpolators[self.intr_family].items():
            # Compare signature of interpolator with the provided input parameters
             if interpr.signature == actual_p:
                if self.grid_obs_requested:
                    interpr_out = interpr(time_point)
                    for nm in interpr_out:
                        out[nm] = interpr_out[nm]
                else:
                    interpr_out = interpr(time_point, self.tri_vertices, self.tri_weights)
                    # Create dictionary of output
                    for nm in interpr_out:
                        out[nm] = interpr_out[nm][0]
                    logging.debug('{sr} model call output {p}'.format(sr=self.name, p=out))
                break

        # Return dictionary of outputs
        return out

    def reset(self):
        pass


if __name__ == "__main__":
    logging.basicConfig(level=logging.WARNING)
    test_case = 4
    if test_case == 1:
        data_set_fldr = 'Kimb_54_sims'
        locX = 37478.0
        locY = 48333.0
        num_years = 50
    elif test_case == 2:
        data_set_fldr = 'Stone_28_sims'
        locX = 8450.0
        locY = 8600.0
        num_years = 50
    elif test_case == 3:
        data_set_fldr = 'FG2_16_sims'
        locX = 240202.0
        locY = 4405050.0
        num_years = 20
    elif test_case == 4:
        data_set_fldr = 'Test_2_sims'
        locX = 37478.0
        locY = 48233.0
        num_years = 50

    # Define keyword arguments of the system model
    time_array = 365.25*np.arange(0.0,num_years+1)
    sm_model_kwargs = {'time_array': time_array} # time is given in days

    # Create system model
    sm = SystemModel(model_kwargs=sm_model_kwargs)

    # Read file with signatures of interpolators and names of files with the corresponding data
    signature_data = np.genfromtxt(os.path.join('..','components','reservoir',
        'lookuptables',data_set_fldr,'parameters_and_filenames.csv'),
        delimiter=",", dtype='str')

    # The first row (except the last element) of the file contains names of the parameters.
    # The last element of the first row is supposed to be named 'filename'
    # The last column is for the filenames containing data for a particular realization
    par_names = signature_data[0,0:-1]
    num_pars = len(par_names)

    # Add reservoir component
    ltres = sm.add_component_model_object(LookupTableReservoir(name='ltres', parent=sm,
        locX=locX, locY=locY))
    file_directory = os.path.join('..','components', 'reservoir', 'lookuptables', data_set_fldr)
    ltres.build_and_link_interpolators(file_directory=file_directory, intr_family='reservoir',
                                       default_values={'salinity': 0.1, 'temperature': 50.0})

    # Add parameters of reservoir component model
    for j in range(num_pars):
        # For the parameters values use arbitrary line data (in this case from the second line) from signature_file
        ltres.add_par(par_names[j], value=float(signature_data[2,j]), vary=False)

    # Add observations of reservoir component model
    ltres.add_obs('pressure')
    ltres.add_obs('CO2saturation')
    if test_case == 4:
        ltres.add_obs('area')

    # Run system model using current values of its parameters
    sm.forward()

    print('------------------------------------------------------------------')
    print('                  Forward method illustration ')
    print('------------------------------------------------------------------')

    # Collect pressure and saturation observations
    pressure = sm.collect_observations_as_time_series(ltres,'pressure')
    saturation = sm.collect_observations_as_time_series(ltres,'CO2saturation')
    print('Pressure:', pressure, sep='\n')
    print('CO2saturation:', saturation, sep='\n')
    if test_case == 4:
        area = sm.collect_observations_as_time_series(ltres,'area')
        print('Area:', area, sep='\n')

    # Plot results
    fig = plt.figure(figsize=(12,4))
    ax = fig.add_subplot(121)
    plt.plot(time_array/365.25, pressure/1.0e+6, color="maroon", linewidth=1)
    plt.xlabel('Time, t (years)', fontsize=14)
    plt.ylabel('Pressure, P (MPa)', fontsize=14)
    plt.title('Pressure: leaking well', fontsize=18)
    plt.tight_layout()
    plt.tick_params(labelsize=12)
    plt.xlim([0,50])
    ax.get_yaxis().set_label_coords(-0.12,0.5)

    ax = fig.add_subplot(122)
    plt.plot(time_array/365.25, saturation, color="green", linewidth=1)
    plt.xlabel('Time, t (years)', fontsize=14)
    plt.ylabel('Saturation, S (-)', fontsize=14)
    plt.title(r'CO$_2$ saturation: leaking well', fontsize=18)
    plt.tight_layout()
    plt.tick_params(labelsize=12)
    plt.xlim([0,50])
    ax.get_yaxis().set_label_coords(-0.12,0.5)